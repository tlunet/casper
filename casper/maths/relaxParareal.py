#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing functions to compute relaxation matrices for Parareal
"""
import numpy as _np
from casper.maths.neumann import eigenDtAdvDiff, amplificationFactor


def generateP(n):
    w = _np.exp(-2j*_np.pi/n)
    line = _np.hstack([w**i for i in range(n)])
    P = _np.vstack([line**i for i in range(n)])
    return P/_np.sqrt(n)


def idealRelax(spaceSchemeG, spaceSchemeF, timeSchemeG, timeSchemeF,
               cflG, cflF, sG, sF, ndf, cutIndex=0):

    P = generateP(ndf)

    pi = _np.pi
    zG = eigenDtAdvDiff((-pi, pi, ndf), cflG, spaceSchemeG)
    zF = eigenDtAdvDiff((-pi, pi, ndf), cflF, spaceSchemeF)

    gG = amplificationFactor(timeSchemeG)
    gF = amplificationFactor(timeSchemeF)

    g = gG(zG)**sG
    f = gF(zF)**sF

    h = f/g
    if cutIndex > 1:
        h[cutIndex:-cutIndex+1] = 1.
    else:
        h[:] = 1.
    D = _np.diag(h)
    Ha = P.dot(D).dot(P.conj())

    return Ha, g, f


if __name__ == '__main__':
    import matplotlib.pyplot as _plt

    N = 500

    timeSchemeF = 'BE'
    timeSchemeG = 'BE'
    spaceSchemeF = 'C6'
    spaceSchemeG = 'C2'
    sF = 40
    sG = 2
    cflF = 1.0
    cflG = sF/sG*cflF

    P = generateP(N)

    Ha, g, f = idealRelax(spaceSchemeG, spaceSchemeF, timeSchemeG, timeSchemeF,
                          cflG, cflF, sG, sF, N)

    _plt.figure('Eigenvalues')
    _plt.semilogy(abs(g), 'o', label='Coarse')
    _plt.semilogy(abs(f), 'o', label='Fine')
    h = f/g
    cutIndex = 19
    if cutIndex > 1:
        h[cutIndex:-cutIndex+1] = 1.
    else:
        h[:] = 1.
    _plt.semilogy(abs(h), 'o', label='Ratio')
    _plt.legend()
