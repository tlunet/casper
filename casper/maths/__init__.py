#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Description
-----------
Package containing some usefull mathematical objects and functions
"""
from . import polyobj, polyclassic, quadrature, findiff, neumann, butcher, \
    krylov, analytic, errest

__all__ = ["polyobj", "polyclassic", "quadrature", "findiff",
           "neumann", "butcher", "krylov", "analytic", "errest"]
