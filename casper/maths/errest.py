#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 25 14:22:58 2019

@author: lunet
"""
import numpy as np

from casper.maths.findiff import tablesAdvection, tablesDiffusion
from casper.maths.butcher import tablesExplicit, tablesImplicit


def getDiscretizationCoeff(scheme, discrType):

    # Time discretization scheme
    if discrType == 'TIME':
        if scheme in tablesExplicit:
            tables = tablesExplicit
        elif scheme in tablesImplicit:
            tables = tablesImplicit
        else:
            raise ValueError(f'unknown time integration scheme {scheme}')
        dico = tables[scheme]

    # Advection scheme
    elif discrType == 'ADV':
        if scheme not in tablesAdvection:
            raise ValueError(f'unknown advection scheme {scheme}')
        dico = tablesAdvection[scheme]

    # Diffusion scheme
    elif discrType == 'DIFF':
        if scheme not in tablesDiffusion:
            raise ValueError(f'unknown diffusion scheme {scheme}')
        dico = tablesDiffusion[scheme]

    return dico['order'], dico['c1']


class ErrEstimateAdvDiff(object):

    def __init__(self, timeScheme=None, advScheme=None, diffScheme=None,
                 cflAdv=None, cflDiff=None):

        # Set cfl values
        if cflAdv is None and cflDiff is None:
            raise ValueError('cannot have both cflAdv and cflDiff undefined')
        if cflAdv is not None and cflDiff is not None:
            raise ValueError('cannot have both cflAdv and cflDiff constant')
        self.cflAdv, self.cflDiff = cflAdv, cflDiff

        # Space-time discretization
        if timeScheme is None:
            self.p, self.alpha = np.inf, None
        else:
            self.p, self.alpha = getDiscretizationCoeff(timeScheme, 'TIME')
        if advScheme is None:
            self.q, self.beta = np.inf, None
        else:
            self.q, self.beta = getDiscretizationCoeff(advScheme, 'ADV')
        if diffScheme is None:
            self.r, self.gamma = np.inf, None
        else:
            self.r, self.gamma = getDiscretizationCoeff(diffScheme, 'DIFF')

        if not self.s < np.inf:
            raise ValueError()
        self.timeScheme, self.advScheme, self.diffScheme = \
            timeScheme, advScheme, diffScheme

    @property
    def cstCflAdv(self):
        return self.cflAdv is not None

    @property
    def cstCflDiff(self):
        return self.cflDiff is not None

    @property
    def s(self):
        return min(self.p, self.q, self.r)

    @property
    def alphaStar(self):
        if self.cstCflAdv and self.p > self.s:
            return 0
        if self.cstCflDiff and self.p > self.s/2:
            return 0
        return self.alpha

    @property
    def betaStar(self):
        if self.q > self.s:
            return 0
        return self.beta

    @property
    def gammaStar(self):
        if self.r > self.s:
            return 0
        return self.gamma

    @staticmethod
    def kMaxRelevant(Re, TBar=1):
        k2P = (Re/TBar)**0.5
        return int(k2P/2/np.pi)

    def Gamma(self, Re, kappa):
        Rek = Re/(1j*2*kappa*np.pi)
        if self.cstCflDiff:
            out = 1j*(Rek-1)**(self.p + 1)
            out /= Rek
            out *= self.cflDiff**self.p
        else:
            out = 1j*(Rek-1)/Rek
            out **= self.p + 1
            out *= self.cflAdv**self.p
        return out

    def GammaPrim(self, Re, TBar=1):
        sRe = self.Re**0.5
        if self.cstCflDiff:
            out = -(-1j*sRe-1)**(self.p+1)
            out /= sRe
            out *= self.cflDiff**self.p
        else:
            out = (1j + 1/sRe)
            out **= self.p + 1
            out *= self.cflAdv**self.p
        return out

    def error(self, kappa, Re, nX, tBar=None, ampl=1, relErr=False):
        k2P = 2*kappa*np.pi
        err = abs(self.alphaStar*self.Gamma(Re, kappa)
                  + self.betaStar - self.gammaStar * k2P/Re)
        if tBar is not None:
            fac = k2P*tBar * np.exp(-k2P**2*tBar/Re)
        else:
            fac = Re/k2P/np.exp(1)
        err = err * fac
        if not relErr:
            err *= ampl/2**0.5
        err *= (k2P/nX)**self.s
        return err

    def nXMin(self, errTol, TBar=1, relErr=False):
        pass


class ErrEstimateAdv(object):

    def __init__(self, timeScheme=None, advScheme=None, cfl=1):

        # CFL value
        self.cfl = cfl

        # Space-time discretization
        if timeScheme is not None:
            self.p, self.alpha = getDiscretizationCoeff(timeScheme, 'TIME')
        else:
            self.p = np.inf
            self.alpha = None
        if advScheme is not None:
            self.q, self.beta = getDiscretizationCoeff(advScheme, 'ADV')
        else:
            self.q = np.inf
            self.beta = None
        if (timeScheme is None) and (spaceScheme is None):
            raise ValueError('at least one scheme should be specified')

        self.timeScheme, self.advScheme = timeScheme, advScheme

    @property
    def s(self):
        return min(self.p, self.q)

    @property
    def alphaStar(self):
        if self.p > self.s:
            return 0
        return self.alpha

    @property
    def betaStar(self):
        if self.q > self.s:
            return 0
        return self.beta

    def error(self, kappa, nX, tBar=1, ampl=1, relErr=False):
        print(self.cfl)
        err = tBar * (2*kappa*np.pi)**(self.s+1) * (1/nX)**self.s
        err = err*abs(self.alphaStar*(-1j)**(self.s+1)*self.cfl**self.s
                      - self.betaStar)
        if not relErr:
            err = err*ampl/2**0.5
        return err

    def n0(self, errTol):
        n0 = (abs(-self.alphaStar
                  + self.betaStar*(-1j)**(self.s+1)*self.cfl**self.s)
              / errTol)**(1/self.s)
        n0 *= (2*np.pi)**(1+1/self.s)
        return n0

    def nXMin(self, errTol, kappa=1, TBar=1, ampl=1, relErr=False):
        nX = self.n0(errTol) * TBar**(1/self.s) * kappa**(1+1/self.s)
        if relErr:
            nX *= (ampl/2**0.5)**(1/self.s)
        return nX


if __name__ == '__main__':
    import matplotlib.pyplot as plt

    nFlops = np.logspace(1, 3)
    for spaceScheme in ['U1', 'U2', 'C2', 'U3', 'U4',
                        'C4', 'U5', 'C6', 'C8']:
        nX = nFlops/tablesAdvection[spaceScheme]['nFlops']
        err = ErrEstimateAdv(advScheme=spaceScheme).error(
            kappa=3, nX=nX, tBar=1, relErr=False)
        plt.loglog(nFlops, err, label=spaceScheme)
    plt.legend()
    plt.ylim(1e-3, 10)
    plt.grid()

    for scheme in dict(**tablesExplicit, **tablesImplicit):
        nXMin = ErrEstimateAdv(timeScheme=scheme).n0(0.01)
        print(f'{scheme:8} : {nXMin:5.1f}')
    for scheme in tablesAdvection:
        nXMin = ErrEstimateAdv(advScheme=scheme).n0(0.01)
        print(f'{scheme:8} : {nXMin:5.1f}')
