#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 30 12:08:53 2017

@author: telu
"""
import numpy as np


def arnoldi(u, evalFunc, nK, V, H, w):
    """
    Generate the orthonormal basis of a krylov subspace using the arnoldi
    algorithm (modified Gram-Schmidt)

    Parameters
    ----------
    u : ``numpy.array`` of size p
        The initial vector of the Krylov basis
    evalFunc : ``function``
        The operator to generate the Krylov basis. Must take two arguments :
        the input vector (size p) and the output vector (size p)
    nK : ``int``
        The dimension of the Krylov subspace
    V : ``numpy.array`` of size (p, nK)
        Matrix containing the basis vector
    H : ``numpy.array`` of size (nK+1, nK)
        The Hessenberg matrix (projection the operator evalFunc in the
        Krylov subspace)
    w : ``numpy.array`` of size p
        Storage vector used during computations
    """
    norm = np.linalg.norm
    normU = norm(u)
    np.copyto(V[:, 0], u)
    V[:, 0] /= normU
    for j in range(nK):
        evalFunc(V[:, j], w)
        for i in range(j+1):
            np.dot(V[:, i], w, out=H[i, j])
            w -= H[i, j]*V[:, i]
        H[j+1, j] = norm(w)
        print("h[{},{}]".format(j+1, j), H[j+1, j])
        np.copyto(V[:, j+1], w)
        V[:, j+1] /= H[j+1, j]

    return normU


if __name__ == '__main__':
    bTest = np.random.rand(4, 5)
