# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the definition of polynomial objects, compatible with all
standard operations

Classes
-------
Poly : Base
    defining a polynomial
"""
import numpy as np


class Poly(object):
    """
    Class defining a polynomial, with operator overloading, so a polynome
    object is compatible with classical ``+``, ``-``, ``*`` with ``integer``,
    ``float`` or ``Poly`` objects, and ``**`` with ``integer``.

    Parameters
    ----------
    coeff : ``numpy.ndarray`` of ``float``
        Coefficients of the polynomial
    n : ``int``
        Number of coefficient

    Examples
    --------
    >>> from casper.maths.polyobj import Poly
    >>> p1 = Poly([1,1,1,1])
    >>> print p1
    1 + x + x^2 + x^3
    >>> p2 = Poly([1,1,1])
    >>> print p2
    1 + x + x^2
    >>> print p1+p2
    2 + 2x + 2x^2 + x^3
    >>> print -p1
    -1 - x - x^2 - x^3
    >>> print p1-p2
    x^3
    >>> print p1+1
    2 + x + x^2 + x^3
    >>> print p1-1
    x + x^2 + x^3
    >>> print p1**0
    1.0
    >>> print p1**1
    1 + x + x^2 + x^3
    >>> print p1**2
    1.0 + 2.0x + 3.0x^2 + 4.0x^3 + 3.0x^4 + 2.0x^5 + x^6

    Methods
    -------
    val(self,x) :
        Evaluate the polynomial with x

    integ(self,a,b) :
        Evaluate the integral of the polynomial between a and b

    prim(self,c=0.0) :
        Return the primitive of the polynomial

    deriv(self) :
        Return the derivative of the polynomial

    Raises
    ------
    ValueError
        If y'all doing some sheet while operating with ``Poly`` objects
    """

    def __init__(self, coeff=None):
        if coeff is None:
            self.coeff = np.array([0.])
        else:
            self.coeff = np.array(coeff, dtype=float)
        self.n = np.size(self.coeff)

    def val(self, xVal):
        """
        Evaluate the polynomial in x

        Parameters
        ----------
        xVal : ``numpy.ndarray`` or ``list``
            Value where the polynomial is evaluated
        """
        # With hand possibility
        # xVal = np.array(xVal)
        # c = xVal*0.0 + 1.0
        # res = xVal*0.0 + self.coeff[0]
        # for i in range(1, self.n):
        #     c *= xVal
        #     res += c*self.coeff[i]
        # return res

        # Other possibility, using numpy
        xValPow = np.vander(xVal, N=self.n, increasing=True)
        return xValPow.dot(self.coeff)

    def integ(self, a, b):
        """
        Integrate the polynomial between ``a`` and ``b``, exactly.

        Parameters
        ----------
        a : ``float``
            Left value of the integration interval
        b : ``float``
            Right value of the integration interval
        """
        polyPrim = self.prim()
        return polyPrim.val([b])-polyPrim.val([a])

    def prim(self, c=0.0):
        """
        Return the primitive of the polynomial, associated to the constant c

        Parameters
        ----------
        c : ``float``
            The primitive constant (default=0.0)
        """
        return Poly([c]+[self.coeff[i]/(float(i)+1) for i in range(self.n)])

    def deriv(self):
        """
        Return the derivative of the polynomial (new object)

        Examples
        --------
        >>> from casper.maths.polyobj import Poly
        >>> p1 = Poly([1,1,1,1])
        >>> print p1
        1 + x + x^2 + x^3
        >>> print p1.deriv()
        1.0 + 2.0x + 3.0x^2
        >>> print p1
        1 + x + x^2 + x^3
        """
        if self.n != 1:
            return Poly([self.coeff[i]*float(i) for i in range(1, self.n)])
        else:
            return Poly()

    def __str__(self):
        s = ''+str(self.coeff[0])*int(self.coeff[0] != 0)
        for i in range(1, self.n):
            c = self.coeff[i]
            s = s+(' + '*int(c > 0)+' - '*int(c < 0))*int(s != '') + \
                (str(abs(c))*int(abs(c) != 1) +
                 'x' + ('^'+str(i))*int(i != 1)) * \
                int(c != 0)
        if s == '':
            s = str(self.coeff[0])
        return s

    def __add__(self, obj):
        if isinstance(obj, Poly):
            if (self.n > obj.n):
                nMin = obj.n
                valMax = self.coeff[nMin:]
            else:
                nMin = self.n
                valMax = obj.coeff[nMin:]
            newCoeff = np.hstack((self.coeff[0:nMin] + obj.coeff[0:nMin],
                                 valMax))
        elif isinstance(obj, float) or isinstance(obj, int):
            newCoeff = self.coeff.copy()
            newCoeff[0] += obj
        else:
            raise ValueError("Wrong operation with polynomial")
        return Poly(newCoeff)

    def __radd__(self, obj):
        return self + obj

    def __sub__(self, obj):
        if isinstance(obj, Poly):
            if (self.n > obj.n):
                nMin = obj.n
                valMax = self.coeff[nMin:]
            else:
                nMin = self.n
                valMax = - obj.coeff[nMin:]
            newCoeff = np.hstack((self.coeff[0:nMin] - obj.coeff[0:nMin],
                                 valMax))
        else:
            try:
                newCoeff = self.coeff.copy()
                newCoeff[0] -= obj
            except Exception:
                raise ValueError("Wrong operation with polynomial")
        return Poly(newCoeff)

    def __rsub__(self, obj):
        if isinstance(obj, Poly):
            if (self.n > obj.n):
                nMin = obj.n
                valMax = - self.coeff[nMin:]
            else:
                nMin = self.n
                valMax = obj.coeff[nMin:]
            newCoeff = np.hstack((obj.coeff[0:nMin] - self.coeff[0:nMin],
                                 valMax))
        else:
            try:
                newCoeff = - self.coeff
                newCoeff[0] += obj
            except Exception:
                raise ValueError("Wrong operation with polynomial")
        return Poly(newCoeff)

    def __neg__(self):
        return Poly(-self.coeff)

    def __mul__(self, obj):
        if not isinstance(obj, Poly):
            try:
                return Poly(self.coeff*obj)
            except Exception:
                raise ValueError("Wrong operation with polynomial")
        else:
            newCoeff = np.zeros(self.n*obj.n)
            for i in range(self.n):
                for j in range(obj.n):
                    newCoeff[i+j] += self.coeff[i]*obj.coeff[j]
            return Poly(newCoeff)

    def __rmul__(self, obj):
        return self * obj

    def __div__(self, obj):
        try:
            return self*(1.0/obj)
        except Exception:
            raise ValueError("Wrong operation with polynomial")

    def __truediv__(self, obj):
        return self.__div__(obj)

    def __pow__(self, obj):
        if isinstance(obj, int):
            if (obj == 0):
                return Poly([1.0])
            elif (obj == 1):
                return self
            elif (obj >= 0):
                return self*self**(obj-1)
            else:
                raise ValueError("Wrong operation with polynomial")
        else:
            raise ValueError("Wrong operation with polynomial")
