# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing functions returning classical polynomials in form of Poly
objects

Functions
---------
monom() :
    return the polynomial :math:``P(x)=x``
unity() :
    return the polynomial :math:``P(x)=1``
null() :
    return the null polynomial
legendre(n) :
    return the :math:`n^{th}` legendre polynomial
lagrange(points,i) :
    Return the :math:`i^{th}` lagrange polynomial on the :math:`n` points
    :math:`(x_i)_{0 \\leq i \\leq n-1}`
"""
from .polyobj import Poly


def monom():
    """
    Return the polynomial

    .. math::
        P(x) = x

    Examples
    --------

    >>> import casper.maths.polyclassic as poly
    >>> x = poly.monom()
    >>> print x
    x
    >>> print 1+2*x-4*x**3
    1.0 + 2.0x - 4.0x^3
    >>> print (1+2*x-4*x**3).val(1)
    -1.0
    """
    return Poly([0.0, 1.0])


def unity():
    """
    Return the polynomial

    .. math::
        P(x) = 1
    """
    return Poly([1.0])


def null():
    """
    Return the polynomial

    .. math::
        P(x) = 0
    """
    return Poly()


def legendre(n):
    """
    Return the :math:`n^{th}` legendre polynomial, defined by the recurrence
    relation

    .. math::
        nP_n(x) = (2n-1)xP_{n-1}(x)-(n-1)P_{n-2}(x)

    Parameters
    ----------
    n : ``integer``
        Number of the Legendre polynomial

    Examples
    --------

    >>> import casper.maths.polyclassic as poly
    >>> x = poly.monom()
    >>> print (15*x - 70*x**3 + 63*x**5)/8
    1.875x - 8.75x^3 + 7.875x^5
    >>> print poly.legendre(5)
    1.875x - 8.75x^3 + 7.875x^5
    """
    n = float(n)
    x = monom()
    if (n == 0.):
        return unity()
    elif (n == 1.):
        return x
    else:
        return ((2.0*n-1.0)*x*legendre(n-1)-(n-1.0)*legendre(n-2))/n


def lagrange(points, i):
    """
    Return the :math:`i^{th}` lagrange polynomial on the :math:`n`
    points :math:`(x_i)_{0 \\leq i \\leq n-1}`, defined as following :

    .. math::
        l_{i,(x_i)}(x) = \\prod_{0 \\leq m \\leq n, \\hspace{1mm} m \\neq i}
        \\frac{x-x_m}{x_i-x_m}

    Parameters
    ----------
    points : ``numpy.ndarray``
        The points :math:`(x_i)_{0 \\leq i \\leq n}`
    """
    polyLagrange = unity()
    x = monom()
    for xi in points[:i]:
        polyLagrange = polyLagrange*(x-xi)/(points[i]-xi)
    for xi in points[i+1:]:
        print('indice {}, lag={}'.format(i, (points[i]-xi)))
        polyLagrange = polyLagrange*(x-xi)/(points[i]-xi)
    return polyLagrange
