#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Description
-----------
Module containing an analytical solvers for particular equations
(advection-diffusion, ...)
"""
import warnings
from time import time
import numpy as np
from scipy.integrate import quad
from scipy.special import erf, roots_hermite


def quadRuleFejer(n):
    """
    Compute the nodes and weights of a Fejer-I quadrature rule on [-1,1].

    Parameters
    ----------
    n : ``int``
        Number of quadrature points.

    Returns
    -------
    nodes : ``np.1darray``
        An array containing the nodes.
    weights : ``np.1darray``
        An array containing the weights.
    """
    # Computation using DFT (Waldvogel 2006)
    # Inspired from quadpy code
    # quadpy/line_segment/fejer.py
    # @Nico Schlömer

    # -- Initialize output variables
    n = int(n)
    nodes = np.empty(n, dtype=float)
    weights = np.empty(n, dtype=float)

    # Compute nodes
    theta = np.arange(1, n+1, dtype=float)[-1::-1]
    theta *= 2
    theta -= 1
    theta *= np.pi/(2*n)
    np.cos(theta, out=nodes)

    # Compute weights
    # -- Initial variables
    N = np.arange(1, n, 2)
    lN = len(N)
    m = n - lN
    K = np.arange(m)
    # -- Build v0
    v0 = np.concatenate([
        2 * np.exp(1j*np.pi*K/n) / (1 - 4*K**2),
        np.zeros(lN+1)
        ])
    # -- Build v1 from v0
    v1 = np.empty(len(v0)-1, dtype=complex)
    np.conjugate(v0[:0:-1], out=v1)
    v1 += v0[:-1]
    # -- Compute inverse fourier transform
    w = np.fft.ifft(v1)
    if max(w.imag) > 1.0e-15:
        raise ValueError('Max imaginary value to important for' +
                         ' ifft: {}'.format(max(w.imag)))
    # -- Store weights
    weights[:] = w.real

    return nodes, weights


def reshapeVars(*lVar, extraDim=0):
    """
    Reshape variables to enable vectorized computations

    Example
    -------
    >>> # Transform list-like variables into np.ndarray, keep scalar as scalar
    >>> x, t = reshapeVars([1, 2, 3], 4)
    >>> x
    array([1, 2, 3])
    >>> t
    4
    >>> # Modification of a variable's shape depending on the other's
    >>> xBase = [[1, 2], [3, 4]]
    >>> tBase = [5, 6, 7]
    >>> x, t = reshapeVars(xBase, tBase)
    >>> x.shape
    (2, 2, 1)
    >>> t.shape
    (1, 1, 3)
    >>> # Addition of eventual additional dimension for further manipulation
    >>> x, t = reshapeVars(xBase, tBase, extraDim=1)
    >>> x.shape
    (2, 2, 1, 1)
    >>> t.shape
    (1, 1, 3, 1)

    Parameters
    ----------
    *lVar : TYPE
        DESCRIPTION.
    extraDim : TYPE, optional
        Number of extra array dimension to add (default=0).

    Returns
    -------
    lReshapesVar: ``generator``
        A python object generating the reshaped variables.
    """
    nVar = len(lVar)
    reshapes = [False for i in range(nVar)]
    shapes = [() for i in range(nVar)]
    lVar = [v for v in lVar]
    for i in range(nVar):
        try:
            getattr(lVar[i], '__iter__')
            lVar[i] = np.array(lVar[i], copy=False)
            shapes[i] = lVar[i].shape
            if len(shapes[i]) > 1:
                lVar[i] = lVar[i].reshape(-1)
                reshapes[i] = True
        except AttributeError:
            pass
    vect = [len(s) > 0 for s in shapes]

    def _concatShapes(shapes):
        return np.array(np.hstack(shapes), dtype=int).tolist()

    def varShape(i):
        return _concatShapes(
            [s if j == i else [1]*len(s) for j, s in enumerate(shapes)])

    return (v.reshape(varShape(i) + extraDim*[1]) if vect[i] else v
            for i, v in enumerate(lVar))


class Results(object):
    """
    Implement a conveniency object that can be used to return multiple results

    Example
    -------
    >>> res = Results(out=[1,2,3], tol=1e-5, nIter=10)
    >>> print(res.out)
    [1, 2, 3]
    >>> print(res['tol'])
    1e-05
    >>> out, tol, nIter = res
    >>> print(out, tol, nIter)
    [1, 2, 3] 1e-05 10
    """

    def __init__(self, **kwargs):
        try:
            self._res = dict(kwargs)
        except TypeError:
            raise ValueError(f'invalid argument {kwargs}')

    def keys(self):
        return self._res.keys()

    def __getitem__(self, key):
        try:
            return self._res[key]
        except KeyError:
            try:
                return self._res[list(self.keys())[key]]
            except KeyError:
                raise KeyError(f'unknown attribute {key}')

    def __setattr__(self, name, value):
        if name == '_res':
            super().__setattr__(name, value)
        else:
            raise Exception('cannot modify object results')

    def __getattribute__(self, name):
        try:
            return super().__getattribute__(name)
        except AttributeError:
            return self.__getitem__(name)

    def __str__(self):
        return self._res.__str__()

    def __repr__(self):
        return self._res.__repr__()


def approxLimit(approx, vData, sData,
                nInit, nStep, nMax, tol, convMask=True):
    r"""
    Approximate the limit of a converging function sequence

    .. math::
        \lim_{n\rightarrow\infty} f_n(x,y,t,...,c_1,c_2,c_3)

    on given vectorial (:math:`x,y,t,...`) or scalar (:math:`c_1,c_2,c_3,...`)
    evaluation points, with :math:`f_n` a given function.

    Starting from :math:`n=n_0`, this algorithm iteratively computes
    :math:`f_n(...)` and :math:`f_{n+\Delta n}(...)` until the following
    convergence criterion is satisfied :

    .. math::
        \epsilon = \max_{x,y,t,...} |f_{n+\Delta n}(...) - f_n(...)| < tol.

    In addition, the algorithm can use a convergence mask, that allows to
    compute finer :math:`f_{n+\Delta n}(x,y,t)` approximations only for
    non-converged :math:`(x,y,t, ...)` points.
    This can bring substancial speedup when the number of evaluated points
    is large enough.

    Parameters
    ----------
    approx : function
        Approximation function, should be called with arguments
        **(n, vData, sData)**, with **n** an integer, **vData** the list
        of vector arguments, and **sData** the list of scalar arguments.
    vData : list of np.ndarray
        The vector arguments :math:`(x,y,t, ...)` to be given as argument to
        the approx function. If **convMask==True**, partial parts of those
        vectors may be passed to the approx function.
    sData : list of object
        The scalar arguments :math:`(c1,c2,c3 ...)` to be given as argument to
        the approx function, without any modification.
    nInit : int
        Starting :math:`n_0` value.
    nStep : int
        Step :math:`\Delta n` value.
    nMax : int
        Maximum value for :math:`n`.
    tol : float
        The desired tolerance :math:`tol`.
    convMask : TYPE, optional
        Wether or not use convergence mask to (eventually) speedup convergence
        (default=True).

    Returns
    -------
    res : Results
        An object storing the results with the following keys (or attributes) :
    - sol : np.ndarray or scalar
        The numerical approximation of the limit.
    - converged : bool
        If the convergence criterion is reached.
    - n : int
        Higher :math:`n` value at the end of iterations.
    - eps : float
        :math:`\epsilon` value at the end of iterations.
    - noConv : float
        Mean number of non-converged evaluation points after iterations.
    - tComp : float
        Computation time.

    Example
    -------
    Compute the approximation of

    .. math::
        \lim_{n\rightarrow\infty} \left( 1 + \frac{x}{n} \right)^n = e^x

    >>> x = np.linspace(-1, 1, num=1000)
    >>> res = approxLimit(lambda n, v, s: (1+v[0]/n)**n, [x], [],
                          tol=1e-12, nInit=1000, nStep=1000, nMax=10000000)
    >>> print(f'converged : {res.converged}, in {res.tComp:.2f}s')
    converged : True, in 0.15s
    >>> print(np.max(np.abs(res.sol - np.exp(x))))
    8.909123305755884e-07
    """

    tBeg = time()

    # Vectorial and scalar arguments used by approx function
    args = [vData, sData]

    # Initial approximations
    n = nInit
    f = [approx(n, *args)]
    n += nStep
    f += [approx(n, *args)]

    # Initial absolute variation between approximations
    eps = np.abs(f[1]-f[0])

    # Initial convergence masks
    noConv = eps > tol
    conv = ~noConv

    # Convergence indicator
    converged = True

    while np.max(eps) > tol:

        if convMask:
            # Restrict vectorial arguments to non-converged data
            args[0] = [v[noConv] for v in vData]
            # Keep converged data (to avoid recomputing it)
            np.copyto(f[0], f[1], where=conv)

        # Switch approximation pointers
        f = f[-1::-1]

        # Finer approximation
        n += nStep
        if convMask:
            np.place(f[1], mask=noConv, vals=approx(n, *args))
        else:
            np.copyto(f[1], approx(n, *args))

        # Absolute variation between approximations
        np.abs(f[1]-f[0], out=eps)

        # Convergence masks
        if convMask:
            np.greater(eps, tol, out=noConv)
            np.logical_not(noConv, out=conv)

        # Safety in case of no convergence with given tolerance
        if n > nMax:
            converged = False
            break

    tEnd = time()

    return Results(
        sol=f[1], converged=converged, n=n, eps=np.max(eps),
        noConv=noConv.mean(), tComp=tEnd-tBeg)


def approxSeries(seriesTerm, vData, sData, doubleSided=False,
                 iInit=0, iStep=1, iMax=500, tol=1e-15, convMask=True):
    r"""
    Approximate the limit of a converging function series

    .. math::
        \lim_{n\rightarrow\infty} \sum_{i=i_0}^{n}
        f_i(x,y,t,...,c_1,c_2,c_3) = \lim_{n\rightarrow\infty} S_n

    on given vectorial (:math:`x,y,t,...`) or scalar (:math:`c_1,c_2,c_3,...`)
    evaluation points, with :math:`f_n` a given function.
    Can also approximate the limit of the doubled-sided series

    .. math::
        \lim_{n\rightarrow\infty} \sum_{i=i_0}^{n} \left[
            f_i(x,y,t,...,c_1,c_2,c_3) + f_{-i}(x,y,t,...,c_1,c_2,c_3)
        \right]
        = \lim_{n\rightarrow\infty} S_n

    Starting from :math:`n=i_0`, this algorithm iteratively computes
    :math:`S_n` and :math:`f_{n+\Delta i}(...)` until the following
    convergence criterion is satisfied :

    .. math::
        \epsilon = \max_{x,y,t,...} |S_{n+\Delta i}(...) - S_n(...)| < tol.

    In addition, the algorithm can use a convergence mask, that allows to
    compute finer :math:`D_{n+\Delta i}(x,y,t)` approximations only for
    non-converged :math:`(x,y,t, ...)` points.
    This can bring substancial speedup when the number of evaluated points
    is large enough.

    Parameters
    ----------
    seriesTerm : function
        Series term function, should be called with arguments
        **(i, vData, sData)**, with **i** an integer, **vData** the list
        of vector arguments, and **sData** the list of scalar arguments.
    vData : list of np.ndarray
        The vector arguments :math:`(x,y,t, ...)` to be given as argument to
        the approx function. If **convMask==True**, partial parts of those
        vectors may be passed to the approx function.
    sData : list of object
        The scalar arguments :math:`(c1,c2,c3 ...)` to be given as argument to
        the approx function, without any modification.
    doubleSided : bool, optional
        Wether or not approximate the double sided series
    iInit : int, optional
        Starting :math:`i_0` value (default=0).
    iStep : int, optional
        Step :math:`\Delta i` value (default=1).
    iMax : int, optional
        Maximum value for :math:`i` (default=500).
    tol : float
        The desired tolerance :math:`tol`.
    convMask : TYPE, optional
        Wether or not use convergence mask to (eventually) speedup convergence
        (default=True).

    Returns
    -------
    res : Results
        An object storing the results with the following keys (or attributes) :
    - sol : np.ndarray
        The numerical approximation of the limit.
    - converged : bool
        If the convergence criterion is reached.
    - i : int
        Higher :math:`i` value at the end of iterations.
    - eps : float
        :math:`\epsilon` value at the end of iterations.
    - noConv : float
        Mean number of non-converged evaluation points after iterations.
    - tComp : float
        Computation time.

    Example
    -------
    Compute the approximation of

    .. math::
        \sum_{i=0}^{\infty} x^i = \frac{1}{1-x}

    >>> x = np.linspace(0, 1, num=1000, endpoint=False)
    >>> res = approxSeries(lambda i, v, s: v[0]**i, [x], [], iInit=0)
    >>> print(f'% non converged : {res.noConv}')
    % non converged : 0.066
    >>> res = approxSeries(lambda i, v, s: v[0]**i, [x], [], iInit=0, iMax=1e5)
    >>> print(f'converged : {res.converged}')
    converged : True
    >>> print(np.max(np.abs(res.sol - 1/(1-x))))
    4.8544279707130045e-11
    """

    tBeg = time()

    # Vectorial and scalar arguments used by seriesTerm function
    args = [vData, sData]

    # Compute first term of the series
    i = iInit
    s = seriesTerm(i, *args)

    # Modification of seriesTerm for double-sided series
    if doubleSided:
        def symmetrize(sTerm):
            def symTerm(i, vData, sData):
                res = sTerm(i, vData, sData)
                res += sTerm(-i, vData, sData)
                return res
            return symTerm
        seriesTerm = symmetrize(seriesTerm)

    # Compute first next term of the series
    i += 1
    ds = seriesTerm(i, *args)
    # Compute eventual other terms if iInit > 1
    for _ in range(iStep-1):
        i += 1
        ds += seriesTerm(i, *args)
    # Add to the sum
    s += ds

    # Initial absolute next-term variation
    eps = np.abs(ds)

    # Initial convergence masks
    noConv = eps > tol
    conv = ~noConv

    # Convergence indicator
    converged = True

    while np.max(eps) > tol:

        # Add next series element
        i += 1
        if convMask:
            # Restrict vectorial arguments to non-converged data
            args[0] = [v[noConv] for v in vData]
            # Compute new ds on non-converged data
            np.place(ds, mask=noConv, vals=seriesTerm(i, *args))
            for _ in range(iStep-1):
                i += 1
                ds[noConv] += seriesTerm(i, *args)
            # Add ds to s, just for non-converged values
            s[noConv] += ds[noConv]
        else:
            np.copyto(ds, seriesTerm(i, *args))
            for _ in range(iStep-1):
                i += 1
                ds += seriesTerm(i, *args)
            s += ds

        # Absolute next-term variation
        np.abs(ds, out=eps)

        # Convergence masks
        if convMask:
            np.greater(eps, tol, out=noConv)
            np.logical_not(noConv, out=conv)

        # Safety in case of no convergence with given tolerance
        if i > iMax:
            converged = False
            break

    tEnd = time()

    return Results(
        sol=s, converged=converged, i=i, eps=np.max(eps), noConv=noConv.mean(),
        tComp=tEnd-tBeg)


class AdvDiffSolver(object):
    r"""
    Analytic solver for the periodic advection diffusion equation in one
    dimension, that is :

    .. math::
        \frac{\partial u}{\partial t} = -a \frac{\partial u}{\partial x}
        + \nu \frac{\partial^2 u}{\partial x^2},

    with :math:`a` and :math:`\\nu` respectively the advection and diffusion
    coefficients, and :math:`x \in [0, L[`.

    The generic analytic solution can be found using continuous Fourier
    transforms and Laplace transforms, and can be written under this form :

    .. math::
        u(x,t) = \int_{\mathbb{R}} \frac{1}{2\sqrt{\nu\pi t}}
            \exp\left(-\frac{y^2}{4\nu t}\right) u_0(x-at-y)dy, \;\;(1)

    with :math:`u_0` the initial solution defined on :math:`x \in [0, L[`
    (and periodic on :math:`\mathbb{R}` with period :math:`L`).
    For enhanced numerical stability, generic solutions are obtained by
    evaluating an equivalent form for eq. (1) :

    .. math::
        u(x,t) = \frac{1}{\sqrt{\pi}} \int_{\mathbb{R}} e^{-z^2}
            u_0(x-at-2\sqrt{\nu t}z) dz. \;\; (2)

    Parameters
    ----------
    a : ``float``
        The advection coefficient.
    nu : ``float``
        The diffusion coefficient.
    L : ``float``, optional
        The lenght of the domain (default=1).

    Example of use
    --------------
    Once the solver is instanciated with given **a**, **nu** and **L**
    parameters, then the solution can be obtained using the ``solve`` method,
    for example :

    >>> solver = AdvDiffSolver(a, nu, L)
    >>> solution = solver.solve(x, t, u0)
    >>> # equivalent solution, since the __call__ method is implemented
    >>> solution = solver(x, t, u0)

    Two types of implementations are available : generic and specific.

    Generic implementations
    -----------------------
    Those solve the advection-diffusion for general initial solution. To
    avoid some numerical singularities, a change of variable is applied to
    eq. (1), and we obtain the following form of the solution :

    .. math::
        u(x,t) = \frac{1}{\sqrt{\pi}} \int_{\mathbb{R}} e^{-z^2}
            u_0(x-at-2\sqrt{\nu t}z) dz. \;\; (2)

    Integral of eq. (2) must be evaluated numericaly, and the following
    approaches are available :

    - smooth (default) :
        Use a given quadrature rule to estimate the integral in eq. (2)
        (see ``solve_smooth`` documentation).

    - quadpack :
        Use the ``quad`` function from ``scipy.integral`` module to estimate
        the integral in eq. (2) (see ``solve_quadpack`` documentation).

    Even if it depends on the initial solution, generally the smooth
    implementation with Fejer quadrature rule is the most efficient.
    Be aware though that each generic implementation
    can struggle when :math:`u_0 \notin \mathcal{C}^{1}_{[0,L[}`.
    Those cases may require a first analytic computation of the
    advection-diffusion solution using eq. (1) or eq. (2)
    (see, e.g, solutions for CRENEL or SAW initial solution).

    Specific implementations
    ------------------------
    Depending on the parameters of the solver or provided to the ``solve``
    method, specific implementations (more accurate, efficient and/or stable)
    will be used prior to a generic implementation.

    - noDiff :
        Analytic solution for pure advection, which solution is simply

        .. math::
            u(x,t) = u_0(x-at)

        This implementation is used automatically by the ``solve`` method
        if :math:`\nu=0`.

    - COS :
        Analytic solution for a cosinus initial solution, used if
        **u0** = 'COS' (see ``solve_COS`` documentation).

    - CRENEL :
        Analytic solution for CRENEL-type initial solutions, used if
        **u0** = 'CRENEL' (see ``solve_CRENEL`` documentation).

    - SAW :
        Analytic solution for a SAW-type initial solution, used if
        **u0** = 'SAW' (see ``solve_SAW`` documentation).

    Examples
    --------
    >>> x = np.linspace(0, 1, 500, endpoint=False)
    >>> solver = AdvDiffSolver(a=1, nu=0.01, L=1)
    >>> # Solve with Gaussian initial solution
    >>> def u0(x):
            return np.exp(-100*(x-1/2)**2)
    >>> sol = solver(x, t, u0)
    >>> # Solve for cosinus initial solution, with wavenumber kappa=3
    >>> sol = solver(x, t, 'COS', k=3)
    """

    def __init__(self, a, nu, L=1):
        self.a = a
        self.nu = nu
        self.L = L

    @property
    def noAdv(self):
        """Whether of not the problem has no advection"""
        return self.a == 0

    @property
    def noDiff(self):
        """Whether of not the problem has no diffusion"""
        return self.nu == 0

    @property
    def Re(self):
        """Reynolds number of the problem (np.inf if no diffusion)"""
        try:
            return self.L*self.a/self.nu
        except ZeroDivisionError:
            return np.inf

    @property
    def tP(self):
        """Temporal period of the domain (1 if no advection)"""
        try:
            return self.L/self.a
        except ZeroDivisionError:
            return 1

    def solve(self, x, t, u0, generic='smooth', **kwargs):
        """
        Solve the 1D periodic advection-diffusion equation for the given
        parameters.

        Parameters
        ----------
        x : ``float`` or ``np.ndarray``
            The x points where to evaluate the solution.
        t : ``float`` or ``np.ndarray``
            The t points where to evaluate the solution.
        u0 : ``function`` of ``str``
            The initial solution, must be a vectorized function, or a string.
        generic : ``str``, optional
            Type of generic implementation to use (default='smooth').
        **kwargs : keyword argument, optional
            Additional parameters used for the different implementations.

        Returns
        -------
        sol : ``np.ndarray``
            The analytic solution.
        """
        if isinstance(u0, str):
            try:
                return eval(f'self.solve_{u0}(x, t, **kwargs)')
            except AttributeError:
                raise NotImplementedError(
                    f'analytic solution not implemented for u0={u0}')
        elif callable(u0):
            if self.noDiff:
                return self.solve_noDiff(x, t, u0)
            else:
                if generic in ['smooth', 'disc', 'quadpack']:
                    return eval(
                        f'self.solve_{generic}(x, t, u0, **kwargs)')
                else:
                    raise NotImplementedError(
                        f"generic implementation '{generic}' not "
                        "available")
        else:
            raise ValueError(f'incorrect u0 argument {u0}')

    def __call__(self, x, t, u0, generic='smooth', **kwargs):
        """Wrapper to the solve method"""
        return self.solve(x, t, u0, generic, **kwargs)

    def solve_noDiff(self, x, t, u0):
        """Implementation for pure advection"""
        return u0((x-self.a*t) % self.L)

    def solve_quadpack(self, x, t, u0):
        """
        Generic implementation using the ``quad`` function from
        ``scipy.integral`` module to estimate the integral in eq. (2)
        (see ``scipy.integral.quad`` documentation)

        Note
        ----
        The current implementation does not allows to change default parameters
        from the ``scipy.integral.quad`` function, used in this case as a
        black box.

        Parameters
        ----------
        x : ``float`` or ``np.ndarray``
            The x points where to evaluate the solution.
        t : ``float`` or ``np.ndarray``
            The t points where to evaluate the solution.
        u0 : ``function`` of ``str``
            The initial solution, MUST be a vectorized function.

        Returns
        -------
        sol : ``np.ndarray``
            The analytic solution.
        """
        a, nu, L = self.a, self.nu, self.L

        def integrand(z, x, t):
            pos = (x - a*t - 2*(nu*t)**0.5*z) % L
            val = u0(pos)
            val *= np.exp(-z**2)
            return val

        def integral(x, t):
            return quad(integrand, -np.inf, np.inf, args=(x, t))[0]

        vecIntegral = np.vectorize(integral)

        sol = vecIntegral(x, t)
        sol /= np.pi**0.5

        return sol

    def solve_smooth(self, x, t, u0, quadRule='fejer',
                     nNodesInit=200, nodeStep=200, nNodesMax=10000, tol=1e-15,
                     convMask=True):
        r"""
        Generic implementation for general (smooth) initial solutions.
        It estimates the solution given by

        .. math::
            u(x,t) = \frac{1}{\sqrt{\pi}} \int_{\mathbb{R}} e^{-z^2}
            u_0(x-at-2\sqrt{\nu t}z) dz, \;\; (1)

        using a quadrature rule on :math:`]-\infty, \infty[`,
        or eventually a quadrature rule on :math:`]-1, 1[` using the following
        change of variable :

        .. math::
            u(x,t) = \frac{1}{\sqrt{\pi}}
            \int_{-1}^{1} \exp\left[-\phi(s)^2\right]
            u_0(x-at-2\sqrt{\nu t}\phi(s))\phi'(s)ds, \;\; (2)

        with

        .. math::
            \phi(s) = \frac{s}{1-s^2}, \; \; \;
            \phi'(s) = \frac{1+s^2}{(1-s^2)^2}.

        Noting :math:`u_n(x,t)` an estimation of the solution using a
        quadrature rule on :math:`n` points, and starting from :math:`n=n_0`,
        an iterative algorithm computes :math:`u_n(x,t)` and
        :math:`u_{n+\Delta n}(x,t)` until the following convergence criterion
        is satisfied :

        .. math::
            \max_{x,t} |u_{n+\Delta n}(x,t) - u_n(x,t)| < tol,

        or a maximum number of nodes :math:`n_{max}` is reached.

        In addition, the algorithm can use a convergence mask, that allows to
        compute finer :math:`u_{n+\Delta n}(x,t)` approximations only for
        non-converged :math:`(x,t)` points.
        This can bring substancial speedup when the number of evaluated points
        is large enough.

        Values differents from the default ones can be specified for
        :math:`tol`, :math:`n_{max}`, :math:`n_{0}` and :math:`\Delta n`.

        Quadrature rules
        ----------------
        fejer : (default)
            Estimate the solution of (2) with

            .. math::
                u_n(x,t) \simeq \frac{1}{\sqrt{\pi}} \sum_{i=1}^{n}
                \exp\left[ -\phi(\tau_i)^2\right]
                u_0(x-at-2\sqrt{\nu t}\phi(\tau_i)) \phi'(\tau_i)\omega_i,

            where :math:`(\tau_i, \omega_i)_{1\leq i \leq n}` are the nodes
            and weights of a Fejer-I quadrature rule on :math:`]-1, 1[`.

        hermite :
            Estimate the solution of (1) with

            .. math::
                u_n(x,t) \simeq \frac{1}{\sqrt{\pi}} \sum_{i=1}^{n}
                u_0(x-at-2\sqrt{\nu t}\tau_i) \omega_i,

            where :math:`(\tau_i, \omega_i)_{1\leq i \leq n}` are the nodes
            and weights of a Gauss-Hermite quadrature rule on
            :math:`]-\infty, \infty[`.

        Parameters
        ----------
        x : ``float`` or ``np.ndarray``
            The x points where to evaluate the solution.
        t : ``float`` or ``np.ndarray``
            The t points where to evaluate the solution.
        u0 : ``function`` of ``str``
            The initial solution, MUST be a vectorized function.
        quadRule : ``str``
            The quadrature rule used to estimate the integral
            (default='fejer').
        nNodesInit : ``int``
            Value for :math:`n_{0}` parameter (default=200).
        nodeStep : ``int``
            Value for :math:`\Delta n` parameter (default=200).
        nNodesMax : ``int``
            Value for :math:`n_{max}` parameter (default=10000).
        tol : ``float``
            Value for :math:`tol` parameter (default=1e-15).
        convMask : ``bool``
            Use a convergence mask to eventually speedup computation, when the
            number of points (x,t) evaluated is large enough (default=True).

        Returns
        -------
        sol : ``np.ndarray``
            The analytic solution.
        """
        # Problem parameters
        a, nu, L = self.a, self.nu, self.L

        # Prep x and t for vectorial computations
        x, t = reshapeVars(x, t, extraDim=1)

        # Integrand definition (depends on the quadrature rule)
        if quadRule == 'fejer':

            def phi(s):
                return s/(1-s**2)

            def dPhi(s):
                return (1+s**2)/(1-s**2)**2

            def approx(n, vData, sData):
                nodes, weights = quadRuleFejer(n)
                nodes = nodes[None, :]
                weights = weights[None, :]

                y = phi(nodes)
                w = dPhi(nodes)
                w *= np.exp(-y**2)
                w *= weights

                c1, c2 = vData
                L, u0 = sData
                integrand = c2*y
                integrand *= -1
                integrand += c1
                integrand %= L
                integrand = u0(integrand)
                integrand *= w

                approx = np.sum(integrand, axis=-1)
                approx /= np.pi**0.5
                return approx

        elif quadRule == 'hermite':

            def approx(n, vData, sData):
                nodes, weights = roots_hermite(n)
                nodes = nodes[None, :]
                weights = weights[None, :]

                c1, c2 = vData
                L, u0 = sData
                integrand = c2*nodes
                integrand *= -1
                integrand += c1
                integrand %= L
                integrand = u0(integrand)
                integrand *= weights

                approx = np.sum(integrand, axis=-1)
                approx /= np.pi**0.5
                return approx

        else:
            raise ValueError(
                'quadRule {quadRule} not implemented for smooth solutions')

        # Approximation of the integral
        vData = x - a*t, 2*(nu*t)**0.5 + 0*x

        sol, conv, n, epsMax, nNoConv, tComp = approxLimit(
            approx, vData, [L, u0],
            nInit=nNodesInit, nStep=nodeStep, nMax=nNodesMax, tol=tol,
            convMask=convMask)

        # Output messages
        if conv:
            print(f'solve_smooth({quadRule}) : {n} quadrature nodes used '
                  f'to go under tol={tol}, in {tComp:1.2e}s '
                  f'(epsMax={epsMax:1.2e})')
        else:
            print(f'solve_smooth({quadRule}) : reached nNodesMax={nNodesMax}, '
                  f'{nNoConv*100:1.2f}% evaluations over tol={tol} '
                  f'(max={epsMax:1.2e})')

        return sol

    def solve_disc(self, x, t, u0, decomp=[0, 1], quadRule='fejer',
                   nNodesInit=200, nodeStep=200, nNodesMax=10000,
                   tolInt=1e-15, nMax=500, tolSum=1e-15,
                   convMaskInt=True, convMaskSum=True):
        """Experimental implementation for non C_2 initial solution"""

        # Problem parameters
        a, nu, L = self.a, self.nu, self.L

        # Prep x and t for vectorial computations
        x, t = reshapeVars(x, t, extraDim=1)

        def integral(z1, z2, vData, sData):

            def phi(s):
                return 0.5*((z2-z1)*s + (z2+z1))

            def dPhi(s):
                return 0.5*(z2-z1) + 0*s

            def approx(n, vData, sData):
                nodes, weights = quadRuleFejer(n)
                nodes = nodes[None, :]
                weights = weights[None, :]

                y = phi(nodes)
                w = dPhi(nodes)
                w *= np.exp(-y**2)
                w *= weights

                c1, c2 = vData
                L, u0 = sData
                integrand = c2*y
                integrand *= -1
                integrand += c1
                integrand %= L
                integrand = u0(integrand)
                integrand *= w

                approx = np.sum(integrand, axis=-1)
                approx /= np.pi**0.5
                return approx

            # Approximation of the integral
            res, conv, n, epsMax, nNoConv, tComp = approxLimit(
                approx, vData, sData,
                nInit=nNodesInit, nStep=nodeStep, nMax=nNodesMax,
                tol=tolInt, convMask=convMaskInt)

            return res

        def seriesTerm(i, vData, sData):

            args = [vData, sData]

            def chi(i, vData, sData):
                pos, denom = vData
                L = sData[0]
                res = L*i
                res *= -1
                res += pos
                res /= denom
                return res

            term = 0
            for s_j, s_jp1 in zip(decomp[:-1], decomp[1:]):
                term += integral(chi(i+s_jp1, *args), chi(i+s_j, *args), *args)

            return term

        # Approximation of the series
        vData = x - a*t, 2*(nu*t)**0.5 + 0*x
        sol, conv, n, epsMax, nNoConv, tComp = approxSeries(
            seriesTerm, vData, [L, u0], doubleSided=True,
            iInit=0, iStep=1, iMax=nMax, tol=tolSum, convMask=convMaskSum)

        return sol

    def solve_COS(self, x, t, k=1):
        r"""
        Solve the 1D advection-diffusion periodic solution for

        .. math::
            u_0 = \cos\left(\frac{2\kappa\pi x}{L}\right),

        that is

        .. math::
            u(x, t) = e^{-\lambda^2 \nu t} \cos(\lambda(x-at)),

        with :math:`\lambda=2\kappa\pi/L`.

        Parameters
        ----------
        x : ``float`` or ``np.ndarray``
            The x points where to evaluate the solution.
        t : ``float`` or ``np.ndarray``
            The t points where to evaluate the solution.
        k : ``int``, optional
            The wavenumber :math:`\kappa` of the cosinus wave (default=1).

        Returns
        -------
        sol : ``np.ndarray``
            The analytic solution.
        """
        a, nu, L = self.a, self.nu, self.L

        # Prep x and t for vectorial computations
        x, t = reshapeVars(x, t, extraDim=1)

        lam = 2*k*np.pi/L
        sol = np.exp(-(lam**2)*nu*t)*np.cos(lam*(x-a*t))
        return sol

    def solve_CRENEL(self, x, t, w=0.5, tol=1e-15, nMax=500, convMask=True):
        r"""
        Solve the 1D advection-diffusion periodic solution for a 'crenel'
        function of the form

        .. math::
            u_0 =
            \begin{cases}
                1 &\text{if $x$ (mod $L$) $< w L$}\\
                0 &\text{if $x$ (mod $L$ ) $> w L$}\\
                0.5 &\text{else.}
            \end{cases}

        The solution is

        .. math::
            u(x, t) = \frac{1}{2} \sum_{i \in \mathbb{Z}}
            \left[
                erf\left(\frac{x-at-iL}{2\sqrt{\nu t}}\right)
                - erf\left(\frac{x-at-(i+w)L}{2\sqrt{\nu t}}\right)
            \right],

        with :math:`erf` the error function defined as follow :

        .. math::
            erf(z) =\frac{2}{\sqrt{\pi}} \int_0^z e^{-z^2}dz.

        The infinite sum is evaluated iteratively by computing the first
        terms, starting from :math:`i=0`, then :math:`i\in\{-1, 0, 1\}`, ...,
        until the minimum absolute variation defined with the **tol**
        parameter is reached, or if the maximum number of iterations
        **nIterMax** is reached (i iterations <=> use of 2i+1 sum indexes).

        Parameters
        ----------
        x : ``float`` or ``np.ndarray``
            The x points where to evaluate the solution.
        t : ``float`` or ``np.ndarray``
            The t points where to evaluate the solution.
        w : ``float``, optional
            Width coefficient of the crenel function (default=0.5).
        tol : ``float``, optional
            The minimum absolute variation (default=1e-15).
        nMax : ``int``, optional
            The maximum number of iterations (default=500).
        convMask : ``bool``
            Use a convergence mask to eventually speedup computation, when the
            number of points (x,t) evaluated is large enough (default=True).

        Returns
        -------
        sol : ``np.ndarray``
            The analytic solution.
        """

        a, nu, L = self.a, self.nu, self.L

        # Prep x and t for vectorial computations
        x, t = reshapeVars(x, t, extraDim=1)

        def sTerm(i, vData, sData):
            pos, denom = vData
            L = sData

            def chi(idx):
                res = pos - idx*L
                res /= denom
                return res

            warnings.filterwarnings('ignore')
            elt = erf(chi(i))
            elt -= erf(chi(i+w))
            elt /= 2
            elt[np.isnan(elt)] = 0.5
            warnings.filters.pop(0)
            return elt

        # Approximation algorithm
        vData = x - ((a*t) % self.tP), 2*(nu*t)**0.5 + 0*x

        # Approximation algorithm
        sol, conv, n, epsMax, nNoConv, tComp = approxSeries(
            sTerm, vData, L, doubleSided=True,
            iInit=0, iStep=1, iMax=nMax, tol=tol, convMask=convMask)

        # Output messages
        if conv:
            print(f'solve_CRENEL : {2*n+1} sum indexes used '
                  f'to go under tol={tol}, in {tComp:1.2e}s '
                  f'(epsMax={epsMax:1.2e})')
        else:
            print(f'solve_CRENEL : reached n={nMax}, '
                  f'{nNoConv*100:1.2f}% evaluations over tol={tol} '
                  f'(epsMax={epsMax:1.2e})')

        return sol

    def solve_SAW(self, x, t, tol=1e-15, nMax=500, convMask=True):
        r"""
        TODO

        Parameters
        ----------
        x : ``float`` or ``np.ndarray``
            The x points where to evaluate the solution.
        t : ``float`` or ``np.ndarray``
            The t points where to evaluate the solution.
        tol : ``float``, optional
            The minimum absolute variation (default=1e-15).
        nMax : ``int``, optional
            The maximum number of iterations (default=500).
        convMask : ``bool``
            Use a convergence mask to eventually speedup computation, when the
            number of points (x,t) evaluated is large enough (default=True).

        Returns
        -------
        sol : ``np.ndarray``
            The analytic solution.
        """

        a, nu, L = self.a, self.nu, self.L

        # Prep x and t for vectorial computations
        x, t = reshapeVars(x, t, extraDim=1)

        def sTerm(i, vData, sData):
            pos, denom = vData
            L = sData

            def chi(idx):
                res = pos - idx*L
                res /= denom
                return res

            warnings.filterwarnings('ignore')

            fac = pos-i*L

            elt = erf(chi(i))*fac - erf(chi(i+1))*(1-fac)
            elt += erf(chi(i+0.5))*(1-2*fac)

            tmp = np.exp(-chi(i)**2)
            tmp -= np.exp(-chi(i+0.5)**2)

            tmp *= denom
            tmp *= 2/np.pi**0.5

            elt += tmp
            elt[np.isnan(elt)] = 1*((pos[np.isnan(elt)] % L) == 0.5)

            warnings.filters.pop(0)

            return elt

        # Prep vector computation data
        vData = x - ((a*t) % self.tP), 2*(nu*t)**0.5 + 0*x

        # Approximation algorithm
        sol, conv, n, epsMax, nNoConv, tComp = approxSeries(
            sTerm, vData, L, doubleSided=True,
            iInit=0, iStep=1, iMax=nMax, tol=tol, convMask=convMask)

        # Output messages
        if conv:
            print(f'solve_SAW : {2*n+1} sum indexes used '
                  f'to go under tol={tol}, in {tComp:1.2e}s '
                  f'(epsMax={epsMax:1.2e})')
        else:
            print(f'solve_SAW : reached n={nMax}, '
                  f'{nNoConv*100:1.2f}% evaluations over tol={tol} '
                  f'(max={epsMax:1.2e})')

        return sol


# Quick module testing, run script alone to check
if __name__ == '__main__':
    import matplotlib.pyplot as plt

    x = np.linspace(0, 1, 500, endpoint=False)
    lT = [0, 0.25, 0.5, 1, 5]
    solver = AdvDiffSolver(a=1, nu=0.01, L=1)

    plt.figure('COS')
    sol = solver(x, lT, u0='COS')
    for i, t in enumerate(lT):
        plt.plot(x, sol[:, i], label=f't={t}')
    plt.legend()

    plt.figure('CRENEL')
    sol = solver(x, lT, u0='CRENEL')
    for i, t in enumerate(lT):
        plt.plot(x, sol[:, i], label=f't={t}')
    plt.legend()

    plt.figure('SAW')
    sol = solver(x, lT, u0='SAW')
    for i, t in enumerate(lT):
        plt.plot(x, sol[:, i], label=f't={t}')
    plt.legend()

    def u0(x):
        # return np.cos(2*np.pi*x)  # COS function
        # return 1*(x < 0.5) + 0.5*(x == 0.5)  # CRENEL function
        # return (x <= 0.25)*4*x + (x > 0.25)*(x < 0.75)*(2-4*x) \
        #    + (x >= 0.75)*(-4+4*x)  # SAW function
        out = x-1/2
        out **= 2
        out *= -100
        out = np.exp(out, out=out)
        return out

    plt.figure('GAUSS')
    # lT = [0, 0.25, 0.5, 1]
    sol = solver(x, lT, u0, quadRule='fejer', generic='smooth')
    for i, t in enumerate(lT):
        plt.plot(x, sol[:, i], label=f't={t}')
    plt.legend()
