# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing functions for quadrature and polynomial interpolation

Functions
---------
legendreQuadPoints(n) :
    DocTODO
legendreQuadWeights(n,points=None) :
    DocTODO
iLegendre(n,a,b) :
    DocTODO
legendre(n,x) :
    DocTODO
dLegendre(n,x) :
    DocTODO
ddLegendre(n,x) :
    DocTODO
ddLegendre(n,x) :
    DocTODO
dLegendreRoots(i,n,tol=1e-16) :
    DocTODO
matrixIntegLegendre(n,points=None) :
    DocTODO
matrixCoeffLegendre(n,points=None,weights=None) :
    DocTODO
legendreInterpol(func,x,n,points=None,matrixPL=None) :
    DocTODO
matrixIntegLagrange(points) :
    DocTODO
lagrangeInterpol(func,x,n,points=None) :
    DocTODO
"""

import numpy as np
from . import polyclassic as pol

def legendreQuadPoints(n):
    points = []
    for i in range(int(n/2)+1,n):
        points.append(dLegendreRoots(i-1,n-1))
        print(i)
    points.append(-1.0)
    points = np.array(points)
    if n%2==0:
        points = np.concatenate((points[::-1],-1.0*points))
    else:
        points = np.concatenate((points[::-1],-1.0*points[1:]))
    return np.array(points)


def legendreQuadWeights(n,points=None):
    """
    """
    if points is None:
        points = legendreQuadPoints(n)
    n = float(n)
    weigths = [2./(n*(n-1)*legendre(n-1,xi)**2) for xi in points]
    weigths[0]=2/float(n*(n-1))
    weigths[-1]=2/float(n*(n-1))
    return np.array(weigths)

def iLegendre(n,a,b):
    n = float(n)
    if (n==0.):
        return (b-a)
    else:
        return (legendre(n+1,b)-legendre(n-1,b)-legendre(n+1,a)+legendre(n-1,a))/(2.0*n+1.0)

def legendre(n,x):
    x = np.array(x)
    n = float(n)
    if (n==0.):
        return x*0+1.0
    elif (n==1.):
        return x
    else:
        return ((2.0*n-1.0)*x*legendre(n-1,x)-(n-1.)*legendre(n-2,x))/n

def dLegendre(n,x):
    x = np.array(x)
    n = float(n)
    if (n==0.):
        return x*0
    else:
        return (n/(x**2-1.0))*(x*legendre(n,x)-legendre(n-1,x))

def ddLegendre(n,x):
    x = np.array(x)
    n = float(n)
    return (n*(n+1)*legendre(n,x)-2.0*x*dLegendre(n,x))/(x**2-1.0)

def dddLegendre(n,x):
    x = np.array(x)
    n = float(n)
    return (2.0*x*ddLegendre(n,x)-(n*(n+1.0)-2.0)*dLegendre(n,x))/(1.0-x**2)

def dLegendreRoots(i,n,tol=1e-16):
    if n<2:
        print("n must be > 2")
        raise AttributeError()
    else:
        # Approximation of ???
        x=(1.0-3.*(float(n)-2.0)/(8.0*(float(n)-1.0)**3)) \
          * np.cos(np.pi*(4.0*float(i)-3.0)/(4.0*(float(n)-1.0)+1.0))
        error=10.*tol
        iters=0
        while (error>tol) and (iters<10):
            dx=-2.0*dLegendre(n,x)*ddLegendre(n,x) \
               / (2.0*ddLegendre(n,x)**2 - dLegendre(n,x)*dddLegendre(n,x))
            x=x+dx
            iters=iters+1
            error=abs(dx)
    return x

def chebyshevFKQuadPoints(n):
    points = []
    for i in range(int(n/2),n):
        points.append(np.cos(np.pi*(2*(i+1)-1)/(2*n)))
    points = np.array(points)
    if n%2==0:
        points = np.concatenate((points[::-1],-1.0*points))
    else:
        points = np.concatenate((points[::-1],-1.0*points[1:]))
    return np.array(points)

def chebyshevSKQuadPoints(n):
    points = []
    for i in range(0,n):
        points.append((-np.cos(i*np.pi/(n-1))))
    points = np.array(points)
    return np.array(points)

## Other version, using Poly objects to compute the roots => slower than recursive method
#def dLegendreRoots(i,n,tol=1e-16):
#    if n<2:
#        print "n must be > 2"
#        raise AttributeError()
#    else:
#        n = float(n)
#        i = float(i)
#        polyDxNum = -2.0*pol.legendre(n).deriv()*pol.legendre(n).deriv().deriv()
#        polyDxDen =2.0*pol.legendre(n).deriv().deriv()**2 - pol.legendre(n).deriv()*pol.legendre(n).deriv().deriv().deriv()
#        print polyDxNum
#        print polyDxDen
#        # Approximation of ???
#        x=(1.0-3.*(n-2.0)/(8.0*(n-1.0)**3)) \
#          * np.cos(np.pi*(4.0*i-3.0)/(4.0*(n-1.0)+1.0))
#        error=10.*tol
#        iters=0
#        while (error>tol) and (iters<1000):
#            print 'Num = ',polyDxNum.val(x)
#            print 'Den = ',polyDxDen.val(x)
#            dx=polyDxNum.val(x)/polyDxDen.val(x)
#            print 'dx= ',dx
#            x=x+dx
#            print 'x= ',x
#            iters=iters+1
#            error=abs(dx)
#    return x

def matrixIntegLegendre(n,points=None):
    """
    """
    if points is None:
        points = legendreQuadPoints(n)
    matrixIL = [[iLegendre(j,points[i],points[i+1]) for j in range(n)] for i in range (n-1)]
    return np.array(matrixIL)

def matrixCoeffLegendre(n,points=None,weights=None):
    """
    """
    if points is None:
        points = legendreQuadPoints(n)
    if weights is None:
        weights = legendreQuadWeights(n,points)
    gamma = np.array([[weights[i]*legendre(j,points[i])**2 for j in range(n)] for i in range(n)]).sum(axis=0)
    matrixPL = [[1./gamma[i]*weights[j]*legendre(i,points[j]) for j in range(n)] for i in range(n)]
    return np.array(matrixPL)

def legendreInterpol(func,x,n,points=None,matrixPL=None):
    """
    """
    if matrixPL is None:
        matrixPL = matrixCoeffLegendre(n,points=points)
    if points is None:
        points = legendreQuadPoints(n)
    b = x[-1]
    a = x[0]
    pointsFunc = 0.5*(b-a)*points + 0.5*(a+b)
    xNorm = 2./(b-a)*(x-0.5*(a+b))
    coeff = matrixPL.dot(func(pointsFunc))
    res = [coeff[i]*legendre(i,xNorm) for i in range(n)]
    res = np.array(res)
    return res.sum(axis=0)

def chebyshevpol(n,x):
    x = np.array(x)
    n = float(n)
    if (n==0.):
        return x*0.+1.0
    elif (n==1.):
        return x
    else:
        return 2.*x*chebyshevpol(n-1,x) - 1.*chebyshevpol(n-2,x)
# Trnasform x = 0.5*((b-a)*x + a +b)

def chebweights(n,points=None):
    if points is None:
        points = chebyshevQuadPoints(n)
    n = float(n)
    weights = np.zeros(n)
    weights = [2./(n*(n-1)*chebyshevpol(n-1,xi)**2) for xi in points]
    weights[0]=2./float(n*(n-1))
    weights[-1]=2./float(n*(n-1))
    return np.array(weights)

def matrixIntegLagrange(points):
    """
    """
    n = len(points)
    lag = [pol.lagrange(points,j) for j in range(n)]
    matrixIL = [[lag[j].integ(points[i],points[i+1]) for j in range(n)] for i in range(n-1)]
    return np.array(matrixIL)

def matrixIntegLagrangeRK(points,pointsinteg):
    """
    """
    n = len(points)
    lag = [pol.lagrange(points,j) for j in range(n)]
    matrixIL = [[lag[j].integ(points[i],points[i] + pointsinteg*(points[i+1]-points[i])) for j in range(n)] for i in range(n-1)]
    return np.array(matrixIL)

def lagrangeInterpol(func,x,n,points=None):
    """
    """
    if points is None:
        points = legendreQuadPoints(n)
    b = x[-1]
    a = x[0]
    pointsFunc = 0.5*(b-a)*points + 0.5*(a+b)
    res = [func(pointsFunc[i])*pol.lagrange(pointsFunc,i).val(x) for i in range(n)]
    res = np.array(res)
    return res.sum(axis=0)

def lagrangeInterpolvector(func,x,points):
    """
    """
    if isinstance(x,float) or isinstance(x,int):
        x = [x]
    res = len(x)*[None]
    for j in range(0,len(x)):
        res[j] = np.array([func[i]*pol.lagrange(points,i).val([x[j]]) for i in range(len(points))])
        res[j] = res[j].sum(axis=0)
    return np.array(res)