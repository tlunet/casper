# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the definition functions for finite difference scheme
construction
"""
import sympy as _sy
import numpy as _np
import scipy.sparse as _sp
import os as _os
import pickle as _pickle
from time import time as _time

# Coefficients are sorted from negative indexes to positives

tablesAdvection = \
    {'U1':  # Upwind order 1
     {'coeff': [-1, 1, 0.],
      'denom': 1., 'c1': 1/2, 'nFlops': 2, 'order': 1,
      'label': '$1^{st}$ order Upwind'},
     'C2':  # Centered order 2
     {'coeff': [-1, 0., 1],
      'denom': 2., 'c1': -1j/6, 'nFlops': 2, 'order': 2,
      'label': '$2^{nd}$ order Centered'},
     'U2':  # Upwind order 2
     {'coeff': [1, -4, 3, 0, 0],
      'denom': 2., 'c1': 1/3, 'nFlops': 5, 'order': 2,
      'label': '$3^{rd}$ order Centered'},
     'U3':  # Upwind order 3
     {'coeff': [1., -6., 3., 2., 0.],
      'denom': 6., 'c1': 1/12, 'nFlops': 7, 'order': 3,
      'label': '$3^{rd}$ order Upwind'},
     'C4':  # Centered order 4
     {'coeff': [1., -8., 0., 8., -1.],
      'denom': 12., 'c1': -1j/30, 'nFlops': 6, 'order': 4,
      'label': '$4^{th}$ order Centered'},
     'U4':  # Upwind order 4
     {'coeff': [-1, 6, -18, 10, 3, 0, 0],
      'denom': 12., 'c1': 1/20, 'nFlops': 10, 'order': 4,
      'label': '$4^{th}$ order Centered'},
     'U5':  # Upwind order 5
     {'coeff': [-2., 15., -60., 20., 30., -3., 0.],
      'denom': 60., 'c1': 1/60, 'nFlops': 11, 'order': 5,
      'label': '$5^{th}$ order Upwind'},
     'C6':  # Centered order 6
     {'coeff': [-1., 9., -45., 0., 45., -9., 1.],
      'denom': 60., 'c1': -1j/140, 'nFlops': 10, 'order': 6,
      'label': '$6^{th}$ order Centered'},
     'C8':  # Centered order 8
     {'coeff': [3., -32., 168., -672., 0., 672., -168., 32., -3.],
      'denom': 840., 'c1': -1j/630, 'nFlops': 14, 'order': 8,
      'label': '$8^{th}$ order Centered'}
     }

tablesDiffusion = \
    {'C2':  # Centered order 2
     {'coeff': [1., -2., 1.],
      'denom': 1., 'order': 2, 'c1': 1/12,
      'label': '$2^{nd}$ order Centered'},
     'C4':  # Centered order 4
     {'coeff': [-1., 16., -30., 16., -1],
      'denom': 12., 'order': 4, 'c1': 1/90,
      'label': '$4^{th}$ order Centered'},
     'C6':  # Centered order 6
     {'coeff': [2., -27., 270., -490., 270., -27., 2.],
      'denom': 180., 'order': 6, 'c1': 1/560,
      'label': '$6^{th}$ order Centered'},
     'C8':  # Centered order 8
     {'coeff': [-1./560., 8./315., -1./5., 8./5., -205./72,
                8./5., -1./5., 8./315., -1./560.],
      'denom': 1., 'order': 8, 'c1': 1/3150,
      'label': '$8^{th}$ order Centered'}
     }


def buildStencil(dx, a=1, nu=0, epsilon=0, advScheme='C2', diffScheme='C2'):
    # Advection stencil
    if a == 0:  # No advection
        adv = _np.array([0])
    elif advScheme in tablesAdvection:
        s = tablesAdvection[advScheme]
        adv = _np.array(s['coeff'])
        adv /= s['denom']*dx
        adv *= -a
    else:
        raise ValueError(
            'advection scheme not implemented with {} discretization'
            .format(advScheme))

    adv = list(adv)

    # Diffusion stencil
    if nu == 0:  # No diffusion
        diff = _np.array([0])
    elif diffScheme in tablesDiffusion:
        s = tablesDiffusion[diffScheme]
        diff = _np.array(s['coeff'])
        diff /= s['denom']*(dx**2)
        diff *= nu
    else:
        raise ValueError(
            'diffusion scheme not implemented with {} discretization'
            .format(diffScheme))

    diff = list(diff)

    # Reaction stencil
    if epsilon == 0:
        reac = [0]  # No reaction
    else:
        reac = [epsilon]  # Value of u on grid point

    # Combine stencils
    stencils = [adv, diff, reac]
    sLength = max([len(s) for s in stencils])
    for s in stencils:
        while len(s) < sLength:
            s += [0]
            s.reverse()
            s += [0]
            s.reverse()
    return sum([_np.array(s) for s in stencils])


def matrixDiag(n, scheme):
    """Private method, returns a differential matrix associated to a
    linear 1d operator"""
    # Equation type & spatial scheme
    # n = Number of point used in spatial scheme ! MUST BE ODD !
    n_scheme = _np.size(scheme, 0)
    # Offsets and data vectors
    offsets = [0]
    data = _np.array([_np.ones(n)*scheme[int((n_scheme+1)/2-1)]])
    for i in range(1, int((n_scheme+1)/2)):
        offsets = _np.hstack(([-i], offsets, [i]))
        up = _np.array([[scheme[int((n_scheme+1)/2-1-i)]]]).repeat(n, axis=1)
        down = _np.array([[scheme[int((n_scheme+1)/2-1+i)]]]).repeat(n, axis=1)
        data = _np.vstack((up, data, down))
    # Diagonal sparse matrix
    A = _sp.dia_matrix((data, offsets), shape=(n, n))
    # Result
    return A


def buildDiffMatrices(scheme, n, APrev=0.0,
                      ANordPrev=_np.zeros((1, 1)), ASudPrev=_np.zeros((1, 1))):
    nHaloPrev = _np.size(ANordPrev, 0)
    A = APrev + matrixDiag(n, scheme)
    nHalo = int((len(scheme)-1)/2)   # halo size of the differential scheme
    schemeSud = _np.hstack((_np.zeros((nHalo-1)), scheme[:nHalo]))
    ASudNew = matrixDiag(nHalo, schemeSud).toarray()
    schemeNord = _np.hstack((scheme[-nHalo:], _np.zeros((nHalo-1))))
    ANordNew = matrixDiag(nHalo, schemeNord).toarray()
    # Resizing the matrix ASud and ANord (or self.ASud and self.ANord,
    # depending on which one is the smaller)
    diffSize = nHaloPrev - nHalo
    if diffSize != 0:
        littleMat = _np.zeros((min(nHaloPrev, nHalo), abs(diffSize)))
        bigMat = _np.zeros((abs(diffSize), max(nHaloPrev, nHalo)))
        if diffSize < 0:
            ASudPrev = _np.hstack((littleMat, ASudPrev))
            ASudPrev = _np.vstack((ASudPrev, bigMat))
            ANordPrev = _np.hstack((ANordPrev, littleMat))
            ANordPrev = _np.vstack((bigMat, ANordPrev))
        elif diffSize > 0:
            ASudNew = _np.hstack((littleMat, ASudNew))
            ASudNew = _np.vstack((ASudNew, bigMat))
            ANordNew = _np.hstack((ANordNew, littleMat))
            ANordNew = _np.vstack((bigMat, ANordNew))
    # Adding the scheme halo matrix to the self one
    ASud = ASudPrev + ASudNew
    ANord = ANordPrev + ANordNew
    return [A, ANord, ASud]


def conservativeBaryCoeff(fdScheme):
    s = len(fdScheme)
    if s % 2 != 1:
        raise ValueError("Length of fdScheme has to be odd")
    if int(fdScheme[(s-1)//2]) == 0:
        raise ValueError("fdScheme has to be non symmetric")
    lScheme = [int(coeff) for coeff in fdScheme]
    lAU = [lScheme]
    lU = [s*[0]]

    for i in range(1, s):
        lAU = lAU + [lScheme[i:]+i*[0]]
        lU = lU + [i*[0]+lScheme[:-i]]

    AU = _sy.Matrix(lAU)
    U = _sy.Matrix(lU)
    P = AU + U
    P = P.vstack(P, _sy.ones(1, s))
    b = _sy.Matrix(s*[0]+[1])
    return P.QRsolve(b).tolist()


def polyInterpolationCoeff(order, uniform=True):
    nP = order
    i0 = int((nP-1)/2)

    A = _sy.zeros(nP+1)
    e = _sy.ones(1, nP+1)

    # Construction of dx symbols
    lDx = []
    for i in range(nP):
        lDx.append(_sy.Symbol('\\Delta x_{'+str(i-i0)+'}'))
    lPos = [0]*(nP+1)
    lPos[i0] = 0
    for i in range(int((nP-1)/2)):
        lPos[i0-i-1] = lPos[i0-i]-lDx[i0-i-1]
    for i in range(int((nP+1)/2)):
        lPos[i0+i+1] = lPos[i0+i]+lDx[i0+i]

    # Contruct vandermonde matrix
    for i in range(nP+1):
        if uniform:
            c = i-i0
        else:
            c = lPos[i]
        for j in range(nP+1):
            A[i, j] = c**j

    # Coordinate for middle point
    for i in range(nP+1):
        if uniform:
            e[0, i] /= 2**i
        else:
            e[0, i] *= (lDx[i0]/2)**i

    dataName = 'coeff_{}{}.dat'.format(nP, '_unif' if uniform else '')
    if _os.path.isfile(dataName):
        coeff = _pickle.load(open(dataName, 'rb'))
    else:
        print('Inverting A')
        t0 = _time()
        coeff = e * A.inv()
        print(' -- computation time = {:.2f}'.format(_time()-t0))
        print('Simplifying coeff')
        t0 = _time()
        coeff.simplify()
        print(' -- computation time = {:.2f}'.format(_time()-t0))
        _pickle.dump(coeff, open(dataName, 'wb'))
    print('Coefficients = ')
    maxDenom = 1
    for c in coeff:
        if hasattr(c, 'q'):
            if c.q > maxDenom:
                maxDenom = c.q
    for c in coeff:
        print(c*maxDenom)
    print('devided by : {}'.format(maxDenom))
    return coeff


def fourierSymbolInterpolation(order):
    coeff = polyInterpolationCoeff(order)
    i0 = int((order-1)/2)
    lCoeff = coeff[i0+1:]
    k = _sy.Symbol('\\theta_\\kappa')
    fs = _sy.ones(1)
    fs /= 2
    fs = fs[0]
    for i in range(0, i0+1):
        fs += lCoeff[i]*_sy.cos((2*i+1)*k)
    fs = fs.factor()
    fs.simplify()

    f = _np.vectorize(_sy.lambdify(k, fs))

    return fs, f


if __name__ == '__main__':
    print(conservativeBaryCoeff([1., -1., 0.]))
    print(conservativeBaryCoeff([-1.0, 6.0, -3.0, -2.0, 0.0]))
    print(conservativeBaryCoeff([2., -15., 60., -20., -30., 3., 0.]))
