#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing Von Neumann analysis functions
"""
# Python imports
import numpy as np
import scipy.special as sps
import scipy.linalg as spl
import sympy as sy
from functools import wraps
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap

# Casper imports
from casper.maths.butcher import tablesExplicit, tablesImplicit
from casper.maths.findiff import tablesAdvection, tablesDiffusion

# Module parameters
USE_RAINBOW_DESATURATED = True
USE_QUADRUPLE_PRECISION = False


# Numerical precision
class Precision(object):

    @property
    def float(self):
        if hasattr(np, 'float128') and USE_QUADRUPLE_PRECISION:
            return np.float128
        else:
            return np.float64

    @property
    def complex(self):
        if hasattr(np, 'complex256') and USE_QUADRUPLE_PRECISION:
            return np.complex256
        else:
            return np.complex128


_precision = Precision()
_float = _precision.float
_complex = _precision.complex
j = _complex(1j)

# Symbolic definitions and utilitary functions
thega = sy.Symbol('\\theta')


def symDarkTheme():
    sy.init_printing(forecolor='White')


def symRound(expr, nDigit=15):
    roundedExpr = expr
    for a in sy.preorder_traversal(expr):
        if isinstance(a, sy.Float):
            roundedExpr = roundedExpr.subs(a, round(a, nDigit))
    return roundedExpr


def symRoundingWrapper(fun):

    @wraps(fun)
    def roundedFun(*args, **kwargs):
        return symRound(fun(*args, **kwargs))

    return roundedFun


# Custom colormap definition
_colors = [(0.278431, 0.278431, 0.858824),
           (0, 0, 0.360784),
           (0, 1, 1),
           (0, 0.501961, 0),
           (1, 1, 0),
           (1, 0.380392, 0),
           (0.419608, 0, 0),
           (0.878431, 0.301961, 0.301961)]
_cmap = LinearSegmentedColormap.from_list(
    'rainbow_desaturated', _colors, N=200)


def _stabContourCMap():
    if USE_RAINBOW_DESATURATED:
        return _cmap
    else:
        return plt.cm.jet


# -----------------------------------------------------------------------------
# Eigenvalues of uniform periodic 1D space discretization operators
# -----------------------------------------------------------------------------
def zDiff(theta, spaceScheme, symbolic=False):

    def U(i):
        if symbolic:
            return sy.exp(i*thega*sy.I)
        else:
            return np.exp(i*theta*j)

    if spaceScheme == 'C2':
        z = U(1) - 2*U(0) + U(-1)
    elif spaceScheme == 'C4':
        z = (-U(2) + 16*U(1) - 30*U(0) + 16*U(-1) - U(-2))/12
    elif spaceScheme == 'C6':
        z = (2*U(3) - 27*U(2) + 270*U(1) - 490*U(0)
             + 270*U(-1) - 27*U(-2) + 2*U(-3))/180
    elif spaceScheme == 'F1':
        z = U(0) - 2*U(1) + U(2)
    elif spaceScheme == 'F2':
        z = 2*U(0) - 5*U(1) + 4*U(2) - U(3)
    elif spaceScheme == 'SP':
        if symbolic:
            z = -thega**2
        else:
            z = -theta**2
    elif spaceScheme in tablesDiffusion:
        data = tablesDiffusion[spaceScheme]
        coeff, denom = data['coeff'], data['denom']
        s = int((len(coeff)-1)/2)
        z = sum(c*U(i) for i, c in zip(range(-s, s+1), coeff))
        z /= denom
    else:
        raise ValueError('No zDIFF implemented for spaceScheme={}'
                         .format(spaceScheme))
    return z


def zAdv(theta, spaceScheme, symbolic=False):

    def U(i):
        if symbolic:
            return sy.exp(i*thega*sy.I)
        else:
            return np.exp(i*theta*j)

    def iSin(i):
        if symbolic:
            return sy.sin(i*thega)*sy.I
        else:
            return np.sin(i*theta)*j

    def WENO5_flux(side):
        if side == 'right':
            i = 0
        elif side == 'left':
            i = -1
        beta0 = 13./12*(U(i-2)-2*U(i-1)+U(i))**2 + \
            1./4*(U(i-2)-4*U(i-1)+3*U(i))**2
        beta1 = 13./12*(U(i-1)-2*U(i)+U(1+i))**2 + 1./4*(U(i-1)-U(1+i))**2
        beta2 = 13./12*(U(i)-2*U(1+i)+U(2+i))**2 + \
            1./4*(3*U(i)-4*U(1+i)+U(2+i))**2
        alpha0 = 1./10.*(1./(1e-16+beta0))**2
        alpha1 = 6./10.*(1./(1e-16+beta1))**2
        alpha2 = 3./10.*(1./(1e-16+beta2))**2
        gamma0 = alpha0/(alpha0+alpha1+alpha2)
        gamma1 = alpha1/(alpha0+alpha1+alpha2)
        gamma2 = alpha2/(alpha0+alpha1+alpha2)
        return gamma0*(2./6*U(i-2)-7./6*U(i-1)+11./6*U(i)) + \
            gamma1*(-1./6*U(i-1)+5./6*U(i)+2./6*U(i+1)) + \
            gamma2*(2./6*U(i)+5./6*U(i+1)-1./6*U(i+2))

    def WENO3_flux(side):
        if side == 'right':
            i = 0
        elif side == 'left':
            i = -1
        beta0 = (-U(i-1)+U(i))**2
        beta1 = (-U(i)+U(i+1))**2
        alpha0 = 1./3*(1e-9+beta0)**(-2)
        alpha1 = 2./3*(1e-9+beta1)**(-2)
        gamma0 = alpha0/(alpha0 + alpha1)
        gamma1 = alpha1/(alpha0 + alpha1)
        return gamma0*(-1./2*U(i-1)+3./2*U(i)) + \
            gamma1*(1./2*U(i)+1./2*U(i+1))

    if spaceScheme == 'C2':
        z = iSin(1)
    elif spaceScheme == 'C4':
        z = 4*iSin(1)/3-iSin(2)/6
    elif spaceScheme == 'C6':
        z = (iSin(3)-9*iSin(2)+45*iSin(1))/30
    elif spaceScheme == 'C8':
        z = (-3*iSin(4)+32*iSin(3)-168*iSin(2)+672*iSin(1))/420
    elif spaceScheme == 'WENO3':
        z = WENO3_flux('right') - WENO3_flux('left')
    elif spaceScheme == 'WENO5':
        z = WENO5_flux('right') - WENO5_flux('left')
    elif spaceScheme == 'SP':
        if symbolic:
            z = sy.I*thega
        else:
            z = j*theta
    elif spaceScheme in tablesAdvection:
        data = tablesAdvection[spaceScheme]
        coeff, denom = data['coeff'], data['denom']
        s = int((len(coeff)-1)/2)
        z = sum(c*U(i) for i, c in zip(range(-s, s+1), coeff))
        z /= denom
    else:
        raise ValueError('No zADV implemented for spaceScheme={}'
                         .format(spaceScheme))

    return z


def eigenDtAdvDiff(theta, cfl, advScheme, reMesh=None, diffScheme='C2',
                   returnTheta=False):
    if isinstance(theta, tuple):
        theta = np.linspace(*theta, endpoint=False, dtype=_float)
    lamDt = theta*cfl*0 + zAdv(theta, advScheme)
    lamDt *= -1
    if reMesh is not None:
        lamDt += zDiff(theta, diffScheme)/reMesh
    lamDt *= cfl
    if returnTheta:
        return lamDt, theta
    else:
        return lamDt


def symZAdv(spaceScheme):
    return zAdv(None, spaceScheme, symbolic=True)


@symRoundingWrapper
def taylorZAdv(spaceScheme, nTerms=6):
    return sy.series(symZAdv(spaceScheme), n=nTerms)


def symZDiff(spaceScheme):
    return zDiff(None, spaceScheme, symbolic=True)


@symRoundingWrapper
def taylorZDiff(spaceScheme, nTerms=6):
    return sy.series(symZDiff(spaceScheme), n=nTerms)


# -----------------------------------------------------------------------------
# Stability function of linear time integration methods
# -----------------------------------------------------------------------------

def _isSym(z):
    return isinstance(z, sy.Expr)


def _isMat(z):
    if isinstance(z, np.ndarray):
        return z.ndim == 2 and (z.shape[0] == z.shape[1])
    return False


def _wrapperStabFunc(_g):
    """Generic wrapper for stability functions"""

    def g(z=None, m=1):
        """
        Parameters
        ----------
        z: scalar or list or np.ndarray or sy.Symbol or None
            Parameter of the stability function (default=None).
            Four main cases are possible :
        - z is None:
            In that case, a default sy.Symbol 'z' is evaluated
        - type(z)==sy.Symbol:
            Uses symbolic computation
        - type(z)=np.ndarray and z is a square matrix:
            Uses matrix computation
        - other cases:
            Uses scalar or vectorial computation
        m: integer
            Parameter to evaluate :math:`g(z/m)^m` (ignored if m=1).
        """
        shapeZ = None
        # Default None argument
        if z is None:
            z = sy.Symbol('z')
        # If not matrix/symbolic/scalar computation,
        # transforms z into a 1D array
        if (not _isSym(z)) and (not _isMat(z)):
            z = np.asarray(z, dtype=_complex)
            if z.ndim == 0:
                z = z[None][0]
            else:
                shapeZ = z.shape
                z = z.ravel()
        # Division if m != 1
        if m != 1:
            z = z/m
        # Evaluate stability function
        res = _g(z)
        # Power if m != 1
        if m != 1:
            if _isMat(z):
                res = np.linalg.matrix_power(res, m)
            else:
                res **= m
        # Handle vector case
        if shapeZ is not None:
            res.shape = shapeZ
        return res

    g.__doc__ = _g.__doc__ + g.__doc__

    return g


def stabilityFunction(timeScheme, implementation='DEFAULT'):
    r"""
    Generate the stability function of a given linear time integration
    scheme.
    This function (noted g) take as argument :
    - z, optional: evaluation parameter of the stability function
    - m(int), optional: argument used to evaluate g(z/m)**m

    The argument z can be of different types :

    - None:
        Default behavior, return a symbolic expression of the stability
        function. Example :

    >>> g = stabilityFunction('FE')
    >>> g()
    1.0 + 1.0*z
    >>> g = stabilityFunction('TRAPZ')
    >>> g()
    (0.5*z + 1.0)/(1.0 - 0.5*z)

    - sy.Symbol:
        Evaluate a symbolic expression with z as a symbol. Example :

    >>> g = stabilityFunction('RK2')
    >>> x = sy.Symbol('x')
    >>> z = sy.sin(x)*1j
    >>> g(z).expand()
    -0.5*sin(x)**2 + 1.0*I*sin(x) + 1.0

    - matrix: np.ndarray with shape (n,n)
        Evaluate the stability function on a square matrix. Example :

    >>> jac = np.array(
    >>>  [[-1. ,  0.5,  0. ,  0. ,  0. ],
    >>>   [ 0.5, -1. ,  0.5,  0. ,  0. ],
    >>>   [ 0. ,  0.5, -1. ,  0.5,  0. ],
    >>>   [ 0. ,  0. ,  0.5, -1. ,  0.5],
    >>>   [ 0. ,  0. ,  0. ,  0.5, -1. ]])
    >>> g = stabilityFunction('FE')
    >>> g(jac)
    array([[0. , 0.5, 0. , 0. , 0. ],
       [0.5, 0. , 0.5, 0. , 0. ],
       [0. , 0.5, 0. , 0.5, 0. ],
       [0. , 0. , 0.5, 0. , 0.5],
       [0. , 0. , 0. , 0.5, 0. ]])

    - scalar or np.ndarray:
        Evaluate the stability function on a scalar or an array (vector
        computation). Result is given as complex number(s). Example :

    >>> g = stabilityFunction('RK4')
    >>> g(-1)
    (0.3750000000000001+0j)
    >>> g([-1, -1-j])
    array([0.375     +0.j        , 0.16666667-0.33333333j])
    >>> g(np.linspace(-1,0, num=10))
    array([0.375     +0.j, 0.41512981+0.j, 0.46152136+0.j, 0.51440329+0.j,
       0.57415663+0.j, 0.64131484+0.j, 0.71656379+0.j, 0.80074176+0.j,
       0.89483946+0.j, 1.        +0.j])

    Note
    ----
    As the stability function tends to approximate the exponential function,
    the use of a large m argument should approximate the result given by the
    exponential function. Example :

    >>> # Evaluation on scalar
    >>> stabilityFunction('EXP')(-1)
    (0.36787944117144233+0j)
    >>> stabilityFunction('RK4')(-1, m=10)
    (0.36787977441249825+0j)
    >>> # Evaluation on matrix
    >>> jac = np.array(
    >>>  [[-1. ,  0.5,  0. ,  0. ,  0. ],
    >>>   [ 0.5, -1. ,  0.5,  0. ,  0. ],
    >>>   [ 0. ,  0.5, -1. ,  0.5,  0. ],
    >>>   [ 0. ,  0. ,  0.5, -1. ,  0.5],
    >>>   [ 0. ,  0. ,  0. ,  0.5, -1. ]])
    >>> stabilityFunction('EXP')(jac)
    array([[0.41582083, 0.19975511, 0.04893181, 0.00805486, 0.00099042],
           [0.19975511, 0.46475264, 0.20780996, 0.04992223, 0.00805486],
           [0.04893181, 0.20780996, 0.46574306, 0.20780996, 0.04893181],
           [0.00805486, 0.04992223, 0.20780996, 0.46475264, 0.19975511],
           [0.00099042, 0.00805486, 0.04893181, 0.19975511, 0.41582083]])
    >>> stabilityFunction('RK4')(jac, m=10)
    array([[0.41582163, 0.19975422, 0.04893227, 0.00805476, 0.00099041],
       [0.19975422, 0.4647539 , 0.20780898, 0.04992268, 0.00805476],
       [0.04893227, 0.20780898, 0.46574431, 0.20780898, 0.04893227],
       [0.00805476, 0.04992268, 0.20780898, 0.4647539 , 0.19975422],
       [0.00099041, 0.00805476, 0.04893227, 0.19975422, 0.41582163]])

    Parameters
    ----------
    timeScheme : str
        Name of the time integration scheme. Any other time discretization
        scheme with its butcher table defined in casper.maths.butcher.py
        can be used. Examples are :
    - EXP: exponential time integration
    - BE: Backward Euler
    - FE: Forward Euler
    - TRAPZ: trapezoidal rule
    - RK4: fourth order classical Runge-Kutta explicit
    - SDIRK3: Singly Diagonal Implicit Runge-Kutta method of third order
    implementation : str, optional
        Implementation used for the stability function (default='DEFAULT').
        Some scheme have an 'OPTIM' implementation, that is faster and/or
        shows nicer symbolic results. For the implicit scheme, one can select
        from:
    - DET :
        uses the determinant formulation of the stability function to write
        is as a rationnal function (used when implementation='DEFAULT').

        .. math::
            g(z) = \frac{det(I - (A- e b^T)z}{det(I-Az)},

        with :math:`A` and :math:`b` the matrix and vector from the Butcher
        tables and :math:`e=[1,1,...,1]`.
    - GEN :
        uses the generic formulation of the stability function :

        .. math::
            g(z) = 1 + z b^T (I-zA)^{-1} e,

        with the same notations as for the DET implementation.

        For explicit scheme, the default formulation uses the nilpotent
        aspect of the Butcher table matrix :math:`A`, to compute:

        .. math::
            g(z) = 1 + \sum_{i=1}^{s} z^i b^T A^{i-1},

        where :math:`s` is the number of stage of the RK method (and the size
        of :math:`b`).

    Returns
    -------
    g: function(z=None, m=1)
        The stability function.
    """

    if timeScheme == 'EXP':

        @_wrapperStabFunc
        def gEXP(z):
            """
            Stability function (amplification factor) of exponential
            integration

            .. math::
                g(z) = exp(z)

            Symbolic/matrix computation enabled
            """
            if _isSym(z):
                return sy.exp(z)
            elif _isMat(z):
                return spl.expm(z)
            return np.exp(z)

        g = gEXP

    elif timeScheme == 'FE' and implementation == 'OPTIM':

        @_wrapperStabFunc
        def gFE(z):
            """
            Stability function (amplification factor) of Forward Euler
            (optimized implementation)

            .. math::
                g(z) = 1 + z

            Symbolic/matrix computation enabled
            """
            one = np.eye(z.shape[0]) if _isMat(z) else 1
            return one + z

        g = gFE

    elif timeScheme == 'RK4' and implementation == 'OPTIM':

        @_wrapperStabFunc
        def gRK4(z):
            """
            Stability function (amplification factor) of RK4
            (optimized implementation)

            .. math::
                g(z) = 1 + z + z^2/2 + z^3/6 + z^4/24

            Symbolic/matrix computation enabled
            """
            if _isMat(z):
                # Matrix computation
                I = np.eye(z.shape[0])
                res = z/24
                res += I/6
                np.dot(res, z, out=res)
                res += I/2
                np.dot(res, z, out=res)
                res += I
                np.dot(res, z, out=res)
                res += I
                return res
            elif _isSym(z):
                # Symbolic computation
                res = z/24
                res += sy.Rational(1, 6)
                res *= z
                res += sy.Rational(1, 2)
                res *= z
                res += 1
                res *= z
                res += 1
                res = res.expand()
                return res
            # Generic computation
            res = z/24
            res += 1/6
            res *= z
            res += 1/2
            res *= z
            res += 1
            res *= z
            res += 1
            return res

        g = gRK4

    elif timeScheme == 'BE' and implementation == 'OPTIM':

        @_wrapperStabFunc
        def gBE(z):
            """
            Stability function (amplification factor) of Backward Euler
            (optimized implementation)

            .. math::
                g(z) = \\frac{1}{1-z}

            Symbolic/matrix computation enabled
            """
            if _isMat(z):
                I = np.eye(z.shape[0])
                return np.linalg.inv(I-z)
            res = -z
            res += 1
            res **= -1
            return res

        g = gBE

    elif timeScheme == 'TRAPZ' and implementation == 'OPTIM':

        @_wrapperStabFunc
        def gTRAPZ(z):
            """
            Stability function (amplification factor) of trapezoidal rule
            (optimized implementation)

            .. math::
                g(z) = \\frac{1+z/2}{1-z/2}

            Symbolic/matrix computation enabled
            """
            if _isMat(z):
                I = np.eye(z.shape[0])
                num = I + z/2
                den = I - z/2
                return num.dot(np.linalg.inv(den))
            zHalf = z/2
            res = 1+zHalf
            res /= 1-zHalf
            return res

        g = gTRAPZ

    elif timeScheme == 'SDIRK3' and implementation == 'OPTIM':

        @_wrapperStabFunc
        def gSDIRK3(z):
            """
            Stability function (amplification factor) of SDIRK3
            (optimized implementation)

            .. math::
                g(z) = \\frac{1 + c_1 z + c_2 z^2}{(1-\\alpha z)^3}

            with

            .. math::
                \\alpha = 0.43586652150845967, \\hspace{10pt}
                c_1 = 1 - 3 \\alpha, \\hspace{10pt}
                c_2 = 1/2 - 3\\alpha + 3\\alpha^2.

            Symbolic/matrix computation enabled
            """
            # Coefficients
            a = 0.43586652150845967
            c1 = 1-3*a
            c2 = 1/2 - 3*a+3*a**2
            # Matrix computation
            if _isMat(z):
                I = np.eye(z.shape[0])
                num = I + c1*z + c2*np.dot(z, z)
                den = np.linalg.matrix_power(I-a*z, 3)
                return num.dot(np.linalg.inv(den))
            # Numerator computation
            num = c2*z
            num += c1
            num *= z
            num += 1
            # Denominator computation
            den = -a*z
            den += 1
            den **= 3
            # Final division
            num /= den
            return num

        g = gSDIRK3

    elif timeScheme in tablesExplicit:

        # Check selected implementation
        if implementation != 'DEFAULT':
            raise NotImplementedError(
                f'implementation={implementation} for {timeScheme}')

        but = tablesExplicit[timeScheme]
        A = np.array(but['A'], dtype=_float)
        b = np.array(but['b'], dtype=_float)
        nA = A.shape[0]

        lCoeff = [np.sum(b.dot(np.linalg.matrix_power(A, i)))
                  for i in range(nA)]
        lCoeff = [1.] + lCoeff

        @_wrapperStabFunc
        def gEXPLICIT(z):
            """
            Stability function (amplification factor), timeScheme={}
            (general implementation for explicit methods)

            Symbolic/matrix computation enabled
            """
            # Matrix computation
            if _isMat(z):
                I = np.eye(z.shape[0])
                res = 0
                for coeff in lCoeff[-1::-1]:
                    res = np.dot(res, z) + coeff*I
                return res
            # Symbolic/scalar/vector computation
            res = 0
            for coeff in lCoeff[-1::-1]:
                res = res * z + coeff
            return res

        gEXPLICIT.__doc__ = gEXPLICIT.__doc__.format(timeScheme)
        g = gEXPLICIT

    elif timeScheme in tablesImplicit:

        # Check selected implementation
        if implementation == 'DEFAULT':
            implementation = 'DET'
        elif implementation not in ['DET', 'GEN', 'NUM']:
            raise NotImplementedError(
                f'implementation={implementation} for {timeScheme}')

        but = tablesImplicit[timeScheme]

        if implementation in ['DET', 'GEN']:

            A = sy.Matrix(but['A'])
            b = sy.Matrix(but['b'])
            nA = A.shape[0]
            e = sy.Matrix(np.ones(nA))
            mI = sy.Matrix(np.eye(nA))
            z = sy.Symbol('z')

            # Compute numerator and denominator of stability function
            num = sy.det(mI-(A-e*b.T)*z)
            den = sy.det(mI-A*z)

            # Compute coefficients for matrix computation
            try:
                nPoly = sy.Poly(num, z)
            except sy.PolynomialError:
                nPoly = sy.apart_list(num, x=z)[1]
            nCoeffs = np.array(nPoly.coeffs()).astype(float)
            try:
                dPoly = sy.Poly(den, z)
            except sy.PolynomialError:
                dPoly = sy.apart_list(den, x=z)[1]
            dCoeffs = np.array(dPoly.coeffs()).astype(float)

            if implementation == 'DET':
                # Use determinant formulation
                r = num/den
            elif implementation == 'GEN':
                # Use generic formulation
                r = 1 + (z*b.T*(mI-A*z).inv()*e)[0]

            func = sy.lambdify(z, r)

            @_wrapperStabFunc
            def gIMPLICIT_SYM(z):
                """
                Stability function (amplification factor), timeScheme={}
                (general implementation ({}) for implicit methods)

                Symbolic/matrix computation enabled
                (matrix computation uses determinant formulation)
                """
                if _isMat(z):
                    # Matrix computation
                    I = np.eye(z.shape[0])
                    num = 0
                    for coeff in nCoeffs:
                        num = np.dot(num, z) + coeff*I
                    den = 0
                    for coeff in dCoeffs:
                        den = np.dot(den, z) + coeff*I
                    return num.dot(np.linalg.inv(den))
                # Generic computation
                return func(z)

            gIMPLICIT_SYM.__doc__ = \
                gIMPLICIT_SYM.__doc__.format(timeScheme, implementation)
            g = gIMPLICIT_SYM

        elif implementation == 'NUM':

            A = np.array(but['A'], dtype=_float)
            b = np.array(but['b'], dtype=_float)
            nA = A.shape[0]

            @_wrapperStabFunc
            def gIMPLICIT_NUM(z):
                """
                Stability function (amplification factor), timeScheme={}
                (general implementation (NUM) for implicit methods)

                Symbolic/matrix computation not enabled
                """
                if _isSym(z) or _isMat(z):
                    raise ValueError('symbolic/matrix computation not enabled')
                _b = np.array(b, dtype=_complex)
                _A = np.array(A, dtype=_complex)
                res = np.empty_like(z)
                for i in range(z.size):
                    res[i] = 1. + z[i]*np.sum(
                        _b.dot(np.linalg.inv(np.eye(nA) - z[i]*_A)))
                return res

            gIMPLICIT_NUM.__doc__ = gIMPLICIT_NUM.__doc__.format(timeScheme)
            g = gIMPLICIT_NUM
    else:
        raise NotImplementedError(f'timeScheme={timeScheme}')

    return g


amplificationFactor = stabilityFunction   # Alias


@symRoundingWrapper
def taylorStabFunc(timeScheme, nTerms=6):
    return sy.series(stabilityFunction(timeScheme)(), n=nTerms)


def computeR(advScheme, timeScheme, cfl=None, diffScheme=None,
             numK=201, kMin=0.0, kMax=np.pi,
             numCFL=203, cflSup=3.0):
    if cfl is None:
        cfl = np.linspace(0.01, cflSup, numCFL, dtype=_float
                          ).reshape((numCFL, 1))
    theta = np.linspace(kMin, kMax, numK, dtype=_float, endpoint=True
                        ).reshape((1, numK))

    z = 0
    for adv in advScheme.split('_'):
        advSchemeCoeff = adv.split('-')
        advScheme = advSchemeCoeff[0]
        _z = zAdv(theta, advScheme)
        if len(advSchemeCoeff) > 1:
            _z *= float(advSchemeCoeff[1])
        z = z + _z
    g = amplificationFactor(timeScheme)
    return g(-np.dot(cfl, z)), z, theta, cfl


def findCFLMax(stab, cfl, stabMax=1.0):
    val = (abs(stab) <= stabMax)*cfl
    res = 0
    i = 1
    iMax = val.shape[0]
    while (res == 0) and (i < iMax):
        res = np.sum(val[i, :] == 0)
        i += 1
    return val[i-1, 0], i-1


def computeCFLMax(spaceScheme, timeScheme, cflSup=3.0, numCFL=501):
    stab, z, kappa, cfl = \
        computeR(spaceScheme, timeScheme, numK=501, numCFL=501)
    return findCFLMax(np.abs(stab), cfl)[0]


def gParareal(n, k, rGBar, rFBar, relax=None):
    if k == n:
        return rFBar
    g = rGBar**n
    if relax:
        if k == 0:
            return g
        elif k == 1:
            return rGBar**(n-1)*((1-relax**n)/(1-relax)*rGBar +
                                 (relax-relax**n)/(1-relax)*rFBar)
    for i in range(k):
        g += sps.binom(n, i+1)*(rFBar-rGBar)**(i+1)*rGBar**(n-i-1)
    return g

#    # Optimized implementation
#    numI, numJ = rGBar.shape
#    rFBar = rFBar.ravel()
#    nr = rFBar.size
#    rGBar = rGBar.ravel()
#    qBar = rFBar/rGBar
#    I = np.arange(k+1).reshape(k+1, 1).repeat(nr, axis=1)
#    return (rGBar**n*np.sum(
#                sps.binom(n, I)*(qBar-1)**I, axis=0)).reshape(numI, numJ)


def plotAmplContour(r, theta, cfl, acc=None, figName=None, normCFL=True,
                    plotCFLMax=None, amplificationMax=2):
    plt.figure(figName)
    thetaPlot = theta/np.pi
    levels = np.linspace(0, amplificationMax, num=41)
    stab = np.abs(r)
    if normCFL:
        stab = stab**(1./(cfl+1e-16))
    plt.contourf(thetaPlot, cfl, stab, levels=levels, cmap=_stabContourCMap())
    plt.colorbar()
    if acc is not None:
        levels2 = [1e-1]
        CS30 = plt.contour(thetaPlot, cfl, acc,
                           levels=levels2,
                           colors='magenta',
                           linestyles='dashed')
        plt.clabel(CS30, fontsize=9, inline=1, fmt='%1.e')
    plt.xlim(0, 1)
    plt.ylabel('$CFL$')
    plt.xlabel('$\\theta_\\kappa/\\pi$')
    CS3 = plt.contour(thetaPlot, cfl, stab, levels=[1],
                      colors='red',
                      linestyles='dashed',
                      linewidths=2,
                      )
    plt.clabel(CS3, fontsize=9, inline=1, fmt='%1.1f')
    if plotCFLMax:
        plt.hlines(plotCFLMax, 0, 1, colors='gray', linestyles='--')


def plotPhaseContour(r, theta, cfl, figName=None):
    plt.figure(figName)
    thetaPlot = theta/np.pi
    levels = np.linspace(-1, 1, num=41)
    r = r**(1./(cfl+1e-16))
    shift = (-np.angle(r)/np.pi - thetaPlot + 1) % 2 - 1
    plt.contourf(thetaPlot, cfl, shift, levels=levels, cmap=_stabContourCMap())
    plt.colorbar()
    plt.xlim(0, 1)
    plt.ylabel('$CFL$')
    plt.xlabel('$\\theta_\\kappa/\\pi$')
