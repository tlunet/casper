"""
Description
-----------

Package containing the fields objects of Casper.
The base object, f0d.Fields, represents several fields saved into a
numpy 1d array. Then, the other fields object are inherited from it
(Fields1D, ...).

Contains
--------
f0d.py : Module
    Contains the base definition of a Fields object
f1d.py : Module
    Contains the definition of one-dimensional fields (Fields1D).
"""
from . import f0d, f1d

Field = f0d.Field
Field1D = f1d.Field1D

__all__ = ['f0d', 'f1d',
           'Field', 'Field1D']
