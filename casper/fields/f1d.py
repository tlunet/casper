# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the base definition of a one dimensional mesh fields object

Classes
-------
Fields1D : Inherited
    Define a one dimensional mesh field
"""
# Standard
import numpy as np
import os as _os

# Casper imports
from casper.fields.f0d import Field
from casper import common as util
from casper.mpi import mpi


class Field1D(Field):
    """
    Inherited class, defines an object representing one or several variable of
    a 1-dimensional field defined on a uniform x-mesh, for a given number of
    times, stored into one contiguous numpy array.

    The general shape of u is (nT, nVar, nX)

    Parameters
    ----------
    nVar: ``int``
        The number variables in the field
    nX: ``int``:
        Number of mesh points in x-direction
    numType: ``str``
        The numerical type for the field ('FLOAT' or 'COMPLEX')


    Attributes
    ----------
    u: ``numpy.array``
        The numpy array containing the values of each variable of the field
    x: ``numpy.array``
        The numpy array containing the values of the x-mesh
    dx: ``float``
        Mesh step size
    xBeg: ``float``
        Left bound of the x-mesh
    xEnd: ``float``
        Right bound of the x-mesh
    xPeriodic: ``bool``
        Wether or not periodic boundary condition is used in x-direction
        (default=True)
    nXGlob: ``int``
        Number of points of the global x-mesh (if no space parallelization,
        nXGlob=field.nX)
    iBegGlob: ``int``
        Index of the first x-mesh point in the global x-mesh (if no space
        parallelization, iBegGlob=0)
    numType: ``str``
        The numerical type of the field values
    nDim: ``int``
        Dimension of the field (nDim=0 for Field objects)
    init: ``module``
        Module containing the initialization function for Field objects

    Properties
    ----------
    nVar: ``int``
        Returns the number of variables of the field
    nX: ``int``
        Returns the number of x-mesh points
    size: ``int``
        Returns the total size of the field
    shape: ``tuple`` of ``int``
        Returns the total size of the field
    flat: ``numpy.array``
        Returns a flattened view of the **u** attribute of the field. Any
        modification of its array values will modify the array values of **u**
    memLoc: ``hex``
        Returns the memory location of the numpy array containing the field
        values
    xMemLoc: ``hex``
        Returns the memory location of the numpy array containing the x-mesh
        values

    Methods
    -------
    clone(): Base
        Returns a similar field object, without copying the field values
    copy(): Base
        Returns a similar field object, with copy of the field values
    dumpToFile(fileName): Local
        Dumps the field into a binary file
    fromFile(fileName): Local Static
        Creates a Fields object from a binary file
    """

    def __init__(self, nX, xBeg=0, xEnd=1, xPeriodic=True, x=None,
                 nVar=1, nT=1, numType='FLOAT64'):

        if util.numGroup(numType) != 0:
            raise ValueError('not implemented for non float fields')

        self.xBeg = xBeg
        self.xEnd = xEnd
        self.xPeriodic = xPeriodic
        self.nXGlob = nX

        if mpi.spaceParallel:
            size = mpi.sSize
            rank = mpi.sRank
            nX0 = nX // size
            nRest = nX - size*nX0
            nXLoc = nX0 + 1*(rank < nRest)
            iBegGlob = rank*nX0 + nRest*(rank >= nRest) + rank*(rank < nRest)
        else:
            nXLoc = nX
            iBegGlob = 0
        self.iBegGlob = iBegGlob

        if isinstance(x, np.ndarray):
            self.x = x
        else:
            if xPeriodic:
                xGlob = np.linspace(xBeg, xEnd, num=nX, endpoint=False)
            else:
                xGlob = np.linspace(xBeg, xEnd, num=nX+2)[1:-1]
            self.x = xGlob[iBegGlob:iBegGlob + nXLoc].copy()
        self.x.flags['WRITEABLE'] = False

        if len(self.x) < 2:
            raise ValueError('cannot have less than two local mesh points')
        self.dx = self.x[1] - self.x[0]

        super().__init__(nVar*nXLoc, nT, numType)
        self.u = self.u.reshape((nT, nVar, nXLoc))

    @property
    def nX(self):
        return self.u.shape[2]

    @property
    def xMemLoc(self):
        return hex(id(self.x))

    def getUGlob(self, rank=0):
        # TODO : adapt to new implementation using time-step storage
        if not mpi.spaceParallel:
            return self.u

        localSize = np.array([self.nX])
        splitSizes = np.empty(mpi.sSize, dtype=np.int)
        mpi.sComm.ALLGATHER(localSize, splitSizes)
        splitSizes *= self.nVar

        if mpi.sRank == rank:
            uGlob = np.empty((self.nVar, self.nXGlob), dtype=self.u.dtype)
            recvData = np.empty(self.nVar*self.nXGlob, dtype=self.u.dtype)
        else:
            uGlob = None
            recvData = None
        sendData = self.flat

        mpi.sComm.GATHERV(sendData, [recvData, splitSizes], rank)

        globIndices = [0] + np.cumsum(splitSizes[:-1]).tolist()
        if mpi.sRank == 0:
            for iLoc, nLoc in zip(globIndices, splitSizes):
                i = iLoc//self.nVar
                n = nLoc//self.nVar
                uGlob[:, i:i+n] = \
                    recvData[iLoc: iLoc+nLoc].reshape((self.nVar, n))

        return uGlob

    def getXGlob(self, rank=0):
        # TODO : adapt to new implementation using time-step storage
        if not mpi.spaceParallel:
            return self.x

        localSize = np.array([self.nX])
        splitSizes = np.empty(mpi.sSize, dtype=np.int)
        mpi.sComm.ALLGATHER(localSize, splitSizes)

        if mpi.sRank == rank:
            xGlob = np.empty(self.nXGlob, dtype=self.x.dtype)
            recvData = np.empty(self.nXGlob, dtype=self.x.dtype)
        else:
            xGlob = None
            recvData = None
        sendData = self.x

        mpi.sComm.GATHERV(sendData, [recvData, splitSizes], rank)

        globIndices = [0] + np.cumsum(splitSizes[:-1]).tolist()
        if mpi.sRank == 0:
            for iLoc, nLoc in zip(globIndices, splitSizes):
                i = iLoc
                n = nLoc
                xGlob[i:i+n] = recvData[iLoc: iLoc+nLoc]

        return xGlob

    def __str__(self):
        s = 'Field1D (nVal={}, nT={}, numType={}'.format(
            self.nVar, self.nT, self.numType,)
        s += ', nX={}, xBeg={}, xEnd={}'.format(
            self.nX, self.xBeg, self.xEnd)
        s += ', xPeriodic={}'.format(self.xPeriodic)
        s += ', xMemLoc={}'.format(hex(id(self.x)))
        if mpi.spaceParallel:
            s += ', nXGlob={}, iBegGlob={}'.format(self.nXGlob, self.iBegGlob)
        s += ') at {}'.format(self.memLoc)
        return s

    def clone(self, nT=None):
        """Clone the field (do not copy the values)"""
        if nT is None:
            nT = self.nT
        return Field1D(self.nXGlob, self.xBeg, self.xEnd, self.xPeriodic,
                       self.x, self.nVar, nT, self.numType)

    def dumpToFile(self, fileName):
        """Local method, dump the field into a binary file

        Parameters
        ----------
        filename: ``str``
            The name of the file (with or without the .f1d extension)
        """
        # TODO : adapt to new implementation using time-step storage
        # Get filename
        util.checkPar(fileName, 'fileName', str, self.dumpToFile)
        fileName = fileName + '.f1d'*(not fileName.endswith('.f1d'))
        # Get description of numerical type
        numSize = self.u.itemsize
        numGroup = util.numGroup(self.numType)
        # General header for all Field objects
        hInfo = np.array([numSize, numGroup, self.nDim, self.nVar],
                         dtype=np.int)
        # Specific header for Field1D objects
        hInfoF1 = np.array([self.nXGlob, self.xPeriodic], dtype=np.int)
        hDataF1 = np.array([self.xBeg, self.xEnd], dtype=self.u.dtype)

        if mpi.sRank == 0:

            with open(fileName, 'wb') as f:
                # Write general header into file
                hInfo.tofile(f)
                # Write specific header into file
                hInfoF1.tofile(f)
                hDataF1.tofile(f)

            if not mpi.spaceParallel:
                # Write coordinates and fields values into file (no MPI)
                with open(fileName, 'ab') as f:
                    self.u.tofile(f)

        if mpi.spaceParallel:
            # Write coordinates and fields values into file (MPI)
            mpiFile = mpi.sComm.FILE_OPEN(fileName, 'WRONLY')
            for iVar in range(self.nVar):
                # Computing offset:
                # - lenght of general header
                # - lenght of specific headers
                # - lenght of previous variable
                # - lenght of variable data writen by previous MPI processes
                offsetField = hInfo.nbytes + hInfoF1.nbytes + hDataF1.nbytes \
                    + iVar*self.nXGlob*numSize + self.iBegGlob*numSize
                # Write fields with given offset
                mpi.sComm.WRITE_AT_ALL(mpiFile, offsetField, self.u[iVar])
            mpi.sComm.BARRIER()
            mpi.sComm.FILE_CLOSE(mpiFile)

    @staticmethod
    def fromFile(fileName):
        """Local static method, create a Fields object from a binary file

        Parameters
        ----------
        fileName : ``str``
            The filename of the binary file, has to contain the .f1d extension
        """
        # TODO : adapt to new implementation using time-step storage
        if not fileName.endswith('.f1d'):
            fileName += '.f1d'
        if not _os.path.isfile(fileName):
            raise ValueError('File does not exists')

        with open(fileName, 'rb') as f:
            # Read general header
            hInfo = np.fromfile(f, dtype=np.int, count=4)
            numSize, numGroup, nDim, nVar = hInfo
            if numSize not in [2, 4, 8, 16]:
                raise ValueError(f"wrong numerical size -- found {numSize}")
            if numGroup not in [0]:
                raise ValueError(f"wrong numerical type -- found {numGroup}")
            if not nDim == 1:
                raise ValueError(f"wrong dimension -- found {nDim}")
            # Read specific headers
            hInfoF1 = np.fromfile(f, dtype=np.int, count=2)
            hDataF1 = np.fromfile(f, dtype=util.dtype(numSize, numGroup),
                                  count=2)
            nX, xPeriodic = hInfoF1
            xBeg, xEnd = hDataF1
            f1d = Field1D(nVar, nX, xBeg, xEnd, xPeriodic,
                          numType=util.numType(numSize, numGroup))

            if not mpi.spaceParallel:
                np.copyto(f1d.flat,
                          np.fromfile(f, dtype=f1d.u.dtype, count=nX*nVar))

        if mpi.spaceParallel:
            mpiFile = mpi.sComm.FILE_OPEN(fileName, 'RDONLY')
            numSize = f1d.getU().itemsize
            for iVar in range(nVar):
                # Computing offset
                offsetField = hInfo.nbytes + hInfoF1.nbytes + hDataF1.nbytes \
                    + iVar*nX*numSize + f1d.iBegGlob*numSize
                # Read fields with given offset
                mpi.sComm.READ_AT_ALL(mpiFile, offsetField, f1d[iVar])
            mpi.sComm.BARRIER()
            mpi.sComm.FILE_CLOSE(mpiFile)
        return f1d

    # TODO : Rework below ...

    def restrict(self, nIP, method='INJ'):
        if self.nXGlob % (nIP+1) != 0:
            raise ValueError(
                'nXGlob ({})'.format(self.nXGlob) +
                ' cannot be devided by nIp+1 = {}'.format(nIP+1))

        xG = self.x[0::nIP+1]
        nXG = xG.size
        nXGlobG = xCoordLocG.size
        iBegGlobG = fieldsF.iBegGlob
        # Parallel : broadcast nXGlobG and iBegGlobG
        if mpi.isSpacePar():
            partNXLoc = np.array([nXLocG])
            sumNXLoc = np.empty_like(partNXLoc)
            mpi.ALLREDUCE_S(partNXLoc, sumNXLoc, 'SUM')
            nXGlobG = int(sumNXLoc[0])
            if mpi.sRank == 0:
                mpi.ISEND_S(np.array([nXLocG]), dest=1, tag=118)
            else:
                sumNXLocPrev = np.empty(1)
                mpi.RECV_S(sumNXLocPrev, source=mpi.sRank-1, tag=118)
                iBegGlobG = int(sumNXLocPrev[0])
                if mpi.sRank < mpi._sSize-1:
                    mpi.ISEND_S(sumNXLocPrev+nXLocG, dest=mpi.sRank+1, tag=118)
        # Create the coarse Fields
        fieldsG = Field1D(fieldsF.nFields, nXGlob=nXGlobG, nXLoc=nXLocG,
                          iBegGlob=iBegGlobG, xCoordLoc=xCoordLocG,
                          xEndGlob=fieldsF.xEndGlob, xBegGlob=fieldsF.xBegGlob,
                          meshType=fieldsF.meshType, fieldsID=fieldsF.fieldsID)
        # Set values of the coarse Fields
        for iField in range(fieldsG.nFields):
            if method == 'INJ':
                fieldsG.setUi(iField, fieldsF.getUi(iField)[0::nIP+1])
            elif method == 'FW':
                pass
            else:
                raise NotImplementedError()
        return fieldsG

    def interpol(fieldsG, nIP, method='O1'):
        if not isinstance(fieldsG, Field1D):
            raise TypeError('fieldsG has to be a Fields1D object')
        nXLocF = fieldsG.nXLoc * (nIP+1)
        nXGlobF = nXLocF
        iBegGlobF = 0
        # valEndLoc = [xEndLoc, uEndLoc[1], uEndLoc[2], ... , uEndLoc[nFields]]
        valEndLoc = np.array([0] + [fieldsG.getUi(i)[0]
                                    for i in range(fieldsG.nFields)])
        # Parallel : broadcast nXGlobF, iBegGlobF and xEndLoc
        if mpi.isSpacePar():
            partNXLoc = np.array([nXLocF])
            sumNXLoc = np.empty_like(partNXLoc)
            mpi.ALLREDUCE_S(partNXLoc, sumNXLoc, 'SUM')
            nXGlobF = int(sumNXLoc[0])
            rankSud = (mpi.sRank-1) % mpi._sSize
            rankNord = (mpi.sRank+1) % mpi._sSize
            if mpi.sRank == 0:
                mpi.ISEND_S(np.array([nXLocF]), dest=rankNord, tag=118)
                mpi.ISEND_S(np.hstack((fieldsG.xCoordLoc[0:1],
                                        [fieldsG.getUi(iField)[0]
                                        for iField in range(fieldsG.nFields)
                                         ])),
                            dest=rankSud, tag=218)
                mpi.RECV_S(valEndLoc, source=rankNord, tag=218)
            else:
                sumNXLocPrev = np.empty(1)
                mpi.RECV_S(sumNXLocPrev, source=rankSud, tag=118)
                iBegGlobF = int(sumNXLocPrev[0])
                if mpi.sRank < mpi._sSize-1:
                    mpi.ISEND_S(sumNXLocPrev+nXLocF, dest=rankNord, tag=118)
                mpi.ISEND_S(np.hstack((fieldsG.xCoordLoc[0:1],
                                        [fieldsG.getUi(iField)[0]
                                         for iField in range(fieldsG.nFields)
                                         ])),
                            dest=rankSud, tag=218)
                mpi.RECV_S(valEndLoc, source=rankNord, tag=218)
        # Get dx on coarse grid
        xEndLoc = valEndLoc[0]
        if mpi.sRank == mpi._sSize-1:
            xEndLoc = fieldsG.xEndGlob
        dxG = np.ediff1d(fieldsG.xCoordLoc)
        dxG = np.hstack((dxG, xEndLoc-fieldsG.xCoordLoc[-1]))
        # Interpolate local coordinates
        xCoordLocF = np.empty(nXLocF)
        np.copyto(xCoordLocF[0::nIP+1], fieldsG.xCoordLoc)
        for ii in range(1, nIP+1):
            np.copyto(xCoordLocF[ii::nIP+1], fieldsG.xCoordLoc)
            xCoordLocF[ii::nIP+1] += float(ii)/(nIP+1)*dxG
        # Create the coarse Fields
        fieldsF = Field1D(fieldsG.nFields, nXGlob=nXGlobF, nXLoc=nXLocF,
                          iBegGlob=iBegGlobF, xCoordLoc=xCoordLocF,
                          xEndGlob=fieldsG.xEndGlob, xBegGlob=fieldsG.xBegGlob,
                          meshType=fieldsG.meshType, fieldsID=fieldsG.fieldsID)
        # Define interpolation functions
        if method == 'O1':
            # xd vector = 0 for i \ xf[i]=xg[i]
            #             (xf[i]-xgLeft)/dXG
            xd = np.empty(nXLocF)
            np.copyto(xd[0::nIP+1], np.float64(0))
            for ii in range(1, nIP+1):
                np.copyto(xd[ii::nIP+1], xCoordLocF[ii::nIP+1])
                xd[ii::nIP+1] -= fieldsG.xCoordLoc
                xd[ii::nIP+1] /= dxG
            # Temporary vector 1-xd
            oneMinusXd = np.ones(xd.shape)
            oneMinusXd -= xd

            def interpolate(iField):
                uG = fieldsG.getUi(iField)
                uG1 = np.hstack((uG[1:], valEndLoc[iField+1]))
                uF = np.empty(nXLocF)
                np.copyto(uF[0::nIP+1], uG)
                for ii in range(1, nIP+1):
                    np.copyto(uF[ii::nIP+1], uG)
                    uF[ii::nIP+1] *= oneMinusXd[ii::nIP+1]
                    uF[ii::nIP+1] += uG1*xd[ii::nIP+1]
                return uF
        elif method == 'O7':
            def interpolate(iField):
                uG = fieldsG.getUi(iField)
                uGB = np.hstack((uG[-3:], uG, uG[:4]))
                uF = np.zeros(nXLocF)
                # Same points for coarse and fine
                np.copyto(uF[0::2], uG)
                # Interpolated points
                uF[1::2] += 1225*uGB[3:-4]
                uF[1::2] += 1225*uGB[4:-3]
                uF[1::2] -= 245*uGB[2:-5]
                uF[1::2] -= 245*uGB[5:-2]
                uF[1::2] += 49*uGB[1:-6]
                uF[1::2] += 49*uGB[6:-1]
                uF[1::2] -= 5*uGB[:-7]
                uF[1::2] -= 5*uGB[7:]
                uF[1::2] /= 2048
                return uF
        elif method == 'O5':
            def interpolate(iField):
                uG = fieldsG.getUi(iField)
                uGB = np.hstack((uG[-2:], uG, uG[:3]))
                uF = np.zeros(nXLocF)
                # Same points for coarse and fine
                np.copyto(uF[0::2], uG)
                # Interpolated points
                uF[1::2] += 150*uGB[2:-3]
                uF[1::2] += 150*uGB[3:-2]
                uF[1::2] -= 25*uGB[1:-4]
                uF[1::2] -= 25*uGB[4:-1]
                uF[1::2] += 3*uGB[:-5]
                uF[1::2] += 3*uGB[5:]
                uF[1::2] /= 256
                return uF
        elif method == 'O3':
            def interpolate(iField):
                uG = fieldsG.getUi(iField)
                uGB = np.hstack((uG[-1:], uG, uG[:2]))
                uF = np.zeros(nXLocF)
                # Same points for coarse and fine
                np.copyto(uF[0::2], uG)
                # Interpolated points
                uF[1::2] += 9*uGB[1:-2]
                uF[1::2] += 9*uGB[2:-1]
                uF[1::2] -= uGB[:-3]
                uF[1::2] -= uGB[3:]
                uF[1::2] /= 16
                return uF

        else:
            raise ValueError('Interpolation method {} '.format(method) +
                             'does not exist for Fields1D')
        # Set values of the fine Fields
        for iField in range(fieldsF.nFields):
            fieldsF.setUi(iField, interpolate(iField))
        return fieldsF
