# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the base definition of a field object

Classes
-------
Field : base
    Define a basic field

"""
import numpy as np
import os as os

from casper import common as util


class Field(object):
    """
    Base class, defines an object representing one or several variable of
    a 0-dimensional field, for a given number of times,
    stored into one contiguous numpy array u.

    The general shape of u is (nT, nStep, nVar)

    Parameters
    ----------
    nVar : ``int``
        The number variables in the field
    nT : ``int``, optional
        The number of times on which the field is stored (default=1)
    numType : ``str``, optional
        The numerical type for the field ('FLOAT' or 'COMPLEX')

    Attributes
    ----------
    u : ``numpy.array``
        The numpy array containing the values of each variable of the field
    numType : ``str``
        The numerical type of the field values
    nDim : ``int``
        Dimension of the field (nDim=0 for Field objects)

    Properties
    ----------
    nT, nVar, nDim, size, shape, flat, memLoc, dtype

    Methods
    -------
    clone() : Base
        Returns a similar field object, without copying the field values
    copy() : Base
        Returns a similar field object, with copy of the field values
    dumpToFile(fileName) : Local
        Dumps the field into a binary file
    fromFile(fileName) : Local Static
        Creates a Fields object from a binary file
    """

    def __init__(self, nVar, nT=1, numType='FLOAT64'):
        self.numType = numType
        try:
            self.u = np.empty((nT, nVar), dtype=util.dtype(self.numType))
            self.u[:] = 0
        except MemoryError:
            raise ValueError('not enough memory (see error above)')

    @property
    def nT(self):
        """int: number of times when the field is stored"""
        return self.u.shape[0]

    @property
    def nVar(self):
        """int: number of variables of the field"""
        return self.u.shape[1]

    @property
    def nDim(self):
        """int: dimension of the field, for one variable and one time step"""
        return len(self.u.shape[2:])

    @property
    def size(self):
        """int: total size of the field for ONE time step"""
        return np.prod((self.shape)[1:])

    @property
    def shape(self):
        """tuple: shape of the fields values"""
        return self.u.shape

    @property
    def flat(self):
        """np.ndarray: flattened view of the **u** attribute of the field. Any
        modification of its array values will modify the array values of **u**.
        """
        return self.u.ravel()

    @property
    def memLoc(self):
        """hex: memory location of the numpy array containing the field values
        """
        return hex(id(self.u))

    @property
    def dtype(self):
        """np.dtype: decimal type of the field values"""
        return self.u.dtype

    def extractVars(self, u, iVar=None):
        """
        Extract each variables from a flat vector containing field values
        for ONE time step.

        Parameters
        ----------
        u : ``np.1darray``
            The array where to extract the variables from
        iVar : ``list`` of ``int``
            Indexes of the variables that should be extracted

        Returns
        -------
        arrays : generator of ``np.1darray``
            A generator for the view on each extracted variables
        """
        if iVar is None:
            iVar = range(self.nVar)
        u = np.reshape(u, self.shape[1:])
        arrays = (u[i] for i in iVar)
        return arrays

    def clone(self, nT=None):
        """Base method, returns a similar field object, without copying the
        field values"""
        if nT is None:
            nT = self.nT
        return Field(self.nVar, nT, self.numType)

    def copy(self):
        """Base method, returns a similar field object, with copy of the
        field values"""
        copy = self.clone()
        np.copyto(copy.u, self.u)
        return copy

    def __str__(self):
        return 'Field (nVal={}, nT={}, numType={}) at {}'.format(
            self.nVar, self.nT, self.numType, self.memLoc)

    def __repr__(self):
        return self.__str__()

    def __len__(self):
        return self.u.__len__(self)

    def __iter__(self):
        return self.u.__iter__()

    def __getitem__(self, index):
        return self.u.__getitem__(index)

    def __setitem__(self, index, value):
        self.u.__setitem__(index, value)

    def dumpToFile(self, fileName):
        """Local method, dump the field into a binary file

        Parameters
        ----------
        fileName : ``str``
            The name of the file (with or without the .f0d extension)
        """

        # Get filename
        util.checkPar(fileName, 'fileName', str, self.dumpToFile)
        fileName = fileName + '.f0d'*(not fileName.endswith('.f0d'))
        # Get description of numerical type
        numSize = self.u.itemsize
        numGroup = util.numGroup(self.numType)
        # General header for all fields objects
        hInfo = np.array([numSize, numGroup, self.nDim, self.nVar, self.nT],
                         dtype=np.int)

        # Write data into file
        with open(fileName, 'wb') as f:
            # Write general header into file
            hInfo.tofile(f)
            # Write field values into file (no MPI)
            self.u.tofile(f)

    @staticmethod
    def fromFile(fileName):
        """Local static method, create a Fields object from a binary file

        Parameters
        ----------
        fileName: ``str``
            The filename of the binary file, has to contain the .f0d extension
        varIDs: sequence of ``int`` or ``str``
            Name of each variables
        """
        if not fileName.endswith('.f0d'):
            fileName += '.f0d'
        if not os.path.isfile(fileName):
            raise ValueError('File does not exists')

        with open(fileName, 'rb') as f:
            # Read general header
            hInfo = np.fromfile(f, dtype=np.int, count=5)
            numSize, numGroup, nDim, nVar, nT = hInfo
            if numSize not in [2, 4, 8, 16, 32]:
                raise ValueError(f'wrong numerical size, found {numSize}')
            if numGroup not in [0, 1]:
                raise ValueError(f'wrong numerical group, found {numGroup}')
            if not nDim == 0:
                raise ValueError(f'wrong dimension, found {nDim}')
            # Create field object
            numType = util.numType(numSize, numGroup)
            f0d = Field(nVar, nT, numType)
            # Set field data from file values
            dtype = util.dtype(numSize, numGroup)
            f0d.setU(np.fromfile(f, dtype, count=f0d.nVar))

        return f0d
