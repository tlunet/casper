#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 17 11:28:26 2019

@author: lunet
"""
import numpy as np
import time as time

from casper import common as util
from casper.fields.f0d import Field
from casper.phi.base import Phi


class SeqTimeSolver(object):

    UNIFORM = False

    def __init__(self, f0, phi, tEnd, nStep, t0=0,
                 storeSteps=False, storePerfo=False):

        self.f0 = util.checkPar(f0, 'f0', Field)
        if f0.nT != 1:
            raise ValueError('initial field should store only one step')
        self.phi = util.checkPar(phi, 'phi', Phi)

        nStep = int(nStep)
        self.tEnd = tEnd
        t = np.linspace(t0, tEnd, num=nStep+1)
        self.UNIFORM = True

        self.tData = np.empty((nStep, 2), dtype=float)
        self.tData[:, 0] = t[:-1]
        self.tData[:, 1] = np.ediff1d(t)

        # Instanciate Field object containing the solution
        if storeSteps:
            self.field = f0.clone(nT=nStep+1)
            self._updateN = self._updateN_STORAGE
        else:
            self.field = f0.clone(nT=2)

        # Copy initial solution into solver field
        self.field[0] = self.f0[:]

        # Eventually store performance data into a dictionnary
        if storePerfo:
            perfo = {}
            self.rhs.storePerfo(perfo)
            self.phi.storePerfo(perfo)

            def wrapper(run):
                @util.wraps(run)
                def wrapped():
                    """storePerfo"""
                    clockBeg = time.clock()
                    run()
                    clockEnd = time.clock()
                    perfo['tComp'] = clockEnd - clockBeg
                    perfo['tCompPhi'] -= perfo['tCompRHS']
                    perfo['tRHSEval'] = perfo['tCompRHS']/perfo['nEvalRHS']
                    perfo['tPhiEval'] = perfo['tCompPhi']/self.nT
                return wrapped
            self.run = wrapper(self.run)

            self.perfo = perfo

    @property
    def nStep(self):
        return self.tData.shape[0]

    @property
    def dt(self):
        return self.tData[0, 1] if self.UNIFORM else self.tData[:, 1]

    @property
    def times(self):
        return self.tData[:, 0]

    def run(self):

        # Initialize time step index
        self.n = 0

        for t, dt in self.tData:

            # Evaluate the phi operator
            self.phi.eval(
                self.field[self.n].ravel(),  # u_(t_n)
                t, dt,
                self.field[self.n+1].ravel())  # u_(t_n + dt)

            # Update the step index
            self._updateN()

    def _updateN(self):
        self.n = -1 if self.n == 0 else 0

    def _updateN_STORAGE(self):
        self.n += 1
