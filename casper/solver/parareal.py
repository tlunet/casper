# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the definition of the parareal algorithm

Classes
-------
Parareal : Inherited (PitAlgo)
    define the parareal algorithm
"""
from casper import common as util
from casper.maths import krylov as _kry
from casper.mpi import mpi
import numpy as _np
import scipy as _sp


class Parareal(object):
    """
    Define the parareal algorithm (DOCTODO)

    Methods
    -------
    TODO
    """

    VERBOSE = False

    def __init__(self, f0, timeDec, nIter,
                 gSolverClass, gSolverSettings, fSolverClass, fSolverSettings,
                 gToF=None, fToG=None, **kwargs):

        func = self.__init__

        # MPI settings
        if mpi.timeParallel:
            nChunks = mpi.tSize
            useMPI = True
        else:
            nChunks = util.getInitPar('nChunks', None, int, func, **kwargs)
            useMPI = False

        # Time domain decomposition
        if timeDec == 'UNIF':
            tBegGlob = util.getInitPar('tBeg', 0., float, func, **kwargs)
            tEndGlob = util.getInitPar('tEnd', None, float, func, **kwargs)
            tabTimes = \
                _np.linspace(tBegGlob, tEndGlob, nChunks+1, dtype=float)
        elif timeDec == 'GIVEN':
            tabTimes = util.getInitPar('tabTimes', None, list, func, **kwargs)
            if len(tabTimes) != nChunks + 1:
                raise ValueError('Length of tabTimes is not correct')
            tabTimes = _np.array(tabTimes)
        else:
            raise ValueError('{} time decomposition not yet implemented'
                             .format(timeDec))
        self.tabTimes = tabTimes
        self.dtRestart = tabTimes[-1]-tabTimes[0]

        # Coarse and fine solver initialization
        if gSolverSettings.get('stepping') == 'GIVEN' or \
           fSolverSettings.get('stepping') == 'GIVEN':
            raise NotImplementedError(
                'Parareal not adapted yet to GIVEN solver time stepping')
        self.gSolverSettings = gSolverSettings.copy()
        self.fSolverSettings = fSolverSettings.copy()
        self.gSolver = gSolverClass(**gSolverSettings)
        self.fSolver = fSolverClass(**fSolverSettings)

        # Transfers operators
        if fToG is not None:
            fToG.__doc__ = self.fToG.__doc__
            self.fToG = fToG
        if gToF is not None:
            gToF.__doc__ = self.gToF.__doc__
            self.gToF = gToF

        # Store other arguments
        self.f0 = f0.copy()
        self.nIter = nIter

        # Relaxation
        self.relax = util.getInitPar('relax', 'NONE', str, func, **kwargs)

        if self.relax == 'SCALAR':
            self.relaxCoeff = kwargs.get('relaxCoeff')
        if self.relax == 'MATRIX':
            self.relaxMatrix = kwargs.get('relaxMatrix')
        if self.relax not in ['NONE', 'SCALAR', 'KRYLOV', 'MATRIX']:
            util.infoMsg('Warning, unknown relaxation of type {}'
                         .format(self.relax) + ', will use NONE type')
            self.relax = 'NONE'

        # KEMS or not ...
        self.useKEMS = util.getInitPar('useKEMS', False, bool, func, **kwargs)
        if self.useKEMS:

            # Get KEMS parameters
            self.nK = util.getInitPar(
                'nK', None, int, func, **kwargs)
            self.expAlgo = util.getInitPar(
                'expAlgo', 'EXPM', str, func, **kwargs)

            # Initialize variables for KEMS computation
            nP = f0.getU().size
            dataType = f0.getU().dtype
            self.mV = _np.zeros((nP, self.nK+1), dtype=dataType)
            self.mH = _np.zeros((self.nK+1, self.nK), dtype=dataType)
            self.vW = _np.zeros((nP), dtype=dataType)
            self.uErr = _np.zeros((nP), dtype=dataType)

            # Initialize exponential algorithm
            if self.expAlgo == 'EXPM':
                def _expAlgo(mH, dt):
                    mH *= dt
                    return _sp.linalg.expm(mH)
            elif self.expAlgo == 'DIAG':
                def _expAlgo(mH, dt):
                    d, mE = _np.linalg.eig(mH)
                    d *= dt
                    d = _np.diag(_np.exp(d))
                    return mE.dot(d.dot(_np.linalg.inv(mE))).real
            else:
                raise ValueError('Wrong value ({}) for expAlgo'
                                 .format(self.expAlgo))

            # Instantiate class method
            _expAlgo.__doc__ = self._expAlgo.__doc__
            self._expAlgo = _expAlgo

        # Run the appropriate initialization function
        if useMPI:
            # Real Parareal algorithm (MPI implementation)
            initFunc = self._initMPI
            self.mode = 'MPI'
            self.tRankRoot = mpi.tSize-1
        else:
            # Simulated Parareal algorithm (no MPI implementation)
            initFunc = self._initSIM
            self.mode = 'SIM'

        self.initAlgo, self.iterAlgo, self.getFields, self.restart = \
            initFunc(nChunks)

    # -----------------------------------------------------------------------
    # Generic fixed methods
    # -----------------------------------------------------------------------
    def runAlgo(self):
        """Local method, execute the Parareal algorithm by doing the
        initialization stage and all iterations.
        """
        self.initAlgo()
        if self.VERBOSE:
            # Print a nice message
            util.infoMsg("Parareal initialization done : {} time iteration"
                         .format(self.gSolver.nIter))
        for kIter in range(self.nIter):
            self.iterAlgo(kIter)
            util.infoMsg("Parareal iteration {}/{} done"
                         .format(kIter+1, self.nIter))

    def pararealCorrection(self, fk1n1, fknF, fk1nG, fknG):

        # Get the pointers to numpy arrays
        uk1n1 = fk1n1.getU()
        uk1nG = fk1nG.getU()
        uknG = fknG.getU()
        uknF = fknF.getU()

        # Add coarse solution correction
        _np.copyto(uk1n1, uk1nG)
        uk1n1 -= uknG

        # Eventually apply relaxation
        if self.relax == 'SCALAR':
            util.infoMsg("Applying Scalar Relaxation")
            uk1n1 *= self.relaxCoeff
        elif self.relax == 'KRYLOV':
            util.infoMsg("Applying Krylov Relaxation")
            n = uknG.size
            h = _np.dot(uknG.reshape((1, n)), uknF.reshape((n, 1)))
            h /= _np.linalg.norm(uknG)**4
            n = uknG.size
            v1 = uknG.reshape((n, 1))
            Ar = _np.dot(v1, v1.T)
            Ar *= h
            _np.copyto(uk1n1, _np.dot(Ar, uk1n1))
        elif self.relax == 'MATRIX':
            util.infoMsg("Applying Matrix Relaxation")
            _np.copyto(uk1n1, _np.dot(self.relaxMatrix, uk1n1))

        # Add fine solution
        uk1n1 += uknF

    def KEMSCorrection(self, fk1n1, fknF, t0, tEnd, fk1n, fkn):
        # Get the pointers to numpy arrays
        uk1n1 = fk1n1.getU()
        uknF = fknF.getU()
        uk1n = fk1n.getU()
        ukn = fkn.getU()
        uErr = self.uErr

        # Error vector
        _np.copyto(uErr, uk1n)
        uErr -= ukn
        normErr = _np.linalg.norm(uErr)

        # Perform KEMS error propagation (only if error is not to small !)
        if normErr > 1e-9:

            # Compute Krylov basis
            def evalFunc(u, uEval):
                self.fSolver.evalSpaceOp(u, t0, uEval)

            normU = _kry.arnoldi(
                uErr, evalFunc, self.nK, self.mV, self.mH, self.vW)

            # Exponential propagation
            expH = self._expAlgo(self.mH[:-1, :], tEnd-t0)
            prodVH = self.mV[:, :-1].dot(expH)

            # Add KEMS correction
            _np.copyto(uk1n1, prodVH[:, 0])
            uk1n1 *= normU

            if False:
                A = self.fSolver.spaceOp.computeJacobian(uErr, t0)
                _np.copyto(uk1n1, _sp.linalg.expm((tEnd-t0)*A).dot(uErr))

            # Add fine solution
            uk1n1 += uknF

        else:
            # Just fine solution
            _np.copyto(uk1n1, uknF)

    def _expAlgo(mH, dt):
        raise NotImplementedError()

    def runSolver(self, level, f0, t0, tEnd, fEnd):
        if level == 'G':
            # Initialize the coarse solver
            self.gSolverSettings['f0'] = self.fToG(f0)
            self.gSolverSettings['tBeg'] = t0
            self.gSolverSettings['tEnd'] = tEnd
            self.gSolver.initialize(**self.gSolverSettings)
            # Run coarse simulation
            self.gSolver.solve()
            if self.VERBOSE:
                util.infoMsg("Coarse simulation done : {} time iterations"
                             .format(self.gSolver.nIter))
            # Store results
            _np.copyto(fEnd.getU(),
                       self.gToF(self.gSolver.getCurrentFields()).getU())
        elif level == 'F':
            # Initialize the fine solver
            self.fSolverSettings['f0'] = f0
            self.fSolverSettings['tBeg'] = t0
            self.fSolverSettings['tEnd'] = tEnd
            self.fSolver.initialize(**self.fSolverSettings)
            # Run fine simulation
            self.fSolver.solve()
            if self.VERBOSE:
                util.infoMsg("Fine simulation done : {} time iterations"
                             .format(self.fSolver.nIter))
            # Store results
            _np.copyto(fEnd.getU(),
                       self.fSolver.getCurrentFields().getU())

    # -----------------------------------------------------------------------
    # SIMULATED implementation
    # -----------------------------------------------------------------------
    def _initSIM(self, nChunks):

        # Chunk times (defined for compatibility with _initMPI)
        self.tBegLoc = self.tabTimes[0]
        self.tEndLoc = self.tabTimes[-1]

        # Table to store calculated fields
        self.tabFields = [[self.f0]*(self.nIter+1)] + \
            [[self.f0.clone() for i in range(self.nIter+1)]
             for j in range(nChunks)]
        self.tabFieldsG = [[self.f0.clone() for i in range(self.nIter)]
                           for j in range(nChunks)]
        self.tabFieldsF = [[self.f0.clone() for i in range(self.nIter)]
                           for j in range(nChunks)]

        def initAlgo():
            # Coarse initialization on each chunk
            for iChunk in range(nChunks):
                self.runSolver(
                    'G', self.tabFields[iChunk][0],
                    self.tabTimes[iChunk], self.tabTimes[iChunk+1],
                    self.tabFields[iChunk+1][0])

        def iterAlgo(k):
            # Fine propagation on each chunk
            for iChunk in range(nChunks):
                self.runSolver(
                    'F', self.tabFields[iChunk][k],
                    self.tabTimes[iChunk], self.tabTimes[iChunk+1],
                    self.tabFieldsF[iChunk][k])
            # Perform iteration on each chunk
            for iChunk in range(nChunks):
                if self.useKEMS:
                    # KEMS correction
                    self.KEMSCorrection(
                        # U^{k+1}_{n+1}
                        self.tabFields[iChunk+1][k+1],
                        # Fine solution
                        self.tabFieldsF[iChunk][k],
                        # Initial & final time
                        self.tabTimes[iChunk], self.tabTimes[iChunk+1],
                        # U^{k+1}_{n} & U^{k}_{n}
                        self.tabFields[iChunk][k+1],
                        self.tabFields[iChunk][k])
                else:
                    # Coarse propagation
                    self.runSolver(
                        'G', self.tabFields[iChunk][k+1],
                        self.tabTimes[iChunk], self.tabTimes[iChunk+1],
                        self.tabFieldsG[iChunk][k])
                    # Parareal iteration
                    self.pararealCorrection(
                        self.tabFields[iChunk+1][k+1],
                        self.tabFieldsF[iChunk][k], self.tabFieldsG[iChunk][k],
                        self.tabFields[iChunk+1][0] if k == 0 else
                        self.tabFieldsG[iChunk][k-1])

        def getFields(chunk='LAST', iteration='LAST', stage='ITER'):
            # Get data to extract
            tab = self.tabFields if stage == 'ITER' else \
                self.tabFieldsF if stage == 'FINE' else \
                self.tabFieldsG if stage == 'COARSE' else None
            tab = _np.array(tab)
            nIter = tab.shape[1]
            # Get chunk index(es)
            chunkIndex = -1 if chunk == 'LAST' else \
                range(1, nChunks+1) if chunk == 'ALL' else \
                int(chunk)+1 if isinstance(chunk, int) else None
            # Get iteration index(es)
            iterIndex = -1 if iteration == 'LAST' else \
                range(nIter) if iteration == 'ALL' else \
                int(iteration) if isinstance(iteration, int) else None
            # Return result
            if iteration == 'ALL' and chunk == 'ALL':
                return tab[_np.ix_(chunkIndex, iterIndex)]
            else:
                return tab[chunkIndex, iterIndex]

        def restart():
            # Set new interval times
            self.tabTimes += self.dtRestart
            self.tBegLoc = self.tabTimes[0]
            self.tEndLoc = self.tabTimes[-1]
            # Set new initial field
            _np.copyto(self.tabFields[0][0].getU(),
                       self.tabFields[-1][-1].getU())

        # Documentation stuff
        initAlgo.__doc__ = self.initAlgo.__doc__
        iterAlgo.__doc__ = self.iterAlgo.__doc__
        getFields.__doc__ = self.getFields.__doc__
        restart.__doc__ = self.restart.__doc__

        return initAlgo, iterAlgo, getFields, restart

    # -----------------------------------------------------------------------
    # MPI implementation
    # -----------------------------------------------------------------------
    def _initMPI(self, nChunks):

        tRank = mpi.tRank

        # Chunk times
        self.tBegLoc = self.tabTimes[tRank]
        self.tEndLoc = self.tabTimes[tRank+1]

        # Table to store calculated fields
        self.tabPrevF = \
            [self.f0]*(self.nIter+1) if tRank == 0 else \
            [self.f0.clone() for i in range(self.nIter+1)]
        self.tabNextFG = [self.f0.clone() for i in range(self.nIter)]
        self.tabNextFF = [self.f0.clone() for i in range(self.nIter)]
        self.tabNextF = [self.f0.clone() for i in range(self.nIter+1)]
        hasRecvPrevF = [False]*(self.nIter+1)
        hasSendNextF = [False]*(self.nIter+1)

        def MPI_receivePrevF(k):
            """Local method, receive the k^th element of **tabPrevF** from the
            previous rank
            """
            # Check if this was not already done
            if hasRecvPrevF[k]:
                raise ValueError('tRank {} want to receive previous Fields {}'
                                 .format(tRank, k) +
                                 ' but it was already received')
                mpi.gComm.ABORT()
            # Communication (or not)
            if tRank == 0:
                # Nothing to do, previous fields already set at initialization
                pass
            else:
                # Receive a fields trough MPI time communication
                mpi.tComm.RECV(self.tabPrevF[k].getU(), source=tRank-1, tag=11)
            # Remember that it was received
            hasRecvPrevF[k] = True

        def MPI_sendNextF(k):
            """Local method, send the k^th element of **tabNextF** to the
            next time chunk
            """
            # Check if this was not already done
            if hasSendNextF[k]:
                raise ValueError('tRank {} want to send next Fields {}'
                                 .format(tRank, k) +
                                 ' but it was already sent')
                mpi.gComm.ABORT()
            # Communication (or not)
            if tRank == (nChunks-1):   # Last time rank
                # Nothing to do, nobody want anything from this poor rank
                pass
            else:
                # Send a fields trough MPI time communication
                mpi.tComm.ISEND(self.tabNextF[k].getU(), dest=tRank+1, tag=11)

        def initAlgo():
            # Receive the coarse initialization from previous chunk
            MPI_receivePrevF(0)
            # Run coarse solver
            self.runSolver('G', self.tabPrevF[0], self.tBegLoc, self.tEndLoc,
                           self.tabNextF[0])
            # Send to next chunk
            MPI_sendNextF(0)

        def iterAlgo(k):
            # Fine propagation
            self.runSolver('F', self.tabPrevF[k], self.tBegLoc, self.tEndLoc,
                           self.tabNextFF[k])

            # Receive current iteration fields from previous chunk
            MPI_receivePrevF(k+1)
            if self.useKEMS:
                # Performs KEMS correction
                self.KEMSCorrection(
                    # U^{k+1}_{n+1}
                    self.tabNextF[k+1],
                    # Fine solution
                    self.tabNextFF[k],
                    # Initial & final time
                    self.tBegLoc, self.tEndLoc,
                    # U^{k+1}_{n} & U^{k}_{n}
                    self.tabPrevF[k+1],
                    self.tabPrevF[k])
            else:
                # Performs Parareal correction
                # Coarse propagation
                self.runSolver('G', self.tabPrevF[k+1], self.tBegLoc,
                               self.tEndLoc, self.tabNextFG[k])
                # Parareal Iteration
                self.pararealCorrection(
                    self.tabNextF[k+1], self.tabNextFF[k], self.tabNextFG[k],
                    self.tabNextF[0] if k == 0 else self.tabNextFG[k-1])
            # Communicate because that's the essence of life
            MPI_sendNextF(k+1)

        def getFields(chunk='LAST', iteration='LAST', stage='ITER'):

            # Get data to extract
            if stage == 'ITER':
                tab = self.tabNextF
            elif stage == 'FINE':
                tab = self.tabNextFF
            elif stage == 'COARSE':
                tab = self.tabNextFG
            else:
                errMsg = (
                    "Cannot get stage={}".format(stage) +
                    ", must be in ['ITER', 'FINE','COARSE']")
                raise ValueError(errMsg)
            tab = _np.array(tab)
            nIter = tab.shape[0]

            # Get iteration index(es)
            if iteration == 'LAST':
                iterIndex = slice(-1, None)
            elif iteration == 'ALL':
                iterIndex = range(nIter)
            elif isinstance(iteration, int):
                iterIndex = slice(iteration, iteration+1)
            else:
                errMsg = (
                    "Cannot get iteration={}".format(iteration) +
                    ", must be in ['ALL', 'LAST', 0, 1, ..., nIter]")
                raise ValueError(errMsg)
            iterIndex, localData = zip(
                *[(e[0], e[1]) for e in enumerate(tab[iterIndex])])

            # Get chunk index(es)
            if chunk == 'LAST':
                chunkIndex = slice(-1, None)
            elif isinstance(chunk, int):
                chunkIndex = chunk
            elif chunk != 'ALL':
                errMsg = (
                    "Cannot get chunk={}".format(chunk) +
                    ", must be in ['ALL', 'LAST', 0, 1, ..., nChunk]")
                raise ValueError(errMsg)

            if chunk == 'ALL':
                # Data gathering from all ranks to tRankRoot
                sizeU = self.f0.getU().size
                iterIndex = _np.array(iterIndex).ravel()
                nIterPP = iterIndex.size
                if tRank == self.tRankRoot:
                    allFields = [[self.f0.clone() for j in range(nIterPP)]
                                 for i in range(nChunks)]
                    allU = _np.empty(nChunks*(nIterPP)*sizeU,
                                     dtype=self.f0.getU().dtype)
                else:
                    # Other process than tRankRoot spare memory
                    allFields = None
                    allU = None
                uPar = _np.hstack([f.getU() for f in localData])
                mpi.tComm.BARRIER()
                mpi.tComm.GATHER(uPar, allU, self.tRankRoot)
                if tRank == self.tRankRoot:
                    for iChunk in range(nChunks):
                        for iIter in iterIndex:
                            iUBeg = iChunk*(nIterPP)*sizeU + iIter*sizeU
                            iUEnd = iChunk*(nIterPP)*sizeU + (iIter+1)*sizeU
                            allFields[iChunk][iIter].setU(allU[iUBeg:iUEnd])
                del allU
                allFields = _np.array(allFields)
                if nIterPP == 1 and tRank == self.tRankRoot:
                    return allFields[:, 0]
                else:
                    return allFields

            elif chunkIndex % nChunks == self.tRankRoot:
                # Data is local for tRankRoot
                return localData

            else:
                # Data transmission from one rank to tRankRoot
                sizeU = self.f0.getU().size
                iterIndex = _np.array([iterIndex]).ravel()
                nIterPP = iterIndex.size
                if tRank == self.tRankRoot:
                    tabFields = [self.f0.clone() for j in range(nIterPP)]
                    tabU = _np.empty((nIterPP)*sizeU,
                                     dtype=self.f0.getU().dtype)
                    mpi.tComm.RECV(tabU, source=chunkIndex, tag=111)
                elif tRank == chunkIndex:
                    tabFields = None
                    tabU = _np.hstack([f.getU() for f in localData])
                    mpi.tComm.ISEND(tabU, dest=self.tRankRoot, tag=111)
                if tRank == self.tRankRoot:
                    for iIter in range(nIterPP):
                        iUBeg = iIter*sizeU
                        iUEnd = (iIter+1)*sizeU
                        tabFields[iIter].setU(tabU[iUBeg:iUEnd])
                del tabU
                return tabFields[0] if nIterPP == 1 else tabFields

        def restart():
            # Set new interval times
            self.tabTimes += self.dtRestart
            self.tBegLoc = self.tabTimes[tRank]
            self.tEndLoc = self.tabTimes[tRank+1]
            # Set new initial field
            if mpi.tRank == nChunks-1:
                mpi.tComm.ISEND(self.getFields().getU(), dest=0, tag=7)
            if mpi.tRank == 0:
                mpi.tComm.RECV(self.f0.getU(), source=nChunks-1, tag=7)
                self.tabPrevF = [self.f0]*(self.nIter+1)

        # Documentation stuff
        initAlgo.__doc__ = self.initAlgo.__doc__
        iterAlgo.__doc__ = self.iterAlgo.__doc__
        getFields.__doc__ = self.getFields.__doc__
        restart.__doc__ = self.restart.__doc__

        return initAlgo, iterAlgo, getFields, restart

    # -----------------------------------------------------------------------
    # Generic variables methods
    # -----------------------------------------------------------------------
    def gToF(self, fCoarse):
        """Local method, transform the coarse fields into the fine ones

        Parameters
        ----------
        fCoarse: ``Fields``
            Original coarse fields

        Returns
        -------
        fFine: ``Fields``
            Interpolated fine fields
        """
        return fCoarse.copy()

    def fToG(self, fFine):
        """Local method, transform the fine fields into the coarse ones

        Parameters
        ----------
        fFine: ``Fields``
            Original fine fields

        Returns
        -------
        fCoarse: ``Fields``
            Restrained coarse fields
        """
        return fFine.copy()

    def initAlgo(self):
        """Local method, initialize the parareal algorithm,
        implemented inside an _init[...] method of the Parareal object.
        """
        raise NotImplementedError()

    def iterAlgo(self, k):
        """Local method, make one parareal iteration (requires the
        initialization already done), implemented inside an
        _init[...] method of the Parareal object.

        Parameters
        ----------
        k : ``int``
            The iteration index (0 is for the first one, etc ...)
        """
        raise NotImplementedError()

    def getFields(self, chunk='LAST', iteration='LAST', stage='ITER'):
        """ DOC TODO """
        raise NotImplementedError()

    def applyRelaxation(self, u):
        """ DOC TODO """
        raise NotImplementedError()

    def restart(self):
        """Local, set a restart of the Parareal algorithm"""
        raise NotImplementedError()

    # -----------------------------------------------------------------------
    # Other stuff
    # -----------------------------------------------------------------------
    def theoricalSpeedup(self, nIter, rC, lam=None, npt=None):
        """Local method, compute the theorical speedup of the parallel
        algorithm

        Parameters
        ----------
        nIter : ``int``
            The number of iteration
        rC : ``float``
            The communication ratio between coarse evaluation cost and
            communication cost, that is :math:`rC = Cost(oneComm)/
            Cost(Gsolver)`
        lam : ``float``
            If specified, the lambda coefficient, that is the ratio between
            the fine solver space operator evaluation number and the coarse
            solver one. :math:`\\lambda = Cost(Gsolver)/Cost(Fsolver)`.
            If not specified, calculated with the Parareal
            object settings (method getTheoricalNEval() of TimeSolver)

        Returns
        -------
        speedup : `float`
            The theorical speedup for parareal
        """
        if lam is None:
            lam = float(self.gSolver.getTheoricalNEval()) / \
                float(self.fSolver.getTheoricalNEval())
        if npt is None:
            npt = float(mpi._tSize)
        speedup = npt / ((npt-1)*lam + nIter)
        return speedup
