# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing definitions of space operators for simple
Ordinary Differential Equation

Classes
-------
PolyUT(SpaceOp) : Inherited
    define a polynomial of u and t
"""
import numpy as _np
import scipy.sparse as sps

import casper.common as util
from casper.rhs.base import RHS
from casper.fields import Field


class Linear(RHS):
    """Define a RHS linear with respect to u,

    .. math::
        f(u,t) = Au + b(t),

    with :math:`A` a matrix of fixed size and :math:`b` a vectorial time
    dependant function.

    Parameters
    ----------
    field: ``Field``
        The field object on which the RHS will be evaluated
    jac: ``numpy.ndarray``
        The Jacobian matrix.
    b: ``number`` or ``numpy.ndarray`` or ``function``
        The temporal right hand side.

    Attributes
    ----------
    b: ``number`` or ``numpy.ndarray`` or ``function``
            The temporal Right Hand Side.

    Note
    ----
    The instanciation of this RHS object does not require a Field object.
    """
    LINEAR = True

    def __init__(self, jac, b=0):
        jac = _np.asarray(jac)
        nDOF = int(jac.size**0.5)
        super().__init__(Field(nVar=nDOF))

        self.jac = _np.reshape(jac, (nDOF, nDOF))
        self.jac.flags['WRITEABLE'] = False

        if b != 0:
            if callable(b):
                try:
                    uTest = _np.empty(nDOF)
                    uTest += b(0)
                except Exception:
                    raise ValueError('wrong output format for b')
            else:
                b = _np.asarray(b).ravel()
                if b.size not in [1, nDOF]:
                    raise ValueError('b has not the correct size')
                self.AUTONOMOUS = True
                self.eval = self._eval_AUTONOMOUS
            self.b = b
        else:
            self.HOMOGENEOUS = True
            self.eval = self._eval_HOMOGENEOUS
            self.b = None

        # Deactivate Jacobian matrix computation
        @util.wraps(self.computeJacobian)
        def computeJacobian(u, t, eps=1e-8):
            return self.jac
        self.computeJacobian = computeJacobian

    @util.wraps(RHS.eval)
    def eval(self, u, t, uEval):
        uEval = _np.dot(self.jac, u, out=uEval)
        uEval += self.b(t)
        return uEval

    @util.wraps(RHS.eval)
    def _eval_AUTONOMOUS(self, u, t, uEval):
        uEval = _np.dot(self.jac, u, out=uEval)
        uEval += self.b
        return uEval

    @util.wraps(RHS.eval)
    def _eval_HOMOGENEOUS(self, u, t, uEval):
        return _np.dot(self.jac, u, out=uEval)


class PolyUT(RHS):
    """
    Define a polynomial of u and t, of the form :

    .. math::
        f(u,t) = c + \\sum_{i=1}^{N_u} \\lambda_i u^i +
        \\sum_{j=1}^{N_t} \\alpha_j t^j

    Parameters
    ----------
    cst: ``int`` or ``float`` or ``complex``
        The :math:`c` coefficient.
    coeffU: ``list`` or ``numpy.ndarray`` (any type)
        The :math:`\\lambda_i` coefficients.
    coeffT: ``list`` or ``numpy.ndarray`` (any type)
        The :math:`\\alpha_j` coefficients.

    Note
    ----
    The instanciation of this RHS object does not require a Field object.
    """

    def __init__(self, cst=0, coeffU=[1], coeffT=None):

        # Store constant
        self.cst = cst

        def nothing(*args):
            pass

        # Store u coefficients
        nDOF = 1
        if coeffU is None:
            coeffU = []
        coeffU = _np.asarray(coeffU)
        if coeffU.size == 0:
            self._addUTerms = nothing
            self.TRANSIENT = True
            self.computeJacobian = self._computeJacobian_TRANSIENT
        elif len(coeffU.shape) > 1:
            nDOF = coeffU.shape[1]
        if coeffU.shape[0] == 1:
            self.LINEAR = True
            self.computeJacobian = self._computeJacobian_LINEAR
        self.coeffU = coeffU
        self.coeffU.flags['WRITEABLE'] = False

        # Store t coefficients
        if coeffT is None:
            coeffT = []
        coeffT = _np.asarray(coeffT)
        if coeffT.size == 0:
            self._addTTerms = nothing
            self.AUTONOMOUS = True
            if self.LINEAR and self.cst == 0:
                self.HOMOGENEOUS = True
        elif len(coeffT.shape) > 1:
            if nDOF == 1:
                nDOF = coeffT.shape[1]
            elif coeffT.shape[1] != nDOF:
                raise ValueError('coeffU and coeffT vectorial coefficients ' +
                                 'have not the same size')
        self.coeffT = coeffT
        self.coeffT.flags['WRITEABLE'] = False

        if len(coeffU) > 0:
            dtype = coeffU.dtype
        elif len(coeffT) > 0:
            dtype = coeffT.dtype
        else:
            dtype = _np.array(cst).dtype
        super().__init__(Field(nVar=nDOF, numType=util.numType(dtype)))

    @util.wraps(RHS.eval)
    def eval(self, u, t, uEval):
        _np.copyto(uEval, self.cst)
        self._addUTerms(u, uEval)
        self._addTTerms(t, uEval)
        return uEval

    def _addUTerms(self, u, uEval):
        uTemp = _np.ones_like(uEval)
        for c in self.coeffU:
            uTemp *= u
            uEval += c*uTemp

    def _addTTerms(self, t, uEval):
        tTemp = 1.
        for c in self.coeffT:
            tTemp *= t
            uEval += c*tTemp

    @util.wraps(RHS.computeJacobian)
    def computeJacobian(self, u, t):
        u = _np.asarray(u).ravel()
        c1 = self.coeffU[0]
        uTemp = _np.ones_like(u)
        if len(self.coeffU.shape) == 1:
            nDOF = u.size
            A = sps.dia_matrix(([c1]*nDOF, 0), shape=(nDOF, nDOF))
        else:
            nDOF = self.coeffU.shape[1]
            A = sps.dia_matrix((c1, 0), shape=(nDOF, nDOF))
        for c, i in enumerate(self.coeffU[1:]):
            uTemp *= u
            A += sps.dia_matrix(((i+2)*c*uTemp, 0), shape=(nDOF, nDOF))
        self.jac = A.todia()
        return self.jac

    @util.wraps(RHS.computeJacobian)
    def _computeJacobian_TRANSIENT(self, u, t):
        return 0

    @util.wraps(RHS.computeJacobian)
    def _computeJacobian_LINEAR(self, u, t):
        if self.jac is None and len(self.coeffU.shape) > 1:
            c1 = self.coeffU[0]
            nDOF = self.coeffU.shape[1]
            self.jac = sps.dia_matrix((c1, 0), shape=(nDOF, nDOF))
        elif len(self.coeffU.shape) == 1:
            u = _np.asarray(u)
            nDOF = u.size
            if nDOF != self.jac.shape[0]:
                c1 = self.coeffU[0]
                self.jac = sps.dia_matrix(([c1]*nDOF, 0), shape=(nDOF, nDOF))
        return self.jac

    @util.wraps(RHS.evalJacobian)
    def evalJacobian(self, uJac, tJac, u, uEval=None):
        if uEval is None:
            uEval = _np.empty_like(u)
        A = self.computeJacobian(uJac, tJac)
        try:
            uEval[:] = A.dot(u)
        except Exception:
            uEval[:] = 0
        return uEval


# Append inherited documentation
for child in [Linear, PolyUT]:
    util.addBaseDoc(child, RHS)
