# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the base definition of a space operator

Classes
-------
SpaceOp : Base object
    define an abstract space operator object
"""
import numpy as np
import time as _time

from casper import common as util
from casper.fields import Field


class RHS(object):
    r"""
    Base class defining a Right Hand Side term (RHS)
    in a system of Ordinary Differential Equation (ODE) of the form :

    .. math::
        \frac{du}{dt} = f(u,t).

    The RHS object allows to evaluate the function :math:`f` on flat vector
    :math:`u` and given time :math:`t`.

    Evaluation can be done with the eval method :

    >>> uEval = rhs.eval(u, t, uEval)

    or calling directly the object

    >>> uEval = rhs(u)

    If not specified, t=0 and uEval is created during the call.

    Parameters
    ----------
    field: casper.Field
        The Field object on which can be evaluated the RHS

    Attributes
    ----------
    jac: ``numpy.ndarray``
        If computed, a variable where is stored the Jacobian matrix of the RHS
        operator (initialized to None)

    field: ``Field``
        The field on which the RHS can be evaluated

    LINEAR: ``bool``
        Wether or not the RHS is linear with respect to :math:`u`, i.e :

        .. math::
            f(u,t) = A u + b(t),

        with :math:`A` it's Jacobian matrix and :math:`b` a vectorial
        function.

    AUTONOMOUS: ``bool``
        Wether or not the RHS is autonomous, i.e :

        .. math::
            f(u,t) = f(u).

    HOMOGENEOUS: ``bool``
        Wether or not the RHS is homogeneous, i.e :

        .. math::
            f(u,t) = Au,

        with :math:`A` it's Jacobian matrix (this implies LINEAR=True).

    TRANSIENT: ``bool``
        Wether or not the RHS depends only on time, i.e

        .. math::
            f(u,t) = f(t)

    Methods
    -------
    eval(self, u, t, uEval): Abstract
        Evaluate the RHS on u and t, and store the result into uEval.
    computeJacobian(self, u, t, eps=1e-8): Base
        Compute the Jacobian matrix of the RHS operator.
    evalJacobian(self, uJac, tJac, u, uEval):
        Evaluate the Jacobian of the RHS for given vector.
    storePerfo(self, dico): Base
        Store computation performance data into a dictionnary.
    """

    LINEAR = False
    AUTONOMOUS = False
    HOMOGENEOUS = False
    TRANSIENT = False

    def __init__(self, field):
        self.field = util.checkPar(field, 'name', Field, convert=False)
        self.jac = None

    @property
    def nDOF(self):
        """Number of degrees of freedom"""
        return self.field.size

    @property
    def dtype(self):
        """Numerical type of the field vector"""
        return self.field.dtype

    def eval(self, u, t, uEval):
        """
        Evaluate the RHS on u and t, and store the result into uEval.
        Data already stored in uEval are overwritten.

        Parameters
        ----------
        u: ``numpy.1darray``
            Vector to evaluate
        t: ``float``
            Time of the field to evaluate
        uEval: ``numpy.1darray``
            Where to store the evaluated vector (same size as u).

        Returns
        -------
        uEval: ``numpy.1darray``
            Evaluated vector.
        """
        raise NotImplementedError('eval method must be implemented')

    def __call__(self, u, t=0, uEval=None):
        u = np.asarray(u)
        shape = u.shape
        if uEval is None:
            uEval = np.empty_like(u)
        uEval.shape = (u.size,)
        uEval = self.eval(u.ravel(), t, uEval)
        uEval.shape = shape
        return uEval

    def computeJacobian(self, u, t, eps=1e-8):
        """
        Compute the Jacobian matrix of the RHS operator.
        The result is stored into the **jac** attribute of the RHS object.

        Parameters
        ----------
        u: ``numpy.1darray``
            Vector from which evaluate the Jacobian
        t: ``float``
            Time from which evaluate the Jacobian.
        eps: ``float``
            Finite difference accuracy used when computing the Jacobian matrix.

        Returns
        -------
        jac: ``numpy.2darray``
            The Jacobian matrix.
        """
        u = np.asarray(u, dtype=self.dtype)

        if self.jac is None:
            self.jac = np.empty((self.nDOF, self.nDOF), dtype=self.dtype)

        if self.LINEAR:
            try:
                self.jac[0, 0] = 0
            except ValueError:
                return self.jac
            t = 0

        uPerturb = np.copy(u)
        uEval0 = np.empty(self.nDOF)
        self.eval(u, t, uEval0)
        for i in range(self.nDOF):
            uPerturb[i] += eps
            self.eval(uPerturb, t, self.jac[:, i])
            self.jac[:, i] -= uEval0
            self.jac[:, i] /= eps
            uPerturb[i] = u[i]

        if self.LINEAR:
            self.jac.flags['WRITEABLE'] = False

        return self.jac

    def evalJacobian(self, uJac, tJac, u, uEval=None):
        """
        Evaluate the Jacobian of the RHS for given vector.

        Parameters
        ----------
        uJac: ``numpy.1darray``
            The fields from which evaluate the Jacobian.
        tJac: ``float``
            The time from which evaluate the Jacobian.
        u: ``numpy.1darray``
            The fields to evaluate.
        uEval: ``numpy.1darray``
            Where to store the evaluated vector.

        Returns
        -------
        uEval: ``numpy.1darray``
            The evaluated vector.
        """
        if uEval is None:
            uEval = np.empty_like(u)
        A = self.computeJacobian(uJac, tJac)
        return np.dot(A, u, out=uEval)

    def evalJacobianRest(self, u, t, uEval=None):
        """
        Evaluate the rest of RHS's Jacobian for a given vector.

        Parameters
        ----------
        u: ``numpy.1darray``
            The vector on which to evaluate the rest and the Jacobian.
        t: ``float``
            The time of the evaluation.
        uEval: ``numpy.1darray``
            Where to store the evaluated vector.

        Returns
        -------
        uEval: ``numpy.1darray``
            The evaluated vector.
        """
        if uEval is None:
            uEval = np.empty_like(u)
        uEval = self.eval(u, t, uEval)
        uEval -= self.evalJacobian(u, t, u)
        return uEval

    def storePerfo(self, dico):
        """
        Store computation performance data into a dictionary.
        Two entries will be updated during the computation :

        - 'tCompRHS': computation time spent on evaluating the RHS
        - 'nEvalRHS': number of RHS evaluations

        Parameters
        ----------
        dico: ``dict``
            Where to store the performance data
        """
        dico['nEvalRHS'] = dico.get('nEvalRHS', 0)
        dico['tCompRHS'] = dico.get('tCompRHS', 0.)

        def wrapper(call):
            @util.wraps(call)
            def wrapped(u, t, uEval):
                """**storePerfo** with dictionary at %s"""
                tBeg = _time.time()
                uEval = call(u, t, uEval)
                dico['tCompRHS'] += _time.time() - tBeg
                dico['nEvalRHS'] += 1
                return uEval
            wrapped.__doc__ %= hex(id(dico))
            return wrapped
        self.eval = wrapper(self.eval)

    def initSolution(self, u0, iType, **kwargs):
        """
        Initialize a solution related to the rhs

        Parameters
        ----------
        u0 : ``numpy.1darray``
            Vector to store the initial solution on (same as u used for eval)
        iType : ``str``, or else ...
            The type of initial solution to use.
        **kwargs :
            Aditional arguments depending on the rhs.
        """
        raise NotImplementedError()
