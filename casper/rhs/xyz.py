# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the space opertors applied to vectors with fixed number
of components (like a particle with a position x,y and z)

Classes
-------
Lorentz : Inherited
    define a spacial operator of Lorentz system

RabinovichFabrikant : Inherited
    define a spacial operator for the Rabinovitch-Fabrikant equation
"""
import numpy as np

from casper import common as util
from casper.rhs.base import RHS
from casper.fields import Field


class Lorentz(RHS):
    r"""
    RHS of the Lorentz system, which can be written :

    .. math::
        \frac{dx}{dt} = \sigma (y-x), \; \frac{dy}{dt} = x (\rho - z) - y,
        \; \frac{dz}{dt} = xy - \beta z

    Considering the three dimensional vector :math:`u=(x,y,z)`, the formal
    expression of :math:`f` is then

    .. math::
        f(u,t) = [ \sigma (y-x), x (\rho - z) - y, xy - \beta z ]

    Parameters
    ----------
    sigma: ``float``
        The :math:`\sigma` parameter (default=10).
    rho: ``float``
        The :math:`\rho` parameter (default=28).
    beta: ``float``
        The :math:`\beta` parameter (default=8/3).

    Note
    ----
    The instanciation of this RHS object does not require a Field object.
    """

    def __init__(self, sigma=10, rho=28, beta=8/3):

        # Inherited constructor
        super().__init__(Field(3))

        self.sigma = util.checkPar(sigma, 'sigma', float)
        self.rho = util.checkPar(rho, 'rho', float)
        self.beta = util.checkPar(beta, 'beta', float)

    @util.wraps(RHS.eval)
    def eval(self, u, t, uEval):
        x, y, z = u
        np.copyto(uEval,
                  [self.sigma*(y-x), x*(self.rho-z)-y, x*y-self.beta*z])
        return uEval

    @util.wraps(RHS.computeJacobian)
    def computeJacobian(self, u, t):
        x, y, z = u
        self.jac = np.array(
            [[-self.sigma, self.sigma, 0],
             [self.rho-z, -1, -x],
             [y, x, -self.beta]])
        return self.jac

    @util.wraps(RHS.evalJacobian)
    def evalJacobian(self, uJac, tJac, u, uEval=None):
        if uEval is None:
            uEval = np.empty_like(u)
        x, y, z = u
        xJ, yJ, zJ = uJac
        np.copyto(uEval, [self.sigma*(y-x),
                          x*(self.rho-zJ) - y - xJ*z,
                          x*yJ + xJ*y - self.beta*z])
        return uEval

    @util.wraps(RHS.initSolution)
    def initSolution(self, u0, iType='USER', **kwargs):
        """
        Initial solution for Lorentz rhs. Arguments for **iType** can be :

        - 'MINION' : same as Minion initialization for Lorentz system.
            That is x0=5, y0=-5, z0=20.
        - 'USER' : initialization defined by the user.
            Can define in kwargs 'x0' (default=5), 'y0' (default=-5) and
            'z0' (default=20).
        """
        if iType == 'MINION':
            x0, y0, z0 = 5., -5., 20.
        elif iType == 'USER':
            x0 = kwargs.get('x0', 5.)
            y0 = kwargs.get('y0', -5.)
            z0 = kwargs.get('z0', 20.)
        else:
            raise ValueError(f'{iType} initialization not implemented')
        u0[:] = [x0, y0, z0]


class RabinovichFabrikant(RHS):
    r"""
    RHS of the Rabinovitch-Fabrikant equation, which can be written as :

    .. math::
        \frac{dx}{dt} = y(z-1+x^2)+\gamma x, \; \frac{dy}{dt} = x(3z+1-x^2)
        + \gamma y, \; \frac{dz}{dt} = -2z(\alpha + xy)

    where :math:`\alpha`, :math:`\gamma` are constants that control
    the evolution of the system. For some values of :math:`\alpha` and
    :math:`\gamma`, the system is chaotic, but for others it
    tends to a stable periodic orbit (see Wikipedia).

    Parameters
    ----------
    alpha: ``float``
        The :math:`\\alpha` coefficient (default=1.1).
    gamma: ``float``
        The :math:`\\gamma` coefficient (default=0.87).

    Note
    ----
    The instanciation of this RHS object does not require a Field object.
    """

    def __init__(self, alpha=1.1, gamma=0.87):

        # Inherited constructor
        super().__init__(Field(3))

        self.alpha = alpha
        self.gamma = gamma

    @util.wraps(RHS.eval)
    def eval(self, u, t, uEval):
        x, y, z = u
        np.copyto(uEval,
                  [y*(z-1.+x**2)+self.gamma*x,
                   x*(3.*z+1.-x**2)+self.gamma*y,
                   -2.*z*(self.alpha + x*y)])
        return uEval

    @util.wraps(RHS.initSolution)
    def initSolution(self, u0, iType='USER', **kwargs):
        """
        Initial solution for RabinovichFabrikant rhs.
        Arguments for **iType** can be :

        - 'SPROTT' : same as Sprott initialization for Lorentz system.
            That is x0=-1, y0=0, z0=0.5
        - 'USER' : initialization defined by the user.
            Can define in kwargs 'x0' (default=-1), 'y0' (default=0) and
            'z0' (default=0.5).
        """
        if iType == 'SPROTT':
            x0, y0, z0 = -1., 0., 0.5
        elif iType == 'USER':
            x0 = kwargs.get('x0', -1.)
            y0 = kwargs.get('y0', 0.)
            z0 = kwargs.get('z0', 0.5)
        else:
            raise ValueError(f'{iType} initialization not implemented')
        u0[:] = [x0, y0, z0]


class VanDerPol(RHS):
    """
    DOCTODO

    Note
    ----
    The instanciation of this RHS object does not require a Field object.
    """

    def __init__(self, epsilon=0.001):

        # Inherited constructor
        super().__init__(Field(3))

        self.epsilon = epsilon

    @util.wraps(RHS.eval)
    def eval(self, u, t, uEval):
        x = u[0]
        y = u[1]
        np.copyto(uEval, [y, (-x+(1-x**2)*y)/self.epsilon])
        return uEval

    @util.wraps(RHS.initSolution)
    def initSolution(self, u0, iType='USER', **kwargs):
        """
        Initial solution for VanDerPol rhs.
        Arguments for **iType** can be :

        - 'USER' : initialization defined by the user.
            Can define in kwargs 'x0' (default=2), 'y0' (default=2/3) and
            'z0' (default=0).
        """
        if iType == 'USER':
            x0 = kwargs.get('x0', 2.)
            y0 = kwargs.get('y0', 2./3.)
            z0 = kwargs.get('z0', 0.)
        else:
            raise ValueError(f'{iType} initialization not implemented')
        u0[:] = [x0, y0, z0]


class LotkaVoltera(RHS):
    r"""
    RHS for the Lotka Voltera equations, which can be written as :

    .. math::
        \frac{dx}{dt} = \alpha x - \beta x y, \;
        \frac{dy}{dt} = \delta x y - \gamma y

    where :math:`\alpha`, :math:`\beta`, :math:`\delta` and :math:`\gamma`
    are constants that control the evolution of the system.

    Parameters
    ----------
    alpha: ``float``
        The :math:`\\alpha` coefficient.
    beta: ``float`` in ****kwargs**
        The :math:`\\beta` coefficient.
    delta: ``float`` in ****kwargs**
        The :math:`\\delta` coefficient.
    gamma: ``float`` in ****kwargs**
        The :math:`\\gamma` coefficient.

    Note
    ----
    The instanciation of this RHS object does not require a Field object.
    """

    def __init__(self, alpha, beta, delta, gamma):

        # Inherited constructor
        super().__init__(Field(3))

        self.alpha = alpha
        self.beta = beta
        self.delta = delta
        self.gamma = gamma

    @util.wraps(RHS.eval)
    def eval(self, u, t, uEval):
        x, y = u
        np.copyto(uEval, [self.alpha*x - self.beta*x*y,
                          self.delta*x*y - self.gamma*y])
        return uEval

    @util.wraps(RHS.computeJacobian)
    def computeJacobian(self, u, t):
        x, y = u
        self.jac = np.array([[self.alpha-self.beta*y, -self.beta*x],
                             [self.delta*y, self.delta*x - self.gamma]])
        return self.jac

    @util.wraps(RHS.evalJacobian)
    def evalJacobian(self, uJac, tJac, u, uEval=None):
        if uEval is None:
            uEval = np.empty_like(u)
        x0, y0 = uJac
        x, y = u
        np.copyto(uEval,
                  [x*(self.alpha-self.beta*y0) - y*self.beta*x0,
                   x*self.delta*y0 + y*(self.delta*x0 - self.gamma)])
        return uEval


# Append inherited documentation
for child in [Lorentz, RabinovichFabrikant, VanDerPol, LotkaVoltera]:
    util.addBaseDoc(child, RHS)
