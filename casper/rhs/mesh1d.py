# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the operators applied to 1d mesh vectors
"""
import numpy as np
import scipy.sparse as sps

from casper.rhs.base import RHS
from casper import common as util
from casper.fields.f1d import Field1D
from casper.mpi import mpi
from casper.maths.findiff import buildStencil


class LinAdvDiffReac1D(RHS):
    r"""
    Defines a Right Hand Side of the linear advection-diffusion-reaction
    equation in one dimension

    .. math::
        \frac{\partial u}{\partial t} = -a \frac{\partial u}{\partial x}
        + \nu \frac{\partial^2 u}{\partial x^2}
        + \epsilon u + g(t),

    :math:`a`, :math:`\nu` and :math:`\epsilon` respectively the advection,
    diffusion and reaction coefficient.

    Advection schemes
    -----------------
    - 'U1' : upwind :math:`1^{st}` order,

        .. math::
            \frac{\partial u_i}{\partial x} \simeq
            \frac{u_{i}-u_{i-1}}{\Delta_x}

    - 'C2' : centered :math:`2^{nd}` order,

        .. math::
            \frac{\partial u_i}{\partial x} \simeq
            \frac{u_{i+1}-u_{i-1}}{2\Delta_x}

    - 'U3' : upwind :math:`3^{rd}` order,

        .. math::
            \frac{\partial u_i}{\partial x} \simeq
            \frac{2u_{i+1}+3u_{i}-6u_{i-1}+u_{i-2}}{6\Delta_x}

    - 'C4' : centered :math:`4^{th}` order,

        .. math::
            \frac{\partial u_i}{\partial x} \simeq
            \frac{-u_{i+2}+8u_{i+1}-8u_{i-1}+u_{i-2}}{12\Delta_x}

    - 'U5' : upwind :math:`5^{th}` order,

        .. math::
            \frac{\partial u_i}{\partial x} \simeq
            \frac{-3u_{i+2}+30u_{i+1}+20u_{i}-60u_{i-1}+15u_{i-2}-2u_{i-3}}
            {60\Delta_x}

    - 'C6' : centered :math:`6^{th}` order,

        .. math::
            \frac{\partial u_i}{\partial x} \simeq
            \frac{u_{i+3}-9u_{i+2}+45u_{i+1}-45u_{i-1}+9u_{i-2}-u_{i-3}}
            {60\Delta_x}

    Advection schemes
    -----------------
    - 'C2' : centered :math:`2^{nd}` order,
        .. math::
            \frac{\partial^2 u_i}{\partial x^2}\simeq
            \frac{u_{i+1}-2u_{i}+u_{i-1}}{\Delta_x^2}

    - 'C4' : centered :math:`4^{th}` order
        .. math::
            \frac{\partial^2 u_i}{\partial x^2}\simeq
            \frac{-u_{i+2}+16u_{i+1}-30u_i+16u_{i-1}-u_{i-2}}{12\Delta_x^2}

    - 'C6' : centered :math:`6^{th}` order
        .. math::
            \frac{\partial^2 u_i}{\partial x^2}\simeq
            \frac{2u_{i+3}-27u_{i+2}+270u_{i+1}-490u_i+270u_{i-1}-27u_{i-2}
                  +2u_{i-3}}{180\Delta_x^2}
    """
    LINEAR = True
    HOMOGENEOUS = True
    AUTONOMOUS = True

    def __init__(self, field, a=1, nu=0, epsilon=0, sourceTerm=None,
                 advScheme='C2', diffScheme='C2',
                 wallCond='DIRICHLET', uLeft=0, uRight=0):

        # Inherited constructor
        super().__init__(util.checkPar(field, 'field', Field1D, convert=False))

        # Basic tests and parameters
        if field.nVar != 1:
            raise NotImplementedError('implemented only for 1-variable fields')
        dx = field.dx
        nX = field.nX
        self.a = a
        self.nu = nu
        self.epsilon = epsilon

        # Build stencil
        stencil = buildStencil(dx, a, nu, epsilon, advScheme, diffScheme)
        sLength = stencil.size
        self.stencil = stencil.copy()

        # Build derivation operator matrix A, taking into account boundary
        # conditions and eventual space parallelization
        offset = np.arange(sLength, dtype=int) - sLength//2

        # ---------------------------------------------------------------------
        # Periodic boundary conditions
        # -- no mpi : operator is a circulant matrix
        # -- mpi : operator is a Toeplitz matrix, and evaluation for the
        #          boundaries uses halo points updated through mpi.
        if field.xPeriodic:

            if not mpi.spaceParallel:
                # Modify stencil and offset to create a circulant matrix
                offset += nX*(offset < 0)
                extInd = list(range(sLength//2)) + \
                    list(range(sLength//2+1, sLength))
                offset = np.concatenate((offset, -offset[extInd]))
                stencil = np.concatenate((stencil, stencil[extInd][::-1]))

            else:  # Space parallel implementation
                rankLeft = (mpi.sRank-1) % mpi.sSize
                rankRight = (mpi.sRank+1) % mpi.sSize
                nHalo = self.stencil.size // 2
                uLeft = np.empty(nHalo)
                uRight = np.empty(nHalo)
                sLeft = self.stencil[:nHalo][::-1]
                sRight = self.stencil[-nHalo:][::-1]

                def wrapper(call):
                    @util.wraps(call)
                    def wrapped(u, t, uEval):
                        """MPI periodic implementation"""
                        mpi.sComm.ISEND(u[-nHalo:], dest=rankRight, tag=1)
                        mpi.sComm.ISEND(u[:nHalo], dest=rankLeft, tag=2)
                        req1 = mpi.sComm.IRECV(uLeft, source=rankLeft, tag=1)
                        req2 = mpi.sComm.IRECV(uRight, source=rankRight, tag=2)
                        uEval = call(u, t, uEval)
                        req1.Wait()
                        req2.Wait()
                        for i in range(nHalo):
                            uEval[:nHalo-i] += sLeft[i:]*uLeft[-i-1]
                            uEval[-nHalo+i:] += sRight[:nHalo-i]*uRight[i]
                        return uEval
                    return wrapped
                self.eval = wrapper(self.eval)

            A = sps.diags(stencil, offset, shape=(nX, nX),
                          format='dia', dtype=field.dtype)

        # ---------------------------------------------------------------------
        elif wallCond == 'DIRICHLET':
            if uLeft != 0 or uRight != 0:
                self.HOMOGENEOUS = False
            A = sps.diags(stencil, offset, shape=(nX, nX),
                          format='lil', dtype=field.dtype)

            if not mpi.spaceParallel:

                if callable(uLeft):
                    self.AUTONOMOUS = False
                else:
                    uLeft = util.cstToFunction(uLeft)

                if callable(uRight):
                    self.AUTONOMOUS = False
                else:
                    uRight = util.cstToFunction(uRight)

                sLeft, sRight = [], []
                for i in range(sLength//2-1):
                    scheme = 'C{}'.format((i+1)*2)
                    s = buildStencil(dx, a, nu, epsilon, scheme, scheme)
                    sLeft.append(s[0])
                    sRight.append(s[-1])
                    A[[i, -i-1], :] = 0
                    A[i, :len(s)-1] = s[1:]
                    A[-i-1, -len(s)+1:] = s[:-1]
                sLeft.append(stencil[0])
                sRight.append(stencil[-1])
                sLeft = np.array(sLeft)
                sRight = np.array(sRight)[::-1]
                nHalo = sRight.size

                if not self.HOMOGENEOUS:

                    evalOrig = self.eval

                    @util.wraps(self.evalJacobian)
                    def evalJacobian(uJac, tJac, u, uEval=None):
                        """Non homogeneous Dirichlet implementation"""
                        if uEval is None:
                            uEval = np.empty_like(u)
                        uEval = evalOrig(u, 0, uEval)
                        return uEval
                    self.evalJacobian = evalJacobian

                    evalJacobianRestOrig = self.evalJacobianRest

                    @util.wraps(RHS.evalJacobianRest)
                    def evalJacobianRest(u, t, uEval=None):
                        """Non homogeneous Dirichlet implementation"""
                        uEval = evalJacobianRestOrig(u, t, uEval)
                        uEval[:nHalo] += sLeft * uLeft(t)
                        uEval[-nHalo:] += sRight * uRight(t)
                        return uEval
                    self.evalJacobianRest = evalJacobianRest

                    def wrapper(call):
                        @util.wraps(call)
                        def wrapped(u, t, uEval):
                            """Non homogeneous Dirichlet implementation"""
                            uEval = call(u, t, uEval)
                            uEval[:nHalo] += sLeft * uLeft(t)
                            uEval[-nHalo:] += sRight * uRight(t)
                            return uEval
                        return wrapped
                    self.eval = wrapper(self.eval)

            else:  # Space parallel implementation
                raise NotImplementedError(
                    'space parallel DIRICHLET boundary conditions not ' +
                    'implemented yet')

        else:
            raise NotImplementedError('boundary conditions not implemented')

        # Create sparse Jacobian matrix, removing eventual zeros in it
        self.jac = A.tocsr().todia()

        # Add eventual temporal source term
        if sourceTerm:

            self.HOMOGENEOUS = False
            if callable(sourceTerm):
                self.AUTONOMOUS = False
            else:
                sourceTerm = util.cstToFunction(sourceTerm)

            evalOrig = self.eval

            @util.wraps(self.evalJacobian)
            def evalJacobian(uJac, tJac, u, uEval=None):
                """Source term implementation"""
                if uEval is None:
                    uEval = np.empty_like(u)
                uEval = evalOrig(u, 0, uEval)
                return uEval
            self.evalJacobian = evalJacobian

            evalJacobianRestOrig = self.evalJacobianRest

            @util.wraps(RHS.evalJacobianRest)
            def evalJacobianRest(u, t, uEval=None):
                """Source term implementation"""
                uEval = evalJacobianRestOrig(u, t, uEval)
                uEval += sourceTerm(t)
                return uEval
            self.evalJacobianRest = evalJacobianRest

            def wrapper(call):
                @util.wraps(call)
                def wrapped(u, t, uEval):
                    """Source term implementation"""
                    uEval = call(u, 0, uEval)
                    uEval += sourceTerm(t)
                    return uEval
                return wrapped
            self.eval = wrapper(self.eval)

    @property
    def dx(self):
        return self.field.dx

    @property
    def nX(self):
        return self.field.nX

    @property
    def PERIODIC(self):
        return self.field.xPeriodic

    @util.wraps(RHS.eval)
    def eval(self, u, t, uEval):
        """Default homogeneous implementation"""
        np.copyto(uEval, self.jac.dot(u))
        return uEval

    @util.wraps(RHS.computeJacobian)
    def computeJacobian(self, u, t, eps=1e-8):
        """Default homogeneous implementation"""
        return self.jac

    @util.wraps(RHS.evalJacobian)
    def evalJacobian(self, uJac, tJac, u, uEval=None):
        """Default homogeneous implementation"""
        if uEval is None:
            uEval = np.empty_like(u)
        uEval = self.eval(u, 0, uEval)
        return uEval

    @util.wraps(RHS.evalJacobianRest)
    def evalJacobianRest(self, u, t, uEval=None):
        """Default homogeneous implementation"""
        if uEval is None:
            uEval = np.empty_like(u)
        uEval[:] = 0
        return uEval

    @util.wraps(RHS.initSolution)
    def initSolution(self, u0, iType, **kwargs):
        r"""
        Initial solution for LinAdvDiffReac rhs.
        Arguments for **iType** can be :

        - 'GAUSS' : initialize with a Gaussian curve.
            Can define in kwargs its center ('x0', default=0.5) and a
            sigma coefficient ('sigma', default=1/10).
        - 'STEP' : initialize with a step curve.
            Can define in kwargs its center ('x0', default=0.5) and a
            width ('sigma', default=1/10).
        - 'THI' : initial solution inspired from Turbulent Flow simulation
            build an initial solution with an isotropic turbulence-like
            Fourier spectrum.
        - GAN' : initial solution inspired from M.J. Gander
            It is defined by :math:`u_0(x) = \begin{cases}
            s^\sigma, & s < 0.5 \\
            1-|s-1|^\sigma, & s \geq 0.5
            \end{cases}`
            with :math:`\sigma` the **sigma** parameter given in kwargs
            (default: 0.5).
        - 'ROPE' : initial solution as rope going from 1 to 0
            It is defined by :math:`u_0(x) = -\frac{x}{L}+1`.
        - 'SINROPE' : same as ROPE, but with a sinusoidal form
            :math:`u_0(x) = \sin\left(\frac{\pi x}{2L}\right)`
        """
        # Normalize x from [xBeg, xEnd] to [0, 1]
        xBeg, xEnd = self.field.xBeg, self.field.xEnd
        x = (self.field.x - xBeg)/(xEnd - xBeg)

        # -- Default initial field
        u0[:] = 0

        # -- Specific initialization
        if iType == 'GAUSS':
            x0 = kwargs.get('x0', 0.5)
            sigma = kwargs.get('sigma', 1/10.)
            np.exp(-(x-x0)**2/sigma**2, out=u0)
        elif iType == 'STEP':
            x0 = kwargs.get('x0', 0.5)
            sigma = kwargs.get('sigma', 1/10.)
            u0[:] = 1*(np.abs(x-x0) < sigma/2)
        elif iType == 'STEP2':
            sigma = kwargs.get('sigma', 1/10.)
            u0[:] = 1*(x <= sigma)
        elif iType == 'SINUS':
            listK = kwargs.get('listK', [1])
            listA = kwargs.get('listA', [1])
            if len(listA) == 1:
                listA *= len(listK)
            for k, a in zip(listK, listA):
                u0 += a*np.sin(2*k*np.pi*x)
            u0 /= len(listK)
        elif iType == 'THI':
            # Isotropic spectrum parameters
            cL = 6.78
            C = 1.5
            p0 = 2
            # Build isotropic turbulence spectrum
            kappaMax = 6.2e3
            kappa = np.arange(1, kappaMax+1)
            spectrumTHI = C*kappa**(-5./3) * \
                (kappa/np.sqrt(kappa**2+cL))**(p0+5./3)
            # Build initial solution
            for k, Ek in zip(kappa, spectrumTHI):
                u0 += np.sin(2*k*np.pi*(x-0.5))*Ek
                # u0 += np.cos(2*k*np.pi*(x-0.5))*Ek
        elif iType == 'GAN':
            sigma = kwargs.get('sigma', 0.5)
            u0[:] = (x < 0.5)*(2*x)**sigma + (x >= 0.5)*(1-abs(2*x-1)**sigma)
        elif iType == 'ROPE':
            u0[:] = - x + 1
        elif iType == 'SINROPE':
            u0[:] = np.cos(x*np.pi/2)
        elif iType == 'SINROPE2':
            u0[:] = 0.5*np.cos(x*np.pi)+0.5
        elif iType == 'ROPEWAVE':
            k = kwargs.get('k', 4)
            amp = kwargs.get('amp', 0.5)
            u0[:] = (-x + 1) + +amp*np.sin(2*np.pi*x*k)

        return u0


class DampedString(RHS):
    r"""
    TODO
    """
    LINEAR = True
    HOMOGENEOUS = True
    AUTONOMOUS = True

    def __init__(self, field, c, gamma, r):

        # Inherited constructor
        super().__init__(util.checkPar(field, 'field', Field1D, convert=False))

        # Basic tests and parameters
        if field.nVar != 2:
            raise ValueError('field should be a 2-variables fields')
        if field.xPeriodic:
            raise ValueError('field should be non periodic')
        dx = field.dx
        nX = field.nX
        self.c = c
        self.cSquare = c**2
        self.gamma = gamma
        self.r = r

        # Build stencil and Laplace operator matrix A
        stencil = buildStencil(dx, 0, 1, 0, None, 'C2')
        sLength = stencil.size
        self.stencil = stencil.copy()
        offset = np.arange(sLength, dtype=int) - sLength//2
        L = sps.diags(
            stencil, offset, shape=(nX, nX), format='dia', dtype=field.dtype)
        self.L = L

        # Build the rhs Jacobian matrix
        eye = sps.dia_matrix(
            (np.ones(nX), [0]), shape=(nX, nX), dtype=field.dtype)
        zero = sps.dia_matrix((nX, nX), dtype=field.dtype)
        upper = sps.hstack([zero, eye])
        lower = sps.hstack([c**2 * L, gamma * L - r * eye])
        self.jac = sps.vstack([upper, lower])

    @util.wraps(RHS.eval)
    def eval(self, u, t, uEval):
        """Default implementation"""
        v, dv = self.field.extractVars(u)
        vEval, dvEval = self.field.extractVars(uEval)
        np.copyto(vEval, dv)
        np.copyto(dvEval, self.cSquare*self.L.dot(v))
        dvEval += self.gamma*self.L.dot(dv)
        dvEval -= self.r*dv
        return uEval

    @util.wraps(RHS.computeJacobian)
    def computeJacobian(self, u, t, eps=1e-8):
        """Default implementation"""
        return self.jac

    @util.wraps(RHS.initSolution)
    def initSolution(self, u0, iType, **kwargs):
        """
        Initial solution for DampedString rhs.
        Arguments for **iType** can be :

        - 'PINCH' : pinched string into two linear segment.
            Can define in kwargs the height ('h', default=0.25) and the
            relative pinch position ('p', default=0.5).
        - 'HARMO' : harmonical mode of the string.
            Can define in kwargs the amplitude ('h', default=0.25) and the
            index of the mode ('kappa', default=1).
        """
        # Normalize x from [xBeg, xEnd] to [0, L]
        xBeg, xEnd = self.field.xBeg, self.field.xEnd
        x = (self.field.x - xBeg)/(xEnd - xBeg)

        # Extract elevation and derivative
        v0, dv0 = self.field.extractVars(u0)

        # Initialize string elevation
        h = kwargs.get('h', 0.25)
        if iType == 'PINCH':
            p = kwargs.get('p', 0.5)
            np.copyto(v0,  x*h/p * (x <= p) + (1-x)*h/(1-p) * (x > p))
        elif iType == 'HARMO':
            kappa = kwargs.get('kappa', 1)
            np.sin(kappa*np.pi*x, out=u0)
            u0 *= h

        # Initialize null derivative
        np.copyto(dv0, 0)

        return u0


class FDNonLinAdv1D(RHS):
    r"""
    Inherited class, define a finite differences 1D spatial differential
    operator for the non linear advection term

    .. math::
        f(u,t) = c*u \frac{\partial u}{\partial x}

    It is based on the approximation of the space derivative with finite
    differences of the form

    .. math::
        \frac{\partial u}{\partial x}(x_i)
        \approx \frac{\sum_{i=-s}^{s} \alpha_i u_i}{\Delta_x}

    where :math:`s` is the size of the stencil used. In order to get a
    "conservative" scheme, the :math:`u` term before the derivative is
    evaluated by a barycentric average on the stencil, like :

    .. math::
        u_i \approx \sum_{i=-s}^{s} \beta_i u_i

    So, the finale discretization is written into this form :

    .. math::
        u \frac{\partial u}{\partial x}(x_i)
        \approx \sum_{i=-s}^{s} \beta_i u_i
        \frac{\sum_{i=-s}^{s} \alpha_i u_i}{\Delta_x}

    This space operator can be parallelized, and its sequential and parallel
    version are compatible and optimized for elementar operations with
    ``SpaceOp`` objects.

    Parameters
    ----------
    mesh : ``numpy.array`` (1d)
        The mesh of the field associated to the space operator
    discr : ``str``
        Discretization used for the derivative term
        :math:`\frac{\partial u}{\partial x}`
    - discr='U1 : upwind :math:`1^{st}` order,
      :math:`(\beta_i)_{-1 \leq i \leq 1} = \frac{(1,1,0)}{2}`
    - discr='U3' : upwind :math:`3^{rd}` order,
      :math:`(\beta_i)_{-2 \leq i \leq 2} = \frac{(-1,4,7,2,0)}{12}`
    - discr='U5' : upwind :math:`5^{th}` order,
      :math:`(\beta_i)_{-3 \leq i \leq 3} = \frac{(2, -11,34,74,24,-3,0)}
      {120}`
    boundary : ``str`` in ****kwargs**
        The boundary condition type (default = 'PER')
    - boundary='PER' : periodic boundary conditions

    Examples
    --------
    >>> # TODO

    Attributes
    ----------
    discr : ``str``
        The discretization scheme used for the space derivative
    A : ``numpy.ndarray(float)``
        The differential matrix associated to the operator
    ASud : ``numpy.ndarray(float)``
        The differential matrix associated to the south halo
    ANord : ``numpy.ndarray(float)``
        The differential matrix associated to the north halo
    B : ``numpy.ndarray(float)``
        The barycentric matrix associated to the operator
    BSud : ``numpy.ndarray(float)``
        The barycentric matrix associated to the south halo
    BNord : ``numpy.ndarray(float)``
        The barycentric matrix associated to the north halo
    nHalo : ``int``
        The size of the halo

    Methods
    -------
    evalField(self,u,t) : `Overloaded`
        evaluate the space operator
    getNFlopEval(self) : `Overloaded`
        evaluate the number of floating points operations per evaluation
    """
    def __init__(self, mesh, discr, coeff, **kwargs):
        LinAdvDiffReac1D.__init__(self, mesh, ['ADV', discr, coeff], **kwargs)
        # Set the barycentric matrix B
        if discr == 'U1':   # Upwind order 1
            barScheme = np.array([1., 1., 0.])/2.0
        elif discr == 'C2':   # Centered order 2
            barScheme = np.array([1., 0., 1.])/2.0
        elif discr == 'U3':   # Upwind order 3
            barScheme = np.array([-1., 4., 7., 2., 0.])/12.0
        elif discr == 'U5':   # Upwind order 5
            barScheme = np.array([2., -11., 34., 74., 24., -3., 0.])/120.0
        # [self.B, self.BNord, self.BSud] = \
        #     buildDiffMatrices(barScheme, mesh.size)
        self.discr = discr

    def evalField(self, u, t, uEval):
        """
        Overloaded method, evaluate the space operator

        Parameters
        ----------
        u : ``numpy.array(float)``
            The field to evaluate in array form
        t : ``float``
            The time of the field to evaluate
        uEval : ``numpy.array(float)``
            The numpy array where to store the evaluated field (same form
            as u !!)

        Returns
        -------
        uEval : ``numpy.array(float)``
            The evaluated fields
        """
        self._updateHalo(u)
        dudx = self.A.dot(u**2)
        dudx[:self.nHalo] += self.ASud.dot(self.uSud[-self.nHalo:]**2)
        dudx[-self.nHalo:] += self.ANord.dot(self.uNord[-self.nHalo:]**2)
        dudx /= 2
        np.copyto(uEval, dudx)
#        dudx = self.A.dot(u)
#        dudx[:self.nHalo] += self.ASud.dot(self.uSud[-self.nHalo:])
#        dudx[-self.nHalo:] += self.ANord.dot(self.uNord[-self.nHalo:])
#        np.copyto(uEval, self.B.dot(u))
#        uEval[:self.nHalo] += self.BSud.dot(self.uSud[-self.nHalo:])
#        uEval[-self.nHalo:] += self.BNord.dot(self.uNord[-self.nHalo:])
#        uEval *= dudx
        return uEval


# Append inherited documentation
for child in [LinAdvDiffReac1D, DampedString]:
    util.addBaseDoc(child, RHS)
