"""
Description
-----------

Package containing the RHS objects of Casper, representing the
operator :math:`f` used in the differential equation

.. math::
    \\frac{du}{dt} = f(u,t)

Each RHS object implement a particular **eval** function, which
evaluate the result of :math:`f(u,t)` into a **uEval** numpy vector :

>>> # Example : evaluating the Lorentz system RHS
>>> import numpy as np
>>> from casper.rhs import Lorentz
>>> rhs = Lorentz()
>>> u = np.ones(3)
>>> uEval = np.empty_like(u)
>>> rhs.eval(u, 0, uEval)
array([ 0.        , 26.        , -1.66666667])

The RHS objects are also callable, and their call do not need the definition
of the numpy vector **uEval** :

>>> uEval = rhs(u, 0)
>>> print(uEval)
[ 0.         26.         -1.66666667]

Finally, each RHS objects implement the **storePero** method, which allows
to store the number of evaluations of the **call** method, and the
overall computation time due to all the calls :

>>> perf = {}
>>> rhs.storePerfo(perf)
>>> for i in range(10000):
>>>     rhs.eval(u, 0, uEval)
>>> print(perf)
{'nEvalRHS': 10000, 'tCompRHS': 0.09397200000002126}
>>> print(perf['tCompRHS']/perf['nEvalRHS'])
9.397200000002126e-06
"""
from . import base, general, mesh1d, xyz

Linear = general.Linear
PolyUT = general.PolyUT

Lorentz = xyz.Lorentz
RabinovichFabrikant = xyz.RabinovichFabrikant
VanDerPol = xyz.VanDerPol
LotkaVoltera = xyz.LotkaVoltera

LinAdvDiffReac1D = mesh1d.LinAdvDiffReac1D

__all__ = ['base', 'general', 'mesh1d', 'xyz',
           'Linear', 'PolyUT',
           'Lorentz', 'RabinovichFabrikant', 'VanDerPol', 'LotkaVoltera',
           'LinAdvDiffReac1D']
