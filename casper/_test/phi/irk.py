#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 17 18:02:31 2019

@author: lunet
"""
import numpy as np
import matplotlib.pyplot as plt

from casper.fields import Field1D
from casper.rhs import LinAdvDiffReac1D
from casper.phi import IRK
from casper.mpi import mpi, initMPI

initMPI()
plotSol = True

field = Field1D(200, xEnd=2*np.pi)
xGlob = field.getXGlob()

np.sin(field.x, out=field.flat)
uGlob = field.getUGlob()

if mpi.sRank == 0 and plotSol:
    plt.plot(xGlob, uGlob[0])

rhs = LinAdvDiffReac1D(field, advScheme='C8', a=1, nu=0.1)
rhs.jac = rhs.jac

dt = rhs.dx/rhs.a
phi = IRK(rhs, 'SDIRK3', dt=None)

perfo = {}
rhs.storePerfo(perfo)
phi.storePerfo(perfo)

nT = (field.xEnd-field.xBeg)/rhs.dx
nT = 50
fNum = (field.copy(), field.copy())

tBeg = mpi.MPI.Wtime()
for i in range(nT):
    phi(fNum[0][0], dt=dt, uNext=fNum[1][0])
    fNum = fNum[-1::-1]
perfo['tCompPhi'] -= perfo['tCompRHS']
tEnd = mpi.MPI.Wtime()
print(perfo, tEnd-tBeg)

fNum = fNum[0]
uGlob = fNum.getUGlob()

if mpi.sRank == 0 and plotSol:
    plt.plot(xGlob, uGlob[0])
    plt.grid()
    plt.show()
