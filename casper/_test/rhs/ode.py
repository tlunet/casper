#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  7 13:03:49 2017

@author: lunet
"""
import unittest


class TestPolyUT(unittest.TestCase):

    def test_object(self):
        import casper.spaceop.ode as spop
        polyUT = spop.PolyUT()

        self.assertTrue(hasattr(polyUT, 'cst'))
        self.assertTrue(hasattr(polyUT, 'coeffU'))
        self.assertTrue(hasattr(polyUT, 'coeffT'))

        polyUT.getNFlopEval()

    def test_evalField(self):
        import casper.spaceop.ode as spop
        import numpy as np
        u0 = np.arange(10.)
        uEval = np.empty_like(u0)

        # Default behavior
        f = spop.PolyUT()
        self.assertTrue((f.evalField(u0, 0., uEval) == u0).any(),
                        msg='Default behavior, tTest=0.')
        self.assertTrue((f.evalField(u0, 1., uEval) == u0).any(),
                        msg='Default behavior, tTest=1.')

        # Non-default behavior
        f = spop.PolyUT(1.23, [2.5, 5.6], [1.2, 6.7])

        mT = 'Non-default behavior, polynomial of u only'
        uRes = 1.23 + 2.5*u0 + 5.6*u0**2
        self.assertTrue((f.evalField(u0, 0., uEval) == uRes).any(), msg=mT)

        mT = 'Non-default behavior, polynomial of u and t'
        tTest = 0.8
        uRes += 1.2*tTest + 6.7*tTest**2
        self.assertTrue((f.evalField(u0, tTest, uEval) == uRes).any(), msg=mT)

        u0 = 1j*u0
        uEval = np.empty_like(u0)

        mT = 'u0 complex, non-default behavior, polynomial of u only'
        uRes = 1.23 + 2.5*u0 + 5.6*u0**2
        self.assertTrue((f.evalField(u0, 0., uEval) == uRes).any(), msg=mT)

        mT = 'u0 complex, non-default behavior, polynomial of u and t'
        tTest = 0.8
        uRes += 1.2*tTest + 6.7*tTest**2
        self.assertTrue((f.evalField(u0, tTest, uEval) == uRes).any(), msg=mT)


if __name__ == '__main__':
    unittest.main()
