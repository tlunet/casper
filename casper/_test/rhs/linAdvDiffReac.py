#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 13 02:33:18 2019

@author: telu
"""
import numpy as np
from casper.mpi import initMPI, mpi
from casper.fields import Field1D
from casper.rhs.mesh1d import LinAdvDiffReac1D

initMPI()

f0 = Field1D(16, xPeriodic=True)

rhs = LinAdvDiffReac1D(
    f0, advScheme='C8', uLeft=lambda t: t, uRight=lambda t: 2*t,
    sourceTerm=lambda t: 2*t)

u = np.ones_like(f0.flat)
uEval = u.copy()

print(np.round(rhs.eval(u, 0, uEval), decimals=3))
mpi.sComm.BARRIER()
print(np.round(rhs.jac.toarray(), decimals=3))
mpi.sComm.BARRIER()
print(np.round(rhs.evalJacobianRest(f0.flat, 1), decimals=3))
