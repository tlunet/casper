# -*- coding: utf-8 -*-
"""
Created on Fri Oct  9 12:27:40 2015

@author: lunet
"""

import numpy as np
import time

a = np.arange(100000.)
b = np.empty_like(a)

tCrochet = - time.clock()
b[:] = a
tCrochet += time.clock()
print("tCrochet = {}".format(tCrochet))

b = np.empty_like(a)
tNpCopy = - time.clock()
np.copyto(b, a)
tNpCopy += time.clock()
print("tNpCopy = {}".format(tNpCopy))

A = np.array([[1, 1], [1, 1]])
a = np.ones(2)
b = np.empty_like(a)
np.dot(A, a, out=b)
