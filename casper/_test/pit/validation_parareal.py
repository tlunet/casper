#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 23 17:00:59 2019

@author: lunet
"""
import casper.timesolver.rk as rks
import casper.fields.f1d as f1d
import casper.spaceop.mesh1d as spop
import casper.pitalgo.parareal as pit

from casper.mpi import mpi, initMPI

import numpy as np

# MPI initialization
initMPI(nps=1)

# Field construction and initialization
nX = 16
dx = 1/nX
f0Settings = {'nFields': 1,
              'nXGlob': nX,
              'xBegGlob': -1,
              'xEndGlob': 1,
              'meshType': 'XPER'}
f0 = f1d.Fields1D(**f0Settings)
np.cos(np.pi*f0.xCoordLoc, f0.getU())

# Space operator for F and G
spaceOp = spop.FDLin1D(f0.xCoordLoc, ['ADV', 'U1', -1], boundary='PERIODIC')

# Fine solver settings
fSolverSettings = {
    'f0': f0.copy(), 'spaceOp': spaceOp,
    'stepping': 'FIXED', 'timeStorage': 'CURRENT', 'dumpTimes': 'NEVER',
    'tBeg': 0.0, 'tEnd': 0.0, 'dt': 0.05, 'RKS': 'FE'}

# Coarse solver settings
gSolverSettings = fSolverSettings.copy()
gSolverSettings['dt'] = 0.2

pararealSettings = {
    'f0': f0, 'timeDec': 'UNIF', 'nIter': 2,
    'gSolverClass': rks.RKSolver, 'gSolverSettings': gSolverSettings,
    'fSolverClass': rks.RKSolver, 'fSolverSettings': fSolverSettings,
    'gToF': None, 'fToG': None,
    'tBeg': 0.0, 'tEnd': 1, 'nChunks': 4}

parareal = pit.Parareal(**pararealSettings)
parareal.runAlgo()

fields = parareal.getFields(chunk='ALL')

if mpi.tRank == parareal.tRankRoot:
    uValid = [f0.getU()[0]] + [f.getU()[0] for f in fields]
    np.savetxt('test_data.txt', uValid, fmt='%20.18f')
