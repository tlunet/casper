# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 16:52:54 2015

@author: lunet
"""

from mpi4py import MPI
import time

nps = 5
npt = 3

gcom = MPI.COMM_WORLD
grank = gcom.Get_rank()
gsize = gcom.Get_size()
gname = gcom.Get_name()

if gsize != nps*npt:
    if grank == 0:
        raise ValueError("Script has to be run with 15 MPI processes")
    time.sleep(grank*0.2)
    gcom.Abort()

time.sleep(grank*0.2)
print("P{} - ".format(grank) +
      ("Hello there, I'm processor {0} in the Global communicator {1} of size "
       + "{2}, waiting for the others").format(grank, gname, gsize))

gcom.Barrier()

if grank == 0:
    print("Starting Time Communicator Splitting ...")

tcolor = grank % nps
tcom = gcom.Split(tcolor, grank)

trank = tcom.Get_rank()
tsize = tcom.Get_size()

time.sleep(grank*0.2)
print("P{} - ".format(grank) +
      ("Hello again, I'm now processor {} in a Time communicator of size {},"
       + " wfto").format(trank, tsize))

gcom.Barrier()

if grank == 0:
    print("Starting Space Communicator Splitting ...")

scolor = (grank-grank % nps)/nps
scom = gcom.Split(scolor, grank)

srank = scom.Get_rank()
ssize = scom.Get_size()

time.sleep(grank*0.2)
print("P{} - ".format(grank) +
      ("Hello again, I'm now processor {} in a Space communicator of size {},"
       + " wfto").format(srank, ssize))
