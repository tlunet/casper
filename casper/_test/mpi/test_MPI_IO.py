# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 19:49:34 2015

@author: t.lunet
"""

from mpi4py import MPI
import numpy as np
from time import sleep

amode = MPI.MODE_WRONLY | MPI.MODE_CREATE
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
fh = MPI.File.Open(comm, "_testMPIIO.data", amode)

buff = np.ndarray(shape=(2, 5), dtype=np.float)
buff[:, :] = comm.Get_rank()

comm.Barrier()
sleep(0.1*comm.Get_rank())
print("Process {}: writes array =".format(comm.Get_rank()))
print(buff)

offset = comm.Get_rank()*buff.nbytes
fh.Write_at_all(offset, buff)

fh.Close()

del fh

comm.Barrier()
if comm.Get_rank() == 0:
    print("------------------------------------------------------------")

amode = MPI.MODE_RDONLY
fh = MPI.File.Open(comm, "_testMPIIO.data", amode)

nextRank = (comm.Get_rank() + 1) % comm.Get_size()
buffRead = np.ndarray(shape=(2, 5), dtype=np.float)
offsetRead = nextRank*buffRead.nbytes
fh.Read_at_all(offsetRead, buffRead)

comm.Barrier()
sleep(0.1*comm.Get_rank())
print("Process {}: read array written by process {} =".format(
        comm.Get_rank(), nextRank))
print(buffRead)

fh.Close()
