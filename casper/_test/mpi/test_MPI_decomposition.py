# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 19:45:42 2015

Test program to test MPI interface of Casper
"""
import time
from casper.mpi import initMPI, mpi


initMPI(npt=2)

gRank = mpi.gRank
gSize = mpi.gSize
sRank = mpi.sRank
sSize = mpi.sSize
tRank = mpi.tRank
tSize = mpi.tSize

time.sleep(0.1*gRank)
print(mpi)
