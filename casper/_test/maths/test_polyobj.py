# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 19:25:08 2015

@author: lunet
"""

from casper.maths.polyobj import Poly

p1 = Poly([1, 1, 1, 1])
print('p1 = ', p1)
p2 = Poly([1, 1, 1])
print('p2 = ', p2)

print('p1 + p2 = ', p1 + p2)
print('p2 + p1 = ', p2 + p1)
print('p1 - p2 = ', p1 - p2)
print('p2 - p1 = ', p2 - p1)
print('p1 + 1 = ', p1 + 1)
print('p1 + 1.0 = ', p1 + 1.0)
print('p1 - 1 = ', p1 - 1)
print('p1 - 1.0 = ', p1 - 1.0)
print('1 + p1 = ', 1 + p1)
print('1.0 + p1 = ', 1.0 + p1)
print('p1*2 = ', p1 * 2)
print('p1*2.0 = ', p1 * 2.0)
print('2*p1 = ', 2*p1)
print('2.0*p1 = ', 2.0*p1)
print('p1*p2 = ', p1*p2)
print('p2*p1 = ', p2*p1)
print('p1/2 = ', p1/2)
print('p1/2.0 = ', p1/2.0)
print('p1**0 = ', p1**0)
print('p1**1 = ', p1**1)
print('p1**2 = ', p1**2)
print('p1**3 = ', p1**3)
