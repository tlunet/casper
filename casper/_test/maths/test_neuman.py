#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 23 10:26:24 2019

@author: telu
"""
import numpy as np
from time import time
import matplotlib.pyplot as plt
import casper.maths.neumann as cmn

timeScheme = 'SDIRK3'

plt.figure('Implicit_computation_time_{}'.format(timeScheme))
for name in ['DET', 'GEN', 'NUM', None]:

    # Generation time
    t1 = time()
    g = cmn.stabilityFunction(timeScheme, implicitImplementation=name)
    t2 = time()
    tGen = t2-t1

    # Computation time
    nVal = [1, 10, 100, 1000, 10000, 100000]
    tComp = []
    for n in nVal:
        z = np.linspace(0, -1, num=n)
        t1 = time()
        y = g(z)
        t2 = time()
        if n == 10:
            print(y)
        tComp.append(t2-t1)
    tComp = np.array(tComp)
    p = plt.plot(nVal, tComp, 'o:', label=name if name else 'OPT')
    plt.plot([nVal[0], nVal[-1]], [tGen, tGen], '--', c=p[0].get_color())
plt.legend()
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Size of z')
plt.ylabel('Evaluation time of g(z)')
plt.grid(True)
