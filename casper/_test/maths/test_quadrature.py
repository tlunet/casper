# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 10:18:20 2015

@author: lunet
"""

import casper.maths.quadrature as lob
import matplotlib.pyplot as plt
import numpy as np

plt.figure()
x = np.linspace(-1, 1, 500)


def func(x):
    return np.cos(np.exp(2.*x))


plt.plot(x, func(x), label='Original Function')

n = 8
points = lob.legendreQuadPoints(n)
print('Quadrature points\n', points)
weights = lob.legendreQuadWeights(n, points)
print('Quadrature weights\n', weights)
matPL = lob.matrixCoeffLegendre(n, points, weights)
polyInterpol = lob.legendreInterpol(func, x, n, points, matPL)
plt.plot(x, polyInterpol, label='Legendre interpolator')

plt.plot(points, func(points), 'bo')
polyPoints = lob.lagrangeInterpol(func, x, n, points)
plt.plot(x, polyPoints, '--', label='Lagrange interpolator')
plt.legend()
plt.show()
