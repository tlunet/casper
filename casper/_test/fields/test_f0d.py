#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  5 16:17:26 2019

@author: lunet
"""
from casper.fields import Field

for numType in ['FLOAT', 'COMPLEX']:
    f0 = Field(5, numType)
    f0.flat[:] = range(5)
    print(f0[:])
    f0.dumpToFile('_test_f0d')
    f0Read = Field.fromFile('_test_f0d')
    print(f0Read[:])
