# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 13:54:20 2015

@author: lunet
"""

from casper.fields import Field1D
from casper.mpi import initMPI, mpi
from time import sleep

initMPI()


def syncPrint(*args):
    mpi.sComm.BARRIER()
    sleep(0.05*mpi.sRank)
    print(*args)
    mpi.sComm.BARRIER()


nX = 15
xBeg = 0
xEnd = 15
nVar = 5

# Field initialization
f1 = Field1D(nVar, nX, xBeg, xEnd)

if mpi.sRank == 0:
    print('#'*80)
    print(f'Pi: f1 initialized with nX={nX}, xBeg={xBeg}, xEnd={xEnd}')

syncPrint('P{0}: f1.nX={1}, f1.iBegGlob={2}, f1.x={3}'
          .format(mpi.sRank, f1.nX, f1.iBegGlob, f1.x))

# Test MPI IO
if mpi.sRank == 0:
    print('#'*80)
    print("Pi: f1.dumpToFile('_test_f1d'), " +
          "f2 = Field1D.readFromFile('_test_f1d')")
for iVar in range(f1.nVar):
    f1[iVar] = iVar + 1 + 0.1*mpi.sRank

syncPrint('P{0}: f1.nX={1}, f1.iBegGlob={2}, f1.x={3},\nf1.u=\n{4}'
          .format(mpi.sRank, f1.nX, f1.iBegGlob, f1.x, f1[:]))

f1.dumpToFile('_test_f1d')
f2 = Field1D.fromFile('_test_f1d')
if mpi.sRank == 0:
    print('-'*80)

syncPrint('P{0}: f2.nX={1}, f2.iBegGlob={2}, f2.x={3},\nf2.u=\n{4}'
          .format(mpi.sRank, f2.nX, f2.iBegGlob, f2.x, f2[:]))

uGlob = f2.uGlob
if mpi.sRank == 0:
    print('-'*80)
syncPrint('P{}: uGlob=\n{}'.format(mpi.sRank, uGlob))
