# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 18:25:46 2015

@author: t.lunet
"""

from casper.fields import Field1D
import matplotlib.pyplot as plt
from casper.mpi import initMPI
initMPI()

f0 = Field1D(nFields=1, nXGlob=50, xEndGlob=1.0, xBegGlob=0.0,
             meshType='XPER')

Field1D.initMesh1D(f0, 'GAUSS')

plt.plot(f0.xCoordLoc, f0.getU())

f1 = f0.restrict(1)
plt.plot(f1.xCoordLoc, f1.getU())

f2 = f0.interpol(1)
plt.plot(f2.xCoordLoc, f2.getU())

plt.show()
