# -*- coding: utf-8 -*-
"""
Created on Tue Oct  6 19:18:46 2015

@author: t.lunet
"""

from casper.fields import Field1D
from casper.mpi import initMPI, mpi
import matplotlib.pyplot as plt

# Initialize MPI communications
initMPI()

f1 = Field1D(1, nX=500)
Field1D.init.initMesh1D(f1, 'SINROPE')
plt.plot(f1[0, :], '-')
plt.title('Solution on space rank {}'.format(mpi.sRank))
plt.show()
