r"""
Casper Library
==============

**Combination of Algorithms for Sequential and Parallel in time and space
differential Equation Resolution**

* `Simpler, Easier, Wider ... CASPER !!!`

Description
-----------
Casper is a python project aiming to build a demonstrator for testing and
developping algorithms for solving all kind of differential equations
(Linear or Non-Linear Ordinary/Partial Differential Equations)
with sequential of parallel algorithm in time and space.

It is based on a representation of a differential equation into this general
form

.. math::
        \frac{du}{dt} = f(u,t) \;, \; u(0)=u_0 \; \; (1)

This last equation is modelized by a combination of three fundamental python
objects :

- Field :
    An object representing all the characteristics of the solution
    :math:`u`
    (values, eventually a mesh grid, or different element of the solution, ...)
    Those objects are stored in the **casper.field** subpackage.
- RHS :
    An object representing the Right-Hand-Side :math:`f(u,t)`, containing all
    the informations of the problem (source term, Jacobian, ...), and can be
    evaluated on a vector containing values :math:`u` for a given Field object,
    and at a given time :math:`t`.
    Those objects are stored in the **casper.rhs** subpackage.
- Phi :
    An object representing a time-stepper or integrator for the problem (1).
    It is basically a generic time integration algorithm, and can be evaluated
    to compute the solution at time :math:`t+\Delta t` from the solution at
    time :math:`t`.
    Those objects are stored in the **casper.phi** subpackage.

All those object are linked through the following relation :

    .. math::
        \text{Phi } \underset{\text{require}}{\rightarrow}
        \text{ RHS } \underset{\text{require}}{\rightarrow}
        \text{ Field}

A base definition of those objects is given in the **base.py** module of each
subpackage. Except for the Fields object, the base implementation are abstract
and can be used as they are. Some of their methods have to be implemented, or
can be overwritten depending on the problem or the algorithm.
The practical implementation are given in separate module of each subpackages.
"""
from . import common, fields, rhs, phi, solver, maths, mpi

# Alias for common subpackage
util = common

__all__ = ['common', 'fields',  'rhs', 'phi', 'solver', 'maths', 'mpi',
           'util']
