#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Description
-----------

Package containing the common utility functions for msg

Development Warning
-------------------
This module should NOT import mpi from casper.mpi (infinite recursion
during chained imports).
"""


def _importMPI():
    from casper.mpi import mpi
    return mpi


def _getRankList(rank, size):
    if isinstance(rank, int):
        return [rank]
    elif rank == 'ALL':
        return range(size)
    elif isinstance(rank, list):
        return rank


def infoMsg(*argv, sRank=0, tRank=0):
    """Print an information message with casper header indicating ranks

    Parameters
    ----------
    *argv :
        The objects to print
    """
    mpi = _importMPI()
    msgToPrint = ""
    if mpi.isPar():
        sRankList = _getRankList(sRank, mpi.sSize)
        tRankList = _getRankList(tRank, mpi.tSize)
        if mpi.sRank not in sRankList or mpi.tRank not in tRankList:
            return
        msgToPrint += "(G{}S{}T{}) - ".format(mpi.gRank, mpi.sRank, mpi.tRank)
    msgToPrint += "INFO : "
    for elt in argv:
        if hasattr(elt, "__str__"):
            msgToPrint += str(elt)
        else:
            raise ValueError("Argument cannot be stringified")
    print(msgToPrint)
