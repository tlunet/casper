#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Description
-----------

Common module containing utilities for code documentation
"""
from functools import partial


def addBaseDoc(child, parent):
    """
    Add inherited documentation of a parent class to a given child class.
    """
    child.__doc__ += '\n    Inherited Base class documentation'
    child.__doc__ += '\n    ----------------------------------\n'
    child.__doc__ += parent.__doc__


_WRAPPER_ASSIGNMENTS = ('__module__', '__name__', '__qualname__', '__doc__',
                        '__annotations__')
_WRAPPER_UPDATES = ('__dict__',)


def _update_wrapper(wrapper, wrapped, assigned=_WRAPPER_ASSIGNMENTS,
                    updated=_WRAPPER_UPDATES):
    """
    Update a wrapper function to look like the wrapped function wrapper is the
    function to be updated wrapped is the original function assigned is a tuple
    naming the attributes assigned directly from the wrapped function to the
    wrapper function (defaults to functools.WRAPPER_ASSIGNMENTS) updated is a
    tuple naming the attributes of the wrapper that are updated with the
    corresponding attribute from the wrapped function
    (defaults to functools.WRAPPER_UPDATES)

    EDIT : Add oneline docstring of wrapper to wrapped, if available.
    """
    for attr in assigned:
        try:
            value = getattr(wrapped, attr)
        except AttributeError:
            pass
        else:
            if attr == '__doc__':
                try:
                    origValue = getattr(wrapper, attr)
                except AttributeError:
                    origValue = None
                else:
                    if origValue is None:
                        origValue = 'no documentation'
                    value += '\n        '+20*'-'+'\n'
                    value += f'\n        wrapping : {origValue}\n'
                    value += '\n        '+20*'-'+'\n'
            setattr(wrapper, attr, value)
    for attr in updated:
        getattr(wrapper, attr).update(getattr(wrapped, attr, {}))
    # Issue #17482: set __wrapped__ last so we don't inadvertently copy it
    # from the wrapped function when updating __dict__
    wrapper.__wrapped__ = wrapped
    # Return the wrapper so this can be used as a decorator via partial()
    return wrapper


def wraps(wrapped, assigned=_WRAPPER_ASSIGNMENTS, updated=_WRAPPER_UPDATES):
    """Decorator factory to apply update_wrapper() to a wrapper function

       Returns a decorator that invokes update_wrapper() with the decorated
       function as the wrapper argument and the arguments to wraps() as the
       remaining arguments. Default arguments are as for update_wrapper().
       This is a convenience function to simplify applying partial() to
       update_wrapper().
    """
    return partial(
        _update_wrapper, wrapped=wrapped, assigned=assigned, updated=updated)
