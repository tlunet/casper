#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Description
-----------

Common module containing utilities for symbolic computation with sympy
"""
import sympy as sy


def initLatexPrinting(darkTheme=True):
    """
    Activate Sympy math printing in IPython terminal, and adapt the foreground
    if a dark theme is used
    """
    if darkTheme:
        sy.init_printing(pretty_print=True, forecolor='White')
    else:
        sy.init_printing(pretty_print=True)


def stopLatexPrinting():
    """Deactivate Sympy math printing in IPython terminal"""
    sy.init_printing(pretty_print=False)
