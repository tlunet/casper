"""
Description
-----------

Module containing common functions and definitions for Casper
"""
from . import parameters, msg, numerics, perfo, doc, plot

from .parameters import getInitPar, checkPar, cstToFunction, getPyObjFromHex
from .msg import infoMsg
from .numerics import dtype, availNumTypes, numType, numGroup
from .perfo import Timer
from .doc import addBaseDoc, wraps
from .plot import gSym, getLastPlotColor


__all__ = ['parameters', 'msg', 'numerics', 'perfo', 'doc', 'plot',
           'getInitPar', 'checkPar', 'cstToFunction', 'getPyObjFromHex',
           'infoMsg',
           'dtype', 'availNumTypes', 'numType', 'numGroup',
           'addBaseDoc', 'wraps',
           'Timer',
           'gSym', 'getLastPlotColor']
