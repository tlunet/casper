#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Description
-----------

Common module containing utilities for object and function parameters
"""
import inspect
import _ctypes


def _importMPI():
    """Utility function to import MPI from within Casper and avoid infinite
    recursion during chained imports"""
    from casper.mpi import mpi
    return mpi


def getInitPar(name, default, instance, **kwargs):
    """
    Function to get a parameter from ****kwargs** arguments

    Parameters
    ----------
    name: ``str``
        The name of the parameter
    default: ``instanceof(name)``
        The default value for the parameter. If default is set to `None`,
        an error is raise if the parameter can not be found in ****kwargs**
    instance: ``instance``
        The instance of the parameter
    **kwargs: ``dictionnary``
        The dictionnary containing the parameter
    """
    stack = inspect.stack()[1]
    module = 'casper.'+stack.filename.split('/casper/')[-1].replace('/', '.')
    function = module.replace('.py', '.'+stack.function)
    mpi = _importMPI()
    par = kwargs.get(name)
    header = ""
    if mpi.parallel:
        header += "(G{}S{}T{}) - ".format(mpi.gRank, mpi.sRank, mpi.tRank)
    header += "In {} : ".format(function)
    if par is None:  # Argument not given in kwargs
        if default is None:  # Default value not given
            raise ValueError(header+"{} not specified in **kwargs"
                             .format(name))
        else:  # Default value given
            par = default
            if mpi.sRank == 0:
                print(header +
                      f"{name} not specified in **kwargs, " +
                      f"setting {default} by default")
    if instance is not None:  # Instance of parameter given
        try:
            par = instance(par)
        except Exception:
            raise ValueError(header+"{} ({}, {}) cannot be converted to {}"
                             .format(name, par, type(par), instance))
    return par


def checkPar(param, name, instance, convert=True):
    """
    Function checking the good instance of a parameter.

    Parameters
    ----------
    param: obj
        The parameter to be checked
    name: str
        Name of the parameter
    instance: type
        Instance of the parameters
    convert: bool
        Wether or not automaticaly convert the parameter to the given type.
        If convert=True and parameter cannot be converted to the wanted type,
        an error is raised.
    """
    if isinstance(param, instance):
        return param
    else:
        try:
            assert convert
            return instance(param)
        except Exception:
            mpi = _importMPI()
            header = ""
            if mpi.parallel:
                header += "(G{}S{}T{}) - ".format(
                    mpi.gRank, mpi.sRank, mpi.tRank)
            stack = inspect.stack()[1]
            module = 'casper.'+stack.filename.split('/casper/')[-1]\
                .replace('/', '.')
            function = module.replace('.py', '.'+stack.function)
            header += "in {}.{} : ".format(module, function)
            raise ValueError(header+"{} not of instance {}"
                             .format(name, instance))


def cstToFunction(val):
    """Utility function that creates a function returning a constant value"""
    def fun(*args, **kwrags):
        return val
    fun.__doc__ = f"Function returning constant value : {val}"
    return fun


def getPyObjFromHex(hexStr):
    """
    Retreive a Python object from its hexadecimal adress in string format.
    Hexadecimal adress from any object can be retreived using hex(id(obj))
    """
    objID = int(hexStr, 16)
    return _ctypes.PyObj_FromPtr(objID)
