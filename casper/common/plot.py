#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Description
-----------

Common module containing utilities for plots and data visualization
"""
import matplotlib.pyplot as plt


# Matplotlib settings to get optimized sizes in graphs (for publication)
plt.rc('font', size=12)
plt.rcParams['lines.linewidth'] = 2
plt.rcParams['axes.titlesize'] = 18
plt.rcParams['axes.labelsize'] = 16
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16
plt.rcParams['xtick.major.pad'] = 5
plt.rcParams['ytick.major.pad'] = 5
plt.rcParams['axes.labelpad'] = 6
plt.rcParams['markers.fillstyle'] = 'none'
plt.rcParams['lines.markersize'] = 7.0
plt.rcParams['lines.markeredgewidth'] = 1.5
plt.rcParams['legend.fontsize'] = 16
plt.rcParams['mathtext.fontset'] = 'cm'
plt.rcParams['mathtext.rm'] = 'serif'
plt.rcParams['figure.max_open_warning'] = 100


lSym = [-1, ['s', 'o', '^', 'p', '<', '*', '>']]


def gSym(init=False):
    """
    Function that can be used to generate different symbols for plots,
    and keep in memory the symbols already used.
    """
    if init:
        lSym[0] = -1
        return None
    lSym[0] = (lSym[0] + 1) % len(lSym[1])
    return lSym[1][lSym[0]]


def getLastPlotColor():
    """Return the color of the last ploted line with plt.plot(...)"""
    return plt.gca().get_lines()[-1].get_color()
