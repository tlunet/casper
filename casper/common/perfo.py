#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Description
-----------

Common module containing utilities for code performance measurments
"""
from time import time


class Timer(object):
    """Implement a timer, equivalent to the "tic; ...; toc" command of Matlab.
    To be used with the "with" python command:

    >>> with Timer():
    >>>     # do stuff ...

    At the begining, a start message will be prompt, and at the end of
    # do stuff ..., a message will be prompt indicating the
    elapsed time. Special credits to Stackoverflow.
    """

    def __init__(self, msg=None, start=False):
        self.msg = msg
        self.tBeg = 0
        self.tEnd = 0
        self.value = 0
        self.running = False
        if start:
            self.start()

    @property
    def clock(self):
        if self.running:
            self.tEnd = time()
        return self.tEnd-self.tBeg+self.value

    def store(self):
        self.value = self.clock
        self.tBeg = time()
        self.tEnd = self.tBeg

    def init(self):
        self.value = 0

    def start(self, init=False):
        if self.running:
            return
        if self.msg is None:
            print('Started timer ...')
        elif self.msg is False:
            pass
        else:
            print(self.msg)
        self.running = True
        self.tBeg = time()

    def stop(self, printElapsed=True, printValue=False):
        if not self.running:
            return
        self.tEnd = time()
        self.running = False
        self.store()
        if printElapsed:
            print('Elapsed time is {:.6f} seconds'.format(self.clock))
        if printValue:
            print('Timer value is {:.6f} seconds'.format(self.value))

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, *args):
        self.stop()

    def __str__(self):
        s = 'Timer, {}running, clock={:.6f}, value={:.6f}'.format(
            'not ' if not self.running else '', self.clock, self.value)
        return s

    def __repr__(self):
        return self.__str__()
