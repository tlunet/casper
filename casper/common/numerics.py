#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the definition of numeric types
"""
import numpy as _np

# Numbers precision
# DICO = {nBytes: [name1, name2, ...], ...}
DICO_FLOATS = {2: ['FLOAT16'],
               4: ['FLOAT32'],
               8: ['FLOAT64', 'FLOAT'],
               16: ['FLOAT128']}
DICO_COMPLEX = {8: ['COMPLEX64'],
                16: ['COMPLEX128', 'COMPLEX'],
                32: ['COMPLEX256']}

for key in list(DICO_FLOATS.keys()):
    if not hasattr(_np, DICO_FLOATS[key][0].lower()):
        DICO_FLOATS.pop(key)
for key in list(DICO_COMPLEX.keys()):
    if not hasattr(_np, DICO_COMPLEX[key][0].lower()):
        DICO_COMPLEX.pop(key)


def _get(nBytes, numType):
    if numType == 0 and nBytes in DICO_FLOATS:
        return DICO_FLOATS[nBytes]
    elif numType == 1 and nBytes in DICO_COMPLEX:
        return DICO_COMPLEX[nBytes]
    else:
        raise ValueError('wrong numeric inputs')


def availNumTypes(floatType=True, complexType=True):
    """Get a list of available numerical type names

    Parameters:
    -----------
    floatType: ``boolean``
        include float types (default=True)
    complexType: ``boolean``
        include complex types (default=True)

    Returns
    -------
    availNumTypes: ``list`` of ``str``
        The list with the name of the available types
    """
    l = []
    if floatType:
        lKeys = list(DICO_FLOATS.keys())
        lKeys.sort()
        for key in lKeys:
            for name in DICO_FLOATS[key]:
                l.append(name)
    if complexType:
        lKeys = list(DICO_COMPLEX.keys())
        lKeys.sort()
        for key in lKeys:
            for name in DICO_COMPLEX[key]:
                l.append(name)
    return l


def numType(*args):
    """Get the name of the numerical type from its number of bytes and type

    >>> numType(numSize, numGroup)
    >>> numType(dtype)

    Parameters
    ----------
    numSize: ``int``
        The number of byte used to store the number
    numGroup: ``int``
        The type (float=0, complex=1) of the number
    dtype: ``numpy.dtype``
        The numpy dtype of the number

    Returns
    -------
    numType: ``str``
        The name of the numerical type
    """
    if len(args) == 2:
        return _get(*args)[0]
    elif len(args) == 1:
        return args[0].__name__.upper()


def numGroup(numType):
    """Get the numerical type from the numerical type name

    Parameters
    ----------
    numType: ``str``
        The name of the numerical type

    Returns
    -------
    numGroup: ``int``
        The numerical type (float=0, complex=1)
    """
    return 0 if 'FLOAT' in numType.upper() else \
        1 if 'COMPLEX' in numType.upper() else None


def dtype(*args):
    """Get the numpy dtype. There is two call possibilities::

    >>> dtype(numSize, numGroup)
    >>> dtype(name)

    Parameters
    ----------
    numSize: ``int``
        The number of byte used to store the number
    numGroup: ``int``
        The type (float=0, complex=1) of the number
    name: ``str``
        The name of the numerical type

    Returns
    -------
    dtype: ``numpy.dtype``
        The numerical type in numpy format
    """
    if len(args) == 2:
        numSize = args[0]
        numGroup = args[1]
        return _np.__dict__[_get(numSize, numGroup)[0].lower()]
    elif len(args) == 1:
        name = args[0].upper()
        if name in availNumTypes():
            for numSize in DICO_FLOATS:
                if name in DICO_FLOATS[numSize]:
                    return dtype(numSize, 0)
            for numSize in DICO_COMPLEX:
                if name in DICO_COMPLEX[numSize]:
                    return dtype(numSize, 1)
        else:
            raise ValueError('wrong numeric type name')
