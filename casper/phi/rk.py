# -*- coding: utf-8 -*-
"""
Description
-----------
Module implementing the Runge-Kutta type time-steppers.
"""
import numpy as np
import sympy as sy
import scipy.optimize as so
import scipy.sparse as sps
import scipy.sparse.linalg as spsl

from casper.phi.base import Phi
from casper import common as util
from casper.maths.butcher import tablesExplicit, tablesImplicit
from casper.mpi import mpi


class ERK(Phi):
    r"""
    Defines an Explicit Runge-Kutta (ERK) time stepper.
    One step of the ERK method can be described following

    .. math::
        U(t+\Delta t) = U(t) + \sum_{j=1}^{s}b_j k_j

    with :math:`k_j` defined as

    .. math::
        k_j = \Delta t \; f \left(t +\Delta t c_j,
        U(t) + \sum_{l=1}^{s}a_{j,l} k_l \right)

    The :math:`a_{i,j}`, :math:`b_{i}` and :math:`c_{i}` are the Butcher
    coefficients, and :math:`s=size(b)` is the number of stages (also, the
    number of times the rhs operator :math:`f` has to be evaluated).

    For ERK methods, the :math:`a_{i,j}` coefficients represent a  lower
    triangular matrix (zeros on the diagonal), such that
    :math:`k_j = function(k_{j-1}, k_{j-2}, ...)` can be computed sequentially,
    hence the method qualification of "explicit".

    All available ERK methods have their Butcher table in the
    **tablesExplicit** attribute of the **casper.maths.butcher.py** module.

    Parameters
    ----------
    rhs : ``casper.rhs.base.RHS``
        The rhs operator of the problem.
    scheme : ``str``, optional
        The ID of the ERK method
        (default='RK4', classical fourth order in four stages).
    dt : ``float``, optional
        If specified, a constant time-step value for all time integration,
        that overwrite any time-step value passed in the **eval** method.
        (default=None, i.e default behavior).
    sparseFormat : ``str``, optional
        If specified, a sparse format from **scipy.sparse** used for the
        representation of the Jacobian matrix of the Phi operator, used only
        when the rhs operator is homogeneous and a constant time-step **dt**
        is specified (default=None).

    Implementations
    ---------------
    Depending on the rhs properties and the ERK method considered, several
    major implementations are available, listed below in decreasing order of
    priority.

    - Standard :
        general implementation of the Explicit Runge-Kutta method, requires
        to store each :math:`k_j` during one time step
        (**k** attribute, of size **nDOF** x **nStages**), plus a temporary
        variable of the same size for computations
        (**tmp** attribute, of size **nDOF**).

    - Low-Storage :
        when only the lower diagonal of the :math:`a_{i,j}` coefficients are
        non-zeros, then :math:`k_j = function(k_{j-1})` and the next time-step
        solution can be computed faster while requiering less memory usage
        (only one :math:`k_j` is stored in the **k** attribute, of size
        **nDOF**).
        This implementation is automatically used if
        **tablesExplicit[scheme][lowStorage]==True**.

    - Homogeneous :
        if the rhs operator is homogeneous, and a constant time-step **dt** is
        specified when initializing the time stepper, then the Jacobian matrix
        of the ERK method is easily computed explicitely, and time-stepping is
        performed using a direct matrix-vector multiplication :

        .. math::
            U(t+\Delta t) = g(A)U(t),

        where A is the Jacobian matrix of the rhs operator, and g the
        stability function of the ERK method
        (explicit => :math:`g` is a polynomial).
        The Jacobian matrix (of size **nDOF** x **nDOF**) is stored under the
        **phiEX** attribute, that keeps the eventual sparse format of the
        rhs Jacobian.
        In that case, the **computeJacobian** and **evalJacobian** methods
        are modified accordingly.

    Warning
    -------
    Homogeneous implementation not available when using space parallelization.
    """
    LOWSTORAGE = False

    def __init__(self, rhs, scheme='RK4', dt=None, sparseFormat=None):
        super().__init__(rhs)

        if scheme not in tablesExplicit:
            raise ValueError(
                'ERK time stepper not implemented with' +
                f'{scheme} discretization scheme')

        but = tablesExplicit[scheme]
        A = np.array(but['A'])
        b = np.array(but['b'])
        c = np.array(but['c'])
        nStages = b.size

        A.flags['WRITEABLE'] = False
        b.flags['WRITEABLE'] = False
        c.flags['WRITEABLE'] = False

        if rhs.HOMOGENEOUS and dt is not None:

            # Compute Taylor coefficients of the stability function
            lCoeff = [np.sum(b.dot(np.linalg.matrix_power(A, i)))
                      for i in range(nStages)]

            # Initial setup
            dt = float(dt)
            tmp = sps.eye(self.nDOF)
            phiEx = 1*tmp

            # Compute RHS Jacobian matrix product with dt,
            # checking eventual sparsity format
            dtA = self.rhs.computeJacobian(None, None).copy()
            if sps.issparse(dtA) and sparseFormat is None:
                sparseFormat = dtA.format
            dtA *= dt

            # Compute amplification factor (Jacobian matrix) of time stepper
            for c in lCoeff:
                tmp = tmp.dot(dtA)
                phiEx += c*tmp

            # Convert to sparse format, if necessary
            if sparseFormat:
                phiEx = eval(f'phiEx.to{sparseFormat}()')

            # Setup eval method and associated attributes
            if not mpi.spaceParallel:
                self.dt = dt
                self.FIXED_DT = True
                self.phiEx = phiEx
                self.LINEAR = True
                self.eval = self._eval_HOMOGENEOUS
                self.computeJacobian = self._computeJacobian_HOMOGENEOUS
                self.evalJacobian = self._evalJacobian_HOMOGENEOUS
            else:
                raise ValueError('HOMOGENEOUS eval for ERK not implemented ' +
                                 'with space parallelization')
        else:

            # Standart ERK implementation for non-homogeneous problem
            if but.get('lowStorage', False):
                # -- low storage implementation
                self.LOWSTORAGE = True
                self.eval = self._eval_LOWSTORAGE
                self.k = np.empty_like(rhs.field.flat)
                self.k[:] = 0
            else:
                # -- classical implementation (use the default eval function)
                self.k = np.empty((nStages, rhs.nDOF), dtype=rhs.field.dtype)
                self.tmp = np.empty(rhs.nDOF, dtype=rhs.field.dtype)
                # Initialize memory
                self.tmp[:] = 0
                self.k[:, :] = 0

        # Store attributes
        self.scheme = scheme
        self.A = A
        self.b = b
        self.c = c

    @property
    def nStages(self):
        """Number of stage of the Explicit Runge-Kutta method"""
        return self.b.size

    @util.wraps(Phi.eval)
    def eval(self, u, t, dt, uNext):
        """Standard Explicit Runge-Kutta implementation"""
        # -- set data pointers
        tmp = self.tmp
        k = self.k
        A = self.A
        b = self.b
        c = self.c
        # -- copy values of u into uNext
        np.copyto(uNext, u)
        # -- first stage
        self.evalRHS(u, t, k[0])
        k[0] *= dt
        uNext += b[0]*k[0]
        # -- loop for last stages
        for i in range(1, self.nStages):
            np.copyto(tmp, u)
            for j in range(i):
                tmp += A[i, j]*k[j]
            self.evalRHS(tmp, t + c[i]*dt, k[i])
            k[i] *= dt
            uNext += b[i]*k[i]
        return uNext

    @util.wraps(Phi.eval)
    def _eval_LOWSTORAGE(self, u, t, dt, uNext):
        """Low-Storage Explicit Runge-Kutta implementation"""
        # -- set data pointers
        k = self.k
        A = self.A
        b = self.b
        c = self.c
        # -- copy values of u into uNext
        np.copyto(uNext, u)
        # -- first stage
        self.evalRHS(u, t, k)
        k[:] *= dt
        uNext += b[0]*k
        # -- loop for last stages with low storage
        for i in range(1, self.nStages):
            k[:] *= A[i, i-1]
            k[:] += u
            self.evalRHS(k, t + c[i]*dt, k)
            k[:] *= dt
            uNext += b[i]*k
        return uNext

    @util.wraps(Phi.eval)
    def _eval_HOMOGENEOUS(self, u, t, dt, uNext):
        """Homogeneous Explicit Runge-Kutta implementation"""
        # -- evaluate phi into uNext
        np.copyto(uNext, self.phiEx.dot(u))
        return uNext

    @util.wraps(Phi.computeJacobian)
    def _computeJacobian_HOMOGENEOUS(self, u, t, dt, eps=1e-8):
        """Homogeneous Explicite Runge-Kutta implementation"""
        return self.phiEx

    @util.wraps(Phi.evalJacobian)
    def _evalJacobian_HOMOGENEOUS(self, uJac, tJac, dtJac, u, uEval=None):
        """Homogeneous Explicite Runge-Kutta implementation"""
        return self.phiEx.dot(u)


class IRK(Phi):
    r"""
    Defines an Implicit Runge-Kutta (IRK) time stepper.
    One step of the IRK method can be described following

    .. math::
        U(t+\Delta t) = U(t) + \sum_{j=1}^{s}b_j k_j

    with :math:`k_j` defined as

    .. math::
        k_j = \Delta t \; f \left(t +\Delta t c_j,
        U(t) + \sum_{l=1}^{s}a_{j,l} k_l \right)

    The :math:`a_{i,j}`, :math:`b_{i}` and :math:`c_{i}` are the Butcher
    coefficients, and :math:`s=size(b)` is the number of stages..

    For IRK methods, the :math:`a_{i,j}` coefficients represent square matrix
    with (at least one) non-zero values on the diagonal, such that
    :math:`k_j = function(..., k_{j+1}, k_j, k_{j-1}, k_{j-2}, ...)`.
    This requires then for each time step the solution of an eventualy
    non-linear system of equations to compute the :math:`k_j

    All available IRK methods have their Butcher table in the
    **tablesImplicit** attribute of the **casper.maths.butcher.py** module.

    Parameters
    ----------
    rhs : ``casper.rhs.base.RHS``
        The rhs operator of the problem.
    scheme : ``str``, optional
        The ID of the IRK method
        (default='BE', Backward Euler method).
    dt : ``float``, optional
        If specified, a constant time-step value for all time integration,
        that overwrite any time-step value passed in the **eval** method.
        (default=None, i.e default behavior).
    sparseFormat : ``str``, optional
        If specified, a sparse format from **scipy.sparse** used for the
        representation of the Jacobian matrix of the Phi operator, used only
        when the rhs operator is homogeneous and a constant time-step **dt**
        is specified (default=None).

    Implementations
    ---------------
    Depending on the rhs properties and the IRK method considered, several
    major implementations are available, listed below in decreasing order of
    priority.

    - Standard :
        general (non optimized) implementation for any implicit Runge-Kutta
        method, that solves at each time step the following system of equations

        .. math::
            k_j - \Delta t \; f \left(t +\Delta t c_j,
            U(t) + \sum_{l=1}^{s}a_{j,l} k_l \right) = 0,

        using a general Newton-Krylov solver for large system from
        **scipy.optimize**, and :math:`U(t)` as initial guess.

    - Homogeneous :
        if the rhs operator is homogeneous, and a constant time-step **dt** is
        specified when initializing the time stepper, then the Jacobian matrix
        of the IRK method has an analytic expression, and time-stepping is
        obtained using :

        .. math::
            U(t+\Delta t) = g(A)U(t),

        where :math:`A` is the Jacobian matrix of the rhs operator, and
        :math:`g` the stability function of the IRK method.
        :math:`g` is a rational function of the form

        .. math::
            g(x) = \frac{d(x)}{n(x)},

        where :math:`d` and :math:`n` are polynomials.
        Time-stepping is obtained the following formula :

        .. math::
            U(t+\Delta t) = d(A)[n(A)]^{-1}U(t).

        For numerical accuracy reason, the inverse of :math:`n(A)` is
        never computed, but a sparse LU decomposition from
        **scipy.sparse.linalg** is used. The LU decomposition of :math:`n(A)`
        is stored under the **phiIm** attribute, while the :math:`d(A)`
        matrix is stored under the **phiEx** attribute (only if not unity).

    Warning
    -------
    Homogeneous implementation not available when using space parallelization.
    """

    def __init__(self, rhs, scheme='BE', dt=None, sparseFormat=None):
        super().__init__(rhs)

        if scheme not in tablesImplicit:
            raise ValueError(
                'IRK time stepper not implemented with' +
                f'{scheme} discretization scheme')

        but = tablesImplicit[scheme]
        A = np.array(but['A'])
        b = np.array(but['b'])
        c = np.array(but['c'])
        nStages = b.size

        A.flags['WRITEABLE'] = False
        b.flags['WRITEABLE'] = False
        c.flags['WRITEABLE'] = False

        if rhs.HOMOGENEOUS and dt is not None:

            # Compute stability function as a fraction D/N
            e = sy.Matrix(np.ones(nStages))
            mI = sy.Matrix(np.eye(nStages))
            z = sy.Symbol('z')
            D = sy.det(mI-(A-e*b.T)*z)
            N = sy.det(mI-A*z)

            # Function to extract polynomial coefficients from D and N
            def getNumPolyCoeff(expr):
                expr = sy.simplify(expr)
                d, n = sy.fraction(expr)
                if n == 1:
                    if z in d.free_symbols:
                        d = sy.Poly(d)
                        return [float(c) for c in d.all_coeffs()]
                    else:
                        return [float(d)]
                else:
                    d = sy.Poly(d)
                    n = sy.Poly(n)
                    nTerms = sy.degree(d)-sy.degree(n)+1
                    d = sy.Poly(sy.series(D, n=nTerms).removeO())
                    return [float(c) for c in d.all_coeffs()]

            # Get rationnal coefficient of stability function
            coeffEx = getNumPolyCoeff(D)
            coeffIm = getNumPolyCoeff(N)

            # Compute RHS Jacobian matrix product with dt,
            # checking eventual sparsity format
            dt = float(dt)
            dtA = self.rhs.computeJacobian(None, None).copy()
            if sps.issparse(dtA) and sparseFormat is None:
                sparseFormat = dtA.format
            dtA *= dt

            # Compute explicit part of phi =: phiEx
            if len(coeffEx) < 2:
                phiEx = None
            else:
                tmp = sps.eye(self.nDOF)
                phiEx = 1*tmp
                for c in coeffEx[-2::-1]:
                    tmp = tmp.dot(dtA)
                    phiEx += c*tmp

            # Compute implicit part of phi =: phiIm
            tmp = sps.eye(self.nDOF).tocsc()
            phiIm = 1*tmp
            for c in coeffIm[-2::-1]:
                tmp = tmp.dot(dtA)
                phiIm += c*tmp

            # Compute sparse LU decomposition
            phiImLU = spsl.splu(phiIm)

            # Convert to sparse format, if necessary
            if sparseFormat:
                if phiEx is not None:
                    phiEx = eval(f'phiEx.to{sparseFormat}()')
                phiIm = eval(f'phiIm.to{sparseFormat}()')

            if not mpi.spaceParallel:
                self.dt = dt
                self.phiEx = phiEx
                self.phiIm = phiIm
                self.phiImLU = phiImLU
                self.eval = self._eval_HOMOGENEOUS
            else:
                raise NotImplementedError(
                    'HOMOGENEOUS eval for IRK not implemented ' +
                    'with space parallelization')
        else:
            self.k = np.empty(nStages*rhs.nDOF, dtype=rhs.field.dtype)
            self.tmp = np.empty(rhs.nDOF, dtype=rhs.field.dtype)

        # Store attributes
        self.scheme = scheme
        self.A = A
        self.b = b
        self.c = c

    @property
    def nStages(self):
        return self.b.size

    def implicitSolve(self, u, t, dt):
        """Implicit Newton-Krylov solver used for each time step"""
        # Definition of the function to solve
        def func(x):
            diff = self.k
            tmp = self.tmp
            s = u.size
            np.copyto(diff, x)
            for j in range(self.nStages):
                np.copyto(tmp, u)
                for l in range(self.nStages):
                    if self.A[j, l] != 0:
                        tmp += (dt*self.A[j, l])*x[s*l: s*(l+1)]
                diff[s*j: s*(j+1)] -= self.evalRHS(tmp, t+self.c[j]*dt)
            return diff

        return so.newton_krylov(func, np.hstack((u,)*self.nStages))

    @util.wraps(Phi.eval)
    def eval(self, u, t, dt, uNext):
        """Standard Implicit Runge-Kutta implementation"""
        # -- solve IRK first steps system along all stages
        np.copyto(self.k, self.implicitSolve(u, t, dt))
        # -- last IRK step
        s = self.nDOF
        np.copyto(uNext, u)
        for i in range(self.nStages):
            uNext += dt*self.b[i]*self.k[s*i: s*(i+1)]
        return uNext

    @util.wraps(Phi.eval)
    def _eval_HOMOGENEOUS(self, u, t, dt, uNext):
        """Homogeneous Implicit Runge-Kutta implementation"""
        if self.phiEx is None:
            np.copyto(uNext, self.phiImLU.solve(u))
        else:
            np.copyto(uNext, self.phiImLU.solve(self.phiEx.dot(u)))
        return uNext


def RK(rhs, scheme, dt=None, sparseFormat=None):
    """
    Utility function that initialize an implicit of explicit Runge-Kutta
    time-stepper (see their documentation for more details)
    """
    if scheme in tablesExplicit:
        return ERK(rhs, scheme, dt, sparseFormat)
    elif scheme in tablesImplicit:
        return IRK(rhs, scheme, dt, sparseFormat)
    else:
        raise NotImplementedError(
            'RungeKutta stepper of type {} not implemented'.format(scheme))


# Append inherited documentation
for child in [ERK, IRK]:
    util.addBaseDoc(child, Phi)
