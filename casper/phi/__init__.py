"""
Description
-----------

Package containing the definitions of time-stepper objects, inheriting the base
class Phi, defined in base.py.

Contains
--------
TODO
"""
from . import base, rk, sdc, krylov, expo

ERK = rk.ERK
IRK = rk.IRK
RK = rk.RK

__all__ = ['base', 'rk', 'sdc', 'krylov', 'expo',
           'ERK', 'IRK', 'RK']
