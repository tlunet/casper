# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the Exponential type solver objects

Classes
-------
ExpLinSolver : Inherited (TimeSolver)
    define an exponential solver for linear problem
ExpNLSolver : Inherited (TimeSolver)
    define an exponential solver for non linear problem
"""
import numpy as _np
import scipy as _sp

from .base import Phi
from .. import common as util


class ExpLinSolver(Phi):
    """
    DOC TODO
    """

    def __init__(self, f0, spaceOp,
                 stepping, timeStorage, dumpTimes, **kwargs):
        Phi.__init__(self, f0, spaceOp,
                     stepping, timeStorage, dumpTimes, **kwargs)
        self.expAlgo = \
            util.getInitPar('expAlgo', 'EXPM', str, self.__init__, **kwargs)

        # Initialize exponential algorithm
        self.mA = spaceOp.computeJacobian(f0.getU(), 0)

        if self.expAlgo == 'EXPM':
            def _expAlgo(mA, dt):
                mA *= dt
                return _sp.linalg.expm(mA)
        elif self.expAlgo == 'DIAG':
            def _expAlgo(mA, dt):
                d, mE = _np.linalg.eig(mA)
                d *= dt
                d = _np.diag(_np.exp(d))
                return mE.dot(d.dot(_np.linalg.inv(mE))).real

        _expAlgo.__doc__ = self._expAlgo.__doc__
        self._expAlgo = _expAlgo

    def iterate(self, u, time, dt, uNext):
        """Overloaded method, iterate the solver between the current time
        and the next current time

        Parameters
        ----------
        u : `numpy.array`
            The field(s) to evaluate into vector form
        time : `float`
            The current time
        dt : `float`
            The current dt
        uNext : ``numpy.array(float)``
            The numpy array where to store field after the iteration
        """
        expA = self._expAlgo(self.mA, dt)
        _np.copyto(uNext, expA.dot(u))

    def _expAlgo(mH, dt):
        raise NotImplementedError()


class ExpNLSolver(Phi):
    """
    DOC TODO
    """

    def __init__(self, f0, spaceOp,
                 stepping, timeStorage, dumpTimes, **kwargs):
        Phi.__init__(self, f0, spaceOp,
                     stepping, timeStorage, dumpTimes, **kwargs)
        self.phiAlgo = \
            util.getInitPar('phiAlgo', 'EXPM', str, self.__init__, **kwargs)

        sizeF = f0.getU().size
        self.mA = _np.empty((sizeF, sizeF))
        self.vB = _np.empty((sizeF))

        # Initialize exponential algorithm
        if self.phiAlgo == 'EXPM':
            def _phiAlgo(mA, dt):
                mAinv = _np.linalg.inv(mA)
                return (_sp.linalg.expm(dt*mA)-_np.eye(mA.shape[0])).dot(mAinv)
        elif self.phiAlgo == 'DIAG':
            def _phiAlgo(mA, dt):
                d, mE = _np.linalg.eig(mA)
                d = _np.diag((_np.exp(dt*d)-1)/d)
                return mE.dot(d.dot(_np.linalg.inv(mE))).real

        _phiAlgo.__doc__ = self._phiAlgo.__doc__
        self._phiAlgo = _phiAlgo

    def iterate(self, u, t, dt, uNext):
        """Overloaded method, iterate the solver between the current time
        and the next current time

        Parameters
        ----------
        u: `numpy.array`
            The field(s) to evaluate into vector form
        t: `float`
            The current time
        dt: `float`
            The current dt
        uNext: ``numpy.array(float)``
            The numpy array where to store field after the iteration
        """
        # Evaluate Jacobian and b term at t0
        _np.copyto(self.mA, self.spaceOp.computeJacobian(u, t))
        self.evalSpaceOp(u, t, self.vB)
        # Solve linear part
        uLin = self._phiAlgo(self.mA, dt).dot(self.vB)
        # Non linear correction
        # TODO : implementation
        # Compute next step
        _np.copyto(uNext, u)
        uNext += uLin

    def _phiAlgo(mH, dt):
        raise NotImplementedError()
