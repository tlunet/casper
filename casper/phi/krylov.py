# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the Krylov type solver objects

Classes
-------
KryLinSolver : Inherited (TimeSolver)
    define an exponential solver using Krylov subspace approximation
    for linear problem
"""
import numpy as _np
import scipy as _sp
import matplotlib.pyplot as plt

from .base import Phi
from .. import common as util
from ..maths import quadrature as _lob, krylov as _kry


class KryLinSolver(Phi):
    """
    DOC TODO
    """

    plotEigenvalues = False
    plotHarmonicRitz = False
    testArnoldi = False
    useCorrec = False
    removeCstPart = False

    def __init__(self, f0, spaceOp,
                 stepping, timeStorage, dumpTimes, **kwargs):
        Phi.__init__(self, f0, spaceOp,
                     stepping, timeStorage, dumpTimes, **kwargs)
        self.nK = \
            util.getInitPar('nK', 20, int, self.__init__, **kwargs)
        self.expAlgo = \
            util.getInitPar('expAlgo', 'EXPM', str, self.__init__, **kwargs)
        self.arnoldiType = \
            util.getInitPar('Arnoldi', 'Default', str, self.__init__, **kwargs)

        # Initialize exponential algorithm
        if self.expAlgo == 'EXPM':
            def _expAlgo(mH, dt):
                mH *= dt
                return _sp.linalg.expm(mH)
        elif self.expAlgo == 'DIAG':
            # Meaning of this HR \ It is not the extended Hessenberg matrix
            def _expAlgo(mH, dt):
                d, mE = _np.linalg.eig(mH)
                mHR = \
                    mH + \
                    abs(self.mH[-1, -1]**2) * \
                    _np.linalg.inv(mH.T).dot(_np.ones((self.nK, self.nK)))
                dHR, mHR = _np.linalg.eig(mHR)
                # For vizualisation : plot eingenvalues of Hessien matrix
                if KryLinSolver.plotEigenvalues:
                    plt.figure('Eigenvalues')
                    plt.plot(d.real, d.imag, 'o', label=str(self.nK))
                    if self.plotHarmonicRitz:
                        plt.plot(dHR.real, dHR.imag, 'o',
                                 label='HR'+str(self.nK))
                    plt.legend()
                d *= dt
                d = _np.diag(_np.exp(d))
                return mE.dot(d.dot(_np.linalg.inv(mE))).real

        if self.arnoldiType == 'Default':
            self._initArnoldi(f0)
        # TODO : modify harnoldi for compatibility
        # elif self.arnoldiType == 'Householder':
        #     self._initHarnoldi(f0)
        #     self._arnoldi = self._harnoldi
        else:
            raise ValueError('Wrong value for arnoldi algorithm type')

        _expAlgo.__doc__ = self._expAlgo.__doc__
        self._expAlgo = _expAlgo

    def iterate(self, u, time, dt, uNext):
        """Overloaded method, iterate the solver between the current time
        and the next current time

        Parameters
        ----------
        u : `numpy.array`
            The field(s) to evaluate into vector form
        time : `float`
            The current time
        dt : `float`
            The current dt
        uNext : ``numpy.array(float)``
            The numpy array where to store field after the iteration
        """
        if KryLinSolver.removeCstPart:
            # Removing constant part
            e = _np.ones_like(u)
            e /= _np.linalg.norm(e)
            u0c = u.dot(e)*e
            u0Tilde = u - u0c
        else:
            # Standard method
            u0Tilde = u.copy()

        # For vizualisation : plot eigenvalues of the operator
        if KryLinSolver.plotEigenvalues:
            A = self.spaceOp.computeJacobian(u, time)
            d = _np.linalg.eigvals(A)
            plt.figure('Eigenvalues')
            plt.plot(d.real, d.imag, '.r')

        # Generate Krylov basis by arnoldi process
        def evalFunc(u, uEval):
            self.evalSpaceOp(u, time, uEval)
        normU = _kry.arnoldi(u0Tilde, evalFunc, self.nK,
                             self.mV, self.mH, self.vW)

        # Performe exponential propagation in Krylov subspace
        if not KryLinSolver.useCorrec:
            # Standard method
            expH = self._expAlgo(self.mH[:-1, :], dt)
            prodVH = self.mV[:, :-1].dot(expH)
        else:
            # Use the residual arnoldi residual
            mHBar = _np.hstack((self.mH, _np.zeros((self.nK+1, 1))))
            expH = self._expAlgo(mHBar, dt)
            prodVH = self.mV.dot(expH)
        _np.copyto(uNext, prodVH[:, 0])
        uNext *= normU

        # Eventually add constant part to final solution
        if KryLinSolver.removeCstPart:
            uNext += u0c

    def _initHarnoldi(self, f0):
        nLines = f0.getU().size
        dataType = f0.getU().dtype
        self.mHe = _np.zeros((self.nK+1, self.nK+1), dtype=dataType)
        self.mH = _np.zeros((self.nK+1, self.nK), dtype=dataType)
        self.vW = _np.zeros((nLines), dtype=dataType)
        self.mQ = _np.eye(nLines, dtype=dataType)
        self.mZ = _np.zeros((nLines, self.nK+1), dtype=dataType)
        self.mV = _np.zeros((nLines, self.nK+1), dtype=dataType)
        self.nLines = nLines

    # Householder Arnoldi algorithm for square matrices
    def _harnoldi(self, u):
        # Test Householder Arnoldi
        if KryLinSolver.plotEigenvalues:
            A = self.spaceOp.computeJacobian(u, 0)
            d = _np.linalg.eigvals(A)
            plt.figure('Eigenvalues')
            plt.plot(d.real, d.imag, '.r')
            # Initial vector
        norm = _np.linalg.norm
        normU = norm(u)
        self.mZ[:, 0] = u/normU
        # Start of the loop
        for i in range(self.nK+1):
            # Create Householder matrix
            P = _np.eye(self.nLines)
            P[i:, i:] = self._makeHouseholder(self.mZ[i:, i])
            # Computation of the triangular matrix
            self.mHe[:, i] = _np.dot(P, self.mZ[:, i])[:self.nK+1]
            # Computation of the matrix of column vectors V
            self.mQ = _np.dot(P, self.mQ)
            self.mV[:, i] = _np.transpose(self.mQ[i, :])
            if i < self.nK:
                self.evalSpaceOp(self.mV[:, i], 0, self.vW)
                self.mZ[:, i+1] = _np.dot(self.mQ, self.vW)
        self.mH = self.mHe[:self.nK+1, 1:self.nK+1]
        if KryLinSolver.testArnoldi:
            plt.figure('H ({})'.format(self.nK))
            plt.imshow(self.mH, interpolation='none')
            plt.colorbar()
            plt.figure('VtV-I ({})'.format(self.nK))
            Vt = self.mV[:, :-1]
            plt.imshow(Vt.T.dot(Vt)-_np.eye(self.nK), interpolation='none')
            plt.colorbar()
            print('---------- Householder Arnoldi -----------')
            print('norm VtV-I :', norm(Vt.T.dot(Vt)-_np.eye(self.nK)))
            print('proj :', norm(_np.dot(A, self.mV[:, :-1]) -
                                 _np.dot(self.mV, self.mH)) / norm(A))
            print('------------------------------')
        return normU

    def _makeHouseholder(self, z):
        v = z / (z[0] + _np.copysign(_np.linalg.norm(z), z[0]))
        v[0] = 1
        P = _np.eye(z.shape[0])
        P -= (2 / _np.dot(v, v)) * _np.dot(v[:, None], v[None, :])
        return P

    def _initArnoldi(self, f0):
        nLines = f0.getU().size
        dataType = f0.getU().dtype
        self.mV = _np.zeros((nLines, self.nK+1), dtype=dataType)
        self.mH = _np.zeros((self.nK+1, self.nK), dtype=dataType)
        self.vW = _np.zeros((nLines), dtype=dataType)

    def _arnoldi(self, u):
        V = self.mV
        H = self.mH
        w = self.vW
        norm = _np.linalg.norm
        normU = norm(u)
        _np.copyto(V[:, 0], u)
        V[:, 0] /= normU
        # print('------- Arnoldi Residii -------')
        for j in range(self.nK):
            self.evalSpaceOp(V[:, j], 0, w)
            for i in range(j+1):
                H[i, j] = _np.dot(V[:, i], w)
                w -= H[i, j]*V[:, i]
            H[j+1, j] = norm(w)
            # print("h[{},{}]".format(j+1, j), H[j+1, j])
            _np.copyto(V[:, j+1], w)
            V[:, j+1] /= H[j+1, j]

        if KryLinSolver.testArnoldi:
            A = 0
            plt.figure('H ({})'.format(self.nK))
            plt.imshow(H, interpolation='none')
            plt.colorbar()
            plt.figure('VtV-I ({})'.format(self.nK))
            Vt = V[:, :-1]
            plt.imshow(Vt.T.dot(Vt)-_np.eye(self.nK), interpolation='none')
            plt.colorbar()
            print('---------- Arnoldi -----------')
            print('norm VtV-I :', norm(Vt.T.dot(Vt)-_np.eye(self.nK)))
            print('proj :', norm(_np.dot(A, V[:, :-1])-_np.dot(V, H)) /
                  norm(A))
            print('------------------------------')

        return normU

    def _expAlgo(mH, dt):
        raise NotImplementedError()

    def getNFlopDt(self):
        """Abstract method, get the number of flop per iteration"""
        # TODO : implementation
        return 0.0

    def getTheoricalNEval(self):
        """Abstract method, get the theorical number of evaluation for
        the entire simulation"""
        # TODO : implementation
        return 0.0


class KryNLSolver(Phi):
    """
    DOC TODO
    """

    def __init__(self, f0, spaceOp,
                 stepping, timeStorage, dumpTimes, **kwargs):
        Phi.__init__(self, f0, spaceOp,
                     stepping, timeStorage, dumpTimes, **kwargs)
        self.nK = \
            util.getInitPar('nK', 20, int, self.__init__, **kwargs)
        self.nNLIter = \
            util.getInitPar('nNLIter', 2, int, self.__init__, **kwargs)
        self.phiAlgo = \
            util.getInitPar('phiAlgo', 'EXPM', str, self.__init__, **kwargs)
        self._initArnoldi(f0)
        self._initQuadratureModel(**kwargs)
        self._initTabArnoldi()

        # Initializa phi function computation
        if self.phiAlgo == 'EXPM':
            def _phiAlgo(mH, dt):
                mHinv = _np.linalg.inv(mH)
                return (_sp.linalg.expm(dt*mH)-_np.eye(mH.shape[0])).dot(mHinv)
        elif self.phiAlgo == 'DIAG':
            def _phiAlgo(mH, dt):
                d, mE = _np.linalg.eig(mH)
                d = _np.diag((_np.exp(dt*d)-1)/d)
                return mE.dot(d.dot(_np.linalg.inv(mE))).real

        _phiAlgo.__doc__ = self._phiAlgo.__doc__
        self._phiAlgo = _phiAlgo

    def _initQuadratureModel(self, **kwargs):
        """Initialize the quadrature model"""
        func = self._initQuadratureModel

        quadForm = util.getInitPar('quadForm', 'CHEBYSHEV', str,
                                   func, **kwargs)
        nQuad = util.getInitPar('nQuad', 4, int, func, **kwargs)
        if quadForm == 'LEGENDRE':
            points = _lob.legendreQuadPoints(nQuad)  # points in [-1,1]
        elif quadForm == 'CHEBYSHEV':
            points = _lob.chebyshevSKQuadPoints(nQuad)  # points in (-1,1)
        elif quadForm == 'UNIF':
            points = _np.linspace(-1., 1., nQuad)    # points in [-1,1]
        else:
            raise ValueError("quadform {} not implemented".format(quadForm))
        # util.infoMsg('SDC Quadrature points : {}'.format(points))
        # Normalized quadrature times in [0,1]
        quadTimes = 0.5*points + 0.5
        self.quadTimes = quadTimes
        self.quadDt = _np.ediff1d(quadTimes)
        # Store parameters
        self.nQuad = nQuad
        self.quadForm = quadForm

    def _initTabArnoldi(self):
        """Initialize the current iteration list"""
        self.mHlist = \
            [_np.empty_like(self.mH) for i in range(self.nQuad)]
        self.mElist = \
            [_np.empty_like(self.mH) for i in range(self.nQuad)]
        self.vdlist = \
            [_np.empty_like(self.mH[:, 0]) for i in range(self.nQuad)]
        self.mdlist = \
            [_np.empty_like(self.mH) for i in range(self.nQuad)]
        self.mVlist = \
            [_np.empty_like(self.mV) for i in range(self.nQuad)]
        self.vBlist = \
            [_np.empty_like(self.vB) for i in range(self.nQuad)]
        self.normUlist = \
            [0.0 for i in range(self.nQuad)]

    def iterate(self, u, t, dt, uNext):
        """Overloaded method, iterate the solver between the current time
        and the next current time

        Parameters
        ----------
        u: `numpy.array`
            The field(s) to evaluate into vector form
        t: `float`
            The current time
        dt: `float`
            The current dt
        uNext: ``numpy.array(float)``
            The numpy array where to store field after the iteration
        """
        # Store y the linear approximation, Ry remainder, mCoeff collocation c.
        y0 = _np.zeros((u.size, self.nQuad))
        y = _np.zeros((u.size, self.nQuad))
        Ry = _np.empty_like(y)
        # Evaluate the first approximation of the linear solution and remainder
        nodesCurrent = self.getCurrentDt()*self.quadTimes
        for i in range(self.nQuad):
            self._phiFAlgo(u, t, nodesCurrent[i], y0[:, i])
        # Linear solution (initial approximation)
        _np.copyto(y, y0)
        print("--- Linear DONE ---")
        for i in range(self.nNLIter):
            # Evaluate the remainder term
            for i in range(0, self.nQuad):
                Ry[:, i] = self._evalNLTerm(u, t + nodesCurrent[i],
                                            y[:, i], Ry[:, i])
            # Store Collocation coefficients
            mCoeff = 0  # _col.collCoeff(nodesCurrent[:], Ry[:, :])
            # self.mCoeff = mCoeff
            # Remember it is not needed to store this values!
            # It is useful for debugging
            # Form a Krylov space of size nK
            # TODO : Modify the function arnoldi to accept different nK size
            print("--- Arnoldi Inner Init ---")
            for i in range(1, self.nQuad):
                self.normUlist[i] = self._arnoldi(u, t, mCoeff[:, i])
                self.mHlist[i] = self.mH
                self.mVlist[i] = self.mV
                print("--- Arnoldi Inner DONE ---")
            # Evaluate the convolution integrals
            # We start at phi_3 because the first term is t^2
            # TODO Compute quadrature integral at each node
                self.vdlist[i], self.mElist[i] = _np.linalg.eig(self.mHlist[i])
                # self.vdlist[i] = _sp.misc.factorial(i+2) * \
                # _phiF.vPhiFunction(i+3, self.vdlist[i],
                #                    self.getCurrentDt())
            # It contains the matrix of the diagonal phiFunction
                self.mdlist[i] = _np.diag(self.vdlist[i])
                phiH = self.mElist[i].dot(self.mdlist[i].dot(
                    _np.linalg.inv(self.mElist[i]))).real
                prodVphiH = self.mVlist[i].dot(phiH)
                _np.copyto(y[:, -1], prodVphiH[:, 0])
                y[:, -1] *= self.normUlist[i]
            y[:, -1] += y0[:, -1]
        _np.copyto(uNext, y[:, -1])
        uNext += u
        print("--- Finish ---")

    def _phiFAlgo(self, u, t, dt, uEval):
        """Evaluates the fist phi-function at the current time for a given dt

        Parameters
        ----------
        u: `numpy.array`
            The field(s) to evaluate into vector form
        t: `float`
            The current time
        dt: `float`
            The current dt
        y: ``numpy.array(float)``
            The numpy array where to store field after the iteration
        """
        self.evalSpaceOp(u, t, self.vB)
        normU = self._arnoldi(u, t, self.vB)
        phiH = self._phiAlgo(self.mH, dt)
        prodVphiH = self.mV.dot(phiH)
        _np.copyto(uEval, prodVphiH[:, 0])
        uEval *= normU

    def _evalNLTerm(self, u, t, y, uEval):
        """Evaluates the non-linear term of the RHS"""
        self.evalSpaceOp(u, t, self.vF)
        self.evalSpaceOp(u+y, t, self.vFp)
        self.evalSpacOpJac(u, t, y, self.vB)
        uEval = self.vFp - self.vF - self.vB
        return uEval

    def _initArnoldi(self, f0):
        nLines = f0.getU().size
        dataType = f0.getU().dtype
        self.mV = _np.zeros((nLines, self.nK), dtype=dataType)
        self.mH = _np.zeros((self.nK, self.nK), dtype=dataType)
        self.vW = _np.zeros((nLines), dtype=dataType)
        # Extra column terms. Evaluation of the R(y), Non-linear term
        self.vB = _np.zeros_like(self.vW)
        self.vFp = _np.zeros_like(self.vW)
        self.vF = _np.zeros_like(self.vW)
        self.vR = _np.zeros_like(self.vW)

    def _arnoldi(self, u0, t, u):
        V = self.mV
        H = self.mH
        w = self.vW
        # v1 = u/|u|
        _np.copyto(V[:, 0], u)
        normU = _np.linalg.norm(u)
        V[:, 0] /= normU
        # First loop
        for j in range(self.nK):
            self.evalSpacOpJac(u0, t, V[:, j], w)
            # Second loop
            for i in range(j+1):
                H[i, j] = _np.dot(V[:, i].T, w)
                w -= H[i, j]*V[:, i]
            if j < self.nK-1:
                H[j+1, j] = _np.linalg.norm(w)
                _np.copyto(V[:, j+1], w)
                print(H[j+1, j])
                V[:, j+1] /= H[j+1, j]
        print('----------------')
        return normU

    def _phiAlgo(mH, dt):
        raise NotImplementedError()
