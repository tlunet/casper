# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing a Spectral Deferred Corrections solver

Classes
-------
SDCSolver : Inherited (TimeSolver)
    define Spectral Deferred Corrections solver
"""
import numpy as _np
import scipy.optimize as _so

from .base import Phi
from .. import common as util
from ..maths import quadrature as _lob


class SDCSolver(Phi):
    """
    Inherited class, define a Spectral Deferred Corrections Solver.
    It considers the classical ODE equation into its Picard form :

    .. math::
        u(t) = u(0) + \\int_{0}^{t}f(u(\\tau),\\tau)d\\tau

    It defines an error :math:`e(t)` and residuum :math:`r(t)`, which will
    be used to 'correct' a solution :math:`u^k(t)` to get more approximate
    solution :math:`u^{k+1}(t)`

    .. math::
        e(t) = u(t)-\\tilde{u}(t)

    .. math::
        r(t) = u(0) + \\int_{0}^{t}f(\\tilde{u}(\\tau),\\tau)d\\tau -
        \\tilde{u}(t)

    From that is obtained an error equation

    .. math::
        e(t)' = r(t)' + f(\\tilde{u}(t)+e(t)) - f(\\tilde{u}(t))

    The error :math:`e(t)` and residuum :math:`r(t)` are discretized
    on a subinterval (used for quadrature) :math:`[t_n=t_1,...,t_m,...,
    t_M=t_{n+1}]`, following :

    .. math::
        u^{k+1}(t_m) = u^{k}(t_m) + e^{k}(t_m)

    .. math::
        r^k_m = \\tilde{u}^K_n
        + Quad\\left[ \\int_{t_n}^{t_m} f(\\tilde{u}^k(\\tau),\\tau)
        d\\tau\\right]

    The term :math:`Quad\\left[ \\int_{t_n}^{t_m} f(\\tilde{u}^k(\\tau),
    \\tau)d\\tau \\right]` is calculated using a numerical quadrature
    following a specified rule (Lobatto, Constant, ...)\n
    The error equation is solved using a specified discretization of the
    derivatives :math:`e'(t)` and :math:`r'(t)` (Forward or Backward Euler,
    Second Order, ...). The process to solve the error equation in order to
    obtain a solution :math:`u^{k+1}(t)` from :math:`u^k(t)` is called a SDC
    'sweep'.

    Parameters
    ----------
    f0 : ``Fields``
        The initial field(s)
    spaceOp : ``SpaceOp``
        The spatial operator
    stepping : ``str``
        The time-stepping used by the solver :
    * stepping='FIXED' : fixed time-stepping, **dt**, **tEnd** have to be
      given in **kwargs**, **tBeg**, if not given, will be set to default
      value 0.
    * stepping='LIST' : time-stepping following a list of time, **tabTimes**
      has to be given in **kwargs**. Allows definition of following methods :
          - getTimeAt(i) : returns the time at index i (starts at 0)
          - getDtAt(i) : returns the time step between time(i) and time(i+1)
          - getNTimes() : returns the number of times
    timeStorage: ``str``
        The storage for fields in time :
    * timeStorage='CURRENT' : only store the current fields
    * timeStorage='ALL' : store fields at all time in a list. Allow
      definition of following methods :
          * getFieldsAt(i) : return the ``Fields`` object at time index i
            (starts at 0)
          * nFieldsStored() : return the number of ``Fields`` objects stored
          * getStorageTimeAt() : return time of storage at index i (starts at
            0)
    dumpTimes: ``str``
        Not yet implemented.
    quadForm : ``str`` in ****kwargs**
        The quadrature rule for correction integral (if not specified,
        default : 'LEGENDRE')
    * quadForm='LEGENDRE' : use a lobatto rule with Legendre points
    * quadForm='UNIF' : use a Lobatto rule uniform distribution of points
    * quadForm='CHEBYSHEV' : use a lobatto rule with Chebyshev points
    nQuad : ``int`` in ****kwargs**
        The number of quadrature point (not optional)
    nk : ``int`` in ****kwargs**
        The number of sweep to do (including the initial sweep !)
    initialSweepType : ``str`` in ****kwargs**
        The initialization sweep to get :math:`u^0(t)`
        (if not specified, default : 'SIMPLE')
    * initialSweepType='SIMPLE' : simple initialization, set :math:`u^0(t)
      =u(0)`
    * initialSweepType='FE' : Forward Euler initialization, set :math:`u^0
      (t_{m+1})=u^0(t_m)+\\Delta t_m f(u(t_m),t_m)`
    sweepType : ``str`` in ****kwargs** or ``list`` of ``str``
        The discretization of the error equation (if not specified, default
        : 'FE'). Can be also adaptative using the formulation **sweepType** =
        ['SO','FE'] (for example). In that case, first sweep is 'SO', and if
        there is more sweep, they will be 'FE'.
    * sweepType='FE' : use a Forward Euler discretization (Explicit)
    * sweepType='BE' : use a Backward Euler discretization (Implicit)
    * sweepType='SO' : use a Second Order discretization (Explicit)
    * sweepType='HEUN2' : use a Heun2 method based discretization (Explicit)
    implicitAlgo : ``str`` in ****kwargs**
        The implicit algo used if sweepType='BE' (if not specified, default
        : 'NEWTON_KRYLOV')
    * implicitAlgo='NEWTON_KRYLOV' : use a Newton-Krylov non-linear solver
      for implicit sweep
    sweepStorage : ``str`` in ****kwargs**
        The type of sweep storage to use :
    * sweepStorage='CURRENTK' : store only the current Fields and there
      evaluation at each k level
    * sweepStorage='ALLK' : store all Fields and there evaluation at each k
      level

    Attributes
    ----------
    spaceOp : ``SpaceOp``
        The spatial operator
    stepping : ``str``
        The time-stepping used by the solver.
    timeStorage : ``str``
        The storage for fields in time.
    dumpTimes : ``str``
        Not yet implemented.
    tBeg : ``float``
        The first time of the simulation
    tEnd : ``float``
        The last time of the simulation
    nIter : ``int``
        The number of iteration done
    spopCount : ``int``
        The count of space operator evaluation
    cpuTime : ``float``
        The computational time for solving all time step and estimation levels,
        corresponding to the last **solve()** call.
        If no **solve()** call has been made, **cpuTime** is set to `None`
    nFlops : ``float``
        The number of floating points operations per second during the solve
        process. If no **solve()** call has been made, **nFlops** is set
        to `None`
    quadForm : ``str``
        The quadrature rule for correction integral
    nQuad : ``int``
        The number of quadrature points for each time steps
    matrixQuad : ``numpy.array(nQuad-1,nQuad)``
        The integration matrix to approximate
        :math:`\\int_{t_m}^{t_m+1} f(\\tilde{u}^k(\\tau),\\tau)d\\tau`
        for :math:`m \\in [0, N_{Quad}-1]`

    Methods
    -------
    solve(self) : `Overloaded`
        solve the temporal evolution of the field on each estimation levels
    getFn(self) : `Base`
        return the final field at last estimation level
    getNFlopDt(self) : `Overloaded`
        get the number of floating point operations per time interval
    getTheoricalNEval(self) : `Overloaded`
        get thetheorical number of space operator evaluation
    evalSpaceOp(self,u,t) : `Base`
        evaluate the space operatol on a given field
    solve(self): `Abstract`
        solve the differential equation in time.
    initialize(self, f0, **kwargs) : `Base`
        initialize the time solver
    """
    def __init__(self, f0, spaceOp,
                 stepping, timeStorage, dumpTimes, **kwargs):
        Phi.__init__(self, f0, spaceOp,
                     stepping, timeStorage, dumpTimes, **kwargs)
        self._initQuadratureModel(**kwargs)
        self._initSweepStorage(f0, **kwargs)
        self._initSweepType(**kwargs)
        self._initTabQuadFields()

    def _initQuadratureModel(self, **kwargs):
        """Initialize the quadrature model"""
        func = self._initQuadratureModel

        quadForm = util.getInitPar('quadForm', 'LEGENDRE', str,
                                   func, **kwargs)
        CBut = util.getInitPar('CBut', [0], list, func, **kwargs)

        nQuad = util.getInitPar('nQuad', None, int, func, **kwargs)
        if quadForm == 'LEGENDRE':
            points = _lob.legendreQuadPoints(nQuad)  # points in [-1,1]
        elif quadForm == 'CHEBYSHEV':
            points = _lob.chebyshevQuadPoints(nQuad)  # points in (-1,1)
        elif quadForm == 'UNIF':
            points = _np.linspace(-1., 1., nQuad)    # points in [-1,1]
        elif quadForm == 'HERMIT':
            nQuad = 5
            points = _np.array([-1, -1.180716053/2., 0., 1.180716053/2., 1.])
        elif quadForm == 'VARIABLE':
            dtVar = util.getInitPar('dtVar', _np.ndarray((nQuad-2,), buffer =\
                        _np.linspace(-1., 1., nQuad)[1:-1]) , _np.ndarray,\
                        func, **kwargs)
            if nQuad == 2:
                points = _np.array([-1,1])
            elif nQuad == 3:
                points = _np.array([-1,-1 + 2*dtVar[0],1])
            elif nQuad == 4:
                points = _np.array([-1,-1 + 2*dtVar[0], -1 + 2*dtVar[0] + 2*dtVar[1],1])
            elif nQuad == 5:
                points = _np.array([-1, -1 + 2*dtVar[0],\
                -1 + 2*dtVar[0] + 2*dtVar[1],\
                -1 + 2*dtVar[0] + 2*dtVar[1] + 2*dtVar[2],1])
        else:
            raise ValueError("quadform {} not implemented".format(quadForm))
        # util.infoMsg('SDC Quadrature points : {}'.format(points))
        # Normalized quadrature times in [0,1]
        quadTimes = 0.5*points + 0.5
        self.quadTimes = quadTimes
        self.quadDt = _np.ediff1d(quadTimes)
        # Normalized integration matrix
        self.matrixQuad = _lob.matrixIntegLagrange(points)/2.
        # Add number of matrices to perform a RK corrector (Quadrature)
        self.CBut = CBut
        self.matrixQuadStage = (len(CBut)-1)*[_np.empty_like(self.matrixQuad)]
        for i in range(1, len(CBut)):
            self.matrixQuadStage[i-1] = \
                _lob.matrixIntegLagrangeRK(points, CBut[i])/2.
        # Store parameters
        self.nQuad = nQuad
        self.quadForm = quadForm

    def _initSweepType(self, **kwargs):
        """Initialze the type of sweep (FE, BE, SO, ...)"""
        func = self._initSweepType

        # Set sweep type from kwargs parameters
        self.sweepType =\
            util.getInitPar('sweepType', 'FE', None, func, **kwargs)
        if not isinstance(self.sweepType, list):
            self.sweepType = [self.sweepType]

        # Set the sweep type(s) used for one iteration
        origDoc = self._sweep.__doc__
        self._sweep = []  # The list where will be stored the sweep functions

        for st in self.sweepType:
            # Forward Euler sweep
            if st == 'FE':
                def _sweep(k):
                    for m1 in range(1, self.nQuad):
                        self._sweepFE(k-1, m1)
            # Backward Euler sweep
            elif st == 'BE':
                if not hasattr(self, 'implicitAlgo'):
                    self.implicitAlgo =\
                        util.getInitPar('implicitAlgo', 'NEWTON_KRYLOV',
                                        str, func, **kwargs)

                def _sweep(k):
                    for m1 in range(1, self.nQuad):
                        self._sweepBE(k-1, m1)
            # Second order sweep
            elif st == 'SO':
                def _sweep(k):
                    self._sweepFE(k-1, 1)
                    for m1 in range(2, self.nQuad):
                        self._sweepSO(k-1, m1)
            # Third order sweep
            elif st == 'TO':
                def _sweep(k):
                    self._sweepFE(k-1, 1)
                    self._sweepSO(k-1, 2)
                    for m1 in range(3, self.nQuad):
                        self._sweepTO(k-1, m1)
            # Fourth order sweep
            elif st == 'FO':
                def _sweep(k):
                    self._sweepFE(k-1, 1)
                    self._sweepSO(k-1, 2)
                    self._sweepTO(k-1, 3)
                    for m1 in range(4, self.nQuad):
                        self._sweepFO(k-1, m1)
            # Variant 1
            elif st == 'SO-AB':
                def _sweep(k):
                    for m1 in range(1, self.nQuad):
                        self._sweepSOAB(k-1, m1)
            elif st == 'TO-AB':
                def _sweep(k):
                    for m1 in range(1, self.nQuad):
                        self._sweepTOAB(k-1, m1)
            elif st == 'FO-AB':
                def _sweep(k):
                    self._sweepFE(k-1, 1)
                    self._sweepSOAB(k-1, 2)
                    self._sweepTOAB(k-1, 3)
                    for m1 in range(4, self.nQuad):
                        self._sweepFOAB(k-1, m1)
            # RK2 sweep
            elif st == 'RK2':
                def _sweep(k):
                    for m1 in range(1, self.nQuad):
                        self._sweepRK2(k-1, m1)
            elif st == 'RK3':
                def _sweep(k):
                    for m1 in range(1, self.nQuad):
                        self._sweepRK3(k-1, m1)
            elif st == 'RK4':
                def _sweep(k):
                    for m1 in range(1, self.nQuad):
                        self._sweepRK4(k-1, m1)
            elif st == 'HybridTO':
                def _sweep(k):
                    for m1 in range(1, self.nQuad):
                        self._sweepHybridTO(k-1, m1)
            # Adaptative sweep type
            elif st == 'ADAPTATIVE':
                # Set Adaptative sweep type from kwargs parameters
                self.adapSweepType = util.getInitPar(
                    'adapSweepType',
                    [(self.nQuad-1)*['FE'] for k in range(self.nSweep)],
                    None, func, **kwargs)
                for k in range(self.nSweep):
                    if len(self.adapSweepType[k-1]) is not (self.nQuad-1):
                        raise ValueError(
                            "adapSweepType should have a length of {},"
                            .format(self.nQuad-1) + "remember nQuad-1")

                # Adaptative sweep [FE-SO-FE-HEUN2...]
                def _sweep(k):
                    for m1 in range(1, self.nQuad):
                        if self.adapSweepType[k-1][m1-1] == 'FE':
                            self._sweepFE(k-1, m1)
                        elif self.adapSweepType[k-1][m1-1] == 'SO':
                            if m1 == 1:
                                self._sweepFE(k-1, m1)
                            else:
                                self._sweepSO(k-1, m1)
                        elif self.adapSweepType[k-1][m1-1] == 'SO-AB':
                            if m1 == 1:
                                self._sweepFE(k-1, m1)
                            else:
                                self._sweepSOAB(k-1, m1)
                        elif self.adapSweepType[k-1][m1-1] == 'RK2':
                            self._sweepRK2(k-1, m1)
                        elif self.adapSweepType[k-1][m1-1] == 'RK2-Reuse':
                            self._sweepRK2Reuse(k-1, m1)
                        elif self.adapSweepType[k-1][m1-1] == 'HEUN2':
                            self._sweepHEUN2(k-1, m1)
                        elif self.adapSweepType[k-1][m1-1] == 'RK3':
                            self._sweepRK3(k-1, m1)
                        elif self.adapSweepType[k-1][m1-1] == 'RK21':
                            self._sweepRK21(k-1, m1)
                        elif self.adapSweepType[k-1][m1-1] == 'RK4':
                            self._sweepRK4(k-1, m1)
                        elif self.adapSweepType[k-1][m1-1] == 'RK3-Reuse':
                            self._sweepRK3Reuse(k-1, m1)
                        elif self.adapSweepType[k-1][m1-1] == 'RK4-Reuse':
                            self._sweepRK4Reuse(k-1, m1)

                        elif self.adapSweepType[k-1][m1-1] == 'HybridTO':
                            self._sweepHybridTO(k-1, m1)
                        elif self.adapSweepType[k-1][m1-1] == 'HybridTOAB':
                            self._sweepHybridTOAB(k-1, m1)
                        elif self.adapSweepType[k-1][m1-1] == 'TO':
                            if m1 == 1:
                                self._sweepFE(k-1, m1)
                            elif m1 == 2:
                                self._sweepSOwFE(k-1, m1)
                            else:
                                self._sweepTO(k-1, m1)
                        elif self.adapSweepType[k-1][m1-1] == 'FO':
                            if m1 == 1:
                                self._sweepFE(k-1, m1)
                            elif m1 == 2:
                                self._sweepSOwFE(k-1, m1)
                            elif m1 == 3:
                                self._sweepTO(k-1, m1)
                            else:
                                self._sweepFO(k-1, m1)
                        elif self.adapSweepType[k-1][m1-1] == 'FO-AB':
                            if m1 == 1:
                                self._sweepFE(k-1, m1)
                            elif m1 == 2:
                                self._sweepSOAB(k-1, m1)
                            elif m1 == 3:
                                self._sweepTOAB(k-1, m1)
                            else:
                                self._sweepFOAB(k-1, m1)
                        elif self.adapSweepType[k-1][m1-1] == 'TO-AB':
                            if m1 == 1:
                                self._sweepFE(k-1, m1)
                            elif m1 == 2:
                                self._sweepSOAB(k-1, m1)
                            else:
                                self._sweepTOAB(k-1, m1)

            else:
                raise ValueError('Sweep type {} not implemented'
                                 .format(self.sweepType))

            _sweep.__doc__ = origDoc
            self._sweep.append(_sweep)

    def _initSweepStorage(self, f0, **kwargs):
        """Initialize the sweep storage type"""
        func = self._initSweepStorage

        self.nSweep = util.getInitPar('nSweep', None, int, func, **kwargs)
        self.initialSweepType =\
            util.getInitPar('initialSweepType', 'SIMPLE', str, func, **kwargs)

        # Set storage of Fields at each k level
        self.sweepStorage = util.getInitPar('sweepStorage', 'CURRENTK', str,
                                            func, **kwargs)

        # Storage only of current sweep fields
        if self.sweepStorage == 'CURRENTK':
            tabCurrentFieldsK = \
                [f0.copy() for k in range(self.nSweep)]
            fu0 = self.evalSpaceOp(f0.getU(), self.tBeg, f0.clone().getU())
            tabCurrentEvalFieldsK = \
                [f0.clone() for k in range(self.nSweep)]
            for fields in tabCurrentEvalFieldsK:
                fields.setU(fu0)

            def _getCurrentFieldsK(k):
                return tabCurrentFieldsK[k]

            def _getCurrentEvalFieldsK(k):
                return tabCurrentEvalFieldsK[k]

            def _setNextEvalFieldsK(k, uNew):
                tabCurrentEvalFieldsK[k].setU(uNew)

            def _setNextFieldsK(k, uNew):
                tabCurrentFieldsK[k].setU(uNew)

        # Storage of all sweep fields
        elif self.sweepStorage == 'ALLK':
            tabFieldsK = \
                [[f0.copy()] for k in range(self.nSweep)]
            fu0 = self.evalSpaceOp(f0.getU(), self.tBeg, f0.clone().getU())
            tabEvalFieldsK = \
                [[fu0.clone()] for k1 in range(1, self.nSweep)]
            for fields in tabEvalFieldsK:
                fields.setU(fu0)

            def _getCurrentFieldsK(k):
                return tabFieldsK[k][-1]

            def _getCurrentEvalFieldsK(k):
                return tabEvalFieldsK[k][-1]

            def _setNextEvalFieldsK(k, uNew):
                tabEvalFieldsK[k].append(f0.clone())
                tabEvalFieldsK[k][-1].setU(uNew)

            def _setNextFieldsK(k, uNew):
                tabFieldsK[k].append(f0.clone())
                tabFieldsK[k][-1].setU(uNew)

            def getFieldsKAt(k, i):
                return tabFieldsK[k][i]

            self.getFieldsKAt = getFieldsKAt

        else:
            raise ValueError('sweepStorage {} not implemented'
                             .format(self.sweepStorage))

        _getCurrentFieldsK.__doc__ = self.getCurrentFieldsK.__doc__
        self.getCurrentFieldsK = _getCurrentFieldsK
        _getCurrentEvalFieldsK.__doc__ = self.getCurrentEvalFieldsK.__doc__
        self.getCurrentEvalFieldsK = _getCurrentEvalFieldsK
        _setNextEvalFieldsK.__doc__ = self.setNextEvalFieldsK.__doc__
        self.setNextEvalFieldsK = _setNextEvalFieldsK
        _setNextFieldsK.__doc__ = self.setNextFieldsK.__doc__
        self.setNextFieldsK = _setNextFieldsK

    def _initTabQuadFields(self):
        """Initialize the current quadrature Fields list"""
        f0 = self.getCurrentFieldsK(0)
        self.tabQuadFields = \
            [[f0.copy() for m1 in range(self.nQuad)]
             for k in range(self.nSweep)]
        self.tabQuadFieldsM1 = \
            [[f0.copy() for m1 in range(self.nQuad)]
             for k in range(self.nSweep)]
        evalF0 = self.getCurrentEvalFieldsK(0)
        self.tabEvalQuadFields = \
            [[evalF0.copy() for m1 in range(self.nQuad)]
             for k in range(self.nSweep)]
        # RK corrector Stages
        self.tabEvalQuadFieldsStage = \
            [[evalF0.copy() for m1 in range((len(self.CBut)-1)*(self.nQuad-1))]
             for k in range(self.nSweep)]
        # For multistep method r>1
        self.tabEvalQuadFieldsM1 = \
            [[evalF0.copy() for m1 in range(self.nQuad)]
             for k in range(self.nSweep)]

    def _updateTabQuadFields(self):
        """Private method, update the quadrature Fields list"""
        for k in range(self.nSweep):
            for m in range(self.nQuad):
                self.tabQuadFields[k][m].setU(
                    self.getCurrentFieldsK(k).getU())
                self.tabEvalQuadFields[k][m].setU(
                    self.getCurrentEvalFieldsK(k).getU())

    def _dumpTabQuadFields(self):
        """Private method, dump the quadrature Fields list"""
        for m in range(self.nQuad):
            self.tabQuadFields[-1][m].dumpToFile(
                'FieldsFile/tabQuadFields[{}][{}]'.format(self.nIter, m))
            self.tabEvalQuadFields[-1][m].dumpToFile(
                'FieldsFile/tabEvalQuadFields[{}][{}]'.format(self.nIter, m))
        for m in range((len(self.CBut)-1)*(self.nQuad-1)):
            self.tabEvalQuadFieldsStage[-1][m].dumpToFile(
                'FieldsFile/tabEvalQuadFields[{}][{}]'.format(self.nIter, m))

    def _readTabQuadFields(self):
        """Private method, read the quadrature Fields list from a file"""
        n = int(_np.ceil(self.tEnd/self.getCurrentDt()))
        for m in range(n):
            for m in range(self.nQuad):
                self.tabQuadFields[0][m].fromFile(
                    'FieldsFile/tabQuadFields[{}][{}].f1d'
                    .format(self.nIter, m))
                self.tabEvalQuadFields[0][m].fromFile(
                    'FieldsFile/tabEvalQuadFields[{}][{}].f1d'
                    .format(self.nIter, m))
            for m in range((len(self.CBut)-1)*(self.nQuad-1)):
                self.tabEvalQuadFieldsStage[0][m].fromFile(
                    'FieldsFile/tabEvalQuadFields[{}][{}].f1d'
                    .format(self.nIter, m))

    def _updateTabQuadFieldsM1(self):
        """Private method, update the previous current values of quadrature
           Fields list"""
        for k in range(self.nSweep):
            for m in range(self.nQuad):
                self.tabQuadFieldsM1[k][m] = self.tabQuadFields[k][m].copy()
                self.tabEvalQuadFieldsM1[k][m] = \
                    self.tabEvalQuadFields[k][m].copy()

    def _updateTabQuadFieldsOpt(self):
        """Private method, update the quadrature Fields from the last
           correction value"""
        for k in range(self.nSweep):
            for m in range(self.nQuad):
                self.tabQuadFields[k][m].setU(
                    self.getCurrentFieldsK(self.nSweep-1).getU())
                self.tabEvalQuadFields[k][m].setU(
                    self.getCurrentEvalFieldsK(self.nSweep-1).getU())

    def getCurrentQuadTime(self, m):
        """Local method, get the current quadrature time at position m"""
        return self.getCurrentTime() + self.getCurrentDt()*self.quadTimes[m]

    def getCurrentQuadDt(self, m):
        """Local method, get the current quadrature time step between
        position m and m+1"""
        return self.getCurrentDt()*self.quadDt[m]

    def iterate(self, u, time, dt, uNext):
        """Overloaded method, iterate the solver between the current time
        and the next current time

        Parameters
        ----------
        u : `numpy.array`
            The field(s) to evaluate into vector form
        time : `float`
            The current time
        dt : `float`
            the current dt
        uNext : ``numpy.array(float)``
            The numpy array where to store field after the iteration
        """
        # Initial time, t_0
        self._updateTabQuadFieldsM1()
        self._updateTabQuadFields()
        # Update the field list at the stage m-1
        # Performs the sweep
        self._initialSweep()
        for k in range(1, self.nSweep):
            if k-1 < len(self.sweepType):
                iSweep = k-1
            else:
                iSweep = -1
            if self.sweepType[iSweep] == 'SO-AB' and time == self.tBeg:
                self._sweepFE(k-1, 1)
                for m1 in range(2, self.nQuad):
                    self._sweepSOAB(k-1, m1)
            elif self.sweepType[iSweep] == 'HybridTO' and time == self.tBeg:
                self._sweepHEUN2(k-1, 1)
                for m1 in range(2, self.nQuad):
                    self._sweepHybridTO(k-1, m1)
            elif self.sweepType[iSweep] == 'TO-AB' and time == self.tBeg:
                self._sweepHEUN2(k-1, 1)
                self._sweepHEUN2(k-1, 2)
                for m1 in range(3, self.nQuad):
                    self._sweepSOAB(k-1, m1)
            else:
                self._sweep[iSweep](k)
        # Set the next Fields at each k-levels
        for k in range(self.nSweep):
            self.setNextFieldsK(k, self.tabQuadFields[k][-1].getU())
            self.setNextEvalFieldsK(k, self.tabEvalQuadFields[k][-1].getU())
        # Store the next fields
        _np.copyto(uNext, self.getCurrentFieldsK(k).getU())

    def getCurrentFieldsK(self, k):
        """Local method, get the current Fields at level k"""
        raise NotImplementedError()

    def getCurrentEvalFieldsK(self, k):
        """Local method, get the current evaluation of the Fields at level k"""
        raise NotImplementedError()

    def setNextEvalFieldsK(self, k, fields):
        """Local method, set the next evaluation of Fields at level k"""
        raise NotImplementedError()

    def setNextFieldsK(self, k, fields):
        """Local method, set the next Fields at level k"""
        raise NotImplementedError()

    def _initialSweep(self):
        """Private method, perform an initial sweep"""
        if self.initialSweepType == "FE":
            # Forward Euler initial sweep
            for m1 in range(1, self.nQuad):
                # uk0m1 = uk0m0 + Dtm0*f(uk0m0)
                uk0m0 = self.tabQuadFields[0][m1-1].getU()
                uk0m1 = self.tabQuadFields[0][m1].getU()
                fuk0m0 = self.tabEvalQuadFields[0][m1-1].getU()
                dtm = self.getCurrentQuadDt(m1-1)
                _np.copyto(uk0m1, fuk0m0)
                uk0m1 *= dtm
                uk0m1 += uk0m0
                self.evalSpaceOp(uk0m1, self.getCurrentQuadTime(m1),
                                 self.tabEvalQuadFields[0][m1].getU())
        elif self.initialSweepType == 'FILE':
            self._readTabQuadFields()
        elif self.initialSweepType == "RK2":
            # RK2
            for m1 in range(1, self.nQuad):
                # uk0m1 = uk0m0 + Dtm0*f(uk0m0)
                uk0m0 = self.tabQuadFields[0][m1-1].getU()
                uk0m1 = self.tabQuadFields[0][m1].getU()
                fuk0m0 = self.tabEvalQuadFields[0][m1-1].getU()
                dtm = self.getCurrentQuadDt(m1-1)
            # First Stage
                self.evalSpaceOp(uk0m0+dtm*fuk0m0,
                                 self.getCurrentQuadTime(m1-1) +
                                 dtm*self.CBut[1],
                                 self.tabEvalQuadFieldsStage[0][1*(m1-1)]
                                 .getU())
                _np.copyto(uk0m1, fuk0m0)
                uk0m1 += self.tabEvalQuadFieldsStage[0][1*(m1-1)].getU()
                uk0m1 *= dtm/2
                uk0m1 += uk0m0
                self.evalSpaceOp(uk0m1, self.getCurrentQuadTime(m1),
                                 self.tabEvalQuadFields[0][m1].getU())
        elif self.initialSweepType == "RK3":
            # Butcher Tableau for order 3 stage 3 RK family
            butC = _np.array([0., self.CBut[1], self.CBut[2]])
            butA = _np.array([[0, 0., 0.],
                             [butC[1], 0., 0.],
                             [(butC[2]*(-3*butC[1]+3*butC[1]**2+butC[2]))/(butC[1]*(3*butC[1]-2)),
                              butC[2]*(butC[1]-butC[2])/(butC[1]*(3*butC[1]-2)), 0.]])
            butB = _np.array([1-(butC[1]+butC[2])/(2*butC[1]*butC[2])+1/(3*butC[1]*butC[2]),
                              (3*butC[2]-2)/(6*butC[1]*(butC[2]-butC[1])),
                             (3*butC[1]-2)/(6*butC[2]*(butC[1]-butC[2]))])
            n = len(butC)-1
            # RK3
            for m1 in range(1, self.nQuad):
                # uk0m1 = uk0m0 + Dtm0*f(uk0m0)
                uk0m0 = self.tabQuadFields[0][m1-1].getU()
                uk0m1 = self.tabQuadFields[0][m1].getU()
                fuk0m0 = self.tabEvalQuadFields[0][m1-1].getU()
                dtm = self.getCurrentQuadDt(m1-1)
                # Second Stage
                self.evalSpaceOp(uk0m0 + butA[1,0]*dtm*fuk0m0,
                                 self.getCurrentQuadTime(m1-1) + butC[1]*dtm ,
                            self.tabEvalQuadFieldsStage[0][n*(m1-1)].getU())
                fuk0m02s = self.tabEvalQuadFieldsStage[0][n*(m1-1)].getU()
                # Third stage
                self.evalSpaceOp(uk0m0 + dtm*(butA[2,0]*fuk0m0 +
                            butA[2,1]*fuk0m02s),
                            self.getCurrentQuadTime(m1-1) + butC[2]*dtm ,
                            self.tabEvalQuadFieldsStage[0][2*(m1-1)+1].getU())
                fuk0m03s = self.tabEvalQuadFieldsStage[0][n*(m1-1)+1].getU()
                _np.copyto(uk0m1, fuk0m0)
                uk0m1 += self.tabEvalQuadFieldsStage[0][1*(m1-1)].getU()
                uk0m1 *= dtm/2
                uk0m1 += uk0m0
                self.evalSpaceOp(uk0m1, self.getCurrentQuadTime(m1),
                                 self.tabEvalQuadFields[0][m1].getU())
                # Output
                _np.copyto(uk0m1, butB[0]*fuk0m0)
                uk0m1 += butB[1]*fuk0m02s
                uk0m1 += butB[2]*fuk0m03s
                uk0m1 *= dtm
                uk0m1 += uk0m0
                self.evalSpaceOp(uk0m1,
                                 self.getCurrentQuadTime(m1),
                                 self.tabEvalQuadFields[0][m1].getU())

        elif self.initialSweepType == "SO-AB":
            # Second Order Adams Bashforth initial sweep
            for m1 in range(1, self.nQuad):
                tm1 = self.getCurrentQuadTime(m1)                   # t{m+1}
                tm0 = self.getCurrentQuadTime(m1-1)                   # t{m}
                uk0m1 = self.tabQuadFields[0][m1].getU()
                if tm0 == self.tBeg:
                    # Start by HEUN method
                    uk0m0 = self.tabQuadFields[0][m1-1].getU()        # u{k+1,m}
                    fuk0m0 = self.tabEvalQuadFields[0][m1-1].getU()   # f(u{k+1,m})
                    dtm = self.getCurrentQuadDt(m1-1)
                    fuk0m1p = _np.copy(fuk0m0)
                    self.evalSpaceOp(uk0m0 + dtm*fuk0m0, tm1, fuk0m1p)
                    _np.copyto(uk0m1, fuk0m0)
                    uk0m1 += fuk0m1p
                    uk0m1 *= dtm/2
                    uk0m1 += uk0m0
                else:
                    dtmP = self.getCurrentQuadDt((m1-1)%(self.nQuad-1))     # dt{m}
                    dtmM = self.getCurrentQuadDt((m1-2)%(self.nQuad-1))     # dt{m-1}
                    alphaM = -(dtmP**2)/(2*dtmM)                           # alpha{m-1}
                    alphaZ = (2*dtmP*dtmM + dtmP**2)/(2*dtmM)              # alpha{m}
                    if m1 != 1:
                        uk0m0 = self.tabQuadFields[0][m1-1].getU()        # u{k+1,m}
                        fuk0m0 = self.tabEvalQuadFields[0][m1-1].getU()   # f(u{k+1,m})
                        fuk0m1M = self.tabEvalQuadFields[0][m1-2].getU()   # f(u{k+1,m-1})
                        uk0m1 = uk0m0 + alphaM*fuk0m1M + alphaZ*fuk0m0
                    else:
                        uk0m0 = self.tabQuadFields[0][0].getU()        # u{k+1,m}
                        fuk0m0 = self.tabEvalQuadFields[0][0].getU()   # f(u{k+1,m})
                        fuk0m1M = self.tabEvalQuadFieldsM1[0][-1].getU()   # f(u{k+1,m-1})
                        uk0m1 = uk0m0 + alphaM*fuk0m1M + alphaZ*fuk0m0
                self.evalSpaceOp(uk0m1, tm1,
                             self.tabEvalQuadFields[0][m1].getU())


        elif self.initialSweepType == "SIMPLE":
            # Initial solution as first guess
            pass  # already done when initializing sweep model
        else:
            raise ValueError("In SDCSolver.initialSweep :" +
                             " incorrect value for initialSweepType ({0})"
                             .format(self.initialSweepType))

    def _sweep(self, k):
        """Private method, perform a sweep at level :math:`k>0`"""
        raise NotImplementedError()

    def _sweepFE(self, k, m1):
        """Peforms a Forward Euler sweep"""
        tm1 = self.getCurrentQuadTime(m1)                   # t{m+1}
        uk1m0 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m}
        fuk1m0 = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})
        fuk0m0 = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
        # Optimized sweep version ------------------------------------------
        uk1m1 = self.tabQuadFields[k+1][m1].getU()          # u{k+1,m+1}
        _np.copyto(uk1m1, fuk1m0)                            # Sweep (beg)
        uk1m1 -= fuk0m0                                     # Sweep ...
        uk1m1 *= 1.*self.getCurrentQuadDt(m1-1)                # Sweep ...
        uk1m1 += uk1m0                                      # Sweep ...
        uk1m1 += self._numInt(m1-1, k)                      # Sweep (end)

        # Easy to read sweep version ---------------------------------------
        # dtm = self.getCurrentQuadDt(m1-1)                   # dt{m}
        # uk1m1 = uk1m0 + dtm*(fuk1m0 - fuk0m0) + self._numInt(m1-1, k)
        # self.tabQuadFields[k+1][m1-1].setU(uk1m1)
        # # Store evaluation of u{k+1,m+1} ---------------------------------
        self.evalSpaceOp(uk1m1, tm1,
                         self.tabEvalQuadFields[k+1][m1].getU())

    def _sweepHEUN2(self, k, m1):
        """Peforms a HEUN2 sweep

        .. math::
            u^{k1}_{m1} = u^{k1}_{m0} + \Delta_{t}/{2}((f(u^{k+1}_{m+1}) +
            f^*(u^{k+1}_{(m+1)})) - (f^*(u^{k}_{m} + f(u^{k}_{m}  )) +
            I_{m}^{m+1}(f(u^{k}))

        With :math:`f^*(u^{k+1}_{m+1},t_{m+1}) = f[u_m^{k+1}+ \Delta_t*
        (f(u^{k+1}_{m},t_{m}) - f(u^{k}_{m},t_{m+1})+I_{m}^{m+1}(f(u^{k})))]`

        """
        #
        tm1 = self.getCurrentQuadTime(m1)                   # t{m+1}
        uk1m0 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m}
        fuk1m0 = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})
        fuk0m0 = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
        fuk0m0p = self.tabEvalQuadFields[k][m1].getU()      # f(u{k,m+1})
        dtm = self.getCurrentQuadDt(m1-1)
        Ik0m0 = self._numInt(m1-1, k)                      # I{m,m+1}(u{k})
        fuk1m0p = _np.copy(fuk1m0)
        self.evalSpaceOp(uk1m0 + dtm*(fuk1m0-fuk0m0) +
                         Ik0m0, tm1, fuk1m0p)
        # Optimized sweep version ------------------------------------------
        uk1m1 = self.tabQuadFields[k+1][m1].getU()          # u{k+1,m+1}
        _np.copyto(uk1m1, fuk1m0)                           # Sweep (beg)
        uk1m1 -= fuk0m0
        uk1m1 -= fuk0m0p                                    # Sweep ...
        uk1m1 += fuk1m0p                                    # Sweep ...
        uk1m1 *= self.getCurrentQuadDt(m1-1)/2              # Sweep ...
        uk1m1 += uk1m0                                      # Sweep ...
        uk1m1 += Ik0m0                                      # Sweep (end)
        # Easy to read sweep version ---------------------------------------
        # dtm = self.getCurrentQuadDt(m1-1)                   # dt{m}
        # uk1m1 = uk1m0 + dtm/2*(fuk1m0+fuk1m0p - fuk0m0-fuk0m0p) + \
        #     self._numInt(m1-1, k)
        # self.tabQuadFields[k+1][m1-1].setU(uk1m1)
        # # Store evaluation of u{k+1,m+1} ---------------------------------
        self.evalSpaceOp(uk1m1, tm1,
                         self.tabEvalQuadFields[k+1][m1].getU())


    def _sweepRK2Reuse(self, k, m1):

        #Butcher Tableau for order 2 stage 2 RK family
        butC = _np.array([0., -1.])
        butA = _np.array([[0, 0.],
                         [butC[1], 0.]])
        butB = _np.array([1.-1./(2.*butC[1]), 1./(2.*butC[1])])
        #Initialization variables
        tm1 = self.getCurrentQuadTime(m1)                   # t{m+1}
        tm0 = self.getCurrentQuadTime(m1-1)                   # t{m}
        dtm = self.getCurrentQuadDt(m1-1)
        uk1m0 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m}
        Ik0m0 =  self._numInt(m1-1, k)                      # I{m,m+1}(u{k})
        fuk1m01s = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})
        fuk0m01s = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
        # Second stage
        Ik0m02s =  -self._numInt(0,m1-2, k)                 # I{m,m+c1*dt}(Fu{k})
        fuk0m02s = self.tabEvalQuadFields[k][m1].getU()
        fuk1m02s = _np.empty_like(fuk1m01s)
        self.evalSpaceOp(uk1m0 + butA[1,0]*dtm*(fuk1m01s-fuk0m01s) +
                         Ik0m02s, tm0 + butC[1]*dtm , fuk1m02s)
        # Output Optimized sweep version --------------------------------------
        uk1m1 = self.tabQuadFields[k+1][m1].getU()          # u{k+1,m+1}
        _np.copyto(uk1m1, butB[0]*fuk1m01s)                 # Sweep (beg)
        uk1m1 -= butB[0]*fuk0m01s                           # Sweep ...
        uk1m1 += butB[1]*fuk1m02s                           # Sweep ...
        uk1m1 -= butB[1]*fuk0m02s                           # Sweep ...
        uk1m1 *= dtm                                        # Sweep ...
        uk1m1 += uk1m0                                      # Sweep ...
        uk1m1 += Ik0m0                                      # Sweep (end)
        self.evalSpaceOp(uk1m1, tm1,
                         self.tabEvalQuadFields[k+1][m1].getU())
    def _sweepRK2(self, k, m1):

        #Butcher Tableau for order 2 stage 2 RK family
        butC = _np.array([0., self.CBut[1]])
        butA = _np.array([[0, 0.],
                         [butC[1], 0.]])
        butB = _np.array([1.-1./(2.*butC[1]), 1./(2.*butC[1])])
        #Initialization variables
        tm1 = self.getCurrentQuadTime(m1)                   # t{m+1}
        tm0 = self.getCurrentQuadTime(m1-1)                   # t{m}
        dtm = self.getCurrentQuadDt(m1-1)
        uk1m0 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m}
        Ik0m0 =  self._numInt(m1-1, k)                      # I{m,m+1}(u{k})
        fuk1m01s = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})
        fuk0m01s = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
        # Second stage
        Ik0m02s =  self._numIntRK(0,m1-1, k)                 # I{m,m+c1*dt}(Fu{k})

        fuk0m02s = self.tabEvalQuadFields[k][m1].getU()
#        fuk0m02s = self.tabEvalQuadFieldsStage[k][2*(m1-1)].getU()
        fuk1m02s = _np.empty_like(fuk1m01s)
        self.evalSpaceOp(uk1m0 + butA[1,0]*dtm*(fuk1m01s-fuk0m01s) +
                         Ik0m02s, tm0 + butC[1]*dtm , self.tabEvalQuadFieldsStage[k+1][(m1-1)].getU())
        fuk1m02s = self.tabEvalQuadFieldsStage[k+1][(m1-1)].getU()
        # Output Optimized sweep version --------------------------------------
        uk1m1 = self.tabQuadFields[k+1][m1].getU()          # u{k+1,m+1}
        _np.copyto(uk1m1, butB[0]*fuk1m01s)                 # Sweep (beg)
        uk1m1 -= butB[0]*fuk0m01s                           # Sweep ...
        uk1m1 += butB[1]*fuk1m02s                           # Sweep ...
        uk1m1 -= butB[1]*fuk0m02s                           # Sweep ...
        uk1m1 *= dtm                                        # Sweep ...
        uk1m1 += uk1m0                                      # Sweep ...
        uk1m1 += Ik0m0                                      # Sweep (end)
        self.evalSpaceOp(uk1m1, tm1,
                         self.tabEvalQuadFields[k+1][m1].getU())
    def _sweepRK21(self, k, m1):

        #Butcher Tableau for order 2 stage 2 RK family
        butC = _np.array([0., 3./4.])
        butA = _np.array([[0, 0.],
                         [butC[1], 0.]])
        butB = _np.array([0, 1.])
        #Initialization variables
        tm1 = self.getCurrentQuadTime(m1)                   # t{m+1}
        tm0 = self.getCurrentQuadTime(m1-1)                   # t{m}
        dtm = self.getCurrentQuadDt(m1-1)
        uk1m0 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m}
        Ik0m0 =  self._numInt(m1-1, k)                      # I{m,m+1}(u{k})
        fuk1m01s = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})
        fuk0m01s = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
        # Second stage
        Ik0m02s =  self._numIntRK(0,m1-1, k)                 # I{m,m+c1*dt}(Fu{k})

        fuk0m02s = self.tabEvalQuadFields[k][m1].getU()
#        fuk0m02s = self.tabEvalQuadFieldsStage[k][2*(m1-1)].getU()
        fuk1m02s = _np.empty_like(fuk1m01s)
        self.evalSpaceOp(uk1m0 + butA[1,0]*dtm*(fuk1m01s-fuk0m01s) +
                         Ik0m02s, tm0 + butC[1]*dtm , self.tabEvalQuadFieldsStage[k+1][(m1-1)].getU())
        fuk1m02s = self.tabEvalQuadFieldsStage[k+1][(m1-1)].getU()
        # Output Optimized sweep version --------------------------------------
        uk1m1 = self.tabQuadFields[k+1][m1].getU()          # u{k+1,m+1}
        _np.copyto(uk1m1, butB[0]*fuk1m01s)                 # Sweep (beg)
        uk1m1 -= butB[0]*fuk0m01s                           # Sweep ...
        uk1m1 += butB[1]*fuk1m02s                           # Sweep ...
        uk1m1 -= butB[1]*fuk0m02s                           # Sweep ...
        uk1m1 *= dtm                                        # Sweep ...
        uk1m1 += uk1m0                                      # Sweep ...
        uk1m1 += Ik0m0                                      # Sweep (end)
        self.evalSpaceOp(uk1m1, tm1,
                         self.tabEvalQuadFields[k+1][m1].getU())

    def _sweepRK3(self, k, m1):

        #Butcher Tableau for order 3 stage 3 RK family
        butC = _np.array([0., self.CBut[1], self.CBut[2]])
        butA = _np.array([[0, 0., 0.],
                         [butC[1], 0., 0.],
                         [(butC[2]*(-3*butC[1]+3*butC[1]**2+butC[2]))/(butC[1]*(3*butC[1]-2)),
                          butC[2]*(butC[1]-butC[2])/(butC[1]*(3*butC[1]-2)), 0.]])
        butB = _np.array([1-(butC[1]+butC[2])/(2*butC[1]*butC[2])+1/(3*butC[1]*butC[2]),
                          (3*butC[2]-2)/(6*butC[1]*(butC[2]-butC[1])),
                         (3*butC[1]-2)/(6*butC[2]*(butC[1]-butC[2]))])
        #Initialization variables
        tm1 = self.getCurrentQuadTime(m1)                   # t{m+1}
        tm0 = self.getCurrentQuadTime(m1-1)                 # t{m}
        dtm = self.getCurrentQuadDt(m1-1)                   # dt{m} // Current
        uk1m0 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m}
        fuk1m01s = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})
        fuk0m01s = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
        Ik0m0 =  self._numInt(m1-1, k)                      # I{m,m+1}(u{k})

        # Second stage
        Ik0m02s =  self._numIntRK(0,m1-1, k)                 # I{m,m+c1*dt}(Fu{k})
        # Mapping m1 --> (nstages-1)*(m1-1) in tabEvalQuadFieldsStage
        fuk0m02s = self.tabEvalQuadFieldsStage[k][2*(m1-1)].getU()
        self.evalSpaceOp(uk1m0 + butA[1,0]*dtm*(fuk1m01s-fuk0m01s) +
                         Ik0m02s, tm0 + butC[1]*dtm , self.tabEvalQuadFieldsStage[k+1][2*(m1-1)].getU())
        fuk1m02s = self.tabEvalQuadFieldsStage[k+1][2*(m1-1)].getU()
        # Third stage
        Ik0m03s =  self._numIntRK(1,m1-1, k)                 # I{m,m+c2*dt}(Fu{k})
        # Mapping m1 --> (nstages-1)*(m1-1)+1 in tabEvalQuadFieldsStage
        fuk0m03s = self.tabEvalQuadFields[k][m1].getU()
        self.evalSpaceOp(uk1m0 + dtm*(butA[2,0]*(fuk1m01s-fuk0m01s) +
                                      butA[2,1]*(fuk1m02s-fuk0m02s)) +
                         Ik0m03s, tm0 + butC[2]*dtm , self.tabEvalQuadFieldsStage[k+1][2*(m1-1)+1].getU())
        fuk1m03s = self.tabEvalQuadFieldsStage[k+1][2*(m1-1)+1].getU()
        # Output Optimized sweep version --------------------------------------
        uk1m1 = self.tabQuadFields[k+1][m1].getU()          # u{k+1,m+1}
        _np.copyto(uk1m1, butB[0]*fuk1m01s)                 # Sweep (beg)
        uk1m1 -= butB[0]*fuk0m01s                           # Sweep ...
        uk1m1 += butB[1]*fuk1m02s                           # Sweep ...
        uk1m1 -= butB[1]*fuk0m02s                           # Sweep ...
        uk1m1 += butB[2]*fuk1m03s                           # Sweep ...
        uk1m1 -= butB[2]*fuk0m03s                           # Sweep ...
        uk1m1 *= dtm                                        # Sweep ...
        uk1m1 += uk1m0                                      # Sweep ...
        uk1m1 += Ik0m0                                      # Sweep (end)
        self.evalSpaceOp(uk1m1, tm1,
                         self.tabEvalQuadFields[k+1][m1].getU())
    def _sweepRK3Reuse(self, k, m1):

        #Butcher Tableau for order 3 stage 3 RK family
        butC = _np.array([0., -1., self.CBut[2]])
        butA = _np.array([[0, 0., 0.],
                         [butC[1], 0., 0.],
                         [(butC[2]*(-3*butC[1]+3*butC[1]**2+butC[2]))/(butC[1]*(3*butC[1]-2)),
                          butC[2]*(butC[1]-butC[2])/(butC[1]*(3*butC[1]-2)), 0.]])
        butB = _np.array([1-(butC[1]+butC[2])/(2*butC[1]*butC[2])+1/(3*butC[1]*butC[2]),
                          (3*butC[2]-2)/(6*butC[1]*(butC[2]-butC[1])),
                         (3*butC[1]-2)/(6*butC[2]*(butC[1]-butC[2]))])
        #Initialization variables
        tm1 = self.getCurrentQuadTime(m1)                   # t{m+1}
        tm0 = self.getCurrentQuadTime(m1-1)                 # t{m}
        dtm = self.getCurrentQuadDt(m1-1)                   # dt{m} // Current
        uk1m0 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m}
        fuk1m01s = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})
        fuk0m01s = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
        Ik0m0 =  self._numInt(m1-1, k)                      # I{m,m+1}(u{k})

        # Second stage
        Ik0m02s =  -self._numInt(m1-2, k)                 # I{m,m+c1*dt}(Fu{k})
        # Mapping m1 --> (nstages-1)*(m1-1) in tabEvalQuadFieldsStage
        fuk0m02s = self.tabEvalQuadFields[k][m1-2].getU()
        self.evalSpaceOp(uk1m0 + butA[1,0]*dtm*(fuk1m01s-fuk0m01s) +
                         Ik0m02s, tm0 + butC[1]*dtm , self.tabEvalQuadFieldsStage[k+1][2*(m1-1)].getU())
        fuk1m02s = self.tabEvalQuadFields[k+1][m1-2].getU()
#        print fuk1m02s - self.tabEvalQuadFields[k+1][m1-2].getU()
        # Third stage
        Ik0m03s =  self._numIntRK(1,m1-1, k)                 # I{m,m+c2*dt}(Fu{k})
        # Mapping m1 --> (nstages-1)*(m1-1)+1 in tabEvalQuadFieldsStage
        fuk0m03s = self.tabEvalQuadFields[k][m1].getU()
        fuk1m03s = _np.empty_like(fuk0m03s)
        self.evalSpaceOp(uk1m0 + dtm*(butA[2,0]*(fuk1m01s-fuk0m01s) +
                                      butA[2,1]*(fuk1m02s-fuk0m02s)) +
                         Ik0m03s, tm0 + butC[2]*dtm , fuk1m03s)
        # Output Optimized sweep version --------------------------------------
        uk1m1 = self.tabQuadFields[k+1][m1].getU()          # u{k+1,m+1}
        _np.copyto(uk1m1, butB[0]*fuk1m01s)                 # Sweep (beg)
        uk1m1 -= butB[0]*fuk0m01s                           # Sweep ...
        uk1m1 += butB[1]*fuk1m02s                           # Sweep ...
        uk1m1 -= butB[1]*fuk0m02s                           # Sweep ...
        uk1m1 += butB[2]*fuk1m03s                           # Sweep ...
        uk1m1 -= butB[2]*fuk0m03s                           # Sweep ...
        uk1m1 *= dtm                                        # Sweep ...
        uk1m1 += uk1m0                                      # Sweep ...
        uk1m1 += Ik0m0                                      # Sweep (end)
        self.evalSpaceOp(uk1m1, tm1,
                         self.tabEvalQuadFields[k+1][m1].getU())
    def _sweepRK4(self, k, m1):

        #Butcher Tableau for order 3 stage 3 RK family
        butC = _np.array([0., 1./2., 1./2.,1.])
        butA = _np.array([[0, 0., 0., 0.],
                         [1./2., 0., 0., 0.],
                         [0, 1./2., 0., 0.],
                         [0, 0., 1., 0.]])
        butB = _np.array([1./6.,1./3.,1./3.,1./6.])
        n = len(butC)-1
        #Initialization variables
        tm1 = self.getCurrentQuadTime(m1)                   # t{m+1}
        tm0 = self.getCurrentQuadTime(m1-1)                 # t{m}
        dtm = self.getCurrentQuadDt(m1-1)                   # dt{m} // Current
        uk1m0 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m}
        fuk1m01s = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})
        fuk0m01s = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
        Ik0m0 =  self._numInt(m1-1, k)                      # I{m,m+1}(u{k})

        # Second stage
        Ik0m02s =  self._numIntRK(0,m1-1, k)                 # I{m,m+c1*dt}(Fu{k})
        # Mapping m1 --> (nstages-1)*(m1-1) in tabEvalQuadFieldsStage
        fuk0m02s = self.tabEvalQuadFieldsStage[k][n*(m1-1)].getU()
        self.evalSpaceOp(uk1m0 + butA[1,0]*dtm*(fuk1m01s-fuk0m01s) +
                         Ik0m02s, tm0 + butC[1]*dtm , self.tabEvalQuadFieldsStage[k+1][n*(m1-1)].getU())
        fuk1m02s = self.tabEvalQuadFieldsStage[k+1][n*(m1-1)].getU()
        # Third stage
        Ik0m03s =  self._numIntRK(1,m1-1, k)                 # I{m,m+c2*dt}(Fu{k})
        # Mapping m1 --> (nstages-1)*(m1-1)+1 in tabEvalQuadFieldsStage
        fuk0m03s = self.tabEvalQuadFieldsStage[k][n*(m1-1)+1].getU()
        self.evalSpaceOp(uk1m0 + dtm*(butA[2,0]*(fuk1m01s-fuk0m01s) +
                                      butA[2,1]*(fuk1m02s-fuk0m02s)) +
                         Ik0m03s, tm0 + butC[2]*dtm , self.tabEvalQuadFieldsStage[k+1][n*(m1-1)+1].getU())
        fuk1m03s = self.tabEvalQuadFieldsStage[k+1][n*(m1-1)+1].getU()
        # Fourth stage
        Ik0m04s =  self._numIntRK(2,m1-1, k)                 # I{m,m+c3*dt}(Fu{k})
        # Mapping m1 --> (nstages-1)*(m1-1)+2 in tabEvalQuadFieldsStage
        fuk0m04s = self.tabEvalQuadFieldsStage[k][n*(m1-1)+2].getU()
        self.evalSpaceOp(uk1m0 + dtm*(butA[3,0]*(fuk1m01s-fuk0m01s) +
                                      butA[3,1]*(fuk1m02s-fuk0m02s) +
                                      butA[3,2]*(fuk1m03s-fuk0m03s)) +
                         Ik0m04s, tm0 + butC[3]*dtm , self.tabEvalQuadFieldsStage[k+1][n*(m1-1)+2].getU())
        fuk1m04s = self.tabEvalQuadFieldsStage[k+1][n*(m1-1)+2].getU()
        # Output Optimized sweep version --------------------------------------
        uk1m1 = self.tabQuadFields[k+1][m1].getU()          # u{k+1,m+1}
        _np.copyto(uk1m1, butB[0]*fuk1m01s)                 # Sweep (beg)
        uk1m1 -= butB[0]*fuk0m01s                           # Sweep ...
        uk1m1 += butB[1]*fuk1m02s                           # Sweep ...
        uk1m1 -= butB[1]*fuk0m02s                           # Sweep ...
        uk1m1 += butB[2]*fuk1m03s                           # Sweep ...
        uk1m1 -= butB[2]*fuk0m03s                           # Sweep ...
        uk1m1 += butB[3]*fuk1m04s                           # Sweep ...
        uk1m1 -= butB[3]*fuk0m04s                           # Sweep ...
        uk1m1 *= dtm                                        # Sweep ...
        uk1m1 += uk1m0                                      # Sweep ...
        uk1m1 += Ik0m0                                      # Sweep (end)
        self.evalSpaceOp(uk1m1, tm1,
                         self.tabEvalQuadFields[k+1][m1].getU())
    def _sweepRK4Reuse(self, k, m1):

        #Butcher Tableau for order 4 stage 4 RK family
        butC = _np.array([0., -1., self.CBut[1], 1.])
        butA = _np.array([[0, 0., 0., 0.],
                         [-1, 0., 0.,0.],
                         [1./6.*butC[2]*(7. + butC[2]),1./6.*(-butC[2] - butC[2]**2),0.,0.],
                         [(2.*(3. - 8.*butC[2] + 7*butC[2]**2))/(butC[2]*(-7. + 10.*butC[2])),
                          (-3. + 5.*butC[2] - 4*butC[2]**2)/(-7. + 3.*butC[2] + 10.*butC[2]**2),
                        (6.*(-1. + butC[2]))/(butC[2]*(-7. + 3.*butC[2] + 10.*butC[2]**2)),0.]])
        butB = _np.array([(-3. + 8.*butC[2])/(12.*butC[2]),
                          (1. - 2.*butC[2])/(24.*(1. + butC[2])),
                        -(1./(4.*butC[2]*(-1. + butC[2]**2))),(-7. + 10.*butC[2])/(24.*(-1. + butC[2]))])
        #Initialization variables
        tm1 = self.getCurrentQuadTime(m1)                   # t{m+1}
        tm0 = self.getCurrentQuadTime(m1-1)                 # t{m}
        dtm = self.getCurrentQuadDt(m1-1)                   # dt{m} // Current
        uk1m0 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m}
        fuk1m01s = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})
        fuk0m01s = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
        Ik0m0 =  self._numInt(m1-1, k)                      # I{m,m+1}(u{k})

        # Second stage
        Ik0m02s =  -self._numInt(m1-2, k)                 # I{m,m+c1*dt}(Fu{k})
        fuk0m02s = self.tabEvalQuadFields[k][m1-2].getU()
        fuk1m02s = self.tabEvalQuadFields[k+1][m1-2].getU()
        # Third stage
        Ik0m03s =  self._numIntRK(0,m1-1, k)                 # I{m,m+c2*dt}(Fu{k})
        # Mapping m1 --> (nstages-1)*(m1-1) in tabEvalQuadFieldsStage
        fuk0m03s = self.tabEvalQuadFieldsStage[k][2*(m1-1)].getU()
        self.evalSpaceOp(uk1m0 + dtm*(butA[2,0]*(fuk1m01s-fuk0m01s) +
                                      butA[2,1]*(fuk1m02s-fuk0m02s)) +
                         Ik0m03s, tm0 + butC[2]*dtm , self.tabEvalQuadFieldsStage[k+1][2*(m1-1)].getU())
        fuk1m03s = self.tabEvalQuadFieldsStage[k+1][2*(m1-1)].getU()
        # Fourth stage
        Ik0m04s =  self._numIntRK(1,m1-1, k)                 # I{m,m+c3*dt}(Fu{k})
        # Mapping m1 --> (nstages-1)*(m1-1)+1 in tabEvalQuadFieldsStage
        fuk0m04s = self.tabEvalQuadFieldsStage[k][2*(m1-1)+1].getU()
        self.evalSpaceOp(uk1m0 + dtm*(butA[3,0]*(fuk1m01s-fuk0m01s) +
                                      butA[3,1]*(fuk1m02s-fuk0m02s) +
                                      butA[3,2]*(fuk1m03s-fuk0m03s)) +
                         Ik0m04s, tm0 + butC[3]*dtm , self.tabEvalQuadFieldsStage[k+1][2*(m1-1)+1].getU())
        fuk1m04s = self.tabEvalQuadFieldsStage[k+1][2*(m1-1)+1].getU()
        # Output Optimized sweep version --------------------------------------
        uk1m1 = self.tabQuadFields[k+1][m1].getU()          # u{k+1,m+1}
        _np.copyto(uk1m1, butB[0]*fuk1m01s)                 # Sweep (beg)
        uk1m1 -= butB[0]*fuk0m01s                           # Sweep ...
        uk1m1 += butB[1]*fuk1m02s                           # Sweep ...
        uk1m1 -= butB[1]*fuk0m02s                           # Sweep ...
        uk1m1 += butB[2]*fuk1m03s                           # Sweep ...
        uk1m1 -= butB[2]*fuk0m03s                           # Sweep ...
        uk1m1 += butB[3]*fuk1m04s                           # Sweep ...
        uk1m1 -= butB[3]*fuk0m04s                           # Sweep ...
        uk1m1 *= dtm                                        # Sweep ...
        uk1m1 += uk1m0                                      # Sweep ...
        uk1m1 += Ik0m0                                      # Sweep (end)
        self.evalSpaceOp(uk1m1, tm1,
                         self.tabEvalQuadFields[k+1][m1].getU())

    def _sweepBE(self, k, m1):
        """Peforms a Backward Euler sweep"""
        # Temporary variables
        uk1m0 = self.tabQuadFields[k+1][m1-1].getU()    # u{k+1,m}
        uk0m1 = self.tabQuadFields[k][m1].getU()        # u{k,m+1}
        Im0m1 = self._numInt(m1-1, k)                   # Int_m^{m+1}f(u(k))
        dtm = self.getCurrentQuadDt(m1-1)               # dt{m}
        tm = self.getCurrentQuadTime(m1-1)              # t{m}
        tm1 = self.getCurrentQuadTime(m1)               # t{m+1}
        fuk0m1 = self.tabEvalQuadFields[k][m1].getU()   # f(u{k,m+1})
        # Backward Euler sweep
        uk1m1 = self._implicitSweep(uk1m0, uk0m1, fuk0m1, Im0m1, dtm, tm)
        # Setting new uk1m1
        self.tabQuadFields[k+1][m1].setU(uk1m1)
        # Store evaluation of u{k+1,m+1}
        self.evalSpaceOp(uk1m1, tm1,
                         self.tabEvalQuadFields[k+1][m1].getU())

    def _implicitSweep(self, uk1m0, uk0m1, fuk0m1, Im0m1, Dtm, tm):
        """Solve the Backward Euler implicit sweep"""
        def func(x):
            return uk1m0 + Dtm*(self.evalSpaceOp(x, tm) - fuk0m1) + Im0m1 - x
        if self.implicitAlgo == 'NEWTON_KRYLOV':
            return _so.newton_krylov(func, uk0m1)
        else:
            raise ValueError("In implicitSweep : incorrect value " +
                             "for algo ({0} given)"
                             .format(self.implicitAlgo))

    def _sweepSO(self, k, m1):
        """Peforms an explicit Second Order sweep"""
        # Get pointer to used variables --------------------------------------
        dtmP = self.getCurrentQuadDt(m1-1)                  # dt{m}
        dtmM = self.getCurrentQuadDt(m1-2)                  # dt{m-1}
        alphaM = -dtmP/(dtmP*dtmM+dtmM**2)                  # alpha{m-1}
        alpha0 = (dtmP - dtmM)/(dtmP*dtmM)                  # alpha{m}
        alphaP = dtmM/(dtmP*dtmM+dtmP**2)                   # alpha{m+1}
        uk1m0 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m}
        fuk1m0 = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})
        uk1mM1 = self.tabQuadFields[k+1][m1-2].getU()       # u{k+1,m-1}
        fuk0m0 = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
        # Update u{k+1,m+1} --------------------------------------------------
        uk1m1 = self.tabQuadFields[k+1][m1].getU()
        _np.copyto(uk1m1, 0)
        uk1m1 -= alpha0*uk1m0
        uk1m1 -= alphaM*uk1mM1
        uk1m1 += fuk1m0
        uk1m1 -= fuk0m0
        uk1m1 += alphaP*self._numInt(m1-1, k)
        uk1m1 += (alphaP+alpha0)*self._numInt(m1-2, k)
        uk1m1 /= alphaP
        # Store evaluation of u{k+1,m+1} -------------------------------------
        self.evalSpaceOp(uk1m1, self.getCurrentQuadTime(m1),
                         self.tabEvalQuadFields[k+1][m1].getU())

    def _sweepTO(self, k, m1):
        """Peforms an explicit Third Order sweep"""
        # Easy to read sweep version -----------------------------------------
        dtmP = self.getCurrentQuadDt(m1-1)                 # t{m+1}-t{m}
        dtmM1 = self.getCurrentQuadDt(m1-2)                # t{m}-t{m-1}
        dtmM2 = self.getCurrentQuadDt(m1-3)                # t{m-1}-t{m-2}
        alphaM2 = dtmM1*dtmP / \
            (dtmM2*(dtmM1+dtmM2)*(dtmP+dtmM1+dtmM2))       # alpha{m-2}
        alphaM1 = - dtmP*(dtmM1+dtmM2) / \
            (dtmM1*dtmM2*(dtmP+dtmM1))                     # alpha{m-1}
        alpha0 = -(dtmM1**2+dtmM1*dtmM2-2*dtmM1*dtmP-dtmM2*dtmP) / \
            (dtmM1*dtmP*(dtmM1+dtmM2))                     # alpha{m}
        alphaP = dtmM1*(dtmM1+dtmM2) / \
            (dtmP*(dtmP+dtmM1)*(dtmP+dtmM1+dtmM2))         # alpha{m+1}
        uk1m0 = self.tabQuadFields[k+1][m1-1].getU()       # u{k+1,m}
        uk1mM1 = self.tabQuadFields[k+1][m1-2].getU()      # u{k+1,m-1}
        uk1mM2 = self.tabQuadFields[k+1][m1-3].getU()      # u{k+1,m-2}
        fuk1m0 = self.tabEvalQuadFields[k+1][m1-1].getU()  # f(u{k+1,m})
        fuk0m0 = self.tabEvalQuadFields[k][m1-1].getU()    # f(u{k,m})
        # Update u{k+1,m+1} --------------------------------------------------
        uk1m1 = self.tabQuadFields[k+1][m1].getU()
        _np.copyto(uk1m1, 0)
        uk1m1 -= alphaM2*uk1mM2
        uk1m1 -= alphaM1*uk1mM1
        uk1m1 -= alpha0*uk1m0
        uk1m1 += fuk1m0
        uk1m1 -= fuk0m0
        uk1m1 += alphaP*self._numInt(m1-1, k)
        uk1m1 += (alphaP+alpha0)*self._numInt(m1-2, k)
        uk1m1 += (alphaP+alpha0+alphaM1)*self._numInt(m1-3, k)
        uk1m1 /= alphaP
        # Store evaluation of u{k+1,m+1} -----------------------------------
        self.evalSpaceOp(uk1m1, self.getCurrentQuadTime(m1),
                         self.tabEvalQuadFields[k+1][m1].getU())

    def _sweepFO(self, k, m1):
        """Peforms an explicit Forth Order sweep"""
        # Easy to read sweep version ---------------------------------------
        tm0 = self.getCurrentQuadTime(m1)                   # t{m}
        dtmM1 = self.getCurrentQuadDt(m1-1)                 # dt{m-1}
        dtmM2 = self.getCurrentQuadDt(m1-2)                  # dt{m-2}
        dtmM3 = self.getCurrentQuadDt(m1-3)                       # dt{m-3}
        dtmM4 = self.getCurrentQuadDt(m1-4)                       # dt{m-4}
        alpha0 = ((dtmM2*dtmM3+dtmM2**2)*dtmM4+dtmM2*dtmM3**2+\
                    2*dtmM2**2*dtmM3+dtmM2**3)/(((dtmM1*dtmM2+dtmM1**2)*\
                    dtmM3+dtmM1*dtmM2**2+2*dtmM1**2*dtmM2+dtmM1**3)*dtmM4+\
                    (dtmM1*dtmM2+dtmM1**2)*dtmM3**2+\
                    (2*dtmM1*dtmM2**2+4*dtmM1**2*dtmM2+2*dtmM1**3)*dtmM3+\
                    dtmM1*dtmM2**3+3*dtmM1**2*dtmM2**2+3*dtmM1**3*dtmM2+dtmM1**4\
                    )
        alpha1 = -(((dtmM2-dtmM1)*dtmM3+dtmM2**2-2*dtmM1*dtmM2)*dtmM4+\
                    (dtmM2-dtmM1)*dtmM3**2+(2*dtmM2**2-4*dtmM1*dtmM2)*dtmM3\
                    +dtmM2**3-3*dtmM1*dtmM2**2)/(\
                    (dtmM1*dtmM2*dtmM3+dtmM1*dtmM2**2)*dtmM4+dtmM1*dtmM2*\
                    dtmM3**2+2*dtmM1*dtmM2**2*dtmM3+dtmM1*dtmM2**3)
        alpha2 = -((dtmM1*dtmM3+dtmM1*dtmM2)*dtmM4+dtmM1*
                    dtmM3**2+2*dtmM1*dtmM2*dtmM3+dtmM1*dtmM2**2)/(\
                    (dtmM2**2+dtmM1*dtmM2)*dtmM3*dtmM4+\
                    (dtmM2**2+dtmM1*dtmM2)*dtmM3**2)
        alpha3 = (dtmM1*dtmM2*dtmM4+dtmM1*dtmM2*dtmM3+dtmM1*
                dtmM2**2)/((dtmM3**3+(2*dtmM2+dtmM1)*dtmM3**2+
                (dtmM2**2+dtmM1*dtmM2)*dtmM3)*dtmM4)
        alpha4 = -(dtmM1*dtmM2*dtmM3+dtmM1*dtmM2**2)/(dtmM4**4+
                    (3*dtmM3+2*dtmM2+dtmM1)*dtmM4**3+
                    (3*dtmM3**2+(4*dtmM2+2*dtmM1)*dtmM3+dtmM2**2+dtmM1*dtmM2)*
                    dtmM4**2+(dtmM3**3+(2*dtmM2+dtmM1)*dtmM3**2+
                    (dtmM2**2+dtmM1*dtmM2)*dtmM3)*dtmM4)
        uk1mM1 = self.tabQuadFields[k+1][m1-1].getU()       # u{k+1,m-1}
        uk1mM2 = self.tabQuadFields[k+1][m1-2].getU()       # u{k+1,m-2}
        uk1mM3 = self.tabQuadFields[k+1][m1-3].getU()       # u{k+1,m-3}
        uk1mM4 = self.tabQuadFields[k+1][m1-4].getU()       # u{k+1,m-4}
        fuk1mM1 = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})
        fuk0mM1 = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
        uk1m0 = (-alpha1*uk1mM1 - alpha2*uk1mM2 - alpha3*uk1mM3 - alpha4*uk1mM4\
                + fuk1mM1 - fuk0mM1 +\
                alpha0*self._numInt(m1-1, k) +\
                (alpha0+alpha1)*self._numInt(m1-2, k) +\
                (alpha0+alpha1+alpha2)*self._numInt(m1-3, k) +\
                (alpha0+alpha1+alpha2+alpha3)*self._numInt(m1-4, k))/alpha0
        self.tabQuadFields[k+1][m1].setU(uk1m0)
        # Store evaluation of u{k+1,m+1} -----------------------------------
        self.evalSpaceOp(uk1m0, tm0,
                         self.tabEvalQuadFields[k+1][m1].getU())

    def _sweepSOwFE(self, k, m1):
        """Peforms an explicit Second Order sweep"""
        # Easy to read sweep version ---------------------------------------
        tm1 = self.getCurrentQuadTime(m1)                   # t{m+1}
        dtmP = self.getCurrentQuadDt((m1-1)%(self.nQuad-1))     # dt{m}
        dtmM = self.getCurrentQuadDt((m1-2)%(self.nQuad-1))     # dt{m-1}
#        print m1
#        print dtmP
#        print dtmM
#        print self.tabQuadFieldsM1[k+1][(m1-2)%self.nQuad].getU()
#        print self.tabQuadFields[k+1][m1-2].getU()
#        print self._numIntM1(m1-2, k)
#        print self._numInt(m1-2, k)
        alphaM = -dtmP/(dtmP*dtmM+dtmM**2)                  # alpha{m-1}
        alphaZ = (dtmP - dtmM)/(dtmP*dtmM)                  # alpha{m}
        alphaP = dtmM/(dtmP*dtmM+dtmP**2)                   # alpha{m+1}
        if m1-2>=0:
            uk1m0 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m}
            fuk1m0 = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})
            uk1m1M = self.tabQuadFields[k+1][m1-2].getU()       # u{k+1,m-1}
            fuk0m0 = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
            uk1m1 = (-alphaZ*uk1m0 - alphaM*uk1m1M + fuk1m0 - fuk0m0 +
                     alphaP*self._numInt(m1-1, k) -
                     alphaM*self._numInt(m1-2, k))/alphaP
            self.tabQuadFields[k+1][m1].setU(uk1m1)
            # Store evaluation of u{k+1,m+1} -----------------------------------
            self.evalSpaceOp(uk1m1, tm1,
                             self.tabEvalQuadFields[k+1][m1].getU())
        elif m1-2 < 0:
           self._sweepFE(k,m1)


    def _sweepSOAB(self, k, m1):
        """Peforms an explicit Second Order sweep"""
        # Easy to read sweep version ---------------------------------------

        tm1 = self.getCurrentQuadTime(m1)                   # t{m+1}
        dtmP = self.getCurrentQuadDt((m1-1)%(self.nQuad-1))     # dt{m}
        dtmM = self.getCurrentQuadDt((m1-2)%(self.nQuad-1))     # dt{m-1}
        alphaM = -(dtmP**2)/(2*dtmM)                           # alpha{m-1}
        alphaZ = (2*dtmP*dtmM + dtmP**2)/(2*dtmM)              # alpha{m}

        if m1 != 1:
            uk1m0 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m}
            fuk1m0 = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})
            fuk0m0 = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
            fuk1m1M = self.tabEvalQuadFields[k+1][m1-2].getU()   # f(u{k+1,m-1})
            fuk0m1M = self.tabEvalQuadFields[k][m1-2].getU()     # f(u{k,m-1})
            uk1m1 = (uk1m0 + alphaM*(fuk1m1M - fuk0m1M) + alphaZ*(fuk1m0 - fuk0m0) + self._numInt(m1-1, k))

        else:
#Not k+1 = 1 with FE
#Not k+1 = k-1 // Similar to FE - SO-AB...
# Not k = k-1
# Not reduction to Simple SOAB

            uk1m0 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m}
            fuk1m0 = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})
            fuk0m0 = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
            fuk1m1M = self.tabEvalQuadFields[k+1][(m1-2)%(self.nQuad-1)].getU()   # f(u{k+1,m-1})
            fuk0m1M = self.tabEvalQuadFields[k][(m1-2)%(self.nQuad-1)].getU()     # f(u{k,m-1})
            uk1m1 = (uk1m0  + alphaZ*(fuk1m0 - fuk0m0) + self._numInt(m1-1, k))
#            uk1m0 = self.tabQuadFields[k][m1-1].getU()        # u{0,m}
#            fuk0m0p = self.tabEvalQuadFields[k-1][m1].getU()      # f(u{k,m+1})
#            dtm = self.getCurrentQuadDt(m1-1)
#            Ik0m0 =  self._numInt(m1-1, k-1)                      # I{m,m+1}(u{k})
#            fuk1m0p = _np.copy(fuk0m0p)
#            self.evalSpaceOp(uk1m0 +
#                             Ik0m0, tm1, fuk1m0p)
#            # Optimized sweep version ------------------------------------------
#            uk1m1 = self.tabQuadFields[k+1][m1].getU()          # u{k+1,m+1}
#            _np.copyto(uk1m1, fuk1m0p)                          # Sweep (beg)
#            uk1m1 -= fuk0m0p                                    # Sweep ...
#            uk1m1 *= dtm                                        # Sweep ...
#            uk1m1 += uk1m0                                      # Sweep ...
#            uk1m1 += Ik0m0                                      # Sweep (end)
        self.tabQuadFields[k+1][m1].setU(uk1m1)
        # Store evaluation of u{k+1,m+1} -----------------------------------
        self.evalSpaceOp(uk1m1, tm1,
                         self.tabEvalQuadFields[k+1][m1].getU())




    def _sweepTOAB(self, k, m1):
        """Peforms an explicit Third Order sweep"""
        # Easy to read sweep version ---------------------------------------
        tm0 = self.getCurrentQuadTime(m1)                   # t{m}
        dtmM1 = self.getCurrentQuadDt((m1-1)%(self.nQuad-1))        # dt{m-1}
        dtmM2 = self.getCurrentQuadDt((m1-2)%(self.nQuad-1))                 # dt{m-2}
        dtmM3 = self.getCurrentQuadDt((m1-3)%(self.nQuad-1))                       # dt{m-3}
        beta1 = ((6*dtmM1*dtmM2+3*dtmM1**2)*dtmM3+6*dtmM1*dtmM2**2
                +6*dtmM1**2*dtmM2+2*dtmM1**3)/(6*dtmM2*dtmM3+6*dtmM2**2)
        beta2 = -(3*dtmM1**2*dtmM3+3*dtmM1**2*dtmM2+2*dtmM1**3)/(6*dtmM2*dtmM3)
        beta3 = (3*dtmM1**2*dtmM2+2*dtmM1**3)/(6*dtmM3**2+6*dtmM2*dtmM3)
        if m1 > 2:
            uk1mM1 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m-1}
            fuk1m1M = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m-1})
            fuk0m1M = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m-1})
            fuk1m2M = self.tabEvalQuadFields[k+1][m1-2].getU()   # f(u{k+1,m-2})
            fuk0m2M = self.tabEvalQuadFields[k][m1-2].getU()     # f(u{k,m-2})
            fuk1m3M = self.tabEvalQuadFields[k+1][m1-3].getU()   # f(u{k+1,m-3})
            fuk0m3M = self.tabEvalQuadFields[k][m1-3].getU()     # f(u{k,m-3})
        elif m1 == 2:
            uk1mM1 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m-1}
            fuk1m1M = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m-1})
            fuk0m1M = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m-1})
            fuk1m2M = self.tabEvalQuadFields[k+1][(m1-2)%(self.nQuad) ].getU()   # f(u{k+1,m-2})
            fuk0m2M = self.tabEvalQuadFields[k][(m1-2)%(self.nQuad)].getU()     # f(u{k,m-2})
            fuk1m3M = self.tabEvalQuadFieldsM1[k+1][(m1-3)%(self.nQuad)].getU()   # f(u{k+1,m-3})
            fuk0m3M = self.tabEvalQuadFieldsM1[k][(m1-3)%(self.nQuad)].getU()     # f(u{k,m-3})
        else:
            uk1mM1 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m-1}
            fuk1m1M = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m-1})
            fuk0m1M = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m-1})
            fuk1m2M = self.tabEvalQuadFieldsM1[k+1][(m1-2)%(self.nQuad) ].getU()   # f(u{k+1,m-2})
            fuk0m2M = self.tabEvalQuadFieldsM1[k][(m1-2)%(self.nQuad)].getU()     # f(u{k,m-2})
            fuk1m3M = self.tabEvalQuadFieldsM1[k+1][(m1-3)%(self.nQuad)].getU()   # f(u{k+1,m-3})
            fuk0m3M = self.tabEvalQuadFieldsM1[k][(m1-3)%(self.nQuad)].getU()     # f(u{k,m-3})

        uk1m0 = uk1mM1 + beta1*(fuk1m1M - fuk0m1M) + beta2*(fuk1m2M - fuk0m2M)\
                    + beta3*(fuk1m3M - fuk0m3M) + self._numInt(m1-1, k)

        self.tabQuadFields[k+1][m1].setU(uk1m0)
        # Store evaluation of u{k+1,m+1} -----------------------------------
        self.evalSpaceOp(uk1m0, tm0,
                         self.tabEvalQuadFields[k+1][m1].getU())
    def _sweepFOAB(self, k, m1):
        """Peforms an explicit Third Order sweep"""
        # Easy to read sweep version ---------------------------------------
        tm0 = self.getCurrentQuadTime(m1)                   # t{m}
        dtmM1 = self.getCurrentQuadDt(m1-1)                 # dt{m-1}
        dtmM2 = self.getCurrentQuadDt(m1-2)                  # dt{m-2}
        dtmM3 = self.getCurrentQuadDt(m1-3)                       # dt{m-3}
        dtmM4 = self.getCurrentQuadDt(m1-4)                       # dt{m-4}
        beta1 = (((12*dtmM1*dtmM2+6*dtmM1**2)*dtmM3+12*dtmM1*dtmM2**2\
                +12*dtmM1**2*dtmM2+4*dtmM1**3)*dtmM4+(12*dtmM1*dtmM2+6*dtmM1**2)*\
                dtmM3**2+(24*dtmM1*dtmM2**2+24*dtmM1**2*dtmM2+8*dtmM1**3)*dtmM3+12\
                *dtmM1*dtmM2**3+18*dtmM1**2*dtmM2**2+12*dtmM1**3*dtmM2+3*dtmM1**4)/\
                ((12*dtmM2*dtmM3+12*dtmM2**2)*dtmM4+12*dtmM2*dtmM3**2+24*\
                dtmM2**2*dtmM3+12*dtmM2**3)
        beta2 = -((6*dtmM1**2*dtmM3+6*dtmM1**2*dtmM2+4*dtmM1**3)*dtmM4+\
                6*dtmM1**2*dtmM3**2+(12*dtmM1**2*dtmM2+8*dtmM1**3)*dtmM3+6*dtmM1**2*\
                dtmM2**2+8*dtmM1**3*dtmM2+3*dtmM1**4)/(12*dtmM2*dtmM3*dtmM4+12*\
                dtmM2*dtmM3**2)
        beta3 = ((6*dtmM1**2*dtmM2+4*dtmM1**3)*dtmM4+\
                (6*dtmM1**2*dtmM2+4*dtmM1**3)*dtmM3+6*dtmM1**2*dtmM2**2+8*dtmM1**3*\
                dtmM2+3*dtmM1**4)/((12*dtmM3**2+12*dtmM2*dtmM3)*dtmM4)
        beta4 = -((6*dtmM1**2*dtmM2+4*dtmM1**3)*dtmM3+6*dtmM1**2*dtmM2**2+\
                8*dtmM1**3*dtmM2+3*dtmM1**4)/(12*dtmM4**3+(24*dtmM3+12*dtmM2)*\
                dtmM4**2+(12*dtmM3**2+12*dtmM2*dtmM3)*dtmM4)
        uk1mM1 = self.tabQuadFields[k+1][m1-1].getU()       # u{k+1,m-1}
        fuk1m1M = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m-1})
        fuk0m1M = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m-1})
        fuk1m2M = self.tabEvalQuadFields[k+1][m1-2].getU()   # f(u{k+1,m-2})
        fuk0m2M = self.tabEvalQuadFields[k][m1-2].getU()     # f(u{k,m-2})
        fuk1m3M = self.tabEvalQuadFields[k+1][m1-3].getU()   # f(u{k+1,m-3})
        fuk0m3M = self.tabEvalQuadFields[k][m1-3].getU()     # f(u{k,m-3})
        fuk1m4M = self.tabEvalQuadFields[k+1][m1-4].getU()   # f(u{k+1,m-4})
        fuk0m4M = self.tabEvalQuadFields[k][m1-4].getU()     # f(u{k,m-4})

        uk1m0 = uk1mM1 + beta1*(fuk1m1M - fuk0m1M) + beta2*(fuk1m2M - fuk0m2M)\
                + beta3*(fuk1m3M - fuk0m3M) + beta4*(fuk1m4M - fuk0m4M)\
                + self._numInt(m1-1, k)

        self.tabQuadFields[k+1][m1].setU(uk1m0)
        # Store evaluation of u{k+1,m+1} -----------------------------------
        self.evalSpaceOp(uk1m0, tm0,
                         self.tabEvalQuadFields[k+1][m1].getU())
    def _sweepHybridTO(self, k, m1):

        #In the context of General Linear methods this is a 2-stage 2-step
        #It is composed of a Second Order Non-Adams Bashforth and a second stage RK
        """Peforms a Hybrid Multistep Predictor Corrector sweep

        The following formula has not been updated yet!
        .. math::
                u^{k1}_{m1} = u^{k1}_{m0} + \Delta_{t}/{2}((f(u^{k+1}_{m+1}) + \\
                f^*(u^{k+1}_{(m+1)})) - (f^*(u^{k}_{m} + f(u^{k}_{m}  )) + \\
                I_{m}^{m+1}(f(u^{k}))

        Being :math:`f^*(u^{k+1}_{m+1},t_{m+1}) = f[u_m^{k+1}+ \Delta_t*(f(u^{k+1}_{m},t_{m}) - f(u^{k}_{m},t_{m+1})+I_{m}^{m+1}(f(u^{k})))]`

        """
        # Initialisation
        tm1 = self.getCurrentQuadTime(m1)                            # t{m+1}
        dtmP = self.getCurrentQuadDt((m1-1)%(self.nQuad-1))          # dt{m}
        dtmM = self.getCurrentQuadDt((m1-2)%(self.nQuad-1))          # dt{m-1}
        alphaM = -dtmP/(dtmP*dtmM+dtmM**2)                  # alpha{m-1}
        alphaZ = (dtmP - dtmM)/(dtmP*dtmM)                  # alpha{m}
        alphaP = dtmM/(dtmP*dtmM+dtmP**2)                   # alpha{m+1}
        uk1m0 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m}
        uk1m1M = self.tabQuadFields[k+1][m1-2].getU()       # u{k+1,m-1}
        fuk0m0 = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
        fuk1m0 = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})

        # Reuse technique
        fuk0m0p = self.tabEvalQuadFields[k][m1].getU()      # f(u{k,m+1})
        uk1m1p = (-alphaZ*uk1m0 - alphaM*uk1m1M + fuk1m0 - fuk0m0 +
                     alphaP*self._numInt(m1-1, k) -
                     alphaM*self._numInt(m1-2, k))/alphaP
        fuk1m0p = _np.empty_like(fuk1m0)
        self.evalSpaceOp(uk1m1p, tm1, fuk1m0p)
        # Optimized sweep version ------------------------------------------
        uk1m1 = self.tabQuadFields[k+1][m1].getU()          # u{k+1,m+1}
        _np.copyto(uk1m1, fuk1m0)                           # Sweep (beg)
        uk1m1 -= fuk0m0
        uk1m1 -= fuk0m0p                                    # Sweep ...
        uk1m1 += fuk1m0p                                    # Sweep ...
        uk1m1 *= self.getCurrentQuadDt(m1-1)/2              # Sweep ...
        uk1m1 += uk1m0                                      # Sweep ...
        uk1m1 += self._numInt(m1-1, k)                      # Sweep (end)
        # Easy to read sweep version ---------------------------------------
        # dtm = self.getCurrentQuadDt(m1-1)                   # dt{m}
        # uk1m1 = uk1m0 + dtm/2*(fuk1m0+fuk1m0p - fuk0m0-fuk0m0p) + \
        #     self._numInt(m1-1, k)
        # self.tabQuadFields[k+1][m1-1].setU(uk1m1)
        # # Store evaluation of u{k+1,m+1} ---------------------------------
        self.evalSpaceOp(uk1m1, tm1,
                         self.tabEvalQuadFields[k+1][m1].getU())
    def _sweepHybridTOAB(self, k, m1):
        #In the context of General Linear methods this is a 2-stage 2-step
        #It is composed of a Second Order Adams Bashforth and a second stage RK(HEUN2)
        """Peforms a Hybrid Multistep Predictor Corrector sweep

        The following formula has not been updated yet!

        .. math::
            u^{k1}_{m1} = u^{k1}_{m0} + \Delta_{t}/{2}((f(u^{k+1}_{m+1}) + \\
            f^*(u^{k+1}_{(m+1)})) - (f^*(u^{k}_{m} + f(u^{k}_{m}  )) + \\
            I_{m}^{m+1}(f(u^{k}))

        Being :math:`f^*(u^{k+1}_{m+1},t_{m+1}) = f[u_m^{k+1}+ \Delta_t*(f(u^{k+1}_{m},t_{m}) - f(u^{k}_{m},t_{m+1})+I_{m}^{m+1}(f(u^{k})))]`

        """
        tm1 = self.getCurrentQuadTime(m1)                             # t{m+1}
        dtmM1 = self.getCurrentQuadDt((m1-1)%(self.nQuad-1))          # dt{m}
        dtmM2 = self.getCurrentQuadDt((m1-2)%(self.nQuad-1))          # dt{m-1}
        alpha2 = -(dtmM1**2)/(2*dtmM2)                           # alpha{m-1}
        alpha1 = (2*dtmM1*dtmM2 + dtmM1**2)/(2*dtmM2)              # alpha{m}
        uk1m0 = self.tabQuadFields[k+1][m1-1].getU()        # u{k+1,m}
        fuk0m0 = self.tabEvalQuadFields[k][m1-1].getU()     # f(u{k,m})
        # Reuse technique
        fuk0m0p = self.tabEvalQuadFields[k][m1].getU()      # f(u{k,m+1})
        fuk1m0 = self.tabEvalQuadFields[k+1][m1-1].getU()   # f(u{k+1,m})
        if m1 > 1:
            fuk1m1M = self.tabEvalQuadFields[k+1][m1-2].getU()   # f(u{k+1,m-1})
            fuk0m1M = self.tabEvalQuadFields[k][m1-2].getU()     # f(u{k,m-1})
        else:
            fuk1m1M = self.tabEvalQuadFieldsM1[k+1][(m1-2)%(self.nQuad-1)].getU()   # f(u{k+1,m-1})
            fuk0m1M = self.tabEvalQuadFieldsM1[k][(m1-2)%(self.nQuad-1)].getU()     # f(u{k,m-1})
        uk1m1p = (uk1m0 + alpha2*(fuk1m1M - fuk0m1M) + alpha1*(fuk1m0 - fuk0m0) + self._numInt(m1-1, k))
        fuk1m0p = _np.copy(fuk1m0)
        self.evalSpaceOp(uk1m1p, tm1, fuk1m0p)
        # Optimized sweep version ------------------------------------------
        uk1m1 = self.tabQuadFields[k+1][m1].getU()          # u{k+1,m+1}
        _np.copyto(uk1m1, fuk1m0)                           # Sweep (beg)
        uk1m1 -= fuk0m0
        uk1m1 -= fuk0m0p                                    # Sweep ...
        uk1m1 += fuk1m0p                                    # Sweep ...
        uk1m1 *= self.getCurrentQuadDt(m1-1)/2              # Sweep ...
        uk1m1 += uk1m0                                      # Sweep ...
        uk1m1 += self._numInt(m1-1, k)                      # Sweep (end)
        # Easy to read sweep version ---------------------------------------
        # dtm = self.getCurrentQuadDt(m1-1)                   # dt{m}
        # uk1m1 = uk1m0 + dtm/2*(fuk1m0+fuk1m0p - fuk0m0-fuk0m0p) + \
        #     self._numInt(m1-1, k)
        # self.tabQuadFields[k+1][m1-1].setU(uk1m1)
        # # Store evaluation of u{k+1,m+1} ---------------------------------
        self.evalSpaceOp(uk1m1, tm1,
                         self.tabEvalQuadFields[k+1][m1].getU())

    def _numInt(self, m, k):
        """Numerical quadrature of correction integral"""
#       Not implemented because it is order 1!, the integral is approximated by
#       a constant value
#        if self.initialSweepType == 'SIMPLE' and k == 0:
#            integ = self.tabEvalQuadFields[k][m].getU()
#            integ *= self.getCurrentQuadDt(m)
#        else:
#            integ = self.matrixQuad[m, 0]*self.tabEvalQuadFields[k][0].getU()
#            for i in range(1, self.nQuad):
#                integ += self.matrixQuad[m, i]*self.tabEvalQuadFields[k][i].getU()
#            integ *= self.getCurrentDt()
#        return integ
        integ = self.matrixQuad[m, 0]*self.tabEvalQuadFields[k][0].getU()
        for i in range(1, self.nQuad):
            integ += self.matrixQuad[m, i]*self.tabEvalQuadFields[k][i].getU()
        integ *= self.getCurrentDt()
        return integ
    def _numIntRK(self,scurrent, m, k):
        """Numerical quadrature of correction integral"""
#       scurrent : Number of the current stage
#       Not implemented because it is order 1!, the integral is approximated by
#       a constant value
#        if self.initialSweepType == 'SIMPLE' and k == 0:
#            integ = self.tabEvalQuadFields[k][m].getU()
#            integ *= self.getCurrentQuadDt(m)
#        else:
#            integ = self.matrixQuad[m, 0]*self.tabEvalQuadFields[k][0].getU()
#            for i in range(1, self.nQuad):
#                integ += self.matrixQuad[m, i]*self.tabEvalQuadFields[k][i].getU()
#            integ *= self.getCurrentDt()
#        return integ
        integ = self.matrixQuadStage[scurrent][m, 0]*self.tabEvalQuadFields[k][0].getU()
        for i in range(1, self.nQuad):
            integ += self.matrixQuadStage[scurrent][m, i]*self.tabEvalQuadFields[k][i].getU()
        integ *= self.getCurrentDt()
        return integ
        # NFlop = nm*2*ndf + ndf
    def _numIntM1(self, m, k):
        """Numerical quadrature of correction integral"""
        integ = self.matrixQuad[m, 0]*self.tabEvalQuadFieldsM1[k][0].getU()
        for i in range(1, self.nQuad):
            integ += self.matrixQuad[m, i]*self.tabEvalQuadFieldsM1[k][i].getU()
        integ *= self.getCurrentDt()
        return integ
        # NFlop = nm*2*ndf + ndf
