# -*- coding: utf-8 -*-
"""
Description
-----------

TODO
"""
# Python imports
import time
import numpy as np

# Casper imports
from casper import common as util
from casper.rhs.base import RHS


class Phi(object):
    r"""
    Base class defining a time-stepper for a system of Ordinary Differential
    Equation (ODE) of the form :

    .. math::
        \frac{du}{dt} = f(u,t).

    The Phi object allows to evaluate the time-stepper on flat vector
    :math:`u` and given time :math:`t` using time step :math:`dt`.
    """

    LINEAR = False
    FIXED_DT = False

    def __init__(self, rhs):
        self.rhs = util.checkPar(rhs, 'rhs', RHS)
        self.jac = None

    @property
    def nDOF(self):
        """Number of degree of freedom for phi operator (i.e of its rhs)"""
        return self.rhs.nDOF

    @property
    def dtype(self):
        """Numerical type of the phi operator (i.e of its rhs)"""
        return self.rhs.dtype

    def eval(self, u, t, dt, uNext):
        """
        Evaluate the phi operator, supposing uNext is a different vector
        than u. Values of uNext are overwritten during the evaluation.

        Parameters
        ----------
        u : ``numpy.1darray``
            Initial solution vector.
        t : ``float``
            Time of the initial solution.
        dt : `float``
            Time-step for the next solution.
        uNext : ``numpy.1darray``
            Where to store the next step solution.

        Returns
        -------
        uNext : ``numpy.1darray``
            Next step solution vector

        """
        raise NotImplementedError('eval method must be implemented')

    def __call__(self, u, t=0, dt=1, uNext=None):
        u = np.asarray(u)
        shape = u.shape
        if uNext is None:
            uNext = np.empty_like(u)
        uNext.shape = (u.size,)
        uNext = self.eval(u.ravel(), t, dt, uNext)
        uNext.shape = shape
        return uNext

    def evalRHS(self, u, t=0, uEval=None):
        if uEval is None:
            uEval = np.empty_like(u)
        return self.rhs.eval(u, t, uEval)

    def computeJacobian(self, u, t, dt, eps=1e-8):
        """
        Base method, compute the Jacobian matrix of the Phi operator

        Parameters
        ----------
        u: ``numpy.1darray``
            Vector from which evaluate the Jacobian.
        t: ``float``
            Time which evaluate the Jacobian
        dt: ``float``
            Time step at which evaluate the Jacobian

        Returns
        -------
        jacobian: ``numpy.2darray``
            The Jacobian matrix
        """
        u = np.asarray(u).ravel()

        if self.jac is None:
            self.jac = np.empty((self.nDOF, self.nDOF))

        if self.LINEAR and self.rhs.LINEAR:
            try:
                self.jac[0, 0] = 0
            except ValueError:
                return self.jac

        uPerturb = np.copy(u)
        uEval0 = np.empty(self.nDOF)
        self.eval(u, t, dt, uEval0)
        for i in range(self.nDOF):
            uPerturb[i] += eps
            self.eval(uPerturb, t, dt, self.jac[:, i])
            self.jac[:, i] -= uEval0
            self.jac[:, i] /= eps
            uPerturb[i] = u[i]

        if self.LINEAR and self.rhs.LINEAR:
            self.jac.flags['WRITEABLE'] = False

        return self.jac

    def evalJacobian(self, uJac, tJac, dtJac, u, uEval=None):
        """
        Base method, evaluate the Jacobian of the Phi operator
        for a given vector.

        Parameters
        ----------
        uJac: ``numpy.1darray``
            Vector from which evaluate the Jacobian.
        tJac: ``float``
            Time which evaluate the Jacobian.
        dtJac: ``float``
            Time step at which evaluate the Jacobian.
        u: ``numpy.1darray``
            The fields to evaluate
        uEval: ``numpy.1darray``
            The evaluated fields

        Returns
        -------
        uEval: ``numpy.1darray``
            The evaluated fields
        """
        if uEval is None:
            uEval = np.empty_like(u)
        A = self.computeJacobian(uJac, tJac, dtJac)
        return np.dot(A, u, out=uEval)

    def storePerfo(self, dico):
        """
        Store computation performance data into a dictionary.
        Two entries will be updated during the computation :

        - 'tCompPhi': computation time spent on evaluating the Phi operator
        - 'nEvalPhi': number of Phi evaluations

        Parameters
        ----------
        dico: ``dict``
            Where to store the performance data
        """
        dico['nEvalPhi'] = dico.get('nEvalPhi', 0)
        dico['tCompPhi'] = dico.get('tCompPhi', 0.)

        def wrapper(call):
            @util.wraps(call)
            def wrapped(u, t, dt, uEval):
                """**storePerfo** with dictionary at %s"""
                tBeg = time.time()
                uEval = call(u, t, dt, uEval)
                dico['tCompPhi'] += time.time() - tBeg
                dico['nEvalPhi'] += 1
                return uEval
            wrapped.__doc__ %= hex(id(dico))
            return wrapped
        self.eval = wrapper(self.eval)
