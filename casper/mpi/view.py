# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the MPI initialization function for CASPER.
It imports the mpi4py module and all its mpi functions, if available.

Functions
---------
initMPI :
    Initialize MPI CommWorlds
"""
import inspect as _inspect

from .link import MPI, MPI4PY_AVAILABLE
from .comm import MPIComm


class MPIView(object):

    __PROTECTED__ = ('MPI', 'gComm', 'gRank', 'gSize',
                     'sComm', 'sRank', 'sSize', 'tComm', 'tRank', 'tSize')

    MPI = MPI

    gComm = MPIComm()
    gRank = 0
    gSize = 1

    sComm = MPIComm()
    sRank = 0
    sSize = 1

    tComm = MPIComm()
    tRank = 0
    tSize = 1

    @property
    def parallel(self):
        """Returns True if there is a space mpi parallelization"""
        return self.gSize > 1

    @property
    def spaceParallel(self):
        """Returns True if there is a space mpi parallelization"""
        return self.sSize > 1

    @property
    def timeParallel(self):
        """Returns True if there is a time mpi parallelization"""
        return self.tSize > 1

    def __str__(self):
        s = 'P({},{}-G)({},{}-S)({},{}-T)'.format(
            self.gRank, self.gSize, self.sRank, self.sSize,
            self.tRank, self.tSize)
        return s

    def __repr__(self):
        return self.__str__()

    def __setattr__(self, name, value):
        level = _inspect.stack()[1]
        allowed = level.function == 'initMPI' or \
            level.frame.f_locals.get('self') == self

        if name in self.__PROTECTED__ and not allowed:
            raise AttributeError(
                f"can't modify {name} class attribute of MPIView objects")

        super().__setattr__(name, value)


def initMPI(nps=None, npt=None):
    """Initialize the MPI COMMWORLDs from the global MPI COMMWORLD

    Parameters
    ----------
    nps : ``int`` in ****kwargs**
        The number of processes in space MPI COMMWORLD
    """
    # Initialize global CommWorld if mpi4py is available
    if MPI4PY_AVAILABLE:
        gComm = MPI.COMM_WORLD
        gRank = gComm.Get_rank()
        gSize = gComm.Get_size()
        if (npt is None) and (nps is None):
            npt = 1
            nps = gSize//npt
        elif nps is None:
            nps = gSize//npt
        elif npt is None:
            npt = gSize//nps
        MPIView.gComm.comm = gComm
    else:
        npt = 1
        nps = 1
        gRank = 0
        gSize = 1
    MPIView.gRank = gRank
    MPIView.gSize = gSize

    if gRank == 0:
        print("-- Starting MPI initialization --")

    # Check for inadequate decomposition
    if (gSize != nps*npt) and (gSize != 1):
        raise ValueError(f'product of nps ({nps}) with npt ({npt}) is not '
                         f'equal to the total number of processes ({gSize})')

    # Information message
    if gSize == 1:
        print(" - no parallelisation at all")
    else:
        if nps != 1:
            if gRank == 0:
                print(" - space parallelisation activated : {} mpi processes"
                      .format(nps))
        else:
            if gRank == 0:
                print(" - no space parallelisation")
        if npt != 1:
            if gRank == 0:
                print(" - time parallelisation activated : {} mpi processes"
                      .format(npt))
        else:
            if gRank == 0:
                print(" - no time parallelisation")
        if gRank == 0:
            print('-- Finished MPI initialization --')

    # MPI decomposition
    if MPI4PY_AVAILABLE:
        # Construction of MPI CommWorlds
        tColor = gRank % nps
        tComm = gComm.Split(tColor, gRank)
        gComm.Barrier()
        sColor = (gRank-gRank % nps)/nps
        sComm = gComm.Split(sColor, gRank)
        gComm.Barrier()
        # Activate MPI communication
        MPIView.sComm.comm = sComm
        MPIView.sRank = sComm.Get_rank()
        MPIView.sSize = sComm.Get_size()
        MPIView.tComm.comm = tComm
        MPIView.tRank = tComm.Get_rank()
        MPIView.tSize = tComm.Get_size()
