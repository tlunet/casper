"""
Description
-----------

Package containing the Message-Passing-Interface (MPI) objects and functions
for Casper. Contains also the Interface objects for MPI use, and the MPI
initialization function.

Attributes
----------
mpi : ``MPIView``
    MPI interface object

Methods
-------
initMPI(**kwargs) :
    Initialize the MPI COMMWORLDs

Examples
--------

MPI use with Fortran type interface :
(the script has to be run like `mpirun -n $NPROCESS python script.py`)

>>> from casper.mpi import initMPI, mpi
>>> initMPI()
>>> rank = mpi.gRank
>>> size = mpi.gSize
>>> print(f"Hello world ! I'm process {rank} in CommWorld of size {size}")

Definition of space and time MPI COMMWORLDs
(script run with 6 MPI processes)

>>> import time
>>> from casper.mpi import initMPI, mpi
>>> initMPI(nps=3, npt=2)
-- Starting MPI initialization --
 - space parallelisation activated : 3 mpi processes
 - time parallelisation activated : 2 mpi processes
-- Finished MPI initialization --
>>> time.sleep(0.1*gRank)
>>> print(mpi)
P(0,6-G)(0,3-S)(0,2-T)
P(1,6-G)(1,3-S)(0,2-T)
P(2,6-G)(2,3-S)(0,2-T)
P(3,6-G)(0,3-S)(1,2-T)
P(4,6-G)(1,3-S)(1,2-T)
P(5,6-G)(2,3-S)(1,2-T)

Here, P(2,6-G)(2,3-S)(0,2-T) is the message printed by the process with rank
2 in the global CommWorld of size 6 (2,6-G), with rank 2 in a space CommWorld
of size 3 (2,3-6), etc ...

Contains
--------
...
"""
from . import link, comm, view

initMPI = view.initMPI
mpi = view.MPIView()


__all__ = ["link", "comm", "view", "mpi", "initMPI"]
