#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 11 11:25:21 2019

@author: lunet
"""
try:
    from mpi4py import MPI
    MPI4PY_AVAILABLE = True
except ImportError:
    print("Warning: mpi4py package not available => no MPI set")
    MPI = None
    MPI4PY_AVAILABLE = False
