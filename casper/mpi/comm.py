# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the interface for MPI function for CASPER.

Classes
-------
MPICommon : Base interface object
    Interface with the basics functions to check parallelization

MPIFortran : Interface object
    Interface with Fortran name-based MPI functions
"""
import inspect as _inspect
from .link import MPI


class MPIComm():

    __PROTECTED__ = ('MPI', 'comm')

    comm = None
    MPI = MPI

    def __init__(self, comm=None):
        if comm is None:
            try:
                self.comm = MPI.COMM_WORLD
            except Exception:
                self.comm = None
        else:
            self.comm = comm

    def __setattr__(self, name, value):
        level = _inspect.stack()[1]
        allowed = level.function == 'initMPI' or \
            level.frame.f_locals.get('self') == self

        if name in self.__PROTECTED__ and not allowed:
            raise AttributeError(
                f"can't modify {name} class attribute of MPIComm objects")

        super().__setattr__(name, value)

    def COMM_RANK(self):
        """Returns the rank of the process in the MPI CommWorld"""
        return self.comm.Get_rank()

    def COMM_SIZE(self):
        """Returns the size of the MPI CommWorld"""
        return self.comm.Get_rank()

    def ABORT(self):
        """Abort all processes in the MPI CommWorld"""
        self.comm.Abort()

    def BARRIER(self):
        """Wait for all other processes in the MPI CommWorld"""
        self.comm.Barrier()

    def ISEND(self, data, dest, tag):
        """
        Non blocking send in the MPI CommWorld, the process do not wait
        for the message to be received by the destination process to continue.
        Has to be used together with a reception method (e.g RECV).

        Parameters
        ----------
        data: ``numpy.array``
            The numpy array with the data to send
        dest: ``int``
            The rank of the processor to send the data in the time MPI
            commworld
        tag: ``int``
            A tag for the message

        Examples
        --------
        >>> #TODO
        """
        self.comm.Isend(data, dest=dest, tag=tag)

    def RECV(self, data, source, tag):
        """
        Blocking receive in the MPI CommWorld, the process wait to finish
        receiving the data to continue.
        Has to be used together with a sending method (e.g ISEND).

        Parameters
        ----------
        data: ``numpy.array``
            The numpy array in which to receive the data
        dest: ``int``
            The rank of the processor to receive the data from in the time MPI
            commworld
        tag: ``int``
            A tag for the message

        Examples
        --------
        >>> #TODO
        """
        self.comm.Recv(data, source=source, tag=tag)

    def IRECV(self, data, source, tag):
        """
        Non-blocking receive in the MPI CommWorld, the process can perform
        actions even before receiving all the data.
        Has to be used together with a sending method (e.g ISEND).

        Parameters
        ----------
        data: ``numpy.array``
            The numpy array in which to receive the data
        dest: ``int``
            The rank of the processor to receive the data from in the time MPI
            commworld
        tag: ``int``
            A tag for the message

        Examples
        --------
        >>> #TODO
        """
        return self.comm.Irecv(data, source=source, tag=tag)

    def ALLREDUCE(self, sendData, recvData, operation):
        """
        Perform a reducing operation for all processes in the MPI CommWorld.
        The reduction handle data send by all processor, perform a selected
        operation on those data, and then send it back to each processor.

        Parameters
        ----------
        sendData: ``numpy.array``
            The numpy array with the data to reduce
        recvData: ``numpy.array``
            The numpy array with reduced data
        operation: ``str``
            A string representing the operation to do :
        * operation='MIN' : compute the minimum off all send data
        * operation='MAX' : compute the maximum off all send data
        * operation='SUM' : compute the sum of all send data

        Examples
        --------
        >>> #TODO
        """
        if operation == 'MIN':
            op = self.MPI.MIN
        elif operation == 'MAX':
            op = self.MPI.MAX
        elif operation == 'SUM':
            op = self.MPI.SUM
        else:
            raise ValueError('{} reduce operation not yet implemeted'
                             .format(operation))
        self.comm.Allreduce(sendData, recvData, op)

    def GATHER(self, sendData, recvData, root=0):
        """
        Gather data from all process to the root process in MPI CommWorld

        Parameters
        ----------
        sendData: ``numpy.array``
            The numpy array with the data to gather
        recvData: ``numpy.array`
            The numpy array to store the received data. recvData size has
            to be equal to size(sendData)*commSize.
            For ranks other than the root process, recvData can be set to None.
        root: ``int``
            The rank of the root process (default=0)

        Examples
        --------
        >>> #TODO
        """
        self.comm.Gather(sendData, recvData, root)

    def GATHERV(self, sendData, recvData, root=0):
        """
        Gather data of variable length from all process to the root process
        in the MPI CommWorld

        Parameters
        ----------
        sendData: ``numpy.array``
            The numpy array with the data to gather
        recvInfo: ``list``
            List containing (ordered) informations for data reception

        - recvData: ``numpy.array``
            Where to store all the received data on the root process
        - splitSizes: ``list`` or ``numpy.array`` of ``int``
            List the number of elements sent by each processes
            (one number = one element)

        This list can be completed by two additionnal parameters (if given,
        those have to be given together)

        - globIndexes: ``list`` or ``numpy.array`` of ``int``
            List, for each processes, the index where to start writing the
            received data on the **recvData** array
        - elementType: ``mpi4py.MPI.Datatype``
            Type of each sent elements (ex: MPI.INT, MPI.DOUBLE, ...)

        root: ``int``
            The rank of the root process (default=0)

        Examples
        --------
        >>> #TODO
        """
        self.comm.Gatherv(sendData, recvData, root)

    def ALLGATHER(self, sendData, recvData):
        """
        Gather data from all process and distribute it to all processes in
        the MPI CommWorld.

        Parameters
        ----------
        sendData: ``numpy.array``
            The numpy array with the data to gather
        recvData: ``numpy.array``
            Where to store the received data. The size of recvData has
            to be equal to size(sendData)*commSize.

        Examples
        --------
        >>> #TODO
        """
        self.comm.Allgather(sendData, recvData)

    def FILE_OPEN(self, fileName, mode):
        """
        Open a file where all processes in the MPI CommWorld can write
        together.

        Parameters
        ----------
        fileName: ``str``
            The name of the file
        mode: ``str``
            The file opening mode
        - mode='WRONLYCREATE' : open the file in write only mode, create a new
          file if it does not exist, overwrite the old one it exists.
        - mode='WRONLYAPPEND' : open the file in write only mode, and set the
          offset to the end of the file (use not recommended).
        - mode='WRONLY' : open the file in write only mode, the file has to
          be an already existing file.
        - mode='RDONLY' : open the file in read only mode, the file has to
          be an already existing file.

        Returns
        -------
        mpiFile: ``mpi4py.MPI.File``
            the object representing the mpi file (use to write and read)

        Note
        ----
        At the end of the file manipulation, it has to be close with the
        FILE_CLOSE(mpiFile) method

        Examples
        --------
        >>> #TODO
        """
        if mode == 'WRITEONLYCREATE':
            amode = self.MPI.MODE_WRONLY | self.MPI.MODE_CREATE
        elif mode == 'WRONLYAPPEND':
            amode = self.MPI.MODE_WRONLY | self.MPI.MODE_APPEND
        elif mode == 'WRONLY':
            amode = self.MPI.MODE_WRONLY
        elif mode == 'RDONLY':
            amode = self.MPI.MODE_RDONLY
        else:
            raise NotImplementedError()
        return self.MPI.File.Open(self.comm, fileName, amode)

    @staticmethod
    def WRITE_AT_ALL(mpiFile, offset, data):
        """
        Make all processes write data into a mpi file, with a given offset

        Parameters
        ----------
        mpiFile: ``mpi4py.MPI.File``
            The object representing the mpi file (returned by a FILE_OPEN_S
            method)
        offset: ``int``
            The size of the offset (in bytes)
        data: ``numpy.array``
            The data to write

        Examples
        --------
        >>> #TODO
        """
        mpiFile.Write_at_all(offset, data)

    @staticmethod
    def WRITE_AT(mpiFile, offset, data):
        """
        Make only this process write data into a mpi file, with a given offset

        Parameters
        ----------
        mpiFile: ``mpi4py.MPI.File``
            The object representing the mpi file (returned by a FILE_OPEN_S
            method)
        offset: ``int``
            The size of the offset (in bytes)
        data: ``numpy.array``
            The data to write into the mpi file

        Note
        ----
        WRITE_AT behavior is similar as WRITE_AT_ALL, but must be avoided in
        case of simultaneous file writing of each processes.
        """
        mpiFile.Write_at(offset, data)

    @staticmethod
    def READ_AT_ALL(mpiFile, offset, data):
        """
        Make all processes read data from a mpi file, with a given offset

        Parameters
        ----------
        mpiFile : ``mpi4py.MPI.File``
            The object representing the mpi file (returned by a FILE_OPEN_S
            method)
        offset : ``int``
            The size of the offset (in bytes)
        data : ``numpy.array``
            The numy array into which the process will read the data

        Examples
        --------
        >>> #TODO
        """
        mpiFile.Read_at_all(offset, data)

    @staticmethod
    def READ_AT(mpiFile, offset, data):
        """
        Make only this process read data from a mpi file, with a given offset

        Parameters
        ----------
        mpiFile: ``mpi4py.MPI.File``
            The object representing the mpi file (returned by a FILE_OPEN_S
            method)
        offset: ``int``
            The size of the offset (in bytes)
        data: ``numpy.array``
            The numy array into which the process will read the data

        Note
        ----
        READ_AT behavior is similar as READ_AT_ALL, but must be avoided in
        case of simultaneous file reading of each processes.
        """
        mpiFile.Read_at(offset, data)

    @staticmethod
    def FILE_CLOSE(mpiFile):
        """Close the mpi file"""
        mpiFile.Close()
