# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 11:32:47 2015

@author: t.lunet
"""

import os
import sys
import inspect
import glob

def list_dir(package_dir,doc_dir,code_dir):
    tab_elt = os.listdir(package_dir)
    if '__init__.py' not in tab_elt:
        print("Not a python package directory")
        return
    print("Python Package directory = ",tab_elt)
    tab_elt.remove('__init__.py')
    if '__init__.pyc' in tab_elt :
        tab_elt.remove('__init__.pyc')
    tab_packages = []
    tab_modules = []
    for elt in tab_elt:
        if not elt.startswith('.'):
            if os.path.isdir(package_dir+'/'+elt):
                tab_packages += [elt]
            elif elt.endswith('.py'):
                tab_modules += [elt[:-3]]
    package_name = package_dir.replace('/','.')
    print("Packages = ",tab_packages)
    write_package_rst(package_name,tab_packages,tab_modules,doc_dir,code_dir)
    for elt in tab_packages:
        list_dir(package_dir+'/'+elt,doc_dir,code_dir)
    print("Modules = ",tab_modules)
    for elt in tab_modules:
        list_module(package_dir+'/'+elt,doc_dir)


def list_module(module_name, doc_dir):
    module_name = module_name.replace('/', '.')
    module = module_name
    exec('import {0} as module'.format(module))
    print("Python module = ", module_name)

    tab_classes = inspect.getmembers(module, isclassof(module))
    tab_classes = [elt[1].__module__+'.'+elt[0] for elt in tab_classes]
    print("Classes = ", tab_classes)
    for classe in tab_classes:
        write_classe(classe, doc_dir)

    tab_functions = inspect.getmembers(module, isfunctionof(module))
    tab_functions = [elt[1].__module__+'.'+elt[0] for elt in tab_functions]
    print("Functions = ", tab_functions)
    for function in tab_functions:
        write_function(function, doc_dir)

    write_module_rst(module_name, tab_classes, tab_functions,
                     doc_dir)


def isclassof(mod):
    def func(obj):
        if hasattr(mod, '__name__'):
            return inspect.isclass(obj) and obj.__module__ == mod.__name__
        else:
            return False
    return func


def isfunctionof(mod):
    def func(obj):
        return inspect.isfunction(obj) and \
            (obj.__module__ == mod.__name__ or
             obj.__module__ == mod.__package__+'.base')
    return func


def write_module_rst(module_name, tab_classes, tab_functions,
                     doc_dir):
    f = open(doc_dir+module_name+'.rst', 'w')
    f.write(module_name+' module\n' +
            '='*(len(module_name)+7) +
            '\n\n' +
            '.. automodule:: '+module_name+'\n' +
            '.. toctree::\n' +
            '    :maxdepth: 1\n\n')
    for classe in tab_classes:
        f.write('    '+classe+'\n')
    for function in tab_functions:
        f.write('    '+function+'\n')
    f.write('\n\n' +
            'Indices and tables\n' +
            '------------------\n' +
            '\n' +
            '* :ref:`genindex`\n' +
            '* :ref:`modindex`\n' +
            '* :ref:`search`')
    f.close()


def write_package_rst(package_name, tab_packages, tab_modules, doc_dir,
                      code_dir):
    if package_name == code_dir:
        f = open(doc_dir+'index.rst', 'w')
        f.write('.. automodule:: '+package_name+'\n' +
                '   :members:\n' +
                '   :undoc-members:\n' +
                '   :private-members:\n' +
                '.. toctree::\n' +
                '    :maxdepth: 1\n' +
                '\n')
    else:
        f = open(doc_dir+package_name+'.rst', 'w')
        f.write(package_name+' package\n' +
                '='*(len(package_name)+8) +
                '\n\n' +
                '.. automodule:: '+package_name +
                '\n\n' +
                '.. toctree::\n' +
                '    :maxdepth: 1\n\n')
    for package in tab_packages:
        f.write('    '+package_name+'.'+package+'\n')
    for module in tab_modules:
        f.write('    '+package_name+'.'+module+'\n')
    f.write('\n\n' +
            'Indices and tables\n' +
            '------------------\n' +
            '\n' +
            '* :ref:`genindex`\n' +
            '* :ref:`modindex`\n' +
            '* :ref:`search`')
    f.close()


def write_classe(classe_name, doc_dir):
    f = open(doc_dir+classe_name+'.rst', 'w')
    f.write(classe_name+' class\n' +
            '='*(len(classe_name)+6) +
            '\n\n' +
            '.. autoclass:: '+classe_name+'\n' +
            '    :members:\n' +
            '    :private-members:\n' +
            '    :undoc-members:\n' +
            '    :inherited-members:\n' +
            '    :show-inheritance:')
    f.write('\n\n' +
            'Indices and tables\n' +
            '------------------\n' +
            '\n' +
            '* :ref:`genindex`\n' +
            '* :ref:`modindex`\n' +
            '* :ref:`search`')
    f.close()


def write_function(function_name, doc_dir):
    f = open(doc_dir+function_name+'.rst', 'w')
    f.write(function_name+' function\n' +
            '='*(len(function_name)+9) +
            '\n\n' +
            '.. autofunction:: '+function_name+'\n')
    f.write('\n\nIndices and tables\n' +
            '------------------\n' +
            '\n' +
            '* :ref:`genindex`\n' +
            '* :ref:`modindex`\n' +
            '* :ref:`search`')
    f.close()


def write_attr(attr_name, doc_dir):
    f = open(doc_dir+attr_name+'.rst', 'w')
    f.write(attr_name+' function\n' +
            '='*(len(attr_name)+9) +
            '\n\n' +
            '.. autoattribute:: '+attr_name+'\n')
    f.write('\n\nIndices and tables\n' +
            '------------------\n' +
            '\n' +
            '* :ref:`genindex`\n' +
            '* :ref:`modindex`\n' +
            '* :ref:`search`')
    f.close()


def main(code_dir, doc_dir):
    code_dir = code_dir
    doc_dir = doc_dir
    if not doc_dir.endswith('/'):
        doc_dir += '/'
    for old_rst in glob.glob(doc_dir+'*.rst'):
        os.remove(old_rst)
    if code_dir.endswith('.py'):
        list_module(code_dir[:-3], doc_dir)
    elif os.path.isdir(code_dir):
        list_dir(code_dir, doc_dir, code_dir)
    else:
        raise ValueError('code_dir not package or module')

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
