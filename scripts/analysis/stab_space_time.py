# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 15:53:47 2015

@author: Tibo
"""
import numpy as np
import matplotlib.pyplot as plt
from casper.maths.neumann import \
    computeR, findCFLMax, amplificationFactor, fourierSymbol
from casper.maths.butcher import tablesExplicit, tablesImplicit


# Reglage generaux de matplotlib
plt.rc('font', size=12)
plt.rcParams['lines.linewidth'] = 2
plt.rcParams['axes.titlesize'] = 18
plt.rcParams['axes.labelsize'] = 16
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16
plt.rcParams['xtick.major.pad'] = 5
plt.rcParams['ytick.major.pad'] = 5
plt.rcParams['axes.labelpad'] = 1
plt.rcParams['image.cmap'] = 'jet'

# Definition de la discretisation du maillage (CFL, nombre d'onde)
num = 501
marge = 0.0

# Definition du schema spatial et temporel
spaceScheme = 'U3'
timeScheme = 'RK33'

normCFL = True

# Facteur d'amplification, type d'integration
g = amplificationFactor(timeScheme)
tType = 'EXPLICIT' if timeScheme in tablesExplicit else \
    'IMPLICIT' if timeScheme in tablesImplicit else \
    None

# Calcul de la solution numerique
stabx, zx, kappa, cfl = computeR(spaceScheme, timeScheme,
                                 numK=num, numCFL=num)
zz = -np.dot(cfl, zx)

# Definition de la solution theorique (à vérifier)
# solTh = np.exp(-np.dot(cfl, kappa*1j))
# acc = np.abs(g(zz)-np.exp(solTh))

# Definition de la precision relative de l'integration temporelle
# => acc = |solExp - solNum|/|solExp|
acc = np.abs(g(zz)-np.exp(zz))

# Calcul du contour de |g|
stabx = np.abs(stabx)

# Determination du CFL max
cflMax = findCFLMax(stabx, cfl, stabMax=1.0)[0]
print('RK Scheme = {}, Space Discr = {}, CFL Max = {}'
      .format(timeScheme, spaceScheme, cflMax))

# Plot du contour de stabilite-precision (Stability-Accuracy)
if True:
    xpos = 0.6
    plt.figure('StabAcc')
    # Contour de stabilite
    kappa, cfl = np.meshgrid(kappa, cfl)
    if normCFL:
        stabx_ = stabx**(1./(cfl+1e-16))
    else:
        stabx_ = stabx
    plt.contourf(kappa/np.pi, cfl, stabx_, levels=np.arange(0, 2, 0.05))
    plt.colorbar()
    if True:  # Rajouter les contours de precision
        levels2 = [1e-1]
        if normCFL:
            acc_ = acc**(1./(cfl+1e-16))
        else:
            acc_ = acc
        CS30 = plt.contour(kappa/np.pi, cfl, acc_,
                           levels=levels2,
                           colors='magenta',
                           linestyles='dashed')
        ypos = [float(findCFLMax(acc, cfl, stabMax=v)[0]) for v in levels2]
        plt.clabel(CS30, fontsize=9, inline=1, fmt='%1.e',
                   manual=zip([xpos]*len(ypos), ypos))
    # Mise en forme
    plt.xlim(0, 1)
    plt.xlabel('$\\Theta_k/\\pi$')
    plt.ylabel('$CFL$')
    # Iso-contours |g|=1
    CS3 = plt.contour(kappa/np.pi, cfl, stabx, levels=[1],
                      colors='red',
                      linestyles='dashed',
                      linewidths=2)
    if stabx.min() < 0.5:
        CS4 = plt.contour(kappa/np.pi, cfl, stabx, levels=[0.5],
                          colors='blue',
                          linestyles='dashed',
                          linewidths=2)
    if tType == 'EXPLICIT':
        plt.clabel(CS3, fontsize=9, inline=1, manual=[(xpos, 2.5)],
                   fmt='%1.1f')
        if stabx.min() < 0.5:
            plt.clabel(CS4, fontsize=9, inline=1, manual=[(xpos, 1.2)],
                       fmt='%1.1f')
    # Sauvegarder la figure
    plt.savefig('stabAcc_{}_{}{}.pdf'
                .format(spaceScheme, timeScheme,
                        '_normCFL' if normCFL else ''),
                bbox_inches='tight')
    plt.show()

# Plot du symbole de Fourier et du spectre temporel
if True:
    plt.figure('Spectra')
    num2 = 200  # Resolution
    za = np.linspace(-5, 2, num2)
    zb = np.linspace(-5, 5, num2)
    za, zb = np.meshgrid(za, zb)
    grk = abs(g(za+1j*zb))
    gs = fourierSymbol(spaceScheme, numK=num2//2, kMin=-np.pi)[0].ravel()
#    if spaceScheme == 'ADV-S':
#        gs -= 1j*np.pi

    # Forme pour le symbole de Fourier
    fsy = 'o'  # soit 'o'=>ronds, soit '--'=>tirets

    if True:  # Symbole de Fourier
        plt.plot(-gs.real, -gs.imag, fsy, linewidth=2,
                 label='{}'.format(spaceScheme))
    if True:  # Symbole de Fourier, CFL=1 et CFLmax
        plt.plot(-gs.real, -gs.imag, 'y'+fsy,
                 label='{}, CFL=1.00'.format(spaceScheme))
        plt.plot(-cflMax*gs.real, -cflMax*gs.imag, 'g'+fsy,
                 label='{0}, CFL={1:0.2f}'.format(spaceScheme, cflMax))
    if True:  # Spectre temporel
        CS3 = plt.contour(za, zb, grk, levels=[1.], colors='r')
        CS3.collections[0].set_label(timeScheme+' method')
    # Mise en forme
    plt.legend(loc=0)
    plt.axhline(y=0, color='#adadad', linestyle='--')
    plt.axvline(x=0, color='#adadad', linestyle='--')
    marge = 0.5
    plt.axis('scaled')
    if tType == 'EXPLICIT':
        plt.xlim(np.min((grk < 1)*za) - marge, np.max((grk < 1)*za) + marge)
        plt.ylim(np.min((grk < 1)*zb) - marge, np.max((grk < 1)*zb) + 2*marge)
    elif tType == 'IMPLICIT':
        plt.xlim(np.min((grk > 1)*za) - marge, np.max((grk > 1)*za) + marge)
        plt.ylim(np.min((grk > 1)*zb) - marge, np.max((grk > 1)*zb) + 2*marge)
    plt.show()
