#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  8 16:17:49 2017

@author: lunet
"""
import casper.maths.pde as pde
import matplotlib.pyplot as plt

np = pde.np

c = 1
nu = 0.01
tEnd = 0.3
x0 = 0
xn = 1
initType = 'STEP'

eqTermAdv = ['ADV', c]
eqTermDiff = ['DIFF', nu]

x = np.linspace(x0, xn, num=400, endpoint=False)
u0 = pde.initialSolution(x, initType, x0, xn)
uAdv = pde.analyticSolutionAdvDiff(x, initType, x0, xn, tEnd, [eqTermAdv])
uDiff = pde.analyticSolutionAdvDiff(x, initType, x0, xn, tEnd, [eqTermDiff])
uAdvDiff = pde.analyticSolutionAdvDiff(x, initType, x0, xn, tEnd,
                                       [eqTermAdv, eqTermDiff])

plt.plot(x, u0, label='Initial Solution')
plt.plot(x, uAdv, label='Advection')
plt.plot(x, uDiff, label='Diffusion')
plt.plot(x, uAdvDiff, label='Adv.-Diff.')
plt.xlabel('$x$')
plt.ylabel('$u$')
plt.title('Solution $u(x,t)$ at $t={:1.1f}$, $c={}$, $\\nu={}$'
          .format(tEnd, c, nu))
plt.legend()
plt.grid()
plt.savefig(__file__[:-3]+'_fig1.pdf', bbox_inches='tight')
