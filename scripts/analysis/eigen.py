#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 14:22:10 2021

@author: telu
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.linalg import eigvals

from casper.fields import Field1D
from casper.rhs.mesh1d import LinAdvDiffReac1D, DampedString
from casper.phi import ERK, IRK
from casper.maths.neumann import zAdv, stabilityFunction

cmap = cm.get_cmap('coolwarm_r', 9)
nX = 250


# %% Advection with upwind
f0 = Field1D(nX)
rhs = LinAdvDiffReac1D(f0, a=1, nu=0.0, epsilon=0, advScheme='U5')
phi = ERK(rhs, scheme='RK4', dt=f0.dx*0.5)

plt.figure()
theta = np.linspace(0, 2*np.pi, endpoint=False, num=500)
circle = np.exp(1j*theta)

jac = phi.computeJacobian(None, None, None)
A = jac
w = eigvals(A.todense())
plt.plot(w.real, w.imag, 'o', c=cmap(0))

for i in range(8):
    for j in range(1):
        A = A.dot(jac)
    w = eigvals(A.todense())
    plt.plot(w.real, w.imag, 'o', c=cmap(i+1))
plt.plot(circle.real, circle.imag, '--', c='gray')

plt.xticks([-1, -0.5, 0, 0.5, 1])
plt.yticks([-1, -0.5, 0, 0.5, 1])
plt.gca().set_aspect('equal', 'box')
plt.grid(True)

# %% Advection-diffusion
f0 = Field1D(nX)
rhs = LinAdvDiffReac1D(f0, a=1, nu=0.0005, advScheme='C4', diffScheme='C4')
phi = ERK(rhs, scheme='RK4', dt=f0.dx*0.5)

plt.figure()
theta = np.linspace(0, 2*np.pi, endpoint=False, num=500)
circle = np.exp(1j*theta)

jac = phi.computeJacobian(None, None, None)
A = jac
w = eigvals(A.todense())
plt.plot(w.real, w.imag, 'o', c=cmap(0))

for i in range(8):
    for j in range(1):
        A = A.dot(jac)
    w = eigvals(A.todense())
    plt.plot(w.real, w.imag, 'o', c=cmap(i+1))
plt.plot(circle.real, circle.imag, '--', c='gray')

plt.xticks([-1, -0.5, 0, 0.5, 1])
plt.yticks([-1, -0.5, 0, 0.5, 1])
plt.gca().set_aspect('equal', 'box')
plt.grid(True)

# %% Wave equation with viscoelastic damping
f0 = Field1D(nX, xPeriodic=False, nVar=2)
gamma = 0.001
rhs = DampedString(f0, c=1.0, gamma=gamma, r=0.0)
dt = f0.dx*0.45
phi = IRK(rhs, scheme='SDIRK2', dt=dt)

plt.figure()
jac = phi.computeJacobian(f0[0].ravel(), 0, dt)
A = jac
w = eigvals(A)
plt.plot(w.real, w.imag, 'o', c=cmap(0))

for i in range(8):
    for j in range(1):
        A = A.dot(jac)
    w = eigvals(A)
    plt.plot(w.real, w.imag, 'o', c=cmap(i+1))
plt.plot(circle.real, circle.imag, '--', c='gray')

plt.xticks([-1, -0.5, 0, 0.5, 1])
plt.yticks([-1, -0.5, 0, 0.5, 1])
plt.gca().set_aspect('equal', 'box')
plt.grid(True)

# %% Wave equation with fluid damping
f0 = Field1D(nX, xPeriodic=False, nVar=2)
gamma = 0.0
rhs = DampedString(f0, c=1, gamma=gamma, r=100.0)
dt = f0.dx*0.45
phi = IRK(rhs, scheme='SDIRK2', dt=dt)

plt.figure()
jac = phi.computeJacobian(f0[0].ravel(), 0, dt)
A = jac
w = eigvals(A)
plt.plot(w.real, w.imag, 'o', c=cmap(0))

for i in range(8):
    for j in range(1):
        A = A.dot(jac)
    w = eigvals(A)
    plt.plot(w.real, w.imag, 'o', c=cmap(i+1))
plt.plot(circle.real, circle.imag, '--', c='gray')

plt.xticks([-1, -0.5, 0, 0.5, 1])
plt.yticks([-1, -0.5, 0, 0.5, 1])
plt.gca().set_aspect('equal', 'box')
plt.grid(True)

# %% 2D advection with Upwind
thetaX = np.linspace(-np.pi, np.pi, num=16, endpoint=False)[None, :]
thetaY = np.linspace(-np.pi, np.pi, num=16, endpoint=False)[:, None]

cfl = 0.5
lamX = zAdv(thetaX, spaceScheme='C4')
lamY = zAdv(thetaY, spaceScheme='U1')
stabFunc = stabilityFunction('RK4')
eigs = stabFunc(-cfl*(lamX + lamY).ravel())

plt.figure()
w = eigs
plt.plot(w.real, w.imag, 'o', c=cmap(0))

for i in range(8):
    w = eigs*w
    plt.plot(w.real, w.imag, 'o', c=cmap(i+1))
plt.plot(circle.real, circle.imag, '--', c='gray')

plt.xticks([-1, -0.5, 0, 0.5, 1])
plt.yticks([-1, -0.5, 0, 0.5, 1])
plt.gca().set_aspect('equal', 'box')
plt.grid(True)