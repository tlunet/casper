#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  7 14:05:35 2017

@author: lunet
"""
import casper.spaceop.ode as spop
import casper.timesolver as cts
import casper.fields.f0d as f0d
import numpy as np
import matplotlib.pyplot as plt

rhs = spop.PolyUT()
f0 = f0d.Fields(1, sizeFGlob=1)
f0.getU()[:] = 1
u0 = f0.getU()

dtTest = [2., 1., 0.5, 0.1, 0.05]
tFinal = max(dtTest)

globalSettings = {
    'f0': f0, 'spaceOp': rhs,
    'stepping': 'FIXED', 'dt': 'UNDEF', 'tEnd': tFinal, 'tBeg': 0.,
    'timeStorage': 'CURRENT',
    'dumpTimes': 'NEVER'}


for RKS in ['FE', 'RK21', 'RKC2', 'RK33', 'RKC4', 'RK53']:
    settings = globalSettings.copy()
    settings.update({'RKS': RKS})

    errTest = []
    for dt in dtTest:
        settings['dt'] = dt
        solver = cts.ERKSolver(**settings)
        solver.solve()
        uF = solver.getCurrentFields().getU()
        errTest.append(abs(np.exp(tFinal)*u0-uF))

    plt.loglog(dtTest, errTest, 'o-', label=RKS)

for RKS in []:
    settings = globalSettings.copy()
    settings.update({'RKS': RKS})

    errTest = []
    for dt in dtTest:
        settings['dt'] = dt
        solver = cts.IRKSolver(**settings)
        solver.solve()
        uF = solver.getCurrentFields().getU()
        errTest.append(abs(np.exp(tFinal)*u0-uF))

    plt.loglog(dtTest, errTest, label=RKS)

plt.legend()
plt.grid(which='both', color='gray', linestyle='--', linewidth=0.5)
plt.title('Solution of $\\dot{u}=u, \\; u_0=1$ at $t=1$')
plt.xlabel('$\\Delta_t$')
plt.ylabel('Error')
plt.savefig(__file__[:-3]+'_fig1.pdf', bbox_inches='tight')
