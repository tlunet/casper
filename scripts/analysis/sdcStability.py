# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 17:14:35 2015

@author: t.lunet
"""
import casper.timesolver as cts
import casper.spaceop.ode as spo
import casper.fields.f0d as f0d
import numpy as np
import matplotlib.pyplot as plt

num = 200
xMinMax = (-15, 2)
yMinMax = (-6, 6)
reLam = np.linspace(*xMinMax, num=num)
imLam = np.linspace(*yMinMax, num=num)
reLam, imLam = np.meshgrid(reLam, imLam)

coeff = reLam + 1j*imLam
u0 = 1

f0 = f0d.Fields(nFields=num**2, sizeFGlob=1, numType='COMPLEX')
f0.setU(u0)

spaceOp = spo.PolyUT(coeffU=[coeff.reshape((num**2))])

# Runge-Kutta settings
dt = 1.
sdcSettings = {'f0': f0.copy(),
               'spaceOp': spaceOp,
               'stepping': 'FIXED',
               'timeStorage': 'CURRENT',
               'dumpTimes': 'NEVER',
               'tBeg': 0.0,
               'tEnd': dt,
               'dt': dt,
               'nSweep': None,
               'nQuad': 5,
               'quadForm': 'LEGENDRE',
               'initialSweepType': 'SIMPLE',
               'sweepType': None,
               'sweepStorage': 'CURRENTK'
               }

lSweepType = ['FE', ['SO', 'FE']]
lNSweep = [2, 3]
lColor = plt.rcParams['axes.prop_cycle'].by_key()['color']
iColor = -1

plt.figure()
for sweepType in lSweepType:
    for nSweep in lNSweep:
        iColor = (iColor+1) % len(lColor)
        sdcSettings['sweepType'] = sweepType
        sdcSettings['nSweep'] = nSweep
        solver = cts.SDCSolver(**sdcSettings)
        solver.solve()
        u1 = solver.getCurrentFields().getU()

        g = abs(u1).reshape((num, num))
        ls = 'dotted' if sweepType == 'FE' else 'dashed'
        CS3 = plt.contour(reLam, imLam, g, levels=[1.], colors=lColor[iColor],
                          linestyles=ls, linewidths=2)
        CS3.collections[0].set_label(
            '{}, {}={}'.format(sweepType, '$N_{Sweep}$', nSweep-1))

plt.legend(loc=2)
plt.axis('scaled')
plt.xlim(*xMinMax)
plt.ylim(*yMinMax)
plt.xlabel('$\\mathcal{R}(z)$')
plt.ylabel('$\\mathcal{I}(z)$')
plt.grid()
plt.title('Stability spectra')
plt.savefig(__file__[:-3]+'_fig1.pdf', bbox_inches='tight')
