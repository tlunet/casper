# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 17:14:35 2015

@author: t.lunet
"""
import casper.timesolver as cts
import casper.spaceop.ode as spo
import casper.fields.f0d as f0d
import numpy as np
import matplotlib.pyplot as plt

num = 200
xMinMax = (-7, 1)
yMinMax = (-4, 4)
reLam = np.linspace(*xMinMax, num=num)
imLam = np.linspace(*yMinMax, num=num)
reLam, imLam = np.meshgrid(reLam, imLam)

coeff = reLam + 1j*imLam
u0 = 1

f0 = f0d.Fields(nFields=num**2, sizeFGlob=1, numType='COMPLEX')
f0.setU(u0)

spaceOp = spo.PolyUT(coeffU=[coeff.reshape((num**2))])

# Runge-Kutta settings
dt = 1.
rkSettings = {'f0': f0,
              'spaceOp': spaceOp,
              'stepping': 'FIXED',
              'timeStorage': 'CURRENT',
              'dumpTimes': 'NEVER',
              'tBeg': 0.0,
              'tEnd': dt,
              'dt': dt,
              'RKS': 'RKC4'
              }

lRKS = ['FE', 'RK21', 'RKC2', 'RK33', 'RKC4', 'RK53']
lColor = plt.rcParams['axes.prop_cycle'].by_key()['color']
iColor = -1

plt.figure()
for RKS in lRKS:
    iColor = (iColor+1) % len(lColor)
    rkSettings['RKS'] = RKS
    solver = cts.ERKSolver(**rkSettings)
    solver.solve()
    u1 = solver.getCurrentFields().getU()

    g = abs(u1).reshape((num, num))
    CS3 = plt.contour(reLam, imLam, g, levels=[1.], colors=lColor[iColor],
                      linestyles='dashed', linewidths=2)
    CS3.collections[0].set_label(rkSettings['RKS'])

plt.legend(loc=2)
plt.axis('scaled')
plt.xlim(*xMinMax)
plt.ylim(*yMinMax)
plt.xlabel('$\\mathcal{R}(z)$')
plt.ylabel('$\\mathcal{I}(z)$')
plt.grid()
plt.title('Stability spectra')
plt.savefig(__file__[:-3]+'_fig1.pdf', bbox_inches='tight')
