# -*- coding: utf-8 -*-
"""
Created on Sun Feb 14 20:19:41 2016

@author: jsagg
"""

import casper.stability.stability as st
import matplotlib.pyplot as plt
import numpy as np

def isclose(a, b, rel_tol=1e-09, abs_tol=0.003):
    return abs(a-b) <=  abs_tol   
    
def advstability (reLam,imLam,g):
    startRe = 0.
    startIm = 0.
    it = np.where(isclose(reLam, startRe))
    try:
        i = it[0][0]
    except:
        raise ValueError('The {} value of Real axis must be in the interval' + 
                         ' review if {} is closely enough for the given tol'
                         .format(startRe))
    jt = np.where(isclose(imLam, startIm))
    try:
        j = jt[0][0]
    except:
        raise ValueError('The {} value of Imag axis must be in the interval' + 
                         ' review if {} is closely enough for the given tol'
                         .format(startIm))
    #To have a wider margin in the stability region. (It may occur that 
    # due to rounding error the algorithm will report g higher than zero)
    #Create a list of eigenvalues which are not in the stability region
    imLamout = np.where( g[:,i]  > 1.00005)
    try:
        imLamoutnp = np.asarray(imLamout)
    except:
        raise ValueError('The method is either A-stable or the amplification' +
                         ' vector is empty'
                                 .format(g))
    #Create a numpy matrix from a tuple
    
    #Obtain the positive values out of the stability region at Im(lambda) = 0
    imLamoutP = np.where( imLamoutnp[0,:] > j)
    try:
        imLamoutPnp = imLamoutP[0][0]
    except:
        raise ValueError('The method is either A-stable or the amplification' +
                         ' vector is empty')
    
    # Get the intersection between imaginary axis and stability region.
    imLamLimit = imLamoutnp[0,imLamoutPnp]
    return imLam[imLamLimit]
    
    
#Stability
st = st.StabilityA(200,[-10.,0.7,0.,3.2])

SweepType = ['FE-SO-SO-SO','FE-SO-SO-SO','ADAPTATIVE','ADAPTATIVE','RK']
adaptSweepType = [['HEUN2','SO-AB','SO-AB'],['HEUN2','SO','SO'],['HEUN2','SO-AB','SO-AB'],['HEUN2','TO-AB','TO-AB'],['HEUN2','TO','TO']]
adaptSweepTypestr = ['HEUN2-$SO_{AB}$','HEUN2-SO','']
Method = ['SDC','SDC','RK']
colors = ['r','g','b','c','black','c','black']
quadForm = ['LEGENDRE','HERMIT']
nQuad = [5,5,5]
nSweep = [2,4,3]
nQuadstr = ['5','5']
# Array where nodes are stored.
x = np.ndarray((4,), buffer = np.array([0.09, 0.31,0.33,0.07]))
End = len(Method)
for i in range (0,End):
    solverSettings = {'RKS': 'RKC4',
               'nSweep': nSweep[i],
               'nQuad': 5,
               'sweepType': 'ADAPTATIVE',
               'CBut' : [0,1.0],
               'adapSweepType': [['FE','SO','SO','SO'],['FE','SO','SO','SO'],['FE','SO','SO','SO']],              
               'quadForm': 'VARIABLE',
               'dtVar' : x,
               'tEnd': 1.0,
               'initialSweepType': 'SIMPLE',
                }
    if i == End - 1:
        g = st.getAmpDal('RK',**solverSettings)
        accuracy = st.getAccuracy('RK',**solverSettings)
    else:
        g = st.getAmpDal(Method[i],**solverSettings)
        accuracy = st.getAccuracy(Method[i],**solverSettings)
    
    plt.xlabel('$ Re(\\lambda) $', fontsize=16)
    plt.ylabel('$ Im(\\lambda) $', fontsize=16)
    plt.title('Stability region $ ESDC $ '.format(solverSettings['nSweep']-1))
    CS3 = plt.contour(st.reLam,st.imLam,  g, 
                      levels=[1] ,linewidths=2, colors=colors[i])
    if i == End - 1:
        CS3.collections[0].set_label( 'RKC4' )
    else:
        CS3.collections[0].set_label( SweepType[i] +'-$SDC^{}'.format(solverSettings['nSweep']-1)
                                        + '$ method' )
    plt.clabel(CS3, fontsize=9, inline=1)
    plt.rc('font', size=12)
    plt.rcParams['lines.linewidth'] = 1
    plt.rcParams['axes.titlesize'] = 24
    plt.rcParams['axes.labelsize'] = 16
    plt.rcParams['xtick.labelsize'] = 16
    plt.rcParams['ytick.labelsize'] = 16
    plt.rcParams['xtick.major.pad'] = 16
    plt.rcParams['ytick.major.pad'] = 16
    plt.rcParams['legend.fontsize'] = 16
    #Dashed line
    if i != End - 1:
        for c in CS3.collections:
            c.set_dashes([(0, (10., 10.))])
    plt.legend(loc=2,prop={'size':20})




plt.axhline(0)
plt.axvline(0)
plt.axis('scaled')  
 
plt.show()
 

plt.xlim(st.reLam[0], st.reLam[-1])
plt.ylim(st.imLam[0], st.imLam[-1])



