
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 12 15:48:38 2016

@author: j.sierra-ausin
"""

import numpy as np
import casper.stability.accuracy as ac
import casper.timesolver.rk as rks
import matplotlib.pyplot as plt
import casper.common as util


equation = 'Du=u'


if equation == 'LORENZ':
    import casper.fields.f0d as f0d
    import casper.spaceop.xyz as spop
#     Field construction and initialization
    f0 = f0d.Fields(1, sizeFGlob=3)
    f0d.init.initXYZ(f0, model='LORENTZ-MINION')
    # Space operator settings
    spaceOp = spop.Lorentz(model='MINION')
    tEnd = 10.0
elif equation == 'Du=u':
    import casper.fields.f0d as f0d
    import casper.spaceop.ode as spop
#     Field construction and initialization
    u0 = 1.
    f0 = f0d.Fields(nFields=1, sizeFGlob=1, numType='COMPLEX')
    f0.setU(u0)
    coeff = 1.
    #Select a u(t) = \lambda*u(t)
    spaceOp = spop.LambdaU(coeff)
    tEnd = 10.0
# https://www.math.uh.edu/~jingqiu/papers/idc1.pdf-  6. Numerical Examples
elif equation == 'Nexapmple':
    import casper.fields.f0d as f0d
    import casper.spaceop.ode as spop
#     Field construction and initialization
    u0 = 1.
    f0 = f0d.Fields(nFields=1, sizeFGlob=1, numType='COMPLEX')
    f0.setU(u0)
    coeff = 1.
    #Select a u(t) = \lambda*u(t)
    spaceOp1 = spop.LambdaU(coeff)
    spaceOp2 = spop.LambdaT(coeff)
    tEnd = 10.0
    
elif equation == 'LINADV':
    import casper.fields.f1d as f1d
    import casper.spaceop.mesh1d as spop
    # Field construction and initialization
    dX = 0.002
    f0Settings = {'nFields': 1,
                  'nXGlob': int(1./dX) + 1,
                  'xBegGlob': 0.0,
                  'xEndGlob': 1.0,
                  'meshType': 'XPER'}
    f0 = f1d.Fields1D(**f0Settings)
    f1d.init.initMesh1D(f0, 'GAUSS')
    # Space operator settings
    advCoeff = -1.0
    diffCoeff = 0.0001
    etss = ['ADV', 'U3', advCoeff]
    spaceOp = spop.FDLin1D(f0.xCoordLoc, etss, boundary='PER')
    # Global time solver settings
    tEnd = 1.0
    cflADV = 1.815
    cflDIFF = 1.0
    dtADV = cflADV*dX/abs(advCoeff)
    dtDIFF = cflDIFF*dX**2/(2*abs(diffCoeff))
    dt = min([dtADV, dtDIFF])
    util.infoMsg("dtADV, dtDIFF = ", dtADV, ", ", dtDIFF)
 
# Declaration of the object st which is a member of the Class Accuracy      
st = ac.Accuracy(f0,spaceOp)
# Definition of the global setting variables
nInterval = 5
if equation == 'LINADV':
    norm = ['space','L2']
    dtIndexOut = 2
    costIndexOut = 1
elif equation == 'LORENZ': 
    norm = ['time','Erel']
    dX = None 
    dtIndexOut = 4
    costIndexOut = 3
elif equation == 'Du=u':
    norm = ['time','Erel']
    dX = None 
    dtIndexOut = 2
    costIndexOut = 1



# Reference solution computed by RKC4
try:
    solver1init
except NameError:
    solver1init = False
if solver1init == False :
    rkSettings = {'f0': f0.copy(),
              'spaceOp': spaceOp,
              'stepping': 'FIXED',
              'timeStorage': 'ALL',
              'dumpTimes': 'NEVER',
              'tBeg': 0.,
              'tEnd': tEnd,
              'dt': 0.00002,
              'RKS': 'RKC4'
              }
    solver1 = rks.ERKSolver(**rkSettings)
    solver1.solve()
    solver1init = True 
    
#Set-up the data to determine the error of RK
rkSetAcc = {
              'timeSolver': 'RK',              
              'RKS': 'RK53',
              'tEnd': tEnd,
              'nInterval': nInterval,
              'initialdt': 0.5,
#              'finaldt': initialdt/2**nInterval,
              'dX' : dX,
              'norm': norm
              }
errorRK = st.accuracy_vector(solver1,**rkSetAcc)
nfunce3 = np.zeros(26)
nfunce2 = np.zeros(26)
nfunce1 = np.zeros(26)
ordersave = np.zeros(26)


# Merece la pena el RK2 predictor para RK2-SO-SO
# Merce la pena RK3-RK3-TO-TO Predictor SIMPLE
## ESD1
#sequenceI = ['SIMPLE']
#sequenceST= len(sequenceI)*['ADAPTATIVE']
#sequenceA= [[['RK3','RK4-Reuse','TO','TO']],
#            [['RK3','RK4-Reuse','TO','TO']],
#            [['RK3','RK3','TO','TO']],
#            [['RK3','RK3','TO','TO']], 
#            [['RK3','RK4-Reuse','TO','TO'],['RK3','RK4-Reuse','TO','TO']],
#            [['RK3','RK3','TO','TO'],['RK3','RK3','TO','TO']],
#            [['RK2','SO','SO'],['RK2','SO','SO']],
#            [['RK2','SO','SO'],['RK2','SO','SO']],
#            [['RK2','SO','SO']],
#            [['RK2','SO','SO']],
#            [['RK21','SO','SO'],['RK21','SO','SO']],
#            [['RK21','SO','SO'],['RK21','SO','SO']],
#            [['RK21','SO','SO']],
#            [['RK21','SO','SO']],
#            [['RK2','SO-AB','SO-AB'],['RK2','SO','SO']],
#            [['RK2','SO-AB','SO-AB'],['RK2','SO','SO']],
#            [['RK2','SO','SO'],['FE','SO','SO']],
#            [['RK2','SO','SO'],['FE','SO','SO']],
#            [['RK3','RK4-Reuse']],
#            [['RK3','RK4-Reuse']],
#            [['RK3','RK3']],
#            [['RK3','RK3']],
#            [['RK2','RK2','RK2']],
#            [['RK2','RK2','RK2']],
#            [['RK2','RK2','RK2'],['RK2','RK2','RK2']],
#            [['RK2','RK2','RK2'],['RK2','RK2','RK2']]
#]
#
#sequencenSW= [2,2,2,2,3,3,3,3,2,2,3,3,2,2,3,3,3,3,2,2,2,2,2,2,3,3]
#sequencenQ=  [5,5,5,5,5,5,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,3,4,4,4,4]
#dt1 = [0.44, 0.39, 0.40, 0.37, 0.37, 0.35, 0.23, 0.20, 0.42, 0.26, 0.21, 0.19, 0.29, 0.23, 0.33, 0.21, 0.21, 0.20, 0.51, 0.59, 0.49, 0.57, 0.30, 0.34, 0.34, 0.29]
#dt2 = [0.33, 0.40, 0.40, 0.42, 0.41, 0.44, 0.11, 0.10, 0.13, 0.14, 0.10, 0.17, 0.39, 0.20, 0.10, 0.13, 0.13, 0.10, 0.00, 0.00, 0.00, 0.00, 0.34, 0.25, 0.25, 0.32]
#dt3 = [0.09, 0.06 ,0.05, 0.05, 0.06, 0.05, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00]    
#CBut = [[0.,0.5,1.],
#        [0.,0.5,1.],
#        [0,0.5,1.],
#        [0,0.5,1.],
#        [0,0.5,1.],
#        [0,0.5,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,0.5,1.],
#        [0,0.5,1.],
#        [0,0.5,1.],
#        [0,0.5,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.]
#]


# ESD2
#sequenceI = ['SIMPLE','SIMPLE','SIMPLE']
#sequenceST= ['ADAPTATIVE','ADAPTATIVE','ADAPTATIVE','ADAPTATIVE','ADAPTATIVE','ADAPTATIVE','ADAPTATIVE','ADAPTATIVE','ADAPTATIVE','ADAPTATIVE','ADAPTATIVE','ADAPTATIVE']
#sequenceA= [[['RK2','SO','SO'],['RK2','SO','SO']],[['RK2','SO-AB','SO-AB'],['RK2','SO','SO']],[['RK2','SO','SO'],['FE','SO','SO']]    ]
#CBut = [[0.,1.],[0,1.],[0,1.]]
#sequencenSW= [3,3,3]
#sequencenQ= [4,4,4]
#dt1 = [0.24,0.26,0.24]
#dt2 = [0.26,0.12,0.26]
#dt3 = [0.,0.,0.,0.]

sequenceI = 8*['SIMPLE']
#['RK3','SIMPLE','RK3','SIMPLE','SIMPLE','SIMPLE','RK2',
#             'SIMPLE','RK2','SIMPLE','RK2','SIMPLE','RK2','SIMPLE','RK2',
#             'SIMPLE','RK2','SIMPLE','RK3','SIMPLE','RK3','SIMPLE','RK2','SIMPLE','RK2']
# Sweep 4
#sequenceST= len(sequenceI)*['ADAPTATIVE']
#sequenceA= [[['RK21','SO','SO']],
#            [['RK21','SO','SO'],['RK21','SO','SO']],
#            [['RK2','SO','SO']],
#            [['RK2','SO','SO'],['RK2','SO','SO']],
#            [['RK2','SO-AB','SO-AB']],
#            [['RK2','SO-AB','SO-AB'],['RK2','SO-AB','SO-AB']],
#            [['FE','SO','SO']],
#            [['FE','SO','SO'],['FE','SO','SO']],
#            [['RK2','RK2','RK2']],
#            [['RK2','RK2','RK2'],['RK2','RK2','RK2']]]
#
#sequencenSW= [2,3,2,3,2,3,2,3,2,3]
#sequencenQ=  [4,4,4,4,4,4,4,4,4,4]
#CBut = [[0.,3./4.],
#        [0.,3./4.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.],
#        [0,1.]
#]
#dt1 = [0.29,0.26,0.39,0.23,0.36,0.35,0.24,0.38,0.30,0.35]
#dt2 = [0.20,0.01,0.07,0.09,0.24,0.12,0.18,0.28,0.33,0.26]

sequenceST= len(sequenceI)*['ADAPTATIVE']
sequenceA= [
#            [['RK3','RK3','TO','TO']],
#            [['RK3','RK4Reuse','TO','TO'],['RK3','RK4Reuse','TO','TO']],
            [['RK3','RK3','TO','TO']],
            [['RK3','RK3','TO','TO'],['RK3','RK3','TO','TO']],
            [['RK21','SO','SO','SO']],
            [['RK21','SO','SO','SO'],['RK21','SO','SO','SO']],
            [['FE','SO','SO','SO']],
            [['FE','SO','SO','SO'],['FE','SO','SO','SO']],
            [['RK2','SO','SO','SO']],
            [['RK2','SO','SO','SO'],['RK2','SO','SO','SO']]
            ]

sequencenSW= [2,3,2,3,2,3,2,3]
sequencenQ=  [5,5,5,5,5,5,5,5]
CBut = [
        [0.,0.5,1.0],
        [0.,0.5,1.0],
        [0,3./4.],
        [0,3./4.],
        [0,1.],
        [0,1.],
        [0,1.],
        [0,1.]
]

#dt1 = dt1Max[0,2:]
#dt2 = dt2Max[0,2:]
#dt3 = dt3Max[0,2:]

plt.rcParams['lines.linewidth'] = 3
plt.rcParams['axes.titlesize'] = 24
plt.rcParams['axes.labelsize'] = 24
plt.rcParams['xtick.labelsize'] = 24
plt.rcParams['ytick.labelsize'] = 24
plt.rcParams['xtick.major.pad'] = 24
plt.rcParams['ytick.major.pad'] = 24
plt.rcParams['legend.fontsize'] = 16

for i in range (0,len(sequenceI)):
#    x = np.ndarray((3,), buffer = np.array([dt1[i],dt2[i],dt3[i]]))
#Set-up the data to determine the error of SDC
    solverSettings = {
               'timeSolver': 'SDC',               
               'initialSweepType': sequenceI[i],             
               'sweepType': sequenceST[i],
               'adapSweepType': sequenceA[i],               
               'quadForm': 'UNIF',  
#               'dtVar' : x,             
               'nSweep': sequencenSW[i],
               'nQuad': sequencenQ[i],
               'CBut' : CBut[i],
               'tEnd': tEnd,
               'dX' : dX,
               'nInterval': nInterval,
               'initialdt': 0.5,
               'dumpFieldsToFile' : False,
    #          'finaldt': initialdt/2**nInterval,
               'norm': norm
                        }
    error = st.accuracy_vector(solver1,**solverSettings)
    order = abs(np.log10(error[0,0]) - np.log10(error[-1,0]))/ abs(np.log10(error[0,dtIndexOut]) - np.log10(error[-1,dtIndexOut]))
    ordersave[i] = order    
    nfunce3[i] = 10**(np.log10(error[0,dtIndexOut]) + (np.log10(error[0,dtIndexOut])-np.log10(error[-1,dtIndexOut]))/((np.log10(error[0,0])-np.log10(error[-1,0])))*(np.log10(1e-3)-np.log10(error[0,0])))
    nfunce2[i] = 10**(np.log10(error[0,dtIndexOut]) + (np.log10(error[0,dtIndexOut])-np.log10(error[-1,dtIndexOut]))/((np.log10(error[0,0])-np.log10(error[-1,0])))*(np.log10(1e-2)-np.log10(error[0,0])))
    nfunce1[i] = 10**(np.log10(error[0,dtIndexOut]) + (np.log10(error[0,dtIndexOut])-np.log10(error[-1,dtIndexOut]))/((np.log10(error[0,0])-np.log10(error[-1,0])))*(np.log10(1e-1)-np.log10(error[0,0])))
    Corrector1 = "-".join(str(x) for x in solverSettings['adapSweepType'][0])
#    Corrector2 = "-".join(str(x) for x in solverSettings['adapSweepType'][1])
    linesSDC = ['--s','--o','-.s','-.o',':s','--s','--o','-.s','-.o',':s', ':o','--s','--o','-.s','-.o',':s', ':o','--s','--o','-.s','-.o',':s', ':o','--s','--o','-.s','-.o',':s', ':o','--s','--o','-.s','-.o',':s', ':o','--s','--o','-.s','-.o',':s', ':o']
    # Plot the error vs Nfunc
    plt.figure('\\epsilon - N_{func}')
    plt.xlabel('$ \Delta{t}} $')
    plt.ylabel('$ \epsilon $')
    plt.title('  $Error$  vs  $\\Delta{t}$ ')
    plt.loglog(error[:,costIndexOut], error[:,0], linesSDC[i], basex=10, label='${}-SDC^{}_{}$ ' .format(Corrector1,solverSettings['nSweep']-1,solverSettings['nQuad']))
    if i == 0:
        plt.loglog(errorRK[:,costIndexOut], errorRK[:,0],'-.s', basex=10, label='$RKC4$')
    plt.rc('font', size=12)
    plt.annotate('Order {}'.format(round(order,2)),
         xy=(error[i%(nInterval)-1,costIndexOut], error[i%(nInterval)-1,0]), 
         xycoords='data',
         xytext=(+20, +20), 
         textcoords='offset points', 
         fontsize=16,
         arrowprops=dict(facecolor='blue', headwidth=15, frac=0.3, width=2))
    plt.legend(loc='best')
#    plt.xlim(0.03125,0.25)           
    plt.show()
    
    
    plt.figure('\\epsilon - \Delta{t}')
    plt.xlabel('$ N_{func} $', fontsize=16)
    plt.ylabel('$ \epsilon $', fontsize=16)
    plt.title(' $Error$ vs  $N_{func}$ ')
    plt.loglog(error[:,dtIndexOut], error[:,0], linesSDC[i], basex=10, label='$ {}-SDC^{}_{}$ ' .format(Corrector1,solverSettings['nSweep']-1,solverSettings['nQuad']))
    if i == 0:
        plt.loglog(errorRK[:,dtIndexOut], errorRK[:,0],'-.s', basex=10, label='Method: $RKC4$')
    #plt.ylim(1e-10,1e-5)            
#    plt.xlim(500,5e4)
    #plt.loglog(order4[:,1], order4[:,0],':o', basex=10)
    #plt.loglog(order3[:,1], order3[:,0],':o', basex=10)
    #plt.loglog(order5[:,1], order5[:,0],':o', basex=10)
    plt.show()
    