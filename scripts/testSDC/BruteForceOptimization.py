# -*- coding: utf-8 -*-
"""
Created on Sun Feb 14 20:19:41 2016

@author: jsagg
"""


import casper.stability.stability as st
import matplotlib.pyplot as plt
import numpy as np

def isclose(a, b, rel_tol=1e-09, abs_tol=0.02):
    return abs(a-b) <=  abs_tol   
    
def advstability (reLam,imLam,g):
    startRe = 0.
    startIm = 0.
    it = np.where(isclose(reLam, startRe))
    try:
        i = it[0][0]
    except:
        raise ValueError('The {} value of Real axis must be in the interval' + 
                         ' review if {} is closely enough for the given tol'
                         .format(startRe))
    jt = np.where(isclose(imLam, startIm))
    try:
        j = jt[0][0]
    except:
        raise ValueError('The {} value of Imag axis must be in the interval' + 
                         ' review if {} is closely enough for the given tol'
                         .format(startIm))
    #To have a wider margin in the stability region. (It may occur that 
    # due to rounding error the algorithm will report g higher than zero)
    #Create a list of eigenvalues which are not in the stability region
    imLamout = np.where( g[:,i]  > 0.999999)
    try:
        imLamoutnp = np.asarray(imLamout)
    except:
        raise ValueError('The method is either A-stable or the amplification' +
                         ' vector is empty'
                                 .format(g))
    #Create a numpy matrix from a tuple
    
    #Obtain the positive values out of the stability region at Im(lambda) = 0
    imLamoutP = np.where( imLamoutnp[0,:] > j)
    try:
        imLamoutPnp = imLamoutP[0][0]
    except:
        raise ValueError('The method is either A-stable or the amplification' +
                         ' vector is empty')
    
    # Get the intersection between imaginary axis and stability region.
    imLamLimit = imLamoutnp[0,imLamoutPnp]
    return imLam[imLamLimit]
    
mode = 'STAB'
#Stability8
if mode == 'STAB':
    st = st.StabilityA(250,[-4.5,0.5,-0.5,4.5])
#Accuracy
else:
    st = st.StabilityA(250,[-0.25,0.2,-0.2,0.2])
    
reLam = np.linspace(-10, 2, 241)
imLam = np.linspace(-6, 6, 241)



sequenceI = 2*['SIMPLE']
#['RK3','SIMPLE','RK3','SIMPLE','SIMPLE','SIMPLE','RK2',
#             'SIMPLE','RK2','SIMPLE','RK2','SIMPLE','RK2','SIMPLE','RK2',
#             'SIMPLE','RK2','SIMPLE','RK3','SIMPLE','RK3','SIMPLE','RK2','SIMPLE','RK2']
sequenceST= len(sequenceI)*['ADAPTATIVE']
sequenceA= [
#            [['RK21','SO','SO']],
#            [['RK21','SO','SO'],['RK21','SO','SO']],
#            [['RK2','SO','SO']],
#            [['RK2','SO','SO'],['RK2','SO','SO']],
#            [['RK2','SO-AB','SO-AB']],
#            [['RK2','SO-AB','SO-AB'],['RK2','SO-AB','SO-AB']],
            [['FE','SO','SO']],
#            [['FE','SO','SO'],['FE','SO','SO']],
#            [['RK2','RK2','RK2']],
#            [['RK2','RK2','RK2'],['RK2','RK2','RK2']]
            ]

sequencenSW= [2]
sequencenQ=  [4,4,4,4,4,4,4,4,4]
CBut = [
#       [0.,3./4.],
        [0,1.],
        [0,1.],
        [0,1.],
        [0,1.],
        [0,1.],
        [0,1.],
        [0,1.],
        [0,1.]
]


unifstabilityimag = np.zeros(len(sequenceI))
Nschemes=len(sequenceI)
Xrange = 20
Yrange = 30
MaxImag = np.zeros(Nschemes)
dt1Max = np.zeros(Nschemes)
dt2Max = np.zeros(Nschemes)

for i in range(0,Nschemes):
    print "Init: NewScheme"
    for k in range (0,Xrange):
        for j in range (0,Yrange):
            dt1 = k*0.01 + 0.01
            dt2 = j*0.02 + 0.01
            x = np.ndarray((2,), buffer = np.array([dt1,dt2]))
            solverSettings = {'RKS': 'RKC4',
                            'initialSweepType': sequenceI[i],             
                            'sweepType': sequenceST[i],
                            'adapSweepType': sequenceA[i],               
                            'quadForm': 'VARIABLE',  
                            'dtVar' : x,
                            'CBut' : CBut[i],
                            'nSweep': sequencenSW[i],
                            'nQuad': sequencenQ[i],
                            'dt': 1.,
                            'tEnd': 1.
                                      }
            g = st.getAmpDal('SDC',**solverSettings)
            maxProv = advstability (st.reLam,st.imLam,g)
            if maxProv > MaxImag[i]:
                MaxImag[i] = maxProv
                dt1Max[i] = dt1
                dt2Max[i] = dt2




