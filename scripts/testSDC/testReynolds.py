"""
Demonstrates similarities between pcolor, pcolormesh, imshow and pcolorfast
for drawing quadrilateral grids.

"""

import casper.stability.stability as stlib

import matplotlib.pyplot as plt
import numpy as np

# Plot controls
plt.rc('font', size=12)
plt.rcParams['lines.linewidth'] = 2
plt.rcParams['axes.titlesize'] = 24
plt.rcParams['axes.labelsize'] = 24
plt.rcParams['xtick.labelsize'] = 24
plt.rcParams['ytick.labelsize'] = 24
plt.rcParams['xtick.major.pad'] = 16
plt.rcParams['ytick.major.pad'] = 16
plt.rcParams['legend.fontsize'] = 20


scheme = 'RK'
tEnd = 1.0
NumberSchemes = 6
NumberSamples = 20
Re = np.logspace(0.0, 6.0, num=NumberSamples)
SchemeAdv = ['Centered8','Centered6','Centered4','Centered2','Upwind5','Upwind3']
SchemeDiff = ['Centered8','Centered6','Centered4','Centered2','Centered4','Centered2']

st = stlib.StabilityA(200,[-5,5,-5,5],['Advection','Upwind5'])
iquadnodes = np.ndarray((3,), buffer = np.array([0.36,0.43,0.06]))

solverSettings = {'RKS': 'RK53',
                        'initialSweepType': 'SIMPLE', 
#                        'dumpFieldToFile': True,
                        'sweepType': 'ADAPTATIVE',
#                        'CBut' : [0,3./4.,1.0],
#                        'adapSweepType': [['RK3','RK3','RK3','RK3'],['FE','FE','FE','FE']], 
                        'CBut': [0.0,0.5,1.0],
                        'adapSweepType': [['RK3','RK3','TO','TO']], 
                        'quadForm': 'VARIABLE', 
                        'dtVar' : iquadnodes, 
                        'nSweep': 2,
                        'nQuad': 5,
                        'dt': 1.,
                        'tEnd': 1.
                                  }

z = st.getAmpDal(scheme,**solverSettings)

# generate 2 2d grids for the x & y bounds
x = st.phaseeig
y = st.sigma
y, x = np.meshgrid(y, x)

# x and y are bounds, so z should be the value *inside* those bounds.
# Therefore, remove the last value from the z array.


levels = np.arange(0, 2, 0.05)

def findCFLMax(z, v):
    val = (z <= v)*y
    res = 0
    i = 1
    while (res == 0) and (i < st.nElem):
        res = np.sum(val[:, i] == 0)
        i += 1
    return val[0,i-1]

#if True:
#    plt.figure('StabContour')
#    CS = plt.contourf(k, C, stabx, levels=levels)
#    plt.colorbar()
##    CS2 = plt.contour(CS, levels=[0.8, 0.9, 1], colors='r', hold='on')
##    plt.clabel(CS2, fontsize=9, inline=1)
#    plt.xlim(0, np.pi)
##    plt.xlabel('$\Theta_k$', fontsize=16)
##    plt.ylabel('$\sigma$', fontsize=18)

cflMax = findCFLMax(z, 1.00)
print ' CFL Max = {}'\
    .format( cflMax)

acc = np.abs(st.U.reshape(st.nElem,st.nElem)-np.exp(st.coeff*tEnd))/np.abs(np.exp(st.coeff*tEnd))
if True:
    xpos = 0.6
    plt.figure('StabAcc')
    levels2 = [0.001, 0.01, 0.1, 1.]
    plt.contourf(x/np.pi, y, z, levels=levels)
    plt.colorbar()
    CS30 = plt.contour(x/np.pi, y, acc,
                       levels=levels2,
                       colors='black', hold='on',
                       linestyles='dashed')
    ypos = [findCFLMax(acc, v) for v in levels2]

    plt.clabel(CS30, fontsize=9, inline=1, fmt='%1.e',
               manual=zip([xpos]*len(ypos), ypos))
    plt.xlim(0, 1)
    plt.title('Stability region of {}'.format(scheme)+'')
    plt.xlabel('$\Theta_k/\pi$')
    plt.ylabel('$\sigma$')
    CS3 = plt.contour(x/np.pi, y, z, levels=[1.02],
                      colors='red', hold='on',
                      linestyles='dashed',
                      linewidths='25')
    plt.clabel(CS3, fontsize=9, inline=1, manual=[(xpos, 2.5)],
               fmt='%1.1f')



CFLMax = np.zeros((NumberSamples,2*NumberSchemes))
for j in range(NumberSchemes):
    for i in range (NumberSamples):
    
        st = stlib.StabilityA(200,[-5,5,-5,5],['Adv-Diff',SchemeAdv[j],SchemeDiff[j],1.0/Re[i]])
        iquadnodes = np.ndarray((3,), buffer = np.array([0.36,0.43,0.06]))
        
        solverSettings = {'RKS': 'RKC4',
                                'initialSweepType': 'SIMPLE', 
        #                        'dumpFieldToFile': True,
                                'sweepType': 'ADAPTATIVE',
        #                        'CBut' : [0,3./4.,1.0],
        #                        'adapSweepType': [['RK3','RK3','RK3','RK3'],['FE','FE','FE','FE']], 
                                'CBut' : [0,0.5,1.0],
                                'adapSweepType': [['RK3','RK3','TO','TO']], 
                                'quadForm': 'VARIABLE', 
                                'dtVar' : iquadnodes, 
                                'nSweep': 2,
                                'nQuad': 5,
                                'dt': 1.,
                                'tEnd': 1.
                                          }
        
        z = st.getAmpDal(scheme,**solverSettings)
        x = st.phaseeig
        y = st.sigma
        y, x = np.meshgrid(y, x)
        
        levels = np.arange(0, 2, 0.05)
        
        cflMax = findCFLMax(z, 1.001)
        print ' CFL Max = {}'\
            .format( cflMax)
        CFLMax[i,j] = cflMax
        
if True:
    plt.figure('CFL-Re')
    plt.title('Maximum $CFL$ for '+solverSettings['RKS'])
    for j in range(NumberSchemes):
        plt.semilogx(Re,
                 CFLMax[:,j],'--o',
                 label='{} - {}'.format(SchemeAdv[j],SchemeDiff[j]))
        plt.xlabel('$Re$')
        plt.ylabel('$CFL_{max}$')
    plt.legend(loc=0)
    plt.show()


