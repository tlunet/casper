# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 14:44:04 2015

Solve the Lorentz system using Casper

@author: lunet
"""
from casper.fields import Field
from casper.rhs import Lorentz
from casper.phi import ERK
import matplotlib.pyplot as plt
import numpy as np

# Problem definition
f0 = Field(nVar=3)
rhs = Lorentz(sigma=10, rho=28, beta=8/3)
rhs.initSolution(f0.u, iType='USER', x0=5, y0=-5, z0=20)
tEnd = 3

# Time integration settings
nStepPerSec = 100
dt = 1/nStepPerSec
nStep = int(tEnd/dt) + 1
tEnd = nStep*dt
tSpan = np.linspace(0, tEnd, nStep+1)
phi = ERK(rhs, scheme='RK4')

# Time integration
sol = f0.clone(nStep+1)
sol[0] = f0[:]
for i in range(nStep):
    sol[i+1] = phi(sol[i], dt=dt)

# Retrieve trajectories
xRK, yRK, zRK = sol[:, 0], sol[:, 1], sol[:, 2]

# 2D plot
plt.figure('X(t)')
plt.ylabel('$X(t)$', fontsize=16)
plt.xlabel('$Time \; [s]$', fontsize=16)
plt.plot(tSpan, xRK, label='$x(t)$')
plt.plot(tSpan, yRK, label='$y(t)$')
plt.plot(tSpan, zRK, label='$z(t)$')
plt.legend()
plt.title('Trajectories in function of time')
plt.tight_layout()

# 3D plot
plt.figure('3D Trajectory')
plt.subplot(projection='3d')
plt.plot(xRK, yRK, zRK)
ax = plt.gca()
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
plt.title(f"3D trajectory, after {tEnd:.1f}s")
plt.tight_layout()
