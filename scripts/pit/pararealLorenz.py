#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 17 09:19:01 2018

@author: telu
"""
import casper.fields.f0d as f0d
import casper.spaceop.xyz as spop
import casper.timesolver as cts
import casper.pitalgo.parareal as pit
import matplotlib.pyplot as plt

# Field construction and initialization
f0 = f0d.Fields(1, sizeFGlob=3)
f0d.init.initXYZ(f0, model='LORENTZ-MINION')

# Space operator settings
spaceOp = spop.Lorentz(model='MINION')

T = 5.0

N = 180
K = 5
nF = 180*320
nG = 180*4

dtF = T/nF
dtG = T/nG

fSettings = {'f0': f0,
             'spaceOp': spaceOp,
             'stepping': 'FIXED',
             'timeStorage': 'CURRENT',
             'dumpTimes': 'NEVER',
             'tBeg': 0.0,
             'tEnd': T,
             'dt': dtF,
             'RKS': 'FE'
             }

gSettings = fSettings.copy()
gSettings['dt'] = dtG

pararealSettings = {'f0': f0,
                    'timeDec': 'UNIF',
                    'nIter': K,
                    'gSolverClass': cts.RKSolver,
                    'gSolverSettings': gSettings,
                    'fSolverClass': cts.RKSolver,
                    'fSolverSettings': fSettings,
                    'tBeg': 0.0,
                    'tEnd': T,
                    'nChunks': N}

# %% Parareal run
parareal = pit.Parareal(**pararealSettings)
parareal.VERBOSE = True
parareal.runAlgo()

# %% Fine solver run
fSettings['timeStorage'] = 'ALL'
fSolver = cts.RKSolver(**fSettings)
fSolver.solve()

# %% Plots
Uk = parareal.getFields(chunk='ALL', iteration='ALL')
tPlot = parareal.tabTimes[1:]

var = 0  # 0 for x, 1 for y, 2 for z
varName = 'x' if var == 0 else 'y' if var == 1 else 'z'

Uf = [fSolver.getFieldsAt(i).getU()[var]
      for i in range(fSolver.nFieldsStored())]
tf = [fSolver.getStorageTimeAt(i)
      for i in range(fSolver.nFieldsStored())]

plt.plot(tf, Uf, '--', c='gray', label='Fine solution')
for iIter in range(K+1):
    uPlot = [U.getU()[var] for U in Uk[:, iIter]]
    plt.plot(tPlot, uPlot, label=f'Iter={iIter}')
plt.legend()
plt.xlabel('time')
plt.ylabel(varName)
