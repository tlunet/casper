# -*- coding: utf-8 -*-
"""

Solve the viscous burger equation using the parareal algorithm

"""
import casper.timesolver.rk as rks
import casper.fields.f1d as f1d
import casper.spaceop.mesh1d as spop
from casper.mpi import initMPI, mpiF as mpi
import casper.pitalgo.parareal as pit
import matplotlib.pyplot as plt
import casper.common as util
import numpy as np
from time import time

# Time script
tStartScript = time()

# MPI initialization
initMPI(nps=1)

# Field construction and initialization
dX = 0.002
f0Settings = {'nFields': 1,
              'nXGlob': int(1./dX),
              'xBegGlob': 0.0,
              'xEndGlob': 1.0,
              'meshType': 'XPER'}
f0 = f1d.Fields1D(**f0Settings)
util.infoMsg("size of f0 : {}".format(f0.getU().size))
f1d.init.initMesh1D(f0, 'GAUSS')

# Space operator settings
advCoeff = -1.0
diffCoeff = 0.001
spaceOpNonLinF = spop.FDNonLinAdv1D(f0.xCoordLoc,
                                    discr='U5',
                                    coeff=advCoeff,
                                    boundary='PER')
spaceOpDiffF = spop.FDLin1D(f0.xCoordLoc,
                            ['DIFF', 'C2', diffCoeff],
                            boundary='PER')
spaceOpF = spaceOpNonLinF + spaceOpDiffF
nIP = 1


def gToF(fCoarse):
    return f1d.interpol(fCoarse, nIP, method='O7')


def fToG(fFine):
    return f1d.restrict(fFine, nIP)


spaceOpNonLinG = spop.FDNonLinAdv1D(f0.xCoordLoc[0::nIP+1],
                                    discr='U5',
                                    coeff=advCoeff,
                                    boundary='PER')
spaceOpDiffG = spop.FDLin1D(f0.xCoordLoc[0::nIP+1],
                            ['DIFF', 'C2', diffCoeff],
                            boundary='PER')
spaceOpG = spaceOpNonLinG + spaceOpDiffG

# Global time solver settings
cflADV = 0.05
cflDIFF = 1.0
dtADV = cflADV*dX/abs(advCoeff)
dtDIFF = cflDIFF*dX**2/(2*abs(diffCoeff))
dt = min([dtADV, dtDIFF])
if mpi.tRank == 0:
    util.infoMsg("dtADV, dtDIFF = ", dtADV, ", ", dtDIFF)
# Coarse solver settings
gSolverSettings = {'f0': f1d.restrict(f0, nIP),
                   'spaceOp': spaceOpG,
                   'stepping': 'FIXED',
                   'timeStorage': 'CURRENT',
                   'dumpTimes': 'NEVER',
                   'tBeg': 0.0,
                   'tEnd': 0.0,
                   'dt': 2*dt,
                   'RKS': 'RKC2'
                   }
gSolver = rks.ERKSolver

# Fine solver settings
fSolverSettings = {'f0': f0.copy(),
                   'spaceOp': spaceOpF,
                   'stepping': 'FIXED',
                   'timeStorage': 'CURRENT',
                   'dumpTimes': 'NEVER',
                   'tBeg': 0.0,
                   'tEnd': 0.0,
                   'dt': dt,
                   'RKS': 'RK53'
                   }
fSolver = rks.ERKSolver

# PIT Algorithm
plotSol = True
nDtPerChunkToPlot = [16]
plotNorm = True
nDtPerChunk_max = 64
tSize = mpi.tSize
nChunks = 20 if tSize == 1 else tSize
nIterMax = nChunks
tEndGlob = nChunks*nDtPerChunk_max*(2*dt)
listNDtPerChunk = [1, 4, 16, 64]

relErrNorms = np.ones((len(listNDtPerChunk), nIterMax))

if plotSol and mpi.tRank == tSize-1:
    plt.figure('Solution plot')
    plt.plot(f0.xCoordLoc, f0.getU(), label='Initial solution')

# Sequential solve
if mpi.tRank == mpi.tSize-1:
    seqSolverSettings = fSolverSettings
    seqSolverSettings['tEnd'] = tEndGlob
    seqSolver = rks.ERKSolver(**seqSolverSettings)
    seqSolver.solve()
    gSeqSolverSettings = gSolverSettings
    gSeqSolverSettings['tEnd'] = tEndGlob
    gSeqSolver = rks.ERKSolver(**gSeqSolverSettings)
    gSeqSolver.solve()

for iDtPerChunk in range(len(listNDtPerChunk)):

    nDtPerChunk = listNDtPerChunk[iDtPerChunk]
    if mpi.tRank == 0:
        util.infoMsg("----------- nDtPerChunk : {} -----------"
                     .format(nDtPerChunk))

    for iIter in range(nIterMax):

        nRestart = nDtPerChunk_max//nDtPerChunk
        dtRestart = nChunks*nDtPerChunk*(2*dt)

        pararealSettings = {'f0': f0,
                            'timeDec': 'UNIF',
                            'nIter': iIter,
                            'gSolverClass': gSolver,
                            'gSolverSettings': gSolverSettings,
                            'fSolverClass': fSolver,
                            'fSolverSettings': fSolverSettings,
                            'gToF': gToF,
                            'fToG': fToG,
                            'tBeg': 0.0,
                            'tEnd': 0.0,
                            'relax': 'SCALAR',
                            'relaxCoeff': 1.,
                            'nChunks': nChunks}

        for ir in range(nRestart):

            pararealSettings['tBeg'] = ir*dtRestart
            pararealSettings['tEnd'] = (ir+1)*dtRestart
            parareal = pit.Parareal(**pararealSettings)
            if mpi.tRank == 0:
                util.infoMsg("ir, tBegLoc, tEndLoc : ", ir, ", ",
                             parareal.tBegLoc,
                             ", ", parareal.tEndLoc)
            parareal.runAlgo()
            if ir < nRestart-1:
                parareal.restart()
                pararealSettings['f0'] = parareal.f0
                if mpi.tRank == 0:
                    util.infoMsg("------- PARAREAL RESTART {} -------"
                                 .format(ir+1))

        if mpi.tRank == tSize-1:
            relErrNorms[iDtPerChunk, iIter] = \
                np.linalg.norm(parareal.getFields().getU() -
                               seqSolver.getCurrentFields().getU()) / \
                np.linalg.norm(seqSolver.getCurrentFields().getU())

        if mpi.tRank == tSize-1:
            util.infoMsg("Simulation until tEndGlob = {}".format(tEndGlob))

            if plotSol and nDtPerChunk in nDtPerChunkToPlot:
                plt.figure('Solution plot')
                plt.plot(f0.xCoordLoc, parareal.getFields().getU(),
                         label='$N_{Iter,Parareal}$' + ' = {}'.format(iIter))

if plotNorm and mpi.tRank == tSize-1:
    plt.figure('Relative error norm')
    for iDtPerChunk in range(len(listNDtPerChunk)):
        nDtPerChunk = listNDtPerChunk[iDtPerChunk]
        plt.semilogy(relErrNorms[iDtPerChunk, :], 'o--',
                     label='nDtPerChunk = {}'.format(nDtPerChunk))
    plt.legend(loc=3)

if plotSol and mpi.tRank == tSize-1:
    plt.figure('Solution plot')
    plt.plot(f0.xCoordLoc, seqSolver.getCurrentFields().getU(), 'b--',
             label='Sequential solution (Fine Solver)')
    plt.plot(f0.xCoordLoc[0::nIP+1],
             gSeqSolver.getCurrentFields().getU(), 'r--',
             label='Sequential solution (Coarse solver)')
    plt.ylim(0., 1.2)
    plt.xlim(f0.xCoordLoc[0], f0.xCoordLoc[-1])
    plt.legend(loc=0)

if mpi.tRank == 0:
    util.infoMsg("dtADV, dtDIFF = ", dtADV, ", ", dtDIFF)

if (plotNorm or plotSol) and mpi.tRank == tSize-1:
    plt.figure('Relative error norm')
    plt.savefig('err_burger_{}.pdf'.format(parareal.mode), bbox_inches='tight')
    tStopScript = time()
    util.infoMsg("Script execution time :", tStopScript - tStartScript)
