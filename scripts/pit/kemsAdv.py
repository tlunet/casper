#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 30 14:56:25 2017

@author: telu
"""
import casper.timesolver as cts
import casper.fields.f1d as f1d
import casper.spaceop.mesh1d as spop
from casper.mpi import initMPI, mpiF as mpi
import casper.pitalgo.parareal as pit
import matplotlib.pyplot as plt
import casper.common as util
import numpy as np
from time import time

# Time script
tStartScript = time()

# MPI initialization
initMPI(nps=1)

# Field construction and initialization
nX = 100
dX = 1./nX
f0Settings = {'nFields': 1,
              'nXGlob': nX,
              'xBegGlob': 0.0,
              'xEndGlob': 1.0,
              'meshType': 'XPER'}
f0 = f1d.Fields1D(**f0Settings)
util.infoMsg("size of f0 : {}".format(f0.getU().size))
f1d.init.initMesh1D(f0, 'GAUSS',
                    sigCoeff=8.0,
                    listK=[25, 50, 100],
                    listA=[1.0, 1e-2, 1e-4])

# Space operator settings
advCoeff = -1.0
diffCoeff = 0.0001
etssF = ['ADV', 'C6', advCoeff, 'DIFF', 'C6', diffCoeff]
spaceOpF = spop.FDLin1D(f0.xCoordLoc, etssF, boundary='PER')
nIP = 1
etssG = ['ADV', 'U1', advCoeff, 'DIFF', 'C6', diffCoeff]
spaceOpG = spop.FDLin1D(f0.xCoordLoc[0::nIP+1], etssG, boundary='PER')
oI = 1


def gToF(fCoarse):
    return f1d.interpol(fCoarse, nIP, method='O'+str(oI))


def fToG(fFine):
    return f1d.restrict(fFine, nIP)


# Global time solver settings
cflADV = 1.0
dt = cflADV*dX/abs(advCoeff)
if mpi.tRank == 0:
    util.infoMsg("dt = ", dt)

# Coarse solver settings
gSolverSettings = {'f0': f1d.restrict(f0, nIP),
                   'spaceOp': spaceOpG,
                   'stepping': 'FIXED',
                   'timeStorage': 'CURRENT',
                   'dumpTimes': 'NEVER',
                   'tBeg': 0.0,
                   'tEnd': 0.0,
                   'dt': dt*(nIP+1),
                   'RKS': 'RKC4'
                   }
gSolver = cts.ERKSolver

# Fine solver settings
fSolverSettings = {'f0': f0.copy(),
                   'spaceOp': spaceOpF,
                   'stepping': 'FIXED',
                   'timeStorage': 'CURRENT',
                   'dumpTimes': 'NEVER',
                   'tBeg': 0.0,
                   'tEnd': 0.0,
                   'dt': dt,
                   'RKS': 'RKC4'
                   }
fSolver = cts.ERKSolver


# PIT Algorithm
numRestart = 1
nDtGPerChunk = 80
nIterMax = 1

tSize = mpi.tSize
nChunks = 4 if tSize == 1 else tSize

plotSol = True
plotNorm = True

relErrNorms = np.ones(nIterMax+1)
dtRestart = nChunks*nDtGPerChunk*(nIP+1)*dt
tEndGlob = numRestart*dtRestart

if plotSol and mpi.tRank == tSize-1:
    plt.figure('Solution plot')
    plt.plot(f0.xCoordLoc, f0.getU(), label='Initial solution')

# Sequential solutions
if mpi.tRank == mpi.tSize-1:
    # Fine solution
    seqSolverSettings = fSolverSettings
    seqSolverSettings['tEnd'] = tEndGlob
    seqSolver = fSolver(**seqSolverSettings)
    seqSolver.solve()
    uF = seqSolver.getCurrentFields().getU()
    normUF = np.linalg.norm(uF)
    # Coarse solution
    gSeqSolverSettings = gSolverSettings
    gSeqSolverSettings['tEnd'] = tEndGlob
    gSeqSolver = gSolver(**gSeqSolverSettings)
    gSeqSolver.solve()

pararealSettings = {'f0': f0,
                    'timeDec': 'UNIF',
                    'nIter': 0,
                    'gSolverClass': gSolver,
                    'gSolverSettings': gSolverSettings,
                    'fSolverClass': fSolver,
                    'fSolverSettings': fSolverSettings,
                    'gToF': gToF,
                    'fToG': fToG,
                    'tBeg': 0.0,
                    'tEnd': 0.0,
                    'relax': 'NONE',
                    'nChunks': nChunks,
                    'useKEMS': True,
                    'nK': 100,
                    'expAlgo': 'EXPM'}

for iIter in range(nIterMax+1):

    pararealSettings['nIter'] = iIter
    pararealSettings['tEnd'] = dtRestart
    parareal = pit.Parareal(**pararealSettings)

    # Parareal run(s)
    for ir in range(numRestart):

        if mpi.tRank == 0:
            util.infoMsg("------- PARAREAL RUN {} to {} -------"
                         .format(parareal.tabTimes[0],
                                 parareal.tabTimes[-1]))
        parareal.runAlgo()
        if ir < numRestart-1:
            parareal.restart()
            if mpi.tRank == 0:
                util.infoMsg("------- PARAREAL RESTART {} -------"
                             .format(ir+1))
    # Error computation
    if mpi.tRank == tSize-1:
        uP = parareal.getFields().getU()
        relErrNorms[iIter] = np.linalg.norm(uF-uP)/normUF

    if mpi.tRank == tSize-1:
        util.infoMsg("Simulation until tEndGlob = {}".format(tEndGlob))

        if plotSol:
            plt.figure('Solution plot')
            plt.title('$N_{\Delta_t/Chunk}$'+' = {}'.format(nDtGPerChunk))
            plt.plot(f0.xCoordLoc, parareal.getFields().getU(),
                     label='$N_{Iter,Parareal}$' + ' = {}'.format(iIter))

if plotNorm and mpi.tRank == tSize-1:
    plt.figure('Relative error norm')
    plt.semilogy(relErrNorms, 'o--',
                 # label='$N_{\Delta_t/Chunk}$'+' = {}'
                 # .format(nDtPerChunk))
                 # label='$N_{\lambda}'+'={}'.format(cutI)+'$'
                 label='$o_I='+str(oI)+'$')
    plt.legend(loc=3)

if plotSol and mpi.tRank == tSize-1:
    plt.figure('Solution plot')
    plt.plot(f0.xCoordLoc, seqSolver.getCurrentFields().getU(), '--',
             color='blue', label='Fine Solver', linewidth=2)
    plt.plot(f0.xCoordLoc[0::nIP+1],
             gSeqSolver.getCurrentFields().getU(), '--',
             color='brown',
             label='Coarse solver', linewidth=2)
#    plt.ylim(0., 1.2)
    plt.xlim(f0.xCoordLoc[0], f0.xCoordLoc[-1])
    plt.legend(loc=0)

if (plotNorm or plotSol) and mpi.tRank == tSize-1:
    plt.figure('Relative error norm')
    plt.xlabel('Iterations')
    plt.xticks(range(nIterMax))
    plt.ylabel('$L_2$ error')
    tStopScript = time()
    util.infoMsg("Script execution time : ", tStopScript - tStartScript)
    plt.show()
