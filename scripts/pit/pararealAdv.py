# -*- coding: utf-8 -*-
"""

Solve the advection diffusion equation using the parareal algorithm

"""
import casper.timesolver as cts
import casper.fields.f1d as f1d
import casper.spaceop.mesh1d as spop
from casper.mpi import initMPI, mpiF as mpi
import casper.pitalgo.parareal as pit
import matplotlib.pyplot as plt
import casper.common as util
import numpy as np
from time import time
from casper.pitalgo.relaxParareal import idealRelax, generateP
from casper.maths.neumann import zAdv, amplificationFactor, gParareal
from casper.maths.findiff import fourierSymbolInterpolation

# Time script
tStartScript = time()

# MPI initialization
initMPI(nps=1)

# Field construction and initialization
nX = 500
dX = 1./nX
f0Settings = {'nFields': 1,
              'nXGlob': nX,
              'xBegGlob': 0.0,
              'xEndGlob': 1.0,
              'meshType': 'XPER'}
f0 = f1d.Fields1D(**f0Settings)
util.infoMsg("size of f0 : {}".format(f0.getU().size))
f1d.init.initMesh1D(f0, 'GAUSS',
                    sigCoeff=8.0,
                    listK=[25, 50, 100],
                    listA=[1.0, 1e-2, 1e-4])

# Space operator settings
advCoeff = -1.0
diffCoeff = 0.0001
etssF = ['ADV', 'C6', advCoeff, 'DIFF', 'C6', diffCoeff]
spaceOpF = spop.FDLin1D(f0.xCoordLoc, etssF, boundary='PERIODIC')
nIP = 1  # nIP=1 to activate space coarsening
oI = 5


_, gIR = fourierSymbolInterpolation(oI)


def gToF(fCoarse):
    return f1d.interpol(fCoarse, nIP, method='O'+str(oI))


def fToG(fFine):
    return f1d.restrict(fFine, nIP)


etssG = ['ADV', 'C6', advCoeff, 'DIFF', 'C2', diffCoeff]
spaceOpG = spop.FDLin1D(f0.xCoordLoc[0::nIP+1], etssG, boundary='PERIODIC')

# Global time solver settings
cflADV = 1.0
dt = cflADV*dX/abs(advCoeff)
if mpi.tRank == 0:
    util.infoMsg("dt = ", dt)

# Coarse solver settings
gSolverSettings = {'f0': f1d.restrict(f0, nIP),
                   'spaceOp': spaceOpG,
                   'stepping': 'FIXED',
                   'timeStorage': 'CURRENT',
                   'dumpTimes': 'NEVER',
                   'tBeg': 0.0,
                   'tEnd': 0.0,
                   'dt': dt*(nIP+1),
                   'RKS': 'RK4'
                   }
gSolver = cts.ERKSolver

# Fine solver settings
fSolverSettings = {'f0': f0.copy(),
                   'spaceOp': spaceOpF,
                   'stepping': 'FIXED',
                   'timeStorage': 'CURRENT',
                   'dumpTimes': 'NEVER',
                   'tBeg': 0.0,
                   'tEnd': 0.0,
                   'dt': dt,
                   'RKS': 'RK4'
                   }
fSolver = cts.ERKSolver


if False:
    # Jacobian
    p = np.linalg.matrix_power
    inv = np.linalg.inv

    JacF = spaceOpF.computeJacobian(f0.getU(), 0)
    gJF = (np.eye(nX) + dt*JacF + dt**2/2.*p(JacF, 2) + dt**3/6.*p(JacF, 3) +
           dt**4/24.*p(JacF, 4))
    JacG = spaceOpG.computeJacobian(fToG(f0).getU(), 0)
    gJG = (np.eye(fToG(f0).getU().size) + dt*JacG + dt**2/2.*p(JacG, 2))

    P = generateP(int(1./dX))

    Hj_norm = gJF.dot(inv(gJG))

# PIT Algorithm
relax = 1.0
numRestart = 1
useRelax = False
plotSol = True
nDtPerChunkToPlot = [16]
plotNorm = True
plotFourier = False
nDtPerChunk_max = 16*numRestart
tSize = mpi.tSize
nChunks = 4 if tSize == 1 else tSize
nIterMax = 4
tEndGlob = nChunks*nDtPerChunk_max*((nIP+1)*dt)
listNDtPerChunk = nDtPerChunkToPlot

relErrNorms = np.ones((len(listNDtPerChunk), nIterMax))

if plotSol and mpi.tRank == tSize-1:
    plt.figure('Solution plot')
    plt.plot(f0.xCoordLoc, f0.getU(), label='Initial solution')

for iDtPerChunk in range(len(listNDtPerChunk)):

    nDtPerChunk = listNDtPerChunk[iDtPerChunk]

    for iIter in range(nIterMax):

        nRestart = int(nDtPerChunk_max/nDtPerChunk)
        dtRestart = nChunks*nDtPerChunk*((nIP+1)*dt)

        # Relaxation settings
        # -- Eigenvalues
        cutI = 10
        if useRelax:
            Ha, g, f = \
                idealRelax(etssG[1], etssF[1],
                           gSolverSettings['RKS'], fSolverSettings['RKS'],
                           cflADV, cflADV, nDtPerChunk, nDtPerChunk,
                           f0Settings['nXGlob'], cutIndex=cutI)
            Ha = Ha.real
        # -- Jacobian
        if False:
            Hj = p(Hj_norm, nDtPerChunk)

        pararealSettings = {'f0': f0,
                            'timeDec': 'UNIF',
                            'nIter': iIter,
                            'gSolverClass': gSolver,
                            'gSolverSettings': gSolverSettings,
                            'fSolverClass': fSolver,
                            'fSolverSettings': fSolverSettings,
                            'gToF': gToF,
                            'fToG': fToG,
                            'tBeg': 0.0,
                            'tEnd': 0.0,
                            'relax': None if not useRelax else 'MATRIX',
                            'relaxCoeff': relax,
                            'relaxMatrix': None if not useRelax else Ha,
                            'nChunks': nChunks}

        # Sequential solve
        if mpi.tRank == mpi.tSize-1:
            seqSolverSettings = fSolverSettings
            seqSolverSettings['tEnd'] = tEndGlob
            seqSolver = fSolver(**seqSolverSettings)
            seqSolver.solve()
            gSeqSolverSettings = gSolverSettings
            gSeqSolverSettings['tEnd'] = tEndGlob
            if 'nDt' in gSeqSolverSettings:
                gSeqSolverSettings['nDt'] *= nDtPerChunk_max
            gSeqSolver = gSolver(**gSeqSolverSettings)
            gSeqSolver.solve()
            if 'nDt' in gSeqSolverSettings:
                gSeqSolverSettings['nDt'] /= nDtPerChunk_max

        for ir in range(nRestart):

            # Parareal runs
            pararealSettings['tBeg'] = ir*dtRestart
            pararealSettings['tEnd'] = (ir+1)*dtRestart
            parareal = pit.Parareal(**pararealSettings)
            util.infoMsg("ir, tBegLoc, tEndLoc : ", ir, ", ", parareal.tBegLoc,
                         ", ", parareal.tEndLoc)
            parareal.runAlgo()
            if ir < nRestart-1:
                parareal.restart()
                pararealSettings['f0'] = parareal.f0
                if mpi.tRank == 0:
                    util.infoMsg("------- PARAREAL RESTART {} -------"
                                 .format(ir+1))

        if mpi.tRank == tSize-1:
            relErrNorms[iDtPerChunk, iIter] = \
                np.linalg.norm(parareal.getFields().getU() -
                               seqSolver.getCurrentFields().getU(),
                               ord=1) / \
                np.linalg.norm(seqSolver.getCurrentFields().getU(),
                               ord=1)

        if mpi.tRank == tSize-1:
            util.infoMsg("Simulation until tEndGlob = {}".format(tEndGlob))

            if plotSol and nDtPerChunk in nDtPerChunkToPlot:
                plt.figure('Solution plot')
                plt.title('$N_{\Delta_t/Chunk}$'+' = {}'.format(nDtPerChunk))
                plt.plot(f0.xCoordLoc, parareal.getFields().getU(),
                         label='$N_{Iter,Parareal}$' + ' = {}'.format(iIter))

if plotNorm and mpi.tRank == tSize-1:
    plt.figure('Relative error norm')
    for iDtPerChunk in range(len(listNDtPerChunk)):
        nDtPerChunk = listNDtPerChunk[iDtPerChunk]
        plt.semilogy(relErrNorms[iDtPerChunk, :], 'o--',
                     # label='$N_{\Delta_t/Chunk}$'+' = {}'
                     # .format(nDtPerChunk))
                     # label='$N_{\lambda}'+'={}'.format(cutI)+'$'
                     label='$o_I='+str(oI)+'$')
    plt.legend(loc=3)

if plotSol and mpi.tRank == tSize-1:
    plt.figure('Solution plot')
    plt.plot(f0.xCoordLoc, seqSolver.getCurrentFields().getU(), '--',
             color='blue', label='Fine Solver', linewidth=2)
    plt.plot(f0.xCoordLoc[0::nIP+1],
             gSeqSolver.getCurrentFields().getU(), '--',
             color='brown',
             label='Coarse solver', linewidth=2)
#    plt.ylim(0., 1.2)
    plt.xlim(f0.xCoordLoc[0], f0.xCoordLoc[-1])
    plt.legend(loc=0)
    plt.savefig('sol_adv.pdf', bbox_inches='tight')

# Fourier analysis
if plotFourier and mpi.tRank == tSize-1:
    okInit = plt.fignum_exists('Fourier')
    plt.figure('Fourier')
    uF0 = np.fft.rfft(f0.getU())
    uF = np.fft.rfft(parareal.getFields().getU())
    uf = np.fft.rfftfreq(parareal.getFields().getU().size, 0.5)

    if not okInit:
        plt.semilogy(uf, np.abs(uF0)/np.max(np.abs(uF0)),
                     label='Initial Solution')
    theta = np.linspace(0, np.pi, uF0.size)
    thetaG = (nIP+1)*theta

    zG = zAdv(thetaG, etssG[1])
    zF = zAdv(theta, etssF[1])

    gG = amplificationFactor(gSolverSettings['RKS'])
    gF = amplificationFactor(fSolverSettings['RKS'])

    rG = gG(-cflADV*zG)**(listNDtPerChunk[0])
    rF = gF(-cflADV*zF)**(listNDtPerChunk[0]*(nIP+1))

    gP = gParareal(nChunks, nIterMax-1, rG, rF)**nRestart

    if nIP > 0:
        mgFac = (1. + uF0[-1::-1]/uF0)
        plt.semilogy(uf, np.abs(mgFac), label='$|g_{MG}|$')
        gP *= gIR(theta)
        plt.semilogy(uf, abs(gP), label='$|g_{IR}.g_{\\mathcal{P}}|$')
        uMod = uF0 + uF0[-1::-1]
        uFTh = gP*uMod
    else:
        uFTh = gP*uF0
    plt.semilogy(uf, np.abs(uF)/np.max(np.abs(uF0)),
                 label='$k='+str(nIterMax)+'$, numeric')
    plt.semilogy(uf, np.abs(uFTh)/np.max(np.abs(uF0)), '--',
                 label='$k='+str(nIterMax)+'$, analytical')
    plt.xlabel('$\\theta_\kappa/\pi$')
    plt.ylabel('$|\hat{u}|$')
    plt.legend(loc=0)

if (plotNorm or plotSol) and mpi.tRank == tSize-1:
    plt.figure('Relative error norm')
    plt.xlabel('Iterations')
    plt.xticks(range(nIterMax))
    plt.ylabel('$L_1$ error')
    plt.savefig('err_adv.pdf', bbox_inches='tight')
    tStopScript = time()
    util.infoMsg("Script execution time : ", tStopScript - tStartScript)
    plt.show()
