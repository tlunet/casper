#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 14:28:23 2019

@author: lunet
"""
import os
import sys
import shutil as sh
import numpy as np
import scipy.optimize as sco
import matplotlib.pyplot as plt
from matplotlib import cm

from casper.fields import Field1D
from casper.rhs import LinAdvDiffReac1D
from casper.phi import RK
from casper.maths.errest import ErrEstimateAdv, tablesExplicit, tablesImplicit
from casper.common import Timer

# Parameters
xMin = 0
xMax = 1
timeDiscr = sys.argv[1] if len(sys.argv) > 1 else 'SDIRK3'
spaceScheme = sys.argv[2] if len(sys.argv) > 2 else 'C6'
kPlot = int(sys.argv[3]) if len(sys.argv) > 3 else 10
errType = 'L2'

lSym = [-1, ['s', 'o', '^', 'p']]


def gSym():
    lSym[0] = (lSym[0] + 1) % len(lSym[1])
    return lSym[1][lSym[0]]


# Script parameters
cflPlot = 8
eTol = 1e-2
contourPlots = False
ticksPadding = 2

# Parameters to test
list_nP = [1, 2, 4]
list_k = [1, 2]
if kPlot not in list_k:
    list_k.append(kPlot)

# Implicit parameters
estimate = ErrEstimateAdv(timeDiscr, spaceScheme)

if timeDiscr in tablesExplicit:
    list_cfl = [0.5, 1]
elif timeDiscr in tablesImplicit:
    list_cfl = [1, 2, 4, 8]
iCFLPlot = list_cfl.index(cflPlot)


# Initial solution
def initSol(x, k):
    return np.sin(2*k*np.pi*x)


# Error computation
def getErr(u, ut):
    if errType == 'L2':
        return np.linalg.norm(u-ut, ord=2)/(u.size)**0.5
    elif errType == 'INF':
        return np.linalg.norm(u-ut, ord=np.inf)
    elif errType == 'DIFF':
        return abs(max(u)-max(ut))
    else:
        raise ValueError(errType)


# Data storing
dataDir = '_advErr-data'
if not os.path.isdir(dataDir):
    os.mkdir(dataDir)
prefix = '{}_{}'.format(timeDiscr, spaceScheme)
dataFile = dataDir+'/'+prefix+'_testData.txt'


def writeData(dico):
    if os.path.isfile(dataFile):
        sh.move(dataFile, dataFile+'_')
    with open(dataFile, 'w') as f:
        for pt in dico:
            f.write('_{}_{}_{}\n'.format(*pt))
            dataPoint = dico[pt]
            for nX in dataPoint:
                f.write('{} {}\n'.format(nX, dataPoint[nX]))
    if os.path.isfile(dataFile+'_'):
        os.remove(dataFile+'_')


def readData():
    with open(dataFile, 'r') as f:
        lines = f.readlines()
    d = {}
    for l in lines:
        if l.startswith('_'):
            k, nP, cfl = l[1:].split('_')[:3]
            k, nP, cfl = int(k), int(nP), float(cfl)
            pt = {}
            d[k, nP, cfl] = pt
        else:
            nX, err = l.split()[:2]
            nX, err = int(nX), float(err)
            pt[nX] = err
    return d


if os.path.isfile(dataFile):
    data = readData()
else:
    data = {}

# Loop on test parameters
nMin = []
for k in list_k:
    nMin.append([])
    for nP in list_nP:
        nMin[-1].append([])
        for cfl in list_cfl:

            # Enventually get stored data
            if (k, nP, cfl) not in data:
                data[k, nP, cfl] = {}
            dataPoint = data[k, nP, cfl]

            list_err = []
            list_nX = []
            with Timer('-- Simulation for k={}, nP={}, cfl={}'
                       .format(k, nP, cfl)):
                nX = 4*k
                while nX < cfl/nP or nX < 8:
                    nX *= 2

                cont = True
                e1, e2, n1, n2 = 1, 1, nX, nX//2
                while cont or len(list_nX) < 2:
                    e1 = e2
                    n1 = n2
                    n2 = 2*n1
                    if n2 in dataPoint:
                        # Get stored data
                        e2 = dataPoint[n2]
                        msgSuffix = '(stored data)'
                    else:
                        # Numerical simulation
                        f0 = Field1D(n2)
                        x = f0.x
                        u0 = initSol(x, k)

                        rhs = LinAdvDiffReac1D(f0, advScheme=spaceScheme)

                        dt = cfl*f0.dx
                        nT = round(nP/dt)
                        print(' -- initializing time integrator ...', end='\r')
                        tic = Timer(msg=False, start=True)
                        phi = RK(rhs, timeDiscr, dt=dt)
                        tic.stop(printElapsed=False)
                        print(' -- initialized time integrator in {:.6f}s'
                              .format(tic.value))
                        ut = u0
                        tic = Timer(msg=False, start=True)
                        tString = ''
                        for i in range(nT):
                            if (i+1) % 10 == 0:
                                tPerStep = tic.clock/(i+1)
                                tString = ' ({:.2g}s per time step)'.format(
                                    tPerStep)
                                print(' -- computing time step {:,}/{:,}{}'
                                      .format(i+1, nT, tString), end='\r')
                            ut = phi(ut)
                        tic.stop(printElapsed=False)
                        print(' -- computed {} time steps in {:.6f}s'
                              .format(nT, tic.value)+' '*30)

                        e2 = getErr(u0, ut)
                        msgSuffix = '(simulated data)'
                        # Store data
                        dataPoint[n2] = e2
                        writeData(data)

                    print('nX={}, err={:.3f} {}'.format(n2, e2, msgSuffix))
                    list_err.append(e2)
                    list_nX.append(n2)

                    if e2 < eTol:
                        cont = False
                    else:
                        n2 = 2*n1

            # Compute number of mesh points to get to error tolerance
            e1, e2, n1, n2 = \
                list_err[-2], list_err[-1], list_nX[-2], list_nX[-1]
            a = np.log(e2/e1)/np.log(n2/n1)
            b = np.log(e1) - np.log(n1)*a
            nMin_ = np.exp((np.log(eTol)-b)/a)
            nMin[-1][-1].append(nMin_)

            if k == kPlot and cfl == cflPlot:
                plt.figure('err: {} & {}, k={}, cfl={}'
                           .format(timeDiscr, spaceScheme, k, cfl))
                plt.loglog(list_nX, list_err, '-'+gSym(),
                           label='$n_P={}$'.format(nP))
                nXPlot = np.array(list_nX, dtype=float)
                # Add error bound
                estimate.cfl = cfl
                eps = estimate.error(k, nXPlot, nP)
                plt.plot(nXPlot, eps, '--', c='gray')

# plt.plot(nXPlot, 0*nXPlot+eTol, '--', c='black')
plt.gca().set_xscale('log', base=2)
plt.hlines(1/2**0.5, nXPlot[0], nXPlot[-1], linestyles=':')
nXPlot = nXPlot[::ticksPadding]
plt.xticks(nXPlot, [str(int(n)) for n in nXPlot])
plt.legend(loc=3)
plt.grid()
plt.xlabel('$n_x$')
plt.ylabel('$\\epsilon$')
plt.ylim(1e-3, 10)
plt.hlines(1/2**0.5, nXPlot[0], nXPlot[-1], linestyles=':')
figName = 'advErr_{}_{}_k={}_CFL={}.pdf'.format(
    timeDiscr, spaceScheme, kPlot, cflPlot)
plt.tight_layout()
plt.savefig(figName)

# %% Regression
nMin = np.array(nMin)

tMax = np.array(list_nP)
k = np.array(list_k)
cfl = np.array(list_cfl)
tMax, k, cfl = np.meshgrid(tMax, k, cfl)


def minFunc(x):
    c, alpha, beta, gamma = x
    return np.linalg.norm(c * cfl**alpha * tMax**beta * k**gamma - nMin)


res = sco.minimize(minFunc, [10, 1, 0.5, 1.1], method='Nelder-Mead')
print(res)

cReg, alphaReg, betaReg, gammaReg = res.x
print('Regression nMin = c cfl^alpha nP^beta k^gamma:\n' +
      'c={:.1f}, alpha={:.3f}, beta={:.3f}, gamma={:.3f}'.format(
              cReg, alphaReg, betaReg, gammaReg))

# %% Contour plots
if contourPlots:

    suffix = '{} & {}'.format(timeDiscr, spaceScheme)
    plt.figure('nMin - measured '+suffix)
    ax = plt.contourf(tMax[:, :, 0], k[:, :, 0], nMin[:, :, iCFLPlot],
                      cmap=cm.viridis)
    plt.colorbar()
    plt.xlabel('$n_P$')
    plt.ylabel('$k$')

    plt.figure('nMin - regression '+suffix)
    tMaxReg = np.linspace(tMax.min(), tMax.max(), num=200)
    kReg = np.linspace(k.min(), k.max(), num=202)
    tMaxReg, kReg = np.meshgrid(tMaxReg, kReg)
    nMinReg = cReg * cflPlot**alphaReg * tMaxReg**betaReg * kReg**gammaReg
    plt.contourf(tMaxReg, kReg, nMinReg, levels=ax.levels, cmap=cm.viridis)
    plt.colorbar()
    plt.xlabel('$n_P$')
    plt.ylabel('$k$')

    plt.show()
