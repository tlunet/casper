#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 14:28:23 2019

@author: lunet
"""
import os
import shutil as sh
import numpy as np
import matplotlib.pyplot as plt

from casper.fields import Field1D
from casper.rhs import LinAdvDiffReac1D
from casper.phi import RK
from casper.maths.errest import ErrEstimateAdvDiff
from casper.common import Timer
from casper.maths.analytic import AdvDiffSolver

# Parameters
timeDiscr = 'SDIRK2'
advScheme = 'C2'
diffScheme = 'C2'
cfl = 1
cstCFL = 'DIFF'
if cstCFL == 'ADV':
    cflAdv, cflDiff = cfl, None
elif cstCFL == 'DIFF':
    cflAdv, cflDiff = None, cfl
k = 1
eTol = 1e-2

# Parameters to test
lRe = [int(r) for r in [1e2, 1e3, 1e4]]


errType = 'L2'

lSym = [-1, ['s', 'o', '^', 'p']]

def gSym():
    lSym[0] = (lSym[0] + 1) % len(lSym[1])
    return lSym[1][lSym[0]]


ticksPadding = 2

# Implicit parameters
estimate = ErrEstimateAdvDiff(
    timeDiscr, advScheme, diffScheme, cflAdv, cflDiff)

# Initial solution
def initSol(x, k):
    return np.cos(2*k*np.pi*x)


# Error computation
def getErr(u, ut):
    if errType == 'L2':
        return np.linalg.norm(u-ut, ord=2)/(u.size)**0.5
    elif errType == 'INF':
        return np.linalg.norm(u-ut, ord=np.inf)
    elif errType == 'DIFF':
        return abs(max(u)-max(ut))
    else:
        raise ValueError(errType)


# Data storing
dataDir = '_advDiffErr-data'
if not os.path.isdir(dataDir):
    os.mkdir(dataDir)
prefix = f'{timeDiscr}_{advScheme}_{diffScheme}'
dataFile = dataDir+'/'+prefix+'_testData.txt'


def writeData(dico):
    if os.path.isfile(dataFile):
        sh.move(dataFile, dataFile+'_')
    with open(dataFile, 'w') as f:
        for pt in dico:
            f.write('_{}_{}_{}_{}\n'.format(*pt))
            dataPoint = dico[pt]
            for nX in dataPoint:
                f.write('{} {}\n'.format(nX, dataPoint[nX]))
    if os.path.isfile(dataFile+'_'):
        os.remove(dataFile+'_')


def readData():
    with open(dataFile, 'r') as f:
        lines = f.readlines()
    d = {}
    for l in lines:
        if l.startswith('_'):
            k, cstCFL, cfl, Re = l[1:].split('_')[:4]
            k, cstCFL, cfl, Re = int(k), str(cstCFL), float(cfl), float(Re)
            pt = {}
            d[k, cstCFL, cfl, Re] = pt
        else:
            nX, err = l.split()[:2]
            nX, err = int(nX), float(err)
            pt[nX] = err
    return d


if os.path.isfile(dataFile):
    data = readData()
else:
    data = {}

# Loop on test parameters
nMin = []
for Re in lRe:

    # Enventually get stored data
    if (k, cstCFL, cfl, Re) not in data:
        data[k, cstCFL, cfl, Re] = {}
    dataPoint = data[k, cstCFL, cfl, Re]

    list_err = []
    list_nX = []
    with Timer(f'Simulation for k={k}, cstCFL={cstCFL}, cfl={cfl}, Re={Re}'):

        nX = 4*k

        while nX < 8:
            nX *= 2

        cont = True
        e1, e2, n1, n2 = 1, 1, nX, nX//2
        while cont or len(list_nX) < 2:
            e1 = e2
            n1 = n2
            n2 = 2*n1
            if n2 in dataPoint:
                # Get stored data
                e2 = dataPoint[n2]
                msgSuffix = '(stored data)'
            else:
                # Numerical simulation
                f0 = Field1D(n2)
                x = f0.x
                u0 = initSol(x, k)

                rhs = LinAdvDiffReac1D(
                    f0, nu=1/Re, advScheme=advScheme, diffScheme=diffScheme)

                dt = cfl*f0.dx if cstCFL == 'ADV' else cfl*f0.dx**2 * Re
                nT = 2*max(int(round(Re/(2*k*np.pi)**2/dt)), 1)
                print(' -- initializing time integrator ...', end='\r')
                tic = Timer(msg=False, start=True)
                phi = RK(rhs, timeDiscr, dt=dt)
                tic.stop(printElapsed=False)
                print(' -- initialized time integrator in {:.6f}s'
                      .format(tic.value))
                ut = u0
                tic = Timer(msg=False, start=True)
                tString = ''

                err = []
                analyticSolver = AdvDiffSolver(1, 1/Re)

                for i in range(nT):
                    if (i+1) % 10 == 0:
                        tPerStep = tic.clock/(i+1)
                        tString = ' ({:.2g}s per time step)'.format(
                            tPerStep)
                        print(' -- computing time step {:,}/{:,}{}'
                              .format(i+1, nT, tString), end='\r')
                    ut = phi(ut)
                    uTh = analyticSolver.solve_COS(x, dt*(i+1), k).ravel()
                    err.append(getErr(uTh, ut))
                tic.stop(printElapsed=False)
                print(f' -- computed {nT} time steps in {tic.value:.6f}s'
                      f' ({tic.value/nT:.2g}s per time step)')

                e2 = max(err)
                msgSuffix = '(simulated data)'

                # Store data
                dataPoint[n2] = e2
                writeData(data)

            print('nX={}, err={:.3f} {}'.format(n2, e2, msgSuffix))
            list_err.append(e2)
            list_nX.append(n2)

            if e2 < eTol:
                cont = False
            else:
                n2 = 2*n1

    # Compute number of mesh points to get to error tolerance
    e1, e2, n1, n2 = \
        list_err[-2], list_err[-1], list_nX[-2], list_nX[-1]
    a = np.log(e2/e1)/np.log(n2/n1)
    b = np.log(e1) - np.log(n1)*a
    nMin_ = np.exp((np.log(eTol)-b)/a)
    nMin.append(nMin_)

    plt.figure('err: {} & {} & {}, k={}, cfl={} ({})'
               .format(timeDiscr, advScheme, diffScheme, k, cfl, cstCFL))
    plt.loglog(list_nX, list_err, '-'+gSym(), label=f'$Re={Re}$')
    nXPlot = np.array(list_nX, dtype=float)
    # Add error bound
    eps = estimate.error(k, Re, nXPlot)
    plt.plot(nXPlot, eps, '--', color='gray')

# plt.plot(nXPlot, 0*nXPlot+eTol, '--', c='black')
plt.gca().set_xscale('log', base=2)
plt.hlines(1/2**0.5, nXPlot[0], nXPlot[-1], linestyles=':')
nXPlot = nXPlot[::ticksPadding]
plt.xticks(nXPlot, [str(int(n)) for n in nXPlot])
plt.legend()
plt.grid()
plt.xlabel('$n_x$')
plt.ylabel('$\\epsilon$')
plt.ylim(eTol/10, 10)
plt.hlines(1/2**0.5, nXPlot[0], nXPlot[-1], linestyles=':')
figName = 'advDiffErr_{}_{}_{}_k={}_CFL={}_{}.pdf'.format(
    timeDiscr, advScheme, diffScheme, k, cfl, cstCFL)
plt.tight_layout()
plt.savefig(figName)

plt.show()
