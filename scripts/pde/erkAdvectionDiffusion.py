#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  8 14:25:22 2017

@author: lunet
"""
import casper.timesolver as cts
import casper.fields.f1d as f1d
import casper.spaceop.mesh1d as spop
import casper.common as util
import matplotlib.pyplot as plt
import numpy as np

# Field construction and initialization
dX = 0.001
f0Settings = {'nFields': 1,
              'nXGlob': int(1./dX)+1,
              'xBegGlob': 0.0,
              'xEndGlob': 1.0,
              'meshType': 'XPER'}
f0 = f1d.Fields1D(**f0Settings)
f1d.init.initMesh1D(f0, 'GAUSS', sigCoeff=8)
u0 = f0.getU()

# Space operator settings
advCoeff = -1.0
diffCoeff = 0.001
util.infoMsg('Reynolds :', 1./diffCoeff)
etss = ['ADV', 'C6', advCoeff, 'DIFF', 'C4', diffCoeff]
spaceOp = spop.FDLin1D(f0.xCoordLoc, etss, boundary='PER')

# Global time solver settings
cflADV = 1.0
cflDIFF = 1.0
dtADV = cflADV*dX/abs(advCoeff)
dtDIFF = cflDIFF*dX**2/(2*abs(diffCoeff))
util.infoMsg("dtADV, dtDIFF = ", dtADV, ", ", dtDIFF)
if dtADV <= dtDIFF:
    util.infoMsg("CFL condition on Advection scheme")
    dt = dtADV
else:
    util.infoMsg("CFL condition on Diffusion scheme")
    dt = dtDIFF
tEnd = 6000*dt

# Runge-Kutta settings
rkSettings = {'f0': f0.copy(),
              'spaceOp': spaceOp,
              'stepping': 'FIXED',
              'timeStorage': 'CURRENT',
              'dumpTimes': 'NEVER',
              'tBeg': 0.0,
              'tEnd': tEnd,
              'dt': dt,
              'RKS': 'RKC4'
              }

solver1 = cts.ERKSolver(**rkSettings)
solver1.solve()
print("{} : {} RHS evaluations, nDOF={}, cpuTime={:1.3f}s"
      .format(solver1.RKS, solver1.spopCount, f0Settings['nXGlob'],
              solver1.cpuTime))

# Plot solution
plt.figure('Solution')
plt.plot(f0.xCoordLoc,
         f0.getU(),
         label='Initial Solution')
plt.plot(solver1.getCurrentFields().xCoordLoc,
         solver1.getCurrentFields().getU(),
         label='{}, CFL={}'.format(rkSettings['RKS'],
                                   tEnd/solver1.nIter/dX))
plt.xlabel('$X$')
plt.ylabel('$U$')
plt.title('Solution after {} time-steps'.format(solver1.nIter))
plt.legend()
plt.savefig(__file__[:-3]+'_fig1.pdf', bbox_inches='tight')

# Plot energy spectrum
plt.figure('Fourier')
uF = np.fft.rfft(f0.getU())
kappa = np.fft.rfftfreq(f0.getU().size, 0.5)
plt.loglog(kappa, np.abs(uF)/np.max(np.abs(uF)), label='init')
uF = np.fft.rfft(solver1.getCurrentFields().getU())
kappa = np.fft.rfftfreq(solver1.getCurrentFields().getU().size, 0.5)
plt.loglog(kappa, np.abs(uF)/np.max(np.abs(uF)), label=rkSettings['RKS'])
plt.ylabel('$E/E_{0,max}$')
plt.xlabel('$\\kappa/\\kappa_{max}$')
plt.title('Energy spectrum after {} time-steps'.format(solver1.nIter))
plt.legend()
plt.savefig(__file__[:-3]+'_fig2.pdf', bbox_inches='tight')
