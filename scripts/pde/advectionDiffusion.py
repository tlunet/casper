# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 17:29:45 2015

@author: lunet
"""
import casper.timesolver as cts
import casper.fields.f1d as f1d
import casper.spaceop.mesh1d as spop
import casper.common as util
from casper.mpi import initMPI, mpiF as mpi
import matplotlib.pyplot as plt
import numpy as np

# Initialize MPI communications
initMPI()

# Field construction and initialization
f0Settings = {'nFields': 1,
              'nXGlob': (1**0.25)*122,
              'xBegGlob': 0.0,
              'xEndGlob': 1.0,
              'meshType': 'XPER'}
f0 = f1d.Fields1D(**f0Settings)
f1d.init.initMesh1D(f0, 'GAUSS', sigma=0.1, x0=0.5,
                    listK=range(1, 4), k0=10)
dX = 1/f0.nXGlob
u0 = f0.getU()


# Space operator settings
advCoeff = -1.0
diffCoeff = 0.000005
util.infoMsg('Reynolds :', 1./diffCoeff)
etss = ['ADV', 'C6', advCoeff]
spaceOp = spop.FDLin1D(f0.xCoordLoc, etss, boundary='PERIODIC')

# Global time solver settings
cflADV = 1.0  # limit=1.815
cflDIFF = 1.0
dtADV = cflADV*dX/abs(advCoeff)
dtDIFF = cflDIFF*dX**2/(2*abs(diffCoeff))
util.infoMsg("dtADV, dtDIFF = ", dtADV, ", ", dtDIFF)
if dtADV <= dtDIFF:
    util.infoMsg("CFL condition on Advection scheme")
    dt = dtADV
else:
    util.infoMsg("CFL condition on Diffusion scheme")
    dt = dtDIFF
tEnd = 1


# Runge-Kutta settings
rkSettings = {
    'f0': f0.copy(), 'spaceOp': spaceOp,
    'stepping': 'FIXED', 'timeStorage': 'CURRENT', 'dumpTimes': 'NEVER',
    'tBeg': 0.0, 'tEnd': tEnd, 'dt': dt, 'RKS': 'RK4'}

solver1 = cts.ERKSolver(**rkSettings)
solver1.solve()
print("{} : {} evaluations".format(solver1.RKS, solver1.spopCount))

if True:
    plt.figure('Space Rank {}'.format(mpi.sRank))
    plt.plot(f0.xCoordLoc,
             f0.getU(),
             label='Initial Solution')
    plt.xlabel('$X \\, [m]$', fontsize=16)
    plt.ylabel('$U(x) \\, [-]$', fontsize=16)

    plt.figure('Fourier')
    uF = np.fft.rfft(f0.getU())
    uf = np.fft.rfftfreq(f0.getU().size, 1/f0.getU().size)
    plt.semilogy(uf, np.abs(uF)/np.max(np.abs(uF)), label='init')

if True:
    plt.figure('Space Rank {}'.format(mpi.sRank))
    plt.plot(solver1.getCurrentFields().xCoordLoc,
             solver1.getCurrentFields().getU(),
             label='{}, CFL={:1.2f}'.format(rkSettings['RKS'],
                                            tEnd/solver1.nIter/dX))
    plt.figure('Fourier')
    uF = np.fft.rfft(solver1.getCurrentFields().getU())
    plt.semilogy(uf, np.abs(uF)/np.max(np.abs(uF)), label=rkSettings['RKS'])

if False:
    # SDC settings
    sdcSettings = {'f0': f0.copy(),
                   'spaceOp': spaceOp,
                   'stepping': 'FIXED',
                   'timeStorage': 'CURRENT',
                   'dumpTimes': 'NEVER',
                   'tBeg': 0.0,
                   'tEnd': tEnd,
                   'dt': dt,
                   'nSweep': 2,
                   'nQuad': 5,
                   'quadForm': 'LEGENDRE',
                   'initialSweepType': 'SIMPLE',
                   'sweepType': 'SO',
                   'sweepStorage': 'CURRENTK'
                   }
    solver2 = cts.SDCSolver(**sdcSettings)
    solver2.solve()
    print("SDC : {} evaluations".format(solver2.spopCount))

    if False:
        plt.figure('Space Rank {}'.format(mpi.sRank))
        plt.plot(solver2.getCurrentFields().xCoordLoc,
                 solver2.getCurrentFields().getU(),
                 label='SDC {0}, nSweep={1}, nQuad={2}'
                       .format(sdcSettings['sweepType'],
                               sdcSettings['nSweep']-1,
                               sdcSettings['nQuad']))

for i in []:
    # Krylov settings
    krySettings = {'f0': f0.copy(),
                   'spaceOp': spaceOp,
                   'stepping': 'FIXED',
                   'timeStorage': 'CURRENT',
                   'dumpTimes': 'NEVER',
                   'tBeg': 0.0,
                   'tEnd': tEnd,
                   'nDt': 16,
                   'nK': i,
                   'expAlgo': 'DIAG',
                   'Arnoldi': 'Default'
                   }
    cts.KryLinSolver.plotEigenvalues = False
    solver3 = cts.KryLinSolver(**krySettings)
    solver3.solve()
    print("Krylov : {} evaluations".format(solver3.spopCount))

    if True:
        plt.figure('Space Rank {}'.format(mpi.sRank))
        plt.plot(solver3.getCurrentFields().xCoordLoc,
                 solver3.getCurrentFields().getU(),
                 label='Krylov {0}, CFL={1}'.format(krySettings['nK'],
                                                    solver3.getCurrentDt()/dX))
        plt.figure('Fourier')
        uF = np.fft.rfft(solver3.getCurrentFields().getU())
        uf = np.fft.rfftfreq(solver3.getCurrentFields().getU().size, 0.5)
        plt.loglog(uf, np.abs(uF)/np.max(np.abs(uF)), label='KRYLOV-'+str(i))
        plt.legend()

plt.figure('Space Rank {}'.format(mpi.sRank))
plt.ylim(u0.min()*1.2, u0.max()*1.2)
plt.xlim(f0.xCoordLoc[0], f0.xCoordLoc[-1])
plt.legend(loc=0)
plt.show()

# # Jacobian tests
# ANum = spaceOp.computeJacobian(f0.getU(), 0)
# print "Numerical evaluation of the jacobian"
# print ANum
# print "Theoretical Jacobian (without periodic corners)"
# ATh = spaceOp.A.todense()
# print ATh
