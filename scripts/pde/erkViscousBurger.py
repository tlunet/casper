# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 17:29:45 2015

@author: lunet
"""
import casper.timesolver as cts
import casper.fields.f1d as f1d
import casper.spaceop.mesh1d as spop
import matplotlib.pyplot as plt
import numpy as np

# Field construction and initialization
dX = 0.001
f0Settings = {'nFields': 1,
              'nXGlob': int(1./dX) + 1,
              'xBegGlob': 0.0,
              'xEndGlob': 1.0,
              'meshType': 'XPER'}
f0 = f1d.Fields1D(**f0Settings)
f1d.init.initMesh1D(f0, 'GAUSS', sigCoeff=8)

# Space operator settings
advCoeff = -1.0
diffCoeff = 0.0001
spaceOpNonLin = spop.FDNonLinAdv1D(
    f0.xCoordLoc, discr='U1', coeff=advCoeff, boundary='PER')
spaceOpDiff = spop.FDLin1D(
    f0.xCoordLoc, ['DIFF', 'C2', diffCoeff], boundary='PER')
spaceOp = spaceOpNonLin + spaceOpDiff

# Global time solver settings
cfl = 1.2
dt = cfl*dX
tEnd = 5000*dt

# Runge-Kutta settings
rkSettings = {'f0': f0.copy(),
              'spaceOp': spaceOp,
              'stepping': 'FIXED',
              'timeStorage': 'CURRENT',
              'dumpTimes': 'NEVER',
              'tBeg': 0.0,
              'tEnd': tEnd,
              'dt': dt,
              'RKS': 'RKC4'
              }

solver1 = cts.ERKSolver(**rkSettings)
solver1.solve()
print("{} : {} RHS evaluations, nDOF={}, cpuTime={:1.3f}s"
      .format(solver1.RKS, solver1.spopCount, f0Settings['nXGlob'],
              solver1.cpuTime))

# Plot solution
plt.figure('Solution')
plt.plot(f0.xCoordLoc, f0.getU(), label='Initial Solution')
plt.plot(solver1.getCurrentFields().xCoordLoc,
         solver1.getCurrentFields().getU(),
         label='RKC4, CFL={}'.format(dt/dX))
plt.xlabel('$X$')
plt.ylabel('$U$')
plt.title('Solution after {} time-steps'.format(solver1.nIter))
plt.legend()
plt.savefig(__file__[:-3]+'_fig1.pdf', bbox_inches='tight')

# Plot energy spectrum
plt.figure('Fourier')
uF0 = np.fft.rfft(f0.getU())
kappa = np.fft.rfftfreq(f0.getU().size, 0.5)
plt.loglog(kappa, np.abs(uF0)/np.max(np.abs(uF0)), label='init')
uF = np.fft.rfft(solver1.getCurrentFields().getU())
kappa = np.fft.rfftfreq(solver1.getCurrentFields().getU().size, 0.5)
plt.loglog(kappa, np.abs(uF)/np.max(np.abs(uF0)), label=rkSettings['RKS'])
plt.ylabel('$E/E_{0,max}$')
plt.xlabel('$\\kappa/\\kappa_{max}$')
plt.title('Energy spectrum after {} time-steps'.format(solver1.nIter))
plt.legend()
plt.savefig(__file__[:-3]+'_fig2.pdf', bbox_inches='tight')
