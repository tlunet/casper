# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 17:29:45 2015

@author: lunet
"""
import casper.timesolver as cts
import casper.fields.f1d as f1d
import casper.spaceop.mesh1d as spop
import matplotlib.pyplot as plt
from casper.mpi import initMPI
import numpy as np

# Initialize MPI communications
initMPI()

# Field construction and initialization
dX = 0.001
f0Settings = {'nFields': 1,
              'nXGlob': int(1./dX) + 1,
              'xBegGlob': 0.0,
              'xEndGlob': 1.0,
              'meshType': 'XPER'}
f0 = f1d.Fields1D(**f0Settings)
f1d.init.initMesh1D(f0, 'THI', sigCoeff=8)

# Space operator settings
diffCoeff = 0.0001
spaceOp = spop.ViscousBurgers(f0.xCoordLoc, advScheme='C6',
                              diffCoeff=diffCoeff, diffScheme='C6')

# Global time solver settings

cfl = 1.0
dt = cfl*dX
tEnd = 20*dt

# Runge-Kutta settings
rkSettings = {'f0': f0.copy(),
              'spaceOp': spaceOp,
              'stepping': 'FIXED',
              'timeStorage': 'CURRENT',
              'dumpTimes': 'NEVER',
              'tBeg': 0.0,
              'tEnd': tEnd,
              'dt': dt,
              'RKS': 'RKC4'
              }

solver1 = cts.ERKSolver(**rkSettings)
solver1.solve()

if True:
    plt.figure('Solution')
    plt.plot(f0.xCoordLoc,
             f0.getU(),
             label='Initial Solution')
    plt.xlabel('$X \, [m]$', fontsize=16)
    plt.ylabel('$U(x) \, [-]$', fontsize=16)
    plt.figure('Fourier')
    uF0 = np.fft.rfft(f0.getU())
    uf = np.fft.rfftfreq(f0.getU().size, 0.5)
    plt.loglog(uf, np.abs(uF0)/np.max(np.abs(uF0)), label='init')

if True:
    plt.figure('Solution')
    plt.plot(solver1.getCurrentFields().xCoordLoc,
             solver1.getCurrentFields().getU(),
             label='RKC4, CFL={}'.format(dt/dX))
    plt.legend()
    plt.figure('Fourier')
    uF = np.fft.rfft(solver1.getCurrentFields().getU())
    uf = np.fft.rfftfreq(solver1.getCurrentFields().getU().size, 0.5)
    plt.loglog(uf, np.abs(uF)/np.max(np.abs(uF0)), label=rkSettings['RKS'])
    plt.legend()

if False:
    # SDC settings
    sdcSettings = {'f0': f0.copy(),
                   'spaceOp': spaceOp,
                   'stepping': 'FIXED',
                   'timeStorage': 'CURRENT',
                   'dumpTimes': 'NEVER',
                   'tBeg': 0.0,
                   'tEnd': tEnd,
                   'dt': dt,
                   'nSweep': 2,
                   'nQuad': 5,
                   'quadForm': 'LEGENDRE',
                   'initialSweepType': 'SIMPLE',
                   'sweepType': 'SO',
                   'sweepStorage': 'CURRENTK'
                   }
    solver2 = cts.SDCSolver(**sdcSettings)
    solver2.solve()

    if True:
        plt.figure('Solution')
        plt.plot(solver2.getCurrentFields().xCoordLoc,
                 solver2.getCurrentFields().getU(),
                 label='SDC {0}, nSweep={1}, nQuad={2}'
                       .format(sdcSettings['sweepType'],
                               sdcSettings['nSweep']-1,
                               sdcSettings['nQuad']))
        plt.figure('Fourier')
        uF = np.fft.rfft(solver2.getCurrentFields().getU())
        uf = np.fft.rfftfreq(solver1.getCurrentFields().getU().size, 0.5)
        plt.loglog(uf, np.abs(uF)/np.max(np.abs(uF0)), label='SDC')
        plt.legend()

# Krylov settings
for i in [20]:
    krySet = {'f0': f0.copy(),
              'spaceOp': spaceOp,
              'stepping': 'FIXED',
              'timeStorage': 'CURRENT',
              'dumpTimes': 'NEVER',
              'tBeg': 0.0,
              'tEnd': tEnd,
              'nDt': int(tEnd/dt)//20,
              'phiAlgo': 'DIAG',
              'nK': i
              }
    solver3 = cts.KryNLSolver(**krySet)
    solver3.solve()

    if True:
        plt.figure('Solution')
        plt.plot(solver3.getCurrentFields().xCoordLoc,
                 solver3.getCurrentFields().getU(),
                 label='KRY-{}'.format(krySet['nK']) +
                 ', CFL = {}'.format(tEnd/dt/krySet['nDt']))
        plt.legend()

        plt.figure('Fourier')
        uF = np.fft.rfft(solver3.getCurrentFields().getU())
        uf = np.fft.rfftfreq(solver3.getCurrentFields().getU().size, 0.5)
        plt.loglog(uf, np.abs(uF)/np.max(np.abs(uF0)),
                   label='KRY-{}'.format(krySet['nK']) +
                   ', CFL = {}'.format(tEnd/dt/krySet['nDt']))
        plt.legend()

plt.show()
