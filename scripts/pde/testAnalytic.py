#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 25 11:11:56 2018

@author: telu
"""
# Casper imports
import casper.maths.pde as pde
import casper.fields.f1d as f1d
import casper.spaceop.mesh1d as spop
import casper.timesolver as cts
# Python import
import numpy as np
import matplotlib.pyplot as plt

# Main parameters
initSol = 'GAUSS'
advCoeff = 1.0
L = 2
re = 40
diffCoeff = advCoeff*L/re
tEnd = 0.2
xBeg, xEnd = 0., L

# Time settings
cflAdv = 1.0
cflDiff = 0.5

# Scheme
lDiscr = [
    ['BE', 'C2', 'C2'],
    ['RKC4', 'C2', 'C2'],
    ['RKC4', 'C4', 'C4'],
    ['BE', 'C4', 'C4'],
    ['RKC4', 'C6', 'C6']]


lReMesh = [1, 0.5, 0.1, 0.05]

for discr in lDiscr:
    timeScheme, advScheme, diffScheme = discr

    err = []
    for reMesh in lReMesh:
        p = re/reMesh
        dx = L/p
        dtAdv = cflAdv*dx/advCoeff
        dtDiff = cflDiff*dx**2/(2*diffCoeff)
        dt = min([dtAdv, dtDiff])
        print(' -- p =', p, ', dt =', dt)

        # Space mesh
        f0Settings = {
            'nFields': 1, 'nXGlob': p, 'xBegGlob': xBeg, 'xEndGlob': xEnd,
            'meshType': 'XPER'}
        f0 = f1d.Fields1D(**f0Settings)
        f1d.init.initMesh1D(f0, initSol, sigma=1/80**0.5, x0=0.5)
        x = f0.xCoordLoc
        etss = ['ADV', advScheme, -advCoeff, 'DIFF', diffScheme, diffCoeff]
        spaceOp = spop.FDLin1D(f0.xCoordLoc, etss, boundary='PERIODIC')

        # Fine solver settings
        solverSettings = {'f0': f0.copy(),
                          'spaceOp': spaceOp,
                          'stepping': 'FIXED',
                          'timeStorage': 'CURRENT',
                          'dumpTimes': 'NEVER',
                          'tBeg': 0.0,
                          'tEnd': tEnd,
                          'dt': dt,
                          'RKS': timeScheme
                          }
        # Solve
        print(' -- Numerical solution')
        solver = cts.RKSolver(**solverSettings)
        solver.solve()

        uNum = solver.getCurrentFields().getU()

        # Analytic solution
        print(' -- Analytical solution')
        u0 = pde.initialSolution(x, initSol, xBeg, xEnd)
        eqTerms = [('ADV', advCoeff), ('DIFF', diffCoeff)]
        uTh = pde.analyticSolutionAdvDiff(
            x, initSol, xBeg, xEnd, tEnd, eqTerms)

        # Error computation
        err.append(np.linalg.norm(uTh-uNum))

    # Plot error
    plt.figure('error')
    plt.loglog(
        lReMesh, err,
        label='{}-{}-{}'.format(timeScheme, advScheme, diffScheme))

plt.figure('Solution')
plt.plot(x, u0, '--', label='Initial')
plt.plot(x, uTh, label='Analytical')
plt.plot(x, uNum, label='Numerical')

fig = plt.figure('error')
plt.grid(True)
plt.xlabel('$Re_{\\Delta_x}$')
plt.ylabel('$||u_{th}-u_{num}||_2$')
plt.legend()
fig.set_tight_layout(True)
